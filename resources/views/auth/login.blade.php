@extends('layouts.authentication_layout') @section('title', 'Login') @section('body_content')
<section class="login header p-fixed d-flex text-center">

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
					@if (session('message'))
					<div class="alert alert-success">
							{{session('message')}}
					</div>
					@elseif(session('error_messages'))
					<div class="alert alert-danger">
						{{session('error_messages')}}
					</div>
					@endif
			
				<div class="login-card card-block auth-body mr-auto ml-auto">
					<form class="md-float-material" method="POST" action="{{ route('login') }}">
							{{ csrf_field() }}
						<div class="auth-box">
							<div class="row m-b-20">
								<div class="col-md-12">
									<h3 class="text-left txt-primary">{{trans ('login.Sign In') }}</h3>
								</div>
							</div>
							<hr />
							@if ($errors->has('email'))
								<span class="help-block" style="color:red;">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
								@endif
							<div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<input type="email" class="form-control" placeholder="{{trans ('login.Email') }}" name="email" value="{{ old('email') }}" required
								 autofocus>
								<span class="md-line"></span>
							</div>
							@if ($errors->has('password'))
								<span class="help-block" style="color:red;">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
								@endif
							<div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<input type="password" class="form-control" placeholder="{{trans ('login.Password') }}" name="password" required>
								<span class="md-line"></span>
							</div>
							<div class="row m-t-25 text-left">
								<div class="col-12">
									<div class="checkbox-fade fade-in-primary">
										<label>
											<input type="checkbox" name="remember" value="" {{ old('remember') ? 'checked' : '' }}>
											<span class="cr">
												<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
											</span>
											<span class="text-inverse">{{trans ('login.Remember me') }}</span>
										</label>
									</div>
									<div class="forgot-phone text-right f-right">
										<a href="{{ route('password.request') }}" class="text-right f-w-600 text-inverse"> {{trans ('login.FPassword') }}</a>
									</div>
								</div>
							</div>
							<div class="row m-t-30">
								<div class="col-md-12">
									<input type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20" value="{{trans ('login.Sign In') }}">
								</div>
							</div>
							<hr />

						</div>
					</form>

				</div>

			</div>

		</div>

	</div>

</section>

@stop



