<?php

namespace App\Models\Course;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CourseEnrollment extends Model
{
    //
    use Notifiable;
    protected $table = 'course_enrollment';
    protected $primaryKey = 'enrollment_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['course_id', 'trainee_id', 'trainer_id', 'status', 'rating', 'rating_comments', 'created_at','updated_at'];
}
