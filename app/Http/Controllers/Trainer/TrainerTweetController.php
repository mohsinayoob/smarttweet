<?php

namespace App\Http\Controllers\Trainer;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Tweet\Tweets;
use App\Models\Tweet\TweetContent;
use App\Models\Course\Course;
use App\Models\Course\Topic;
use App\Models\Tweet\TweetReaction;
use App\Models\Tweet\CommentReaction;
use App\Models\Tweet\Comments;
use App\Models\Course\CourseEnrollment;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DB;
use Session;
use App\Jobs\TweetNotificationJob;
class TrainerTweetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainer');
        
    }
    //
    public function getEditTweet($topic_id, $tweet_id, Request $request)
    {
        $tweet = Tweets::where('tweet_id', $tweet_id)->first();
        if(empty($tweet))
        {
            return back()->with('error_messages', 'Could not find the tweet');
        }
        $tweet_content = TweetContent::where('tweet_id',$tweet_id)->get();
        return view('trainer.edit-tweet', compact('topic_id','tweet_id','tweet','tweet_content'));
    }

    public function editTweet($topic_id, $tweet_id, Request $request)
    {
        //return $request;
        $data = $request->all();
        DB::beginTransaction();
        try
        {    
            // return $data['tweet_description'];
           // $course = Course::where('course_id',$course_id)->first();
            
            $tweet= Tweets::where('tweet_id',$tweet_id)->first();
            if(!empty($tweet))
            {
               
                $tweet->tweet_description = base64_encode($data['tweet_description']);
                $tweet['content_type'] =$data['content_type'];
                $newTweet  = $tweet->save();
                if($newTweet)
                {
                    if($request->tweet_contents)
                    {
                        TweetContent::where('tweet_id', $tweet_id)->delete();
                        $content_data = $data['tweet_contents'];
                        foreach($content_data as $content)
                        {
                            $tweet_content['tweet_id'] = $tweet_id;
                            $tweet_content['content_type'] = $data['content_type'];
                            $tweet_content['file'] = $content;
                            $tweet_content['status'] = 'Active';
                            TweetContent::create($tweet_content);
                        }
                    }
                    DB::commit();
                    Session::flash('update', 'Tweet Saved Successfully');
                    return redirect()->to('/trainer/tweet/'.$topic_id.'/'.$tweet_id)->with('update', 'Tweet Saved Successfully');
                }

            }
            
            
            DB::rollback();
            return back()->with('error_messages', 'Unable to create tweet');
        }
        catch(Exception $e) 
        {
            DB::rollback();
            return back()->with('error_messages', 'Unable to create tweet');
        }

    }
    public function pictureUpload(Request $request)
    {
        $imageName = time().'.'.$request->file->getClientOriginalExtension();
        $image_uploaded = $request->file->move(public_path('tweet_content'), $imageName);
        if($image_uploaded)
        { 
            return $imageName;
        }
        return 'erorr';
    }

    public function videoUpload(Request $request)
    {
        $video_validator = Validator::make($request->all(), [
            'file' => 'required|image|mimes:jpeg,png,jpg|max:4086',
        ]);

        if ($video_validator->fails()) {

            return 'error';
        }
        $imageName = time().'.'.$request->file->getClientOriginalExtension();
        $image_uploaded = $request->file->move(public_path('tweet_content'), $imageName);
        if($image_uploaded)
        { 
            return $imageName;
        }
        return 'erorr';
    }

    public function saveTweet($course_id,$topic_id, Request $request)
    {
        //return $request;
        $data = $request->all();
        DB::beginTransaction();
        try
        {    
            // return $data['tweet_description'];
            $course = Course::where('course_id',$course_id)->first();
            if(\Auth::user()->user_type!="admin")
            {
                if($course->trainer_id != \Auth::user()->user_id)
                {
                    return $course->trainer_id ."-------". \Auth::user()->user_id;
                    return back()
    		        ->withError('error_messages','You ara not authorized to post');
                }
            }
            $tweet['course_id'] = $course_id;
            $tweet['topic_id'] = $topic_id;
            $tweet['status'] = 'De-active';
            $tweet['content_type'] =$data['content_type'];
            $tweet['tweet_description'] = base64_encode($data['tweet_description']);
            $tweet['user_id'] = \Auth::user()->user_id;
            $newtimestamp = strtotime('+5 minute');
            $tweet['active_time'] = date('Y-m-d H:i:s', $newtimestamp);
            
            $newTweet = Tweets::create($tweet);
            //return $newTweet;
            if($newTweet)
            {
                if($request->tweet_contents)
                {
                    $content_data = $data['tweet_contents'];
                    foreach($content_data as $content)
                    {
                        $tweet_content['tweet_id'] = $newTweet->tweet_id;
                        $tweet_content['content_type'] = $newTweet->content_type;
                        $tweet_content['file'] = $content;
                        $tweet_content['status'] = 'Active';
                        TweetContent::create($tweet_content);
                    }
                }
                
                DB::commit();
                
                Session::flash('update', 'Tweet Saved Successfully');
                return back()->with('update', 'Tweet Saved Successfully');
            }
            DB::rollback();
            return back()->with('update_error', 'Unable to create tweet');
        }
        catch(Exception $e) 
        {
            DB::rollback();
            return back()->with('update_error', 'Unable to create tweet');
        }

    }

    public function viewTweets($course_id, $topic_id)
    {
        //$tweets = Tweets::join('tweet_content','tweet_content.tweet_id','=','tweets.tweet_id')
        //->where('tweets.topic_id',$topic_id)->get();
        

        $topics = Topic::where('topic_id', $topic_id)->first();
        $enrollusers = CourseEnrollment::join('users', 'users.user_id', '=', 'course_enrollment.trainee_id')
        ->where ('course_enrollment.course_id',$course_id)
        ->select('course_enrollment.*', 'users.name', 'users.sector','users.profile_image')->take(10)->get();
        
        $date = Input::get('date'); 
        //return date('d');
        if (!empty($date)) {
            if($date == 'Today'){
            $tweets = Tweets::with('tweet_content')
            ->join('users','users.user_id','=','tweets.user_id')
            ->where('topic_id',$topic_id)
            ->whereDay('tweets.created_at',date('d'))->whereMonth('tweets.created_at',date('m'))->whereYear('tweets.created_at',date('Y'))
            ->select('tweets.*','users.name','users.profile_image', 'users.user_type','users.user_id')
            ->orderBy('created_at', 'DESC')
            ->paginate(10);
            return view('trainer.tweets-list', compact('course_id','topic_id','tweets','topics','enrollusers'));
            }
            if($date == 'Month'){
                $tweets = Tweets::with('tweet_content')
                ->join('users','users.user_id','=','tweets.user_id')
                ->where('topic_id',$topic_id)
                ->whereMonth('tweets.created_at',date('m'))->whereYear('tweets.created_at',date('Y'))
                ->select('tweets.*','users.name','users.profile_image', 'users.user_type','users.user_id')
                ->orderBy('created_at', 'DESC')
                ->paginate(10);
                return view('trainer.tweets-list', compact('course_id','topic_id','tweets','topics','enrollusers'));
                }
                if($date == 'Year'){
                    $tweets = Tweets::with('tweet_content')
                    ->join('users','users.user_id','=','tweets.user_id')
                    ->where('topic_id',$topic_id)
                    ->whereYear('tweets.created_at',date('Y'))
                    ->select('tweets.*','users.name','users.profile_image', 'users.user_type','users.user_id')
                    ->orderBy('created_at', 'DESC')
                    ->paginate(10);
                    return view('trainer.tweets-list', compact('course_id','topic_id','tweets','topics','enrollusers'));
                    }
        }
        $tweets = Tweets::with('tweet_content')
        ->join('users','users.user_id','=','tweets.user_id')
        ->where('topic_id',$topic_id)
        ->select('tweets.*','users.name','users.profile_image', 'users.user_type','users.user_id')
        ->orderBy('created_at', 'DESC')
        ->paginate(10);
        //return $tweets;
        return view('trainer.tweets-list', compact('course_id','topic_id','tweets','topics','enrollusers'));
    }
    public function DeleteTweet($tweet_id)
    {
        //return $topic_id;
        DB::beginTransaction();
        try
        {
            //return $existTweets;
                $existComments= DB::table('comments')->where('tweet_id', $tweet_id)->delete();
                $existtweetcontent= DB::table('tweet_content')->where('tweet_id', $tweet_id)->delete();
                $existtweetreacttion= DB::table('tweet_reaction')->where('tweet_id', $tweet_id)->delete();
                $existTweetsdel= Tweets::where('tweet_id', $tweet_id)->delete();
              
                    DB::commit();
            
            // After creating the user send an email with the random token generated in the create method above

           
            Session::flash('update', 'Tweet Deleted Successfully');
            return back()->with('update', 'Tweet Successfully Deleted');
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Deleted Tweet');
        }

    }

    public function UpdateTweetStatus($topic_id, $tweet_id, $status, Request $request)
    {
        
        DB::beginTransaction();
        try
        {
            
           // Category::create();
           
            $existTweet = Tweets::where('tweet_id', $tweet_id)->first();
            if(!empty($existTweet))
            {
                $existTweet->status = $status;
                
                if($status == "Active")
                {
                    $tweets = Tweets::where('topic_id', $topic_id)
                    ->where('tweet_id', '!=', $tweet_id)->get();
                    foreach($tweets as $tweet)
                    {
                        $tweet->status = 'De-active';
                        $tweet->save();
                    }
                    $minutes = 5;
                    $seconds = 0;
                    if($request->has("minutes") && $request->minutes > 0)
                    {
                        $minutes = $request->minutes;
                    }
                    if($request->has("seconds") && $request->seconds > 0)
                    {
                        $seconds = $request->seconds;
                    }

                    $total_time = $minutes*60 + $seconds;
                    $dt = Carbon::now();
                    $dt->addSeconds($total_time);
                    $existTweet->active_time = $dt;
                    $notification_job = (new TweetNotificationJob($existTweet))->delay(13);
                    $this->dispatch($notification_job);


                }
                $existTweet->save();
                DB::commit();
                Session::flash('update', 'Tweet Status Updated Successfully');
                return back()->with('update', 'Tweet Status Successfully Updated');
            }
            else
            {
                return back()->with('error_messages', 'Error in Updating Tweet Status');
            }
            
            
            
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Updating Tweet Status');
        }

    }

    public function viewSingleTweet($topic_id, $tweet_id)
    {
        //$tweets = Tweets::join('tweet_content','tweet_content.tweet_id','=','tweets.tweet_id')
        //->where('tweets.topic_id',$topic_id)->get();
        $tweet = Tweets::with('tweet_content')
        ->join('users','users.user_id','=','tweets.user_id')
        ->where('tweets.topic_id',$topic_id)
        ->where('tweets.tweet_id',$tweet_id)
        ->select('tweets.*','users.name','users.profile_image', 'users.user_type','users.user_id')->first();
        $reactions = TweetReaction::where('tweet_id', $tweet_id)
        ->where('user_id', \Auth::user()->user_id )
        ->first();
        
        //return $reactions;
        $comments = Comments::with(['comment_reaction_content' => function($query)
        {
            $query->where('user_id', \Auth::user()->user_id);
        
        }])
        ->join('users','users.user_id','=','comments.user_id')
        ->where('comments.tweet_id',$tweet_id)
        //->where('comments_reaction.user_id',\Auth::user()->user_id)
        ->select('comments.*','users.name','users.profile_image', 'users.user_type','users.user_id')
        ->orderBy('comments.comment_id','ASC')->limit(10)->get();
        //return phpinfo();
      //return $comments;
      $current = Carbon::now();
      //$pre = Carbon::createFromDate();
      $difference=0;
      if(!empty($tweet))
      {
        $pre = Carbon::createFromFormat('Y-m-d H:i:s', $tweet->active_time);
        if($current->lt($pre))
        {
            $difference = $current->diffInSeconds($pre);
        }
      }
        return view('trainer.tweet', compact('tweet_id','topic_id','tweet','reactions','comments','difference'));
    }




    public function viewNextTweet($topic_id, $tweet_id)
    {
        //$tweets = Tweets::join('tweet_content','tweet_content.tweet_id','=','tweets.tweet_id')
        //->where('tweets.topic_id',$topic_id)->get();
        $tweet = Tweets::with('tweet_content')
        ->join('users','users.user_id','=','tweets.user_id')
        ->where('tweets.topic_id',$topic_id)
        ->where('tweets.tweet_id','>',$tweet_id)
        ->select('tweets.*','users.name','users.profile_image', 'users.user_type','users.user_id')->first();
        //return $tweet;
        if(empty($tweet))
        {
            $difference = 0;
            $comments = null;
            $reactions = null;
            return view('trainer.tweet', compact('tweet_id','topic_id','tweet','reactions','comments','difference'));
        }
        $reactions = TweetReaction::where('tweet_id', $tweet->tweet_id)
        ->where('user_id', \Auth::user()->user_id )
        ->first();
        
        //return $reactions;
        $comments = Comments::with(['comment_reaction_content' => function($query)
        {
            $query->where('user_id', \Auth::user()->user_id);
        
        }])
        ->join('users','users.user_id','=','comments.user_id')
        ->where('comments.tweet_id',$tweet->tweet_id)
        //->where('comments_reaction.user_id',\Auth::user()->user_id)
        ->select('comments.*','users.name','users.profile_image', 'users.user_type','users.user_id')
        ->orderBy('comments.comment_id','ASC')->limit(10)->get();
        //return phpinfo();
      //return $comments;
      $current = Carbon::now();
      //$pre = Carbon::createFromDate();
      $difference=0;
      if(!empty($tweet))
      {
        $pre = Carbon::createFromFormat('Y-m-d H:i:s', $tweet->active_time);
        if($current->lt($pre))
        {
            $difference = $current->diffInSeconds($pre);
        }
      }
      $tweet_id = $tweet->tweet_id;
        return view('trainer.tweet', compact('tweet_id','topic_id','tweet','reactions','comments','difference'));
    }
}
