@extends('layouts.admin_layout') @section('title', 'Course Reports') @section('css-files')
<link rel="stylesheet" href="{{URL::asset('files/assets/pages/chart/radial/css/radial.css')}}" type="text/css" media="all">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/jquery-bar-rating/css/css-stars.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/jquery-bar-rating/css/fontawesome-stars-o.css')}}">

<style>
	#search-addon {
		margin-top: 0px;
	}
</style>
@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-header card">
					<div class="card-block">
						<h5 class="m-b-10">Course Wise Report</h5>
					</div>
				</div>
				<div class="page-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="card list-view-media">
								<div class="card-header">
									<div class="dropdown-secondary dropdown f-left">
										
										<h6 class="d-inline-block">
											{{$course && $course->course_title? $course->course_title : ''}}
										</h6>
										<div class="f-13 text-muted m-b-15">
											{{$course && $course->course_creation ? $course->course_creation : ''}}	
										</div>
									</div>
									<div class="dropdown-secondary dropdown f-right">
											<select id="example-css" class="rating-star-readonly" name="rating" autocomplete="off" disabled>
													<option value="1"{{sizeof($reports)>0 && $reports[0]->avg_rating>0 &&$reports[0]->avg_rating<=1 ? 'selected="selected"':''}}>1</option>
													<option value="2" {{sizeof($reports)>0 && $reports[0]->avg_rating>1 &&$reports[0]->avg_rating<=2 ? 'selected="selected"':''}}>2</option>
													<option value="3" {{sizeof($reports)>0 && $reports[0]->avg_rating>2 &&$reports[0]->avg_rating<=3 ? 'selected="selected"':''}}>3</option>
													<option value="4" {{sizeof($reports)>0 && $reports[0]->avg_rating>3 &&$reports[0]->avg_rating<=4 ? 'selected="selected"':''}}>4</option>
													<option value="5"{{sizeof($reports)>0 && $reports[0]->avg_rating>4 ? 'selected="selected"':''}}>5</option>
												</select>
									</div>
								</div>
								<div class="card-block">
									<div class="media">

										<div class="media-body">

											<p>
												{{$course && $course->course_description? $course->course_description : ''}}
											</p>

										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-xl-12">
							<div class="card review-project">
								<div class="card-header">
									<div class="card-header-left">
										
									</div>
									<div class="card-header-right">

									</div>
								</div>
								<div class="card-block p-t-0 p-b-0 w-100">
									<div class="table-responsive">
										<table class="table table-hover" id="report-table">
											<thead>
												<tr>
													<th>Users</th>
													<th>Participation</th>
													<th>Comment Favorite</th>
													<th>Comment Like</th>
													<th>Comment Dislike</th>
													<th>Trainee Rating</th>
												</tr>
											</thead>
											<tbody>
												@foreach($reports as $report)
												<tr>
													<td>
														<a href="#!">
															<img class="img-radius img-40" src="{{URL::asset('/'.$report->user_type.'/profile_image/'.$report->profile_image)}}" alt="chat-user">
														</a>
														<div class="project-contain">
															<h6>{{$report->name}}</h6>
															<p class="text-muted">
																<i class="fa fa-clock-o f-12 m-r-10"></i>Joined {{$report->created_at}}</p>
														</div>
													</td>
													<td>
															<span>{{$report->trainees_comments>0 ? ($report->trainees_comments/$report->total_comments *100)."%" :'0%' }}
															</span><span class="pie_2" style="display: none;">{{$report->trainees_comments}}/{{$report->total_comments}}</span>
													</td>
													<td>
														{{$report->favorite_count}}
													</td>
													<td>
														{{$report->like_count}}
													</td>
													<td>
														{{$report->dislike_count}}
													</td>
													<td>
													@if($report->is_rated) 
															<select id="example-css" class="rating-star-readonly" name="rating" autocomplete="off" disabled>
																	<option value="1"{{  $report->rating>0 && $report->rating<=1 ? 'selected="selected"':''}}>1</option>
																	<option value="2" {{ $report->rating>1 && $report->rating<=2 ? 'selected="selected"':''}}>2</option>
																	<option value="3" {{ $report->rating>2 &&$report->rating<=3 ? 'selected="selected"':''}}>3</option>
																	<option value="4" {{ $report->rating>3 &&$report->rating<=4 ? 'selected="selected"':''}}>4</option>
																	<option value="5"{{ $report->rating>4 ? 'selected="selected"':''}}>5</option>
																</select>
													@else 
														Not Rated
													@endif
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')
<script src="../files/bower_components/peity/js/jquery.peity.js"></script>
<script src="{{URL::asset('files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/jquery-bar-rating/js/jquery.barrating.js')}}"></script>
<script src="{{URL::asset('files/assets/js/rating.js')}}"></script>

<script >
		$("span.pie_2").peity("pie", {
            fill: ["#FFB64D", "#4099ff"]
		});
		var table=$('#report-table').DataTable();
</script>
@stop