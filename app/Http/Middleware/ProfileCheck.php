<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;

class ProfileCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->user()->profile_image && Auth::guard($guard)->user()->user_type=='admin') 
        {
            Session::flash('error_messages', 'Kindly Upload your Profile Picture first');
            return redirect('/admin/profile')->with('error_messages', 'Kindly Upload your Profile Picture first');
           
        }
        else if (!Auth::guard($guard)->user()->profile_image && Auth::guard($guard)->user()->user_type=='trainer') {
            Session::flash('error_messages', 'Kindly Upload your Profile Picture first');
            return redirect('/trainer/profile')->with('error_messages', 'Kindly Upload your Profile Picture first');
           
        }
        else if (!Auth::guard($guard)->user()->profile_image && Auth::guard($guard)->user()->user_type=='trainee') {
            Session::flash('error_messages', 'Kindly Upload your Profile Picture first');
            return redirect('/trainee/profile')->with('error_messages', 'Kindly Upload your Profile Picture first');
           
        }
        return $next($request);
    }
}
