<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Models\User;
use App\Models\Ticket\Ticket;

class TicketComment extends Model
{
    //
    use Notifiable;
    protected $table = 'ticket_comments';
    protected $primaryKey = 'id';
    protected $fillable = [
		'ticket_id', 'user_id', 'comment'
	];

    /**
     * A comment belongs to a particular ticket
     */
    public function ticket()
    {
    	return $this->belongsTo(Ticket::class, 'ticket_id');
    }

    /**
     * A comment belongs to a user
     */
    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
}
