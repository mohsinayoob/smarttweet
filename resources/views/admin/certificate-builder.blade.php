@extends('layouts.admin_layout') @section('title', 'Reports')
@section('css-files')
<link rel="stylesheet" href="{{URL::asset('files/bower_components/select2/css/select2.min.css')}}" />

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/multiselect/css/multi-select.css')}}" />
<style>
.button-margin{
	margin-top:30px;
}
</style>
@stop
@section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">

		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-header card">
					<div class="card-block">
						<h5 class="m-b-10">Reports</h5>
					</div>

				</div>
				<div class="page-body">


					<div class="card">
						<div class="card-block">
							<div class="row">
								
								<div class="col-sm-12">
									<h4 class="sub-title">Course Wise Reports</h4>
									<form method="post" action="/admin/certificate/{{$user_id}}">
										{{csrf_field()}}
									<div class="row">
										<div class="col-sm-12">
											<div class="col-sm-6 input-group input-group-lg">
												
												<select class="select-user-single col-sm-12"  placeholder="Select a course" name="course_id">
														@foreach($courses as $course)
														<option value="{{$course->course_id}}">{{$course->course_title}}</option>
														@endforeach
													</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="text-center button-margin">
												<button type="submit" class="btn btn-primary">Coursewise Search</a>
											</div>
										</div>
									</div>
								</form>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>
</div>
@stop @section('javascript-files')
<script src="{{URL::asset('files/assets/pages/task-board/task-board.js')}}"></script>
<script src="{{URL::asset('files/assets/js/modalEffects.js')}}"></script>
<script src="{{URL::asset('files/assets/js/classie.js')}}"></script>
<script src="{{URL::asset('files/bower_components/select2/js/select2.full.min.js')}}"></script>

<script src="{{URL::asset('files/bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js')}}">
    </script>
<script src="{{URL::asset('files/bower_components/multiselect/js/jquery.multi-select.js')}}"></script>
<script src="{{URL::asset('files/assets/js/jquery.quicksearch.js')}}"></script>

<script src="{{URL::asset('files/assets/pages/advance-elements/select2-custom.js')}}"></script>

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}">
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>
@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");
</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");
</script>

@endif
@stop