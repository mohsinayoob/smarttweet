@extends('layouts.admin_layout') @section('title', 'Topics') @section('css-files')
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/css/component.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css')}}"
/>

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datedropper/css/datedropper.min.css')}}"
/>

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/spectrum/css/spectrum.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/switchery/css/switchery.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}"> 
<style>
	.card-comment .comment-desc {
		padding-left: 229px;
		vertical-align: top;
	}
	.card-comment img {
		float: left;
		width: auto;
        max-height: 150px;
        height: auto;
        max-width: 203px;
	}
	.comment-desc img{
		height:auto;
		width:auto;
		float:none;
	}

	@media only screen and (max-width: 720px) {
		.card-comment .comment-desc {
			padding-left: 0px!important;
			vertical-align: top;
		}
		
		
		.card-comment img {
			float: left;
			height: auto;
            width: 100%;
            max-width: 100%;
            max-height: 100%;
			padding-bottom: 10px;
		}
		.comment-desc img{
			height:auto;
			width:auto;
			float:none;
		}
	}
	</style>
@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">

		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-header card">
					<div class="card-block">
						<h5 class="m-b-10">Topics</h5>
					</div>

				</div>
				<div class="page-body">

					<div class="md-modal md-effect-13 addcontact" id="modal-14">
						<div class="md-content">
							<h3 class="f-26">Add Topic</h3>
							<div>
								<form method="post" action='/admin/topic/{{$courses->course_id}}' enctype='multipart/form-data'>
									{{ csrf_field() }}
								<div class="input-group">
									<input type="text" class="form-control pamount" name="topic_name" placeholder="Topic Name">
								</div>
								<div class="input-group">
									
									<textarea id="editor1" name="topic_description"> </textarea>
								</div>
                                <div class="input-group">
									<input type="file" name="topic_image" >
								</div>
								<div class="text-center">
									<button type="submit" class="btn btn-primary waves-effect m-r-20 f-w-600 d-inline-block save_btn">Save</button>
									<button type="button" class="btn btn-primary waves-effect m-r-20 f-w-600 md-close d-inline-block close_btn">Close</button>
								</div>
							</form>
							</div>
						</div>
					</div>
					<div class="md-modal md-effect-13 addcontact" id="modal-edit">
						<div class="md-content">
							<h3 class="f-26">Edit Topic</h3>
							<div>
								
								<form method="post" action='/admin/update-topic' enctype='multipart/form-data'>
									{{ csrf_field() }}
								<div class="input-group">
									<input type="text" class="form-control pamount" name="topic_name" id='topic_name' placeholder="Topic Name">
								</div>
								<div class="input-group">
									
									<textarea id="editor2" name="topic_description" class="topic_description"> </textarea>
								</div>
                                <div class="input-group">
									<input type="file" name="topic_image" id="topic_image" >
									<input type="hidden" name="topic_id" id="topic_id" >
								</div>
								<div class="text-center">
									<button type="submit" class="btn btn-primary waves-effect m-r-20 f-w-600 d-inline-block save_btn">Save</button>
									<button type="button" class="btn btn-primary waves-effect m-r-20 f-w-600 md-close d-inline-block close_btn">Close</button>
								</div>
							</form>
							</div>
						</div>
					</div>
					<div class="md-overlay"></div>

					<div class="row">
						<div class="col-sm-12">
							<div class="card list-view-media">
								<div class="card-header">
									<div class="dropdown-secondary dropdown f-left">
										<h6 class="d-inline-block">
												{{$courses->course_title}}
										</h6>
										<div class="f-13 text-muted m-b-15">
												{{$courses->created_at}}
										</div>
									</div>
									<div class="dropdown-secondary dropdown f-right">
										
										
											
												<label class="label label-success">{{$courses->status}}</label>

											
										

										<span class="f-left m-r-5 text-inverse">Status : </span>
									</div>
								</div>
								<div class="card-block">
									<div class="media">

										<div class="media-body">

											<p>{{$courses->course_description}}</p>

										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-12 push-xl-9">

							<div class="card">
								<div class="card-header">
									<h5 class="card-header-text">Add Topic</h5>
								</div>
								<div class="card-block p-t-10">
										<div class="input-group">
												<button class="btn btn-success btn-round md-trigger" data-modal="modal-14">Add Topic</button>
											</div>
									<div class="task-right">
										<div class="task-right-header-users">
											<span data-toggle="collapse">Assign Users</span>
											<i class="icofont icofont-rounded-down f-right"></i>
										</div>
										<div class="user-box assign-user taskboard-right-users">
												@foreach($enrollusers as $enrolluser)
												<div class="media">
													<div class="media-left media-middle photo-table">
														<a href="#">
															<img class="media-object img-radius" src="{{ $enrolluser->profile_image ? URL::asset('/trainee/profile_image/'.$enrolluser->profile_image): URL::asset('files/assets/images/social/profile.jpg')}}" alt="Generic placeholder image">
															@if($enrolluser->status == 'Pending')
															<div class="live-status bg-danger"></div>
															@else
															<div class="live-status bg-success"></div>
															@endif
														</a>
													</div>
													<div class="media-body">
														<h6>{{$enrolluser->name}}</h6>
														<p>{{$enrolluser->sector}}</p>
													</div>
												</div>
												@endforeach
												
												
											</div>



									</div>

								</div>

							</div>

						</div>
						<div class="col-xl-9 col-lg-12 pull-xl-3 filter-bar">

								<nav class="navbar navbar-light bg-faded m-b-30 p-10">
										<form method="get">
									<ul class="nav navbar-nav p-t-10">
										<li class="nav-item active">
											<a class="nav-link" href="#!">Filter:
												<span class="sr-only">(current)</span>
											</a>
										</li>
										<li class="nav-item dropdown">
											<a class="nav-link dropdown-toggle" href="#!" id="bydate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="icofont icofont-clock-time"></i> By Date</a>
											<div class="dropdown-menu" aria-labelledby="bydate">
												<a class="dropdown-item" href="#!">Show all</a>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" onclick="searchdate('Today')">Today</a>
												<a class="dropdown-item" onclick="searchdate('Month')">This month</a>
												<a class="dropdown-item" onclick="searchdate('Year')">This year</a>
											</div>
										</li>
	
										<li class="nav-item dropdown">
											<a class="nav-link dropdown-toggle" href="#!" id="bystatus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="icofont icofont-chart-histogram-alt"></i> By Status</a>
											<div class="dropdown-menu" aria-labelledby="bystatus">
												<a class="dropdown-item" >Show all</a>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" onclick="searchstatus('Active')">Active</a>
												<a class="dropdown-item" onclick="searchstatus('De-active')">De-active</a>
												
											</div>
										</li>
	
										
									</ul>
									<div class="nav-item nav-grid p-t-10">
											
										<div class="input-group">
											<input type="text" class="form-control" name='q' placeholder="Search here...">
											<button class="btn btn-success f-right m-l-10">
												Filter
											</button>
										</div>
									
									</div>
									
									<input type='hidden' name='date' id='searchdate'>
									<input type='hidden' name='status' id='searchstatus'>
								</form>
								</nav>

							<div class="row">
								
								@if(sizeof($topics) > 0)
								@foreach($topics as $topic)
								
								<div class="col-sm-12">
									<div class="card card-comment card-border-primary">
										<div class="card-block-small">
											<img class="height-width" src="{{ $topic->topic_image ? URL::asset('/topic_images/'.$topic->topic_image): URL::asset('/topic_images/no_image.jpg')}}" alt="Topic Image">
											
											<div class="comment-desc">
												<h6>
													<a href="/admin/tweets-list/{{$topic->course_id}}/{{$topic->topic_id}} "> {{$topic->topic_name}} </a>
												</h6>
												<p class="text-muted ">{!! base64_decode($topic->topic_description)!!} </p>
												<div class="comment-btn">
													@if($topic->status == 'Active')
													<button class="btn bg-c-green btn-round btn-comment ">{{$topic->status}}</button>
													@else
													<button class="btn btn-danger btn-round btn-comment ">{{$topic->status}}</button>
													@endif
												</div>

											</div>
										</div>
										<div class="card-footer">
											<div class="task-list-table">
												<p class="task-due">
													<strong> Date : </strong>
													<strong class="label label-success">{{$topic->created_at}}</strong>
												</p>
											</div>
											<div class="task-board m-0">
												<a href="/admin/tweets-list/{{$topic->course_id}}/{{$topic->topic_id}}" class="btn btn-info btn-mini b-none">
													<i class="icofont icofont-eye-alt m-0"></i>
												</a>

												<div class="dropdown-secondary dropdown">
													<button class="btn btn-info btn-mini dropdown-toggle waves-light b-none txt-muted" type="button" id="dropdown3" data-toggle="dropdown"
													 aria-haspopup="true" aria-expanded="false">
														<i class="icofont icofont-navigation-menu"></i>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdown3" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">

														<a class="dropdown-item waves-light waves-effect md-trigger" href="#!"  data-modal="modal-edit" onclick="edittopic('{{$topic->topic_id}}')">
															<i class="icofont icofont-spinner-alt-5"></i> Edit Topic</a>
														<a class="dropdown-item waves-light waves-effect md-trigger"  onclick='deletetopic({{$topic->topic_id}})'>
																<i class="icofont icofont-delete-alt"></i> Delete Topic</a>
															@if($topic->status == 'Active')
														<a class="dropdown-item waves-light waves-effect" href="/admin/topic-status/{{$topic->topic_id}}/De-active">
															<i class="icofont icofont-ui-edit"></i> De-active</a>
															@else
															<a class="dropdown-item waves-light waves-effect" href="/admin/topic-status/{{$topic->topic_id}}/Active">
																	<i class="icofont icofont-ui-edit"></i> Active</a>
																	@endif
													</div>

												</div>

											</div>

										</div>
									</div>
								</div>
								@endforeach
								@else
								<div class="col-sm-12">
										<div class="card">
										<div class="card-block">
									<h4>No Topic is available </h4>
										</div>
									</div>
								</div>
								@endif
								<div class="col-sm-12">
										<div class="card">
											<div class="card-block">
												<nav class="f-right">
													{{ $topics->appends(request()->query())->links("pagination::bootstrap-4") }}
												</nav>
											</div>
										</div>
									</div>

							</div>





						</div>
						
					</div>

				</div>
			</div>
		</div>

	</div>
</div>
@stop @section('javascript-files')
<script src="{{URL::asset('files/assets/pages/task-board/task-board.js')}}"></script>
<script src="{{URL::asset('files/assets/js/modalEffects.js')}}"></script>
<script src="{{URL::asset('files/assets/js/classie.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/topic/base64js.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/tinymce/tinymce.min.js?apiKey=jdw0qpvvruo7046n3j4g8o122piap638faeuii9s0rlk2fy3')}}"></script>
<script>tinymce.init({ 
	selector:'textarea',
	menubar:false,
	statusbar: false,
	plugins:'smileys, emoticons',
	toolbar1: 'smileys | emoticons | formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat'
 });
</script>
@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif 

<script>
	function Base64Decode(str, encoding = 'utf-8') {
		var bytes = base64js.toByteArray(str);
		return new (TextDecoder || TextDecoderLite)(encoding).decode(bytes);
	}
var topic_array = '{!! json_encode($topics->items()) !!}';
var topics = JSON.parse(topic_array);
function edittopic(id){
 //alert(id);
 var result = $.grep(topics, function(e){ return e.topic_id == id; });
 //alert(result[0]['topic_id']);
$('#topic_name').val(result[0]['topic_name'].toString());
$('#topic_id').val(result[0]['topic_id']);
//$('.topic_description').val(result[0]['topic_description'].toString());
tinymce.editors[1].setContent(Base64Decode(result[0]['topic_description'].toString()));
//alert("umb");

}

</script>
<script>
		function searchdate(date)
		{
			document.getElementById("searchdate").setAttribute('value',date);
		}
		function searchstatus(status)
		{
			document.getElementById("searchstatus").setAttribute('value',status);
		}
		function deletetopic(id){
			//alert(id);
			var href="/admin/delete-topic/"+ id;
			//$("#deletetopic").attr('href', href);
			swal({
				title: "Are you sure?",
				text: "This Topic will be deleted",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			}, function() {
				//$('#deletetopic').trigger("click");
				window.location = href;
			});
		}
	</script>


@stop