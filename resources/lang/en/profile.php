<?php

return [

   

    'about' => 'About',
    'CPass' => 'Change Password',
    'Email' => 'Email Address',
    'mobile' => 'Mobile Number',
    'name' => 'Full Name',
    'gender'=> 'Gender',
    'BDate'=> 'Birth Date',
    'MStatus' => 'Marital Status',
    'basicInfo'=> 'Basic Information',
    'tag'=> 'Tagline',
    'contactInfo'=> 'Contact Information',
    'address'=> 'Address',
    'work'=> 'Work',
    'education'=> 'Education',
    'skills'=>'Skills',
    'occupation'=>'Occupation',
    'OPass'=>'Old Password',
    'NPass'=>'New Password',
    'RPass'=>'Repeat New Password',
    'save'=>'Save',
    'cancel'=>'Cancel',
    'male'=>'Male',
    'female'=>'Female',
    'married'=>'Married',
    'unmarried'=>'Unmarried',
    

];
