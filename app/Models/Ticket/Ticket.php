<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Models\User;
use App\Models\Ticket\TicketComment;
use App\Models\Ticket\TicketCategory;
class Ticket extends Model
{
    //
    use Notifiable;
    protected $table = 'tickets';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'category_id', 'ticket_id', 'title', 'priority', 'message', 'status',
    ];

    /**
     * A ticket belongs to a user
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * A ticket can have many comments
     */
    public function comments()
    {
        return $this->hasMany(TicketComment::class)->with('user');
    }

    /**
     * A ticket belongs to a category
     */
    public function category()
    {
        return $this->belongsTo(TicketCategory::class);
    }
}
