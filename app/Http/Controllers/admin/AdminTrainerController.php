<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Course\Category;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;

class AdminTrainerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    public function UpdateTrainer (Request $request)
    {
        //return $request;
        $data = $request->all();
        //return $data;
        $category_validator = Validator::make($data , [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'sector' => 'required',
        ]);

        if ($category_validator->fails()) {
            $messages = $category_validator->messages();
//return $messages;
            return back()->withError($messages)->with('error_messages', "Error in Validations");
        }
        //return $course_id;
        DB::beginTransaction();
        try{
        $existCourse = User::where('user_id', $data["user_id"])->first();
        if($existCourse){
        
        $existCourse->name = $data["name"];
        $existCourse->email = $data["email"];
        $existCourse->sector = $data["sector"];
        $existCourse->save($data);
        // After creating the user send an email with the random token generated in the create method above

        DB::commit();
        Session::flash('update', 'Data Updated Successfully');
        return back()->with('update', 'Data Successfully Updated');
    } else{
        return back()
        ->withError('error_messages', 'Unable to Update data.');
    }

}catch (Exception $e) {
        DB::rollback();
        return back()->with('error_messages', 'Error in Updating data');
    }

    }

    public function UpdateTrainerStatus(Request $request)
    {
        $data = $request->all();
        //return $data;
        DB::beginTransaction();
        try
        {
            
           // Category::create();
           
            $existCourse = User::where('user_id', $data['user_id'])->first();
            if(!empty($existCourse))
            {
                if($existCourse->is_active == 1){
                    $existCourse->is_active = 0;
                } else {
                    $existCourse->is_active = 1;
                }
            
                $existCourse->save();
                DB::commit();
                return response()->json([
                    'data' => [
                      'success' => 'true',
                    ]
                  ]);
                
            }
            else
            {
                return response()->json([
                    'data' => [
                      'success' => 'false',
                    ]
                  ]);
                
            }
            
            
            
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Updating Course Status');
        }

    }

    public function index()
    {
        $trainers = User::where('user_type', 'trainer')->select('name','email','user_id','sector','is_active','is_verified','profile_image')->get();
        $categories = Category::all();
        //return $categories;
        return view('admin.trainer', compact('trainers','categories'));
    } 
}
