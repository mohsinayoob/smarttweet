@extends('layouts.admin_layout') @section('title', 'Categories')
<title>Add Tranier</title>
@section('css-files')
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/jquery.steps/css/jquery.steps.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}">
@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-header card">
					<div class="card-block">
						<h5 class="m-b-10">{{trans ('addtrainer.tag') }}</h5>
					</div>
				</div>
				<div class="page-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-header">
									
								</div>
								<div class="card-block">
									<div class="row">
										<div class="col-md-12">
											<div id="wizarda">
												<section>
													<form class="wizard-form" id="form-add-trainer" action="/admin/add-trainer" method="post">
														{{csrf_field()}}
														<h3> {{trans ('addtrainer.reg') }} </h3>
														<fieldset>
															<div class="form-group row">
																<div class="col-sm-12">
																	<label for="userName-2" class="block">{{trans ('addtrainer.name') }} *</label>

																</div>
																@if ($errors->has('name'))
																	<span class="help-block" style="color:red;">
																		<strong>{{ $errors->first('name') }}</strong>
																	</span>
																@endif
																<div class="col-sm-12">
																	<input id="userName-2a"  type="text" class=" form-control" name="name" value="{{ old('name') }}" required autofocus>
																</div>
															</div>
															<div class="form-group row">
																<div class="col-sm-12">
																	<label for="email-2" class="block">{{trans ('addtrainer.Email') }} *</label>
																</div>
																@if ($errors->has('email'))
																	<span class="help-block" style="color:red;">
																		<strong>{{ $errors->first('email') }}</strong>
																	</span>
																@endif
																<div class="col-sm-12">
																	<input id="email-2a"  type="email" class=" form-control" name="email" value="{{ old('email') }}" required>
																</div>
															</div>
															<div class="form-group row">
																<div class="col-sm-12">
																	<label for="sector" class="block">{{trans ('addtrainer.sector') }} *</label>
																</div>
																@if ($errors->has('category'))
																	<span class="help-block" style="color:red;">
																		<strong>{{ $errors->first('category') }}</strong>
																	</span>
																@endif
																<div class="col-sm-8 col-lg-12">
																	<select class="form-control form-control-primary" name="sector" required>
																		<option value="">{{trans ('addtrainer.select') }}</option>
																		@foreach($categories as $category)
																		<option value="{{$category->category_title}}" {{ old('sector')==$category->category_title ? "selected":"" }}>{{$category->category_title}}</option>
																		@endforeach
									
																	</select>
																</div>
															</div>
														</fieldset>
														
													</form>
												</section>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')
<script src="{{URL::asset('files/bower_components/jquery.cookie/js/jquery.cookie.js')}}"></script>
<script src="{{URL::asset('files/bower_components/jquery.steps/js/jquery.steps.js')}}"></script>
<script src="{{URL::asset('files/bower_components/jquery-validation/js/jquery.validate.js')}}"></script>
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/form-validation/validate.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/forms-wizard-validation/addtrainer-wizard.js')}}"></script>
@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif 
@stop