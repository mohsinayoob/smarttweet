<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Course\Topic;
use App\Models\Course\Course;
use App\Models\Tweet\Tweets;
use App\Models\User;
use App\Models\Course\CourseEnrollment;
class EnrollmentApprovedNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $enrollment;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($enrollment)
    {
        //
        $this->enrollment = $enrollment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $user = User::where('user_id',$this->enrollment->trainee_id)->first();
        $course = Course::where('course_id', $this->enrollment->course_id)->first();
        $this->enrollment->user = \Auth::user();
        $this->enrollment->url = "/".$user->user_type."/manage-course";
        $this->enrollment->message = " Approved Your Request to join ". $course->course_title;
        $user->notify(new \App\Notifications\SaveNotification($this->enrollment, \Auth::user(), $this->enrollment->message, $this->enrollment->url));
     
        
    }
}
