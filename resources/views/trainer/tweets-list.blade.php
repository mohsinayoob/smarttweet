@extends('layouts.trainer_layout') @section('title', 'Tweets') @section('css-files')
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/lightgallery/css/lightgallery.min.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/simple-line-icons/css/simple-line-icons.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/icofont/css/icofont.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/dropzone/dropzone.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/themify-icons/themify-icons.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}"> 
<style>
	.dropzone{
		margin-top: 10px;
		border: dashed #35AFAD;
		padding: 30px;
	}
</style>

@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">
					<div class="page-header card">
							<div class="card-block">
								<h5 class="m-b-10">All Tweets</h5>
							</div>
		
						</div>
				<div class="page-body">
						<div class="modal fade modal-flex" id="Modal-Timer" tabindex="-1" role="dialog">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<ul class="nav nav-tabs" role="tablist">
												<li class="nav-item">
													<a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">
														<i class="fa fa-clock-o"></i> Pick Time for Tweet
													</a>
												</li>
		
											</ul>
											<form method="post">
											<div class="tab-content modal-body">
												
													{{csrf_field()}}
												<div class="tab-pane active" id="tab-home" role="tabpanel">
													<input type="hidden" id="hidden-status-url" />
														<span class="label label-info" style="font-size:101%;margin-bottom:20px;">
																You can skip this step and Default Time will be 05:00.
															</span>
													<div class="card-block user-box" >
														<label for="minute">Minutes </label>
														<div class="input-group">
															<select name="minutes" class="form-control" id="tweet-minutes">
																@for($i=0; $i<60; $i++)
																	<option value="{{$i}}">{{$i}}</option>
																@endfor
															</select>
														</div>
														<label for="seconds">Seconds </label>
														<div class="input-group">
															<select name="seconds" class="form-control" id="tweet-seconds">
																@for($i=0; $i<60; $i++)
																	<option value="{{$i}}">{{$i}}</option>
																@endfor
															</select>
														</div>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Skip</button>
												<button class="btn btn-primary">Save</button>
											</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						<div class="modal fade modal-flex" id="Modal-tab" tabindex="-1" role="dialog">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<ul class="nav nav-tabs" role="tablist">
												<li class="nav-item">
													<a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">
														<i class="fa fa-heart"></i>
													</a>
												</li>
		
											</ul>
											<div class="tab-content modal-body">
												<div class="tab-pane active" id="tab-home" role="tabpanel">
													<div class="card-block user-box" id="favorite-modal-data">
														<div class="fa fa-spinner fa-spin fa-lg"></div>
														
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="modal fade modal-flex" id="Modal-tab-like" tabindex="-1" role="dialog">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<ul class="nav nav-tabs" role="tablist">
												<li class="nav-item">
													<a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">
														<i class="fa fa-thumbs-up"></i>
													</a>
												</li>
		
											</ul>
											<div class="tab-content modal-body">
												<div class="tab-pane active" id="tab-home" role="tabpanel">
													<div class="card-block user-box" id="like-modal-data">
													
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="modal fade modal-flex" id="Modal-tab-dislike" tabindex="-1" role="dialog">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-body">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											<ul class="nav nav-tabs" role="tablist">
												<li class="nav-item">
													<a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">
														<i class="fa fa-thumbs-down"></i>
													</a>
												</li>
		
											</ul>
											<div class="tab-content modal-body">
												<div class="tab-pane active" id="tab-home" role="tabpanel">
													<div class="card-block user-box" id="dislike-modal-data">
														
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
					<div class="md-overlay"></div>
					
					<div class="row">
						<div class="col-lg-9">
								
							<div class="row">
								<div class="col-md-12">
									<div class="card bg-white">
										<div class="post-new-contain row card-block">
											<div class="col-md-1 col-xs-3 post-profile">
												<img src="{{URL::asset('trainer/profile_image/'.\Auth::user()->profile_image)}}" class="img-fluid" alt="">
											</div>
											<div class="col-md-11 col-xs-9">
											<form  action="/trainer/postTweet/{{$course_id}}/{{$topic_id}}" method="post" id="postTweet">
												{{ csrf_field() }}
												<div class="">
													<div id="tweet_content_section">

													</div>
													<input type="hidden" name="content_type" id="tweet_content_type" value="text" />
													<textarea id="post-message" class="form-control post-input" rows="3" cols="10" placeholder="Write something....." name="tweet_description"></textarea>
													
												</div>
											</form>
												<div class="" id="progress_view">
														
												</div>
											</div>
											
											
										</div>
										
										<div class="post-new-footer b-t-muted p-15">
											<span class="image-upload m-r-15" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add Photos">
												<label id="imageselect" class="file-upload-lbl">
													<i class="icofont icofont-image text-muted"></i>
												</label>
											</span>
											<span class="image-upload m-r-15" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add Video">
													<label  id ="videoselect" class="file-upload-lbl">
														<i class="icofont icofont-video text-muted"></i>
													</label>
												</span>
											<span>
												<button id="post-new" class="btn btn-primary waves-effect waves-light f-right" onclick="tweetSubmition()">
													Post
												</button>
											</span>

										</div>
									</div>
									
									@foreach($tweets as $tweet)
									<div>
										<div class="card bg-white p-relative">
											<div class="card-block">
											<div class="card-header-right">
											<a href="/trainer/tweet/{{$tweet->topic_id}}/{{$tweet->tweet_id}}" class="btn btn-info btn-md b-none f-right" title="watch tweet">
													<i class="icofont icofont-eye-alt icofont-lg m-1"></i>
												</a>
											</div>
												<div class="media">
													<div class="media-left media-middle friend-box">
														<a href="/public-profile/{{$tweet->user_id}}">
															@if($tweet->user_type=="trainer")
															<img class="media-object img-radius m-r-20" src="{{URL::asset('trainer/profile_image/'.$tweet->profile_image)}}" alt="">
															@elseif($tweet->user_type=="admin")
															<img class="media-object img-radius m-r-20" src="{{URL::asset('admin/profile_image/'.$tweet->profile_image)}}" alt="">
															@endif
														</a>
													</div>
													<div class="media-body">
														<div class="chat-header">{{$tweet->name}} </div>
														<div class="f-13 text-muted">{{$tweet->created_at}}</div>
													</div>
												</div>
											</div>
											@if($tweet->content_type=="images")
											<div id="lightgallery1" class="wall-img-preview lightgallery-popup">
												@if($tweet->tweet_content)
													<?php $count=0;?>
													@foreach($tweet->tweet_content as $tweet_content)
													<?php $count ++;?>
													
													@if($count==4 || sizeof($tweet->tweet_content) < 2)
													<div class="col-md-12 p-0 wall-item" data-responsive="{{URL::asset('tweet_content/'.$tweet_content->file)}} 375, img/1-480.jpg 480, img/1.jpg 800"
													data-src="{{URL::asset( 'tweet_content/'.$tweet_content->file)}}" data-sub-html="{{ base64_decode($tweet->tweet_description) }}" >
													   <a href="#">
														   <img src="{{URL::asset('tweet_content/'.$tweet_content->file)}}" class="img-fluid " alt="">
													   </a>
												   </div>
													
													@else
													<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('tweet_content/'.$tweet_content->file)}} 375, img/1-480.jpg
													480, img/1.jpg 800 "
													data-src="{{URL::asset( 'tweet_content/'.$tweet_content->file)}} " data-sub-html="{{base64_decode($tweet->tweet_description) }}">
													   <a href="#">
														   <img src="{{URL::asset('tweet_content/'.$tweet_content->file)}}" class="img-fluid " alt="">
													   </a>
												   </div>
													@endif
													
													@endforeach
												@endif
											</div>
											@elseif($tweet->content_type=="video")
											<!-- data-src should not be provided when you use html5 videos -->
												<div class="wall-img-preview">
													<div class="col-md-12 p-0 wall-item">
												
												@foreach($tweet->tweet_content as $tweet_content)
												<div style="display:none;" id="video{{$tweet_content->content_id}}">
														<video class="lg-video-object lg-html5 video-js vjs-default-skin" controls preload="none">
														<source src="{{URL::asset('tweet_content/'.$tweet_content->file)}}" type="video/mp4">
														 Your browser does not support HTML5 video.
													</video>
												</div>
												<ul id="html5-video" class="html5-videos">
												  <li  data-sub-html="{{base64_decode($tweet->tweet_description) }}" data-html="#video{{$tweet_content->content_id}}" >
													<video class="lg-video-object lg-html5 video-js vjs-default-skin" controls preload="none" style="width: 100%;">
														<source src="{{URL::asset('tweet_content/'.$tweet_content->file)}}" type="video/mp4">
														Your browser does not support HTML5 video.
													</video>
													
												  </li>
												</ul>
												  @endforeach
												
											</div>
										</div>

											@endif
											<div class="card-block">
												<div class="timeline-details">
													
													<p class="text-muted">
                                                    {!! base64_decode($tweet->tweet_description) !!}
                                                    </p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-msg">
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab" onclick='getReactionUsers("{{$tweet->tweet_id}}","favorite")'>
													<i class="icofont icofont-heart-alt text-muted">
													</i>Favorite
													<span class="b-r-theme" id="total_favorites_{{$tweet->tweet_id}}"> ( {{$tweet->total_favorites}} )</span>
												</a>
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab-like" onclick='getReactionUsers("{{$tweet->tweet_id}}","like")'>
													<i class="icofont icofont-thumbs-up text-muted">
													</i>Likes
													<span class="b-r-theme" id="total_likes_{{$tweet->tweet_id}}"> ( {{$tweet->total_likes}} )</span>
												</a>
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab-dislike" onclick='getReactionUsers("{{$tweet->tweet_id}}","dislike")'>
													<i class="icofont icofont-thumbs-down text-muted">
													</i>Dislike
													<span class="b-r-theme" id="total_dislikes_{{$tweet->tweet_id}}"> ( {{$tweet->total_dislikes}} )</span>
												</a>
												<a href="/trainer/tweet/{{$tweet->topic_id}}/{{$tweet->tweet_id}}">
													<i class="icofont icofont-comment text-muted">
													</i>Comments
													<span class="b-r-theme" id="total_comments_{{$tweet->tweet_id}}"> ( {{$tweet->total_comments}} )</span>
												</a>
												
												<div class="dropdown-secondary dropdown f-right" style="display:inline;">
														<button class="btn btn-info btn-mini dropdown-toggle waves-light b-none txt-muted" type="button" id="dropdown3" data-toggle="dropdown"
														 aria-haspopup="true" aria-expanded="false">
															<i class="icofont icofont-navigation-menu"></i>
														</button>
														<div class="dropdown-menu" aria-labelledby="dropdown3" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
	
															<a class="dropdown-item waves-light waves-effect md-trigger" href="/trainer/edit-tweet/{{$tweet->topic_id}}/{{$tweet->tweet_id}}"  >
																<i class="icofont icofont-spinner-alt-5"></i> Edit Tweet</a>
															<a class="dropdown-item waves-light waves-effect md-trigger"  onclick='deletetweet({{$tweet->tweet_id}})'>
																	<i class="icofont icofont-delete-alt"></i> Delete Tweet</a>
																@if($tweet->status == 'Active')
															<a class="dropdown-item waves-light waves-effect"  onclick="updateTweetStatus('{{$tweet->topic_id}}', '{{$tweet->tweet_id}}', 'De-active');">
																<i class="icofont icofont-ui-edit"></i> De-activate</a>
															@else
																<a class="dropdown-item waves-light waves-effect" data-toggle="modal" data-target="#Modal-Timer" onclick="updateTweetStatusActive('{{$tweet->topic_id}}', '{{$tweet->tweet_id}}', 'Active');">
																<i class="icofont icofont-ui-edit"></i> Activate</a>
															@endif
														</div>
	
													</div>
											</div>
										
											
										</div>


									</div>
									@endforeach

								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="card">
										<div class="card-block">
											<nav class="f-right">
												{{ $tweets->appends(request()->query())->links("pagination::bootstrap-4") }}
											</nav>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="col-md-3">
								<div class="card">
										
										<div class="card-block p-t-10">
												<div class="fb-timeliner">
														<h2>Search Tweet</h2>
														<ul>
															<li class="">
																<a href='/trainer/tweets-list/{{$course_id}}/{{$topic_id}}'>Clear Search</a>
															</li>
															<li class="{{ app('request')->input('date')=='Today' ? 'active': ''  }}">
																<a href='?date=Today'>Today</a>
															</li>
															<li  class="{{ app('request')->input('date')=='Month' ? 'active': ''  }}">
																<a href='?date=Month'>This Month</a>
															</li>
															<li class="{{ app('request')->input('date')=='Year' ? 'active': ''  }}">
																<a href='?date=Year'>This Year</a>
															</li>
															
														</ul>
													</div>
											<div class="task-right">
												<div class="task-right-header-users">
													<span data-toggle="collapse">Assign Users</span>
													<i class="icofont icofont-rounded-down f-right"></i>
												</div>
												<div class="user-box assign-user taskboard-right-users">
													@foreach($enrollusers as $enrolluser)
													<div class="media">
														<div class="media-left media-middle photo-table">
															<a href="#">
																<img class="media-object img-radius" src="{{ $enrolluser->profile_image ? URL::asset('/trainee/profile_image/'.$enrolluser->profile_image): URL::asset('files/assets/images/social/profile.jpg')}}" alt="Generic placeholder image">
																@if($enrolluser->status == 'Pending')
																<div class="live-status bg-danger"></div>
																@else
																<div class="live-status bg-success"></div>
																@endif
															</a>
														</div>
														<div class="media-body">
															<h6>{{$enrolluser->name}}</h6>
															<p>{{$enrolluser->sector}}</p>
														</div>
													</div>
													@endforeach
													
													
												</div>
		
		
		
											</div>
		
										</div>
		
									</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')
<script src="{{URL::asset('files/bower_components/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-fullscreen.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-video.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-autoplay.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-zoom.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-hash.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-pager.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/wall/wall.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/dropzone/dropzone.js')}}"></script>
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>
<link href="{{URL::asset('files/assets/pages/videojs/video-js.css')}}" rel="stylesheet">
<script src="{{URL::asset('files/assets/pages/videojs/video.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/tinymce/tinymce.min.js?apiKey=jdw0qpvvruo7046n3j4g8o122piap638faeuii9s0rlk2fy3')}}"></script>
<style>
		.mce-top-part {
			position: absolute;
			bottom: 0px;
		}
</style>
<script>
	
		$(document).ready(function() {
			
			$('.lightgallery-popup').lightGallery();
			$('.html5-videos').lightGallery({
				thumbnail: false,
				videojs:true
			}); 

			var editor =tinymce.init({ 
				selector:'textarea',
				menubar:false,
				statusbar: false,
				plugins:'smileys, emoticons',
				toolbar1: 'smileys | emoticons',
				theme_advanced_toolbar_location : "bottom",
				auto_convert_smileys: true,
				
			});
			var is_video_selected =false;
			var images_count = 0;
			var myDropzone;
			$("#imageselect").click(function() {
				$('#tweet_content_type').val('images');
				$('#tweet_content_section').html('');
				$('#progress_view').html('<form action="{{ url("/trainer/upload-tweet-image")}}" enc-type="multipart/form-data"  class="dropzone" id="image-dropzone">'+'{{csrf_field()}}'+'<div class="dz-default dz-message"><span>Drop images here to upload</span></div></form>');
				myDropzone = new Dropzone("#image-dropzone",{
					
					headers: {
						'X-CSRF-TOKEN': '{{csrf_field()}}',
					},
					paramName: "file", // The name that will be used to transfer the file
    				maxFilesize: 2, // MB
    				addRemoveLinks: true,
					maxFiles: 4,
					acceptedFiles: ".jpeg,.jpg,.png,.gif",
    				init: function() {
      					this.on("maxfilesexceeded", function() {
        					if (this.files[4]!=null){
          						this.removeFile(this.files[4]);
							}
						});
						this.on("error", function(file) {
							this.removeFile(file);
							swal("Error!", "Error Occured in Uploading", "error");
						});
						
						this.on("complete", function (data) {
							//var res = eval('(' + data.xhr.responseText + ')');
							
							if(data.xhr && data.xhr.response !== "error")
							{
								var imgName = data.xhr.response;
								var imgId = data.xhr.response.split('.')[0];
								$('#tweet_content_section').append('<input type="hidden" name="tweet_contents[]" value="'+data.xhr.response+'" id="'+imgId+'">');
							
							}
							else
							{
								swal("Error!", "Error Occured in Uploading", "error");
								this.removeFile(data);
							}
							
							if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
								//alert("all uploaded");
								$('#post-new').show()
							  }
						});
						this.on('removedfile', function (file) {
							if(file.xhr)
							{
								var inputId = file.xhr.response;
								var imgId = file.xhr.response.split('.')[0];
								$( '#'+imgId ).remove();
							}
							
							//alert("removed.. " + inputId);
						});
						  this.on("addedfile", function(file) {
							$('#post-new').hide()
							  //alert("file added");
							/* Maybe display some more file information on your page */
						  });
    				},
				});
			});

			//////////////////////////Video selecting drop zone /////////////////////////
			$("#videoselect").click(function() {
				$('#tweet_content_type').val('video');
				$('#tweet_content_section').html('');
				$('#progress_view').html('<form action="{{ url("/trainer/upload-tweet-image")}}" enc-type="multipart/form-data"  class="dropzone" id="image-dropzone">'+'{{csrf_field()}}'+'<div class="dz-default dz-message"><span>Drop video here to upload<br>You can upload one Video</span></div></form>');
				myDropzone = new Dropzone("#image-dropzone",{
					
					headers: {
						'X-CSRF-TOKEN': '{{csrf_field()}}',
					},
					paramName: "file", // The name that will be used to transfer the file
    				maxFilesize: 100, // MB
    				addRemoveLinks: true,
					maxFiles: 1,
					acceptedFiles: ".mp4,.mkv,.flv,.avi,.mov",
    				init: function() {
      					this.on("maxfilesexceeded", function() {
        					if (this.files[1]!=null){
          						this.removeFile(this.files[1]);
							}
						});
						this.on("error", function(file) {
							this.removeFile(file);
							swal("Error!", "Error Occured in Uploading", "error");
						});
						
						this.on("complete", function (data) {
							//var res = eval('(' + data.xhr.responseText + ')');
							
							if(data.xhr && data.xhr.response !== "error")
							{
								var imgName = data.xhr.response;
								var imgId = data.xhr.response.split('.')[0];
								$('#tweet_content_section').append('<input type="hidden" name="tweet_contents[]" value="'+data.xhr.response+'" id="'+imgId+'">');
							
							}
							else
							{
								swal("Error!", "Error Occured in Uploading", "error");
								this.removeFile(data);
							}
							
							if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
								//alert("all uploaded");
								$('#post-new').show()
							  }
						});
						this.on('removedfile', function (file) {
							if(file.xhr)
							{
								var inputId = file.xhr.response;
								var imgId = file.xhr.response.split('.')[0];
								$( '#'+imgId ).remove();
							}
							
							//alert("removed.. " + inputId);
						});
						  this.on("addedfile", function(file) {
							$('#post-new').hide()
							  //alert("file added");
							/* Maybe display some more file information on your page */
						  });
    				},
				});
			});

	});
	function tweetSubmition()
	{
		var editorContent = tinyMCE.get('post-message').getContent();
		if (editorContent=="")
		{
			swal("Error!", "Description should not be empty", "error");
			return;
		}
		
		$('#postTweet').submit();
		
		//alert('button clicked');
		
	}

	
</script>
@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif 
<script>
		function getReactionUsers(id, reaction){
			$('#'+reaction+'-modal-data').html('<div class="fa fa-spinner fa-spin fa-lg"></div>');
			$.ajax({
					  
				type:'GET',
				url:'/trainer/tweet-reaction-users/'+id+'/'+reaction,
				headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
				
				success: function(data){
				  if(data.success){
					  var appendid = $('#like-modal-data');
					if(reaction == 'favorite'){
						appendid = $('#favorite-modal-data');
	
					}else if(reaction == 'dislike'){
						appendid = $('#dislike-modal-data');
	
					}
					var reactionUsers = data.data;
					var userhtml = '';
					if(reactionUsers.length > 0){
						 reactionUsers.forEach(function(user){
							userhtml+= '<div class="media m-b-10">'+
									'<a class="media-left" href="#!">';
							
							userhtml+=	'<img class="media-object img-radius" src="/'+user.user_type+'/profile_image/'+user.profile_image+'" alt="Generic placeholder image" data-toggle="tooltip" data-placement="top" title="user image">'+
										'</a>'+
										'<div class="media-body">';
							userhtml+='<div class="chat-header">'+user.name+'</div>'+
										'<div class="text-muted social-designation">'+user.user_type+'</div>'+
										'</div></div>';
						  //alert(user.name);
					});
					appendid.html(userhtml);
				}else{
					appendid.html("no data available");
				}
				$('#total_likes_'+id+'').text("("+data.total_likes+")");
				$('#total_favorites_'+id+'').text("("+data.total_favorites+")");
				$('#total_dislikes_'+id+'').text("("+data.total_dislikes+")");
				$('#total_comments_'+id+'').text("( "+data.total_comments+" )");
					
			}else{}
		}
		});
	
	}

	function deletetweet(id){
		//alert(id);
		var href="/trainer/delete-tweet/"+ id;
		//$("#deletetopic").attr('href', href);
		swal({
			title: "Are you sure?",
			text: "This Tweet will be deleted",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function() {
			//$('#deletetopic').trigger("click");
			window.location = href;
		});
	}

	function updateTweetStatus(topic_id, tweet_id, status){
		//alert(id);
		var href="/trainer/tweet-status/"+topic_id+"/"+tweet_id +"/"+status;
		//$("#deletetopic").attr('href', href);
		swal({
			title: "Are you sure?",
			text: "You want to change status of Tweet.",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, Proceed!",
			closeOnConfirm: false
		}, function() {
			//$('#deletetopic').trigger("click");
			window.location = href;
		});
	}

	function updateTweetStatusActive(topic_id, tweet_id, status){
		var href="/trainer/tweet-status/"+topic_id+"/"+tweet_id +"/"+status;
		$('form').attr('action',href)
		$('#hidden-status-url').val(href);
	}
	</script>
	
@stop