@extends('layouts.authentication_layout') @section('title', 'Forget Password') @section('body_content')
<section class="login header p-fixed d-flex text-center">

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">

				<div class="login-card card-block auth-body mr-auto ml-auto">
					<form class="md-float-material" method="POST" action="{{ route('password.request') }}">
						{{ csrf_field() }}


						<div class="auth-box">
							<div class="row m-b-20">
								<div class="col-md-12">
									<h3 class="text-left txt-primary">Reset Password</h3>
								</div>
							</div>
							<hr />
							<input type="hidden" name="token" value="{{ $token }}"> 
							@if ($errors->has('email'))
							<span class="help-block" style="color:red">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
							<div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<input type="email" class="form-control" placeholder="Your Email Address" name="email" value="{{ $email or old('email') }}"
								 required autofocus>
								<span class="md-line"></span>
							</div>
							@if ($errors->has('password'))
                                    <span class="help-block" style="color:red">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
							<div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<input type="password" class="form-control" placeholder="New Password" name="password" required>
								<span class="md-line"></span>
							</div>
							@if ($errors->has('password_confirmation'))
                                    <span class="help-block" style="color:red">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
							<div class="input-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
								<span class="md-line"></span>
							</div>
							<div class="row m-t-25 text-left">
								<div class="col-12">

									<div class="forgot-phone text-right f-right">
										<a href="/" class="text-right f-w-600 text-inverse"> Sign In?</a>
									</div>
								</div>
							</div>

							<div class="row m-t-30">
								<div class="col-md-12">
									<input type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20" value="Submit">
								</div>
							</div>
							<hr />

						</div>
					</form>

				</div>

			</div>

		</div>

	</div>

</section>

@stop