@extends('layouts.'.\Auth::user()->user_type.'_layout')
@section('title', $ticket->title)
 @section('css-files')

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}">

 @stop 
 @section('body_content')
 <div class="pcoded-content">
        <div class="pcoded-inner-content">
    
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">Tickets</h5>
                        </div>
    
                    </div>
                    <div class="page-body">
                    <div class="">
                        <div class="card">
                            <div class="card-block">
                                    <div class="card-header">
                                            #{{ $ticket->ticket_id }} - {{ $ticket->title }}
                                        </div>
                                <div class="timeline-details">
                                
                                        <div class="text-muted">{!! base64_decode($ticket->message) !!}</div>
                                        <p class="text-muted">Categry: {{ $category && $category->name ? $category->name : '' }}</p>
                                        <p class="text-muted">
                                        @if ($ticket->status === 'Open')
                                            Status: <span class="label label-success">{{ $ticket->status }}</span>
                                        @else
                                            Status: <span class="label label-danger">{{ $ticket->status }}</span>
                                        @endif
                                        </p>
                                        <p class="text-muted">Created on: {{ $ticket->created_at->diffForHumans() }}</p>
                                </div>
                            </div>
                            <div class="panel panel-default">
                               
                
                                    <div class="card-block user-box b-t-theme">
                                        @foreach ($comments as $comment)
                                        <div class="media b-b-theme p-t-20">
                                            <a class="media-left" href="/public-profile/{{$comment->user->user_id}}">
                                                <img class="media-object img-radius m-r-20" src="{{URL::asset($comment->user->user_type.'/profile_image/'.$comment->user->profile_image)}}" alt="">
                                            </a>
                                            <div class="media-body social-client-description">
                                                <div class="chat-header">{{$comment->user->name}}
                                                    <span class="text-muted">{{$comment->created_at}}</span>
                                                </div>
                                                <p class="text-muted">{!! base64_decode($comment->comment) !!}</p>
                                               
                                            </div>
                                        </div>
                                        @endforeach
                                        @if ($ticket->status === 'Open' || \Auth::user()->user_type=="admin")
                                        <div class="media m-t-20">
                                                <a class="media-left" href="#">
                                                        <img class="media-object img-radius m-r-20" src="{{URL::asset(\Auth::user()->user_type.'/profile_image/'.\Auth::user()->profile_image)}}" alt="">
                                                </a>
                                                <div class="media-body">
                                                    
                                                        <div class="m-r-20">
                                                                <form action="{{ url(\Auth::user()->user_type.'/comment') }}" method="POST" class="form">
                                                                {{csrf_field()}}
                                                                <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
                                                                <textarea id="post-message" class="f-13 form-control msg-send" rows="3" cols="10" placeholder="Write something....." name="comment"></textarea>
                                                                @if ($errors->has('comment'))
                                                                    <span class="help-block">
                                                                        <strong>{{ $errors->first('comment') }}</strong>
                                                                    </span>
                                                                @endif
                                                            
                                                            <div class="post-new-footer b-t-muted m-t-10 p-t-10">
                                                                    <span>
                                                                        <button type="submit" id="post-new" class="btn btn-primary waves-effect waves-light f-right">
                                                                            Post
                                                                        </button>
                                                                    </span>
                        
                                                                </div>
                                                            </form>
                                                        </div>
                                                    
                                                </div>
                                                
                                            </div>
                                            @endif
                                    </div>
                    </div>
                    
				</div>
			</div>
		</div>
	</div>
</div>

@stop 
@section('javascript-files')

<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/tinymce/tinymce.min.js?apiKey=jdw0qpvvruo7046n3j4g8o122piap638faeuii9s0rlk2fy3')}}"></script>
<style>
		.social-msg a.active i{
			color:#4099ff!important;
		}
		.social-msg a.active span{
			color:#4099ff!important;
		}
		.mce-top-part {
			position: absolute;
			bottom: 0px;
		}
		.image-upload{
			position: relative;
			left: 81px;
			bottom: 32px;
		}
		.max-width-400{
			max-width:400px;
		}
</style>

<script>
var editor =tinymce.init({ 
				selector:'textarea',
				mode : "textareas",
				menubar:false,
				statusbar: false,
				plugins:'smileys, emoticons',
				toolbar1: 'smileys | emoticons',
				theme_advanced_toolbar_location : "bottom",
				auto_convert_smileys: true,
				setup: function (editor) {
					editor.on('change', function () {
						tinymce.triggerSave();
					});
				}
				
			});
</script>
@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif 

@stop