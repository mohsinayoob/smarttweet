<?php

return [

   

    'Mcate' => 'Manage Categories',
    'ACate' => 'Add Category',
    'save' => 'Save',
    'close' => 'Close',
    'eCate' => 'Edit Category',
    'ser'=> 'Serial',
    'det'=> 'Detail',
    'edit' => 'Edit',
    'del'=> 'Delete',
    'sure'=>'Are you sure?',
    'msg'=>'This category will be deleted',
    'yesdel'=>'Yes, delete it!',
    'search'=>'Search',
    'cancel'=>'Cancel',
    
];
