@extends('layouts.authentication_layout') @section('title', 'Login Admin') @section('body_content')
<section class="login header p-fixed d-flex text-center">

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">

				<div class="login-card card-block auth-body mr-auto ml-auto">
					<form class="md-float-material" action="admin/dashboard" method="get">
						
						<div class="auth-box">
							<div class="row m-b-20">
								<div class="col-md-12">
									<h3 class="text-left txt-primary">Sign In</h3>
								</div>
							</div>
							<hr />
							<div class="input-group">
								<input type="email" class="form-control" placeholder="Your Email Address">
								<span class="md-line"></span>
							</div>
							<div class="input-group">
								<input type="password" class="form-control" placeholder="Password">
								<span class="md-line"></span>
							</div>
							<div class="row m-t-25 text-left">
								<div class="col-12">
									<div class="checkbox-fade fade-in-primary">
										<label>
											<input type="checkbox" value="">
											<span class="cr">
												<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
											</span>
											<span class="text-inverse">Remember me</span>
										</label>
									</div>
									<div class="forgot-phone text-right f-right">
										<a href="forgot-password.html" class="text-right f-w-600 text-inverse"> Forgot Password?</a>
									</div>
								</div>
							</div>
							<div class="row m-t-30">
								<div class="col-md-12">
									<input type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20" value="Sign In">
								</div>
							</div>
							<hr />
							
						</div>
					</form>

				</div>

			</div>

		</div>

	</div>

</section>

@stop