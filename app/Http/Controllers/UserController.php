<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    function index(){
        return view('admin.profile');
    }
    function trainee(){
        return view('admin.trainee');
    }
    function trainer(){
        return view('admin.trainer');
    }
}
