<?php

namespace App\Models\Tweet;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TweetReaction extends Model
{
    use Notifiable;
    protected $table = 'tweet_reaction';
    protected $primaryKey = 'reaction_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['tweet_id', 'user_id', 'reaction_content', 'created_at', 'updated_at', 'status'];
}
