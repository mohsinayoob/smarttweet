<?php

namespace App\Http\Controllers\trainee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Tweet\Tweets;
use App\Models\Tweet\TweetContent;
use App\Models\Course\Course;
use App\Models\Course\CourseEnrollment;
use App\Models\Course\Topic;
use App\Models\Tweet\TweetReaction;
use App\Models\Tweet\CommentReaction;
use App\Models\Tweet\Comments;
use Carbon\Carbon;
use DB;
use Session;

class TraineeTweetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainee');
        
    }
    public function viewSingleTweet($course_id, $topic_id)
    {

        $check_enrollment = CourseEnrollment::where('course_id',$course_id)
        ->where('trainee_id', \Auth::user()->user_id)->first();
        if(empty($check_enrollment) && $check_enrollment['status'] != 'Active'){
            return redirect('/trainee/manage-course')->with('error_messages','You are not Enrolled in this Course');
        }
        //$tweets = Tweets::join('tweet_content','tweet_content.tweet_id','=','tweets.tweet_id')
        //->where('tweets.topic_id',$topic_id)->get();
        $tweet = Tweets::with('tweet_content')
        ->join('users','users.user_id','=','tweets.user_id')
        ->where('tweets.topic_id',$topic_id)
        ->where('tweets.course_id',$course_id)
        ->where('tweets.status','Active')
        ->select('tweets.*','users.name','users.profile_image', 'users.user_type','users.user_id')->first();
        $reactions = TweetReaction::where('tweet_id', $tweet['tweet_id'])
        ->where('user_id', \Auth::user()->user_id )
        ->first();
        
       
        
        //return $reactions;
        $comments = Comments::with(['comment_reaction_content' => function($query)
        {
            $query->where('user_id', \Auth::user()->user_id);
        
        }])
        ->join('users','users.user_id','=','comments.user_id')
        ->where('comments.tweet_id',$tweet['tweet_id'])
        //->where('comments_reaction.user_id',\Auth::user()->user_id)
        ->select('comments.*','users.name','users.profile_image', 'users.user_type', 'users.user_id')
        ->orderBy('comments.comment_id','ASC')->limit(10)->get();
        $tweet_id = $tweet['tweet_id'];
        //return phpinfo();
      //return $comments;
      $current = Carbon::now();
      //$pre = Carbon::createFromDate();
      $difference=0;
      if(!empty($tweet))
      {
        $pre = Carbon::createFromFormat('Y-m-d H:i:s', $tweet->active_time);
        if($current->lt($pre))
        {
            $difference = $current->diffInSeconds($pre);
        }
      }
      
        return view('trainee.tweet', compact('tweet_id','topic_id','tweet','reactions','comments','difference'));
    }
}
