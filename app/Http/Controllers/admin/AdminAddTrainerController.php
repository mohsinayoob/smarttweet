<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Mail\EmailVerificationWithPassword;
use App\Models\Course\Category;
use App\Models\User;

use DB;
use Mail;
use Session;

class AdminAddTrainerController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'sector'=>'required'
            
        ]);
    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'user_type'=>'trainer',
            'sector'=>$data['sector'],
            'otp' => str_random(15),
            'is_active'=>1
        ]);
    }
    public function addTrainer(Request $request)
    {
        $data = $request->all();
        $password = str_random(8);
        $data['password'] = $password;
        $this->validator($data)->validate();

        //event(new Registered($user = $this->create($request->all())));

        DB::beginTransaction();
        try
        {
            $user = $this->create($data);
            // After creating the user send an email with the random token generated in the create method above
            $email = new EmailVerificationWithPassword(new User(['otp' => $user->otp, 'name' => $user->name, 'password' => $password]));

            Mail::to($user->email)->send($email);
            DB::commit();
            Session::flash('message', 'We have sent you a verification email!');
            return back()->with('update', 'Trainer Added Successfully');;
        }
        catch(Exception $e)
        {
            DB::rollback(); 
            return back()->with('error_messages', 'An Unknown Error Ocurred');;
        }
    }

    public function index()
    {
        $categories = Category::all();
        return view('admin.add-trainer', compact('categories'));
    }

}
