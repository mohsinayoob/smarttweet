<?php

return [

   

    'Dashboard' => 'لوحة القيادة',
    'Categories' => 'الاقسام',
    'CCourse' => 'إنشاء دورة',
    'MCourse' => 'إدارة الدورة',
    'AddT' => 'إضافة المدرب',
    'Trainer'=> 'مدرب',
    'Trainee'=> 'المتدرب',
    'Profile' => 'الملف الشخصي',
    'Noti'=> 'إخطارات',
    'Msg'=> 'رسائل',
    'Rep'=> 'تقارير',
    'STick'=> 'تذاكر الدعم الفني',
    'Logout'=> 'الخروج',
    'Admin'=>'مشرف',
    'MProfile'=> 'ملفي',
    'ViewTick'=>'عرض التذاكر',

];
