@extends('layouts.trainer_layout') @section('title', 'My Courses') @section('css-files')

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/list-scroll/list.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/stroll/css/stroll.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/css/component.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}"> @stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-header card">
					<div class="card-block">
						<h5 class="m-b-10">Manage Courses</h5>
					</div>
				</div>
				<div class="page-body">

					<div class="row">
						<div class="col-sm-12 filter-bar">
							<nav class="navbar navbar-light bg-faded m-b-30 p-10">
									<form method="get">
								<ul class="nav navbar-nav p-t-10">
									<li class="nav-item active">
										<a class="nav-link" href="#!">Filter:
											<span class="sr-only">(current)</span>
										</a>
									</li>
									<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle" href="#!" id="bydate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<i class="icofont icofont-clock-time"></i> By Date</a>
										<div class="dropdown-menu" aria-labelledby="bydate">
											<a class="dropdown-item" href="#!">Show all</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" onclick="searchdate('Today')">Today</a>
											<a class="dropdown-item" onclick="searchdate('Month')">This month</a>
											<a class="dropdown-item" onclick="searchdate('Year')">This year</a>
										</div>
									</li>

									<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle" href="#!" id="bystatus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<i class="icofont icofont-chart-histogram-alt"></i> By Status</a>
										<div class="dropdown-menu" aria-labelledby="bystatus">
											<a class="dropdown-item" >Show all</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" onclick="searchstatus('Active')">Active</a>
											<a class="dropdown-item" onclick="searchstatus('De-active')">De-active</a>
											
										</div>
									</li>

									
								</ul>
								<div class="nav-item nav-grid p-t-10">
										
									<div class="input-group">
										<input type="text" class="form-control" name='q' placeholder="Search here...">
										<button class="btn btn-success f-right m-l-10">
											Filter
										</button>
									</div>
								
								</div>
								
								<input type='hidden' name='date' id='searchdate'>
								<input type='hidden' name='status' id='searchstatus'>
							</form>
							</nav>

							<div class="card">
								<div class="card-header">
								</div>
								<div class="row card-block">
									<div class="col-md-12">
										@if(sizeof($courses) > 0) 
										<ul class="list-view">

											@foreach($courses as $course)
											<li>
												<div class="card list-view-media">
													<div class="card-header">
														<div class="dropdown-secondary dropdown f-left">
															<h6 class="d-inline-block">
																<a href="/trainer/topic/{{$course->course_id}}">{{$course->course_title}} </a>
															</h6>
															<div class="f-13 text-muted m-b-15">
																{{$course->created_at}}
															</div>
														</div>
														<div class="dropdown-secondary dropdown f-right">
															<button class="btn btn-primary btn-mini dropdown-toggle waves-effect waves-light" type="button" id="dropdown1" data-toggle="dropdown"
															 aria-haspopup="true" aria-expanded="false">{{$course->status}}</button>
															<div class="dropdown-menu" aria-labelledby="dropdown1" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
																
																<a class="dropdown-item waves-light waves-effect" href="/trainer/edit-course/{{$course->course_id}}">

																	<span class="point-marker bg-danger"></span>Edit Course</a>
																	<a class="dropdown-item waves-light waves-effect" onclick='deleteCourse({{$course->course_id}})'>

																		<span class="point-marker bg-danger"></span>Delete Course</a>
																@if($course->status == 'Active')
																<a class="dropdown-item waves-light waves-effect" href="/trainer/activate-course/{{$course->course_id}}/De-active">

																	<span class="point-marker bg-danger"></span>De-active</a>

																@else
																<a class="dropdown-item waves-light waves-effect" href="/trainer/activate-course/{{$course->course_id}}/Active">

																	<span class="point-marker bg-danger"></span>Active</a>
																@endif

															</div>

															<span class="f-left m-r-5 text-inverse">Status : </span>
														</div>
													</div>
													<div class="card-block">
														<div class="media">

															<div class="media-body">

																<p>
																	{{$course->course_description}}
																</p>

															</div>
														</div>
													</div>
												</div>
											</li>
											@endforeach

										</ul>
										@else
										<h3> You have no courses to Show</h3>
										
										@endif
										<nav  class="f-right">
											{{ $courses->appends(request()->query())->links("pagination::bootstrap-4") }}
													
										</nav>
									</div>
								</div>
							</div>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')
<script src="{{URL::asset('files/bower_components/stroll/js/stroll.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/list-scroll/list-custom.js')}}"></script>
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>

@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>



@endif 
<script>
		function searchdate(date)
		{
			document.getElementById("searchdate").setAttribute('value',date);
		}
		function searchstatus(status)
		{
			document.getElementById("searchstatus").setAttribute('value',status);
		}

		function deleteCourse(id){
			//alert(id);
			var href="/trainer/delete-course/"+ id;
			//$("#deletetopic").attr('href', href);
			swal({
				title: "Are you sure?",
				text: "This Course will be deleted",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false
			}, function() {
				//$('#deletetopic').trigger("click");
				window.location = href;
			});
		}
	</script>

@stop