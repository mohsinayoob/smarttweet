<?php

return [

   
    'manage_categories' => 'Manage Categories',
    'tickets' => 'Tickets',
    'empty' => 'There are currently no tickets.',
    'category' => 'Category',
    'title' => 'Title',
    'Status' => 'Status',
    'last_updated' => 'Last Updated',
    'actions' => 'Actions',
    'comment' => 'Comment',
    'close' => 'Close',
  
];
