<?php

return [

   

    'Dashboard' => 'Dashboard',
    'Categories' => 'Categories',
    'CCourse' => 'Create Course',
    'MCourse' => 'Manage Course',
    'AddT' => 'Add Trainer',
    'Trainer'=> 'Trainer',
    'Trainee'=> 'Trainee',
    'Profile' => 'Profile',
    'Noti'=> 'Notifications',
    'Msg'=> 'Messages',
    'Rep'=> 'Reports',
    'STick'=> 'Support Tickets',
    'Logout'=> 'Log Out',
    'MProfile'=> 'My Profile',
    'ViewTick'=>'View Tickets',
    

];
