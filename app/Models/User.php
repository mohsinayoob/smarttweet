<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Notifications\Notifiable;

class User extends Model implements AuthenticatableContract,
AuthorizableContract,
CanResetPasswordContract
{
    //
    
    use Notifiable, Authenticatable, Authorizable, CanResetPassword;
    protected $table = 'users';
    protected $primaryKey = 'user_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email','password','name','user_type','last_login_ip','last_login_time',"is_active",'is_verified','last_password','otp', 'opt_creation_time','created_at','updated_at','sector'];

    protected $hidden = [
        'password', 'remember_token',
    ];
    /*public function getEmailForPasswordReset()
    {

    }
    public function sendPasswordResetNotification($token)
    {

    }*/
    public function verified()
    {
        $this->is_verified = 1;
        $this->is_active = 1;
        $this->otp = null;
        $this->save();
    }
}
