<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Jobs\TweetReactionNotificationJob;
use App\Models\Tweet\TweetReaction;
use App\Models\Tweet\Tweets;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
//use Illuminate\Support\Facades\Request;

class AdminTweetReactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');

    }

    public function createTweetReaction(Request $request)
    {
        //return $course_id;
        $data = $request->all();
        DB::beginTransaction();
        try {
            $existreaction = TweetReaction::where('user_id', \Auth::user()->user_id)
                ->where('tweet_id', $data["tweet_id"])
                ->first();
            $tweet_to_update = Tweets::where('tweet_id', $data["tweet_id"])->first();
            $reaction_content = $data["reaction_content"];
            $total = "total_$reaction_content" . "s";
            if (!empty($existreaction)) {

                if ($data["reaction_content"] == "favorite") {
                    if ($existreaction->reaction_content_favorite == $data["reaction_content"]) {
                        //$update_reaction_tweet = TweetReaction::where('reaction_id', $existreaction->reaction_id);
                        $existreaction->reaction_content_favorite = null;
                        $existreaction->save();
                        $tweet_to_update[$total] -= 1;
                        $tweet_to_update->save();
                        DB::commit();
                        return response()->json([
                            'data' => [
                                'success' => 'true',
                                'status' => 'deleted',
                                'param' => $data["reaction_content"],
                                'total_likes' => $tweet_to_update['total_likes'],
                                'total_favorites' => $tweet_to_update['total_favorites'],
                                'total_dislikes' => $tweet_to_update['total_dislikes'],
                                'total_comments' => $tweet_to_update['total_comments'],
                            ],
                        ]);
                    }
                    $tweet_to_update[$total] += 1;
                    $previous = "favorite";
                    $total = "total_$previous" . "s";

                    $existreaction->reaction_content_favorite = $data["reaction_content"];
                    $existreaction->save($data);
                    $tweet_to_update->save();
                    DB::commit();
                    return response()->json([
                        'data' => [

                            'success' => 'true',
                            'status' => 'updated',
                            'param' => $data["reaction_content"],
                            'previous_param' => "",
                            'total_likes' => $tweet_to_update['total_likes'],
                            'total_favorites' => $tweet_to_update['total_favorites'],
                            'total_dislikes' => $tweet_to_update['total_dislikes'],
                            'total_comments' => $tweet_to_update['total_comments'],
                        ],
                    ]);

                }

                if ($existreaction->reaction_content == $data["reaction_content"]) {
                    //TweetReaction::where('reaction_id', $existreaction->reaction_id)->delete();
                    $existreaction->reaction_content = null;
                    $existreaction->save();
                    $tweet_to_update[$total] -= 1;
                    $tweet_to_update->save();
                    DB::commit();
                    return response()->json([
                        'data' => [
                            'success' => 'true',
                            'status' => 'deleted',
                            'param' => $data["reaction_content"],
                            'total_likes' => $tweet_to_update['total_likes'],
                            'total_favorites' => $tweet_to_update['total_favorites'],
                            'total_dislikes' => $tweet_to_update['total_dislikes'],
                            'total_comments' => $tweet_to_update['total_comments'],
                        ],
                    ]);
                }
                $tweet_to_update[$total] += 1;
                $previous = $existreaction->reaction_content;
                if ($previous != null) {
                    $total = "total_$previous" . "s";
                    $tweet_to_update[$total] -= 1;
                }
                $existreaction->reaction_content = $data["reaction_content"];
                $existreaction->save($data);
                $tweet_to_update->save();
                DB::commit();
                return response()->json([
                    'data' => [

                        'success' => 'true',
                        'status' => 'updated',
                        'param' => $data["reaction_content"],
                        'previous_param' => $previous,
                        'total_likes' => $tweet_to_update['total_likes'],
                        'total_favorites' => $tweet_to_update['total_favorites'],
                        'total_dislikes' => $tweet_to_update['total_dislikes'],
                        'total_comments' => $tweet_to_update['total_comments'],
                    ],
                ]);
            } else {

                if ($data["reaction_content"] == "favorite") {
                    $data["status"] = 'Active';
                    $data["user_id"] = \Auth::user()->user_id;
                    $data["reaction_content"] = null;
                    $data["reaction_content_favorite"] = "favorite";
                    //return $data;

                    $tweet_reaction = TweetReaction::create($data);
                    $tweet_to_update[$total] += 1;
                    $tweet_to_update->save();
                    DB::commit();
                    $notification_job = (new TweetReactionNotificationJob($tweet_reaction))->delay(13);
                    $this->dispatch($notification_job);
                    return response()->json([
                        'data' => [
                            'success' => 'true',
                            'status' => 'inserted',
                            'param' => $data["reaction_content_favorite"],
                            'total_likes' => $tweet_to_update['total_likes'],
                            'total_favorites' => $tweet_to_update['total_favorites'],
                            'total_dislikes' => $tweet_to_update['total_dislikes'],
                            'total_comments' => $tweet_to_update['total_comments'],
                        ],
                    ]);
                }
                $data["status"] = 'Active';
                $data["user_id"] = \Auth::user()->user_id;
                //return $data;

                $tweet_reaction = TweetReaction::create($data);
                $tweet_to_update[$total] += 1;
                $tweet_to_update->save();
                DB::commit();
                $notification_job = (new TweetReactionNotificationJob($tweet_reaction))->delay(13);
                $this->dispatch($notification_job);
                return response()->json([
                    'data' => [
                        'success' => 'true',
                        'status' => 'inserted',
                        'param' => $data["reaction_content"],
                        'total_likes' => $tweet_to_update['total_likes'],
                        'total_favorites' => $tweet_to_update['total_favorites'],
                        'total_dislikes' => $tweet_to_update['total_dislikes'],
                        'total_comments' => $tweet_to_update['total_comments'],
                    ],
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'data' => [
                    'success' => 'false',
                ],
            ]);
        }

    }
    public function getReactionUsers($tweet_id, $reaction_content)
    {
        if ($reaction_content == "favorite") {
            $reactionUsers = TweetReaction::join('users', 'users.user_id', '=', 'tweet_reaction.user_id')
                ->where('tweet_reaction.tweet_id', $tweet_id)
                ->where('tweet_reaction.reaction_content_favorite', $reaction_content)
                ->select('users.name', 'users.profile_image', 'users.user_type')->get();
            $tweet = Tweets::where('tweet_id', $tweet_id)
                ->select('total_likes', 'total_favorites', 'total_dislikes', 'total_comments')->first();

            //return $reactionUsers;
            return response()->json([
                'success' => 'true',
                'data' => $reactionUsers,
                'total_likes' => $tweet['total_likes'],
                'total_favorites' => $tweet['total_favorites'],
                'total_dislikes' => $tweet['total_dislikes'],
                'total_comments' => $tweet['total_comments'],
            ]);

        }
        $reactionUsers = TweetReaction::join('users', 'users.user_id', '=', 'tweet_reaction.user_id')
            ->where('tweet_reaction.tweet_id', $tweet_id)
            ->where('tweet_reaction.reaction_content', $reaction_content)
            ->select('users.name', 'users.profile_image', 'users.user_type')->get();
        $tweet = Tweets::where('tweet_id', $tweet_id)
            ->select('total_likes', 'total_favorites', 'total_dislikes', 'total_comments')->first();

        //return $reactionUsers;
        return response()->json([
            'success' => 'true',
            'data' => $reactionUsers,
            'total_likes' => $tweet['total_likes'],
            'total_favorites' => $tweet['total_favorites'],
            'total_dislikes' => $tweet['total_dislikes'],
            'total_comments' => $tweet['total_comments'],
        ]);
    }
}
