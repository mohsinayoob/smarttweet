@extends('layouts.'.\Auth::user()->user_type.'_layout') 
@section('title', 'Open Ticket')
 @section('css-files')

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}">
<style>
    .error{
        border-color: red;
        color: red;
    }
</style>
 @stop 
 @section('body_content')
 <div class="pcoded-content">
        <div class="pcoded-inner-content">
    
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">Tickets</h5>
                        </div>
    
                    </div>
                    <div class="page-body">
                    <div class="">
                        <div class="col-md-12 card">
                            <div class="panel panel-default">
                                <div class="panel-heading">Open New Ticket</div>
                
                                <div class="panel-body">
                                    
                
                                    <form class="form-horizontal  center" role="form" method="POST" action="{{ url(\Auth::user()->user_type.'/ticket/new_ticket') }}">
                                        {!! csrf_field() !!}
                
                                        <div class="form-group{{ $errors->has('title') ? ' error' : '' }}">
                                            <label for="title" class="col-md-4 control-label">Title</label>
                
                                            <div class="col-md-11">
                                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}">
                
                                                @if ($errors->has('title'))
                                                    <span class="help-block">
                                                        <strong class="error">{{ $errors->first('title') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                
                                        <div class="form-group{{ $errors->has('category') ? ' error' : '' }}">
                                            <label for="category" class="col-md-4 control-label">Category</label>
                
                                            <div class="col-md-11">
                                                <select id="category" type="category" class="form-control" name="category">
                                                    <option value="">Select Category</option>
                                                    @foreach ($categories as $category)
                                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                
                                                @if ($errors->has('category'))
                                                    <span class="help-block">
                                                        <strong class="error">{{ $errors->first('category') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                
                                        <div class="form-group{{ $errors->has('priority') ? ' error' : '' }}">
                                            <label for="priority" class="col-md-4 control-label">Priority</label>
                
                                            <div class="col-md-11">
                                                <select id="priority" type="" class="form-control" name="priority">
                                                    <option value="">Select Priority</option>
                                                    <option value="low">Low</option>
                                                    <option value="medium">Medium</option>
                                                    <option value="high">High</option>
                                                </select>
                
                                                @if ($errors->has('priority'))
                                                    <span class="help-block">
                                                        <strong class="error">{{ $errors->first('priority') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                
                                        <div class="form-group{{ $errors->has('message') ? ' error' : '' }}">
                                            <label for="message" class="col-md-4 control-label">Message</label>
                
                                            <div class="col-md-11">
                                                <textarea rows="10" id="message" class="form-control" name="message"></textarea>
                
                                                @if ($errors->has('message'))
                                                    <span class="help-block">
                                                        <strong class="error">{{ $errors->first('message') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-btn fa-ticket"></i> Open Ticket
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
			</div>
		</div>
	</div>
</div>

@stop 
@section('javascript-files')

<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/tinymce/tinymce.min.js?apiKey=jdw0qpvvruo7046n3j4g8o122piap638faeuii9s0rlk2fy3')}}"></script>
<style>
		.social-msg a.active i{
			color:#4099ff!important;
		}
		.social-msg a.active span{
			color:#4099ff!important;
		}
		.mce-top-part {
			position: absolute;
			bottom: 0px;
		}
		.image-upload{
			position: relative;
			left: 81px;
			bottom: 32px;
		}
		.max-width-400{
			max-width:400px;
		}
</style>

<script>
var editor =tinymce.init({ 
				selector:'textarea',
				mode : "textareas",
				menubar:false,
				statusbar: false,
				plugins:'smileys, emoticons',
				toolbar1: 'smileys | emoticons',
				theme_advanced_toolbar_location : "bottom",
				auto_convert_smileys: true,
				setup: function (editor) {
					editor.on('change', function () {
						tinymce.triggerSave();
					});
				}
				
			});
</script>
@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif 

@stop