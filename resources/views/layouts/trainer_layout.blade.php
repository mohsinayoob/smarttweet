<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">


<head>
	<title>@yield('title') </title>


	<!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')}}"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js')}}/1.4.2/respond.min.js')}}"></script>
      <![endif]-->

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="Smart Tweet"/>
	<meta name="keywords" content="Smart Tweet"/>
	<meta name="author" content="Smart Tweet" />

	<link rel="icon" href="{{URL::asset('files/assets/images/favicon.ico')}}" type="image/x-icon">

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/icofont/css/icofont.css')}}">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/themify-icons/themify-icons.css')}}">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/font-awesome/css/font-awesome.min.css')}}">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/css/jquery.mCustomScrollbar.css')}}">

	<link rel="stylesheet" href="{{URL::asset('files/assets/pages/chart/radial/css/radial.css')}}" type="text/css" media="all">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/pnotify/css/pnotify.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/pnotify/css/pnotify.brighttheme.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/pnotify/css/pnotify.buttons.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/pnotify/css/pnotify.history.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/pnotify/css/pnotify.mobile.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/pnotify/notify.css')}}">
	@yield('css-files')
</head>

<body >

	<div class="theme-loader">
		<div class="loader-track">
			<div class="loader-bar"></div>
		</div>
	</div>

	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<nav class="navbar header-navbar pcoded-header">
				<div class="navbar-wrapper">
					<div class="navbar-logo">
						<a class="mobile-menu" id="mobile-collapse" href="#!">
							<i class="ti-menu"></i>
						</a>
						<div class="mobile-search">
							<div class="header-search">
								<div class="main-search morphsearch-search">
									<div class="input-group">
										<span class="input-group-addon search-close">
											<i class="ti-close"></i>
										</span>
										<input type="text" class="form-control" placeholder="Enter Keyword">
										<span class="input-group-addon search-btn">
											<i class="ti-search"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
						<a href="/">
							<img class="img-fluid" src="{{URL::asset('files/assets/images/logo.png')}}" alt="Theme-Logo" />
						</a>
						<a class="mobile-options">
							<i class="ti-more"></i>
						</a>
					</div>
					<div class="navbar-container container-fluid">
						<ul class="nav-left">
							<li>
								<div class="sidebar_toggle">
									<a href="javascript:void(0)">
										<i class="ti-menu"></i>
									</a>
								</div>
							</li>
							<li>
								<a href="#!" onclick="javascript:toggleFullScreen()">
									<i class="ti-fullscreen"></i>
								</a>
							</li>
						</ul>
						<ul class="nav-right">
							<li class=" header-notification header-notifications">
								<a href="#!" >
									<i class="ti-bell"></i>
									<span data-count="0" class="notification-badge"></span>
								</a>
								<ul class="show-notification notification-dropdown" style="max-height: 350px;overflow-y: scroll;">
										<li>
											<a href="/trainer/notifications" style="padding:0px;">
											<label class="label label-danger f-right">View All</label>
											<h6>Notifications</h6>
											</a>
										</li>
									</ul>
				  
							  </li>
							<li class="">
								<a href="/trainer/chat" class="displayChatbox">
									<i class="ti-comments"></i>
								</a>
							</li>
							<li class="user-profile header-notification">
								<a href="#!">
									<img src="{{ \Auth::user() && \Auth::user()->profile_image ? URL::asset('trainer/profile_image/'.\Auth::user()->profile_image): URL::asset('files/assets/images/social/profile.jpg')}}" class="img-radius" alt="User-Profile-Image">
									<span>{{\Auth::user() && \Auth::user()->name ? \Auth::user()->name : ""}}</span>
									<i class="ti-angle-down"></i>
								</a>
								<ul class="show-notification profile-notification">
									
									<li>
										<a href="/trainer/profile">
											<i class="ti-user"></i> My Profile
										</a>
									</li>
									<li>
										<a href="/trainer/chat">
											<i class="ti-email"></i> My Messages
										</a>
									</li>
									
									<li>
										<a href="{{ route('logout') }}"
										onclick="event.preventDefault();
												 document.getElementById('logout-form').submit();">
											<i class="ti-layout-sidebar-left"></i> Logout
										</a>
										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
												{{ csrf_field() }}
										</form>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- 
			<div id="sidebar" class="users p-chat-user showChat">
				<div class="had-container">
					<div class="card card_main p-fixed users-main">
						<div class="user-box">
							<div class="chat-search-box">
								<a class="back_friendlist">
									<i class="fa fa-chevron-left"></i>
								</a>
								<div class="right-icon-control">
									<input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends">
									<div class="form-icon">
										<i class="fa fa-search"></i>
									</div>
								</div>
							</div>
							<div class="main-friend-list">
								<div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left"
								 title="Josephin Doe">
									<a class="media-left" href="#!">
										<img class="media-object img-radius img-radius" src="{{URL::asset('files/assets/images/avatar-3.jpg')}}" alt="Generic placeholder image ">
										<div class="live-status bg-success"></div>
									</a>
									<div class="media-body">
										<div class="f-13 chat-header">Josephin Doe</div>
									</div>
								</div>
								<div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe" data-toggle="tooltip" data-placement="left"
								 title="Lary Doe">
									<a class="media-left" href="#!">
										<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="Generic placeholder image">
										<div class="live-status bg-success"></div>
									</a>
									<div class="media-body">
										<div class="f-13 chat-header">Lary Doe</div>
									</div>
								</div>
								<div class="media userlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left"
								 title="Alice">
									<a class="media-left" href="#!">
										<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-4.jpg')}}" alt="Generic placeholder image">
										<div class="live-status bg-success"></div>
									</a>
									<div class="media-body">
										<div class="f-13 chat-header">Alice</div>
									</div>
								</div>
								<div class="media userlist-box" data-id="4" data-status="online" data-username="Alia" data-toggle="tooltip" data-placement="left"
								 title="Alia">
									<a class="media-left" href="#!">
										<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-3.jpg')}}" alt="Generic placeholder image">
										<div class="live-status bg-success"></div>
									</a>
									<div class="media-body">
										<div class="f-13 chat-header">Alia</div>
									</div>
								</div>
								<div class="media userlist-box" data-id="5" data-status="online" data-username="Suzen" data-toggle="tooltip" data-placement="left"
								 title="Suzen">
									<a class="media-left" href="#!">
										<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="Generic placeholder image">
										<div class="live-status bg-success"></div>
									</a>
									<div class="media-body">
										<div class="f-13 chat-header">Suzen</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="showChat_inner">
				<div class="media chat-inner-header">
					<a class="back_chatBox">
						<i class="fa fa-chevron-left"></i> Josephin Doe
					</a>
				</div>
				<div class="media chat-messages">
					<a class="media-left photo-table" href="#!">
						<img class="media-object img-radius img-radius m-t-5" src="{{URL::asset('files/assets/images/avatar-3.jpg')}}" alt="Generic placeholder image">
					</a>
					<div class="media-body chat-menu-content">
						<div class="">
							<p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
							<p class="chat-time">8:20 a.m.</p>
						</div>
					</div>
				</div>
				<div class="media chat-messages">
					<div class="media-body chat-menu-reply">
						<div class="">
							<p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
							<p class="chat-time">8:20 a.m.</p>
						</div>
					</div>
					<div class="media-right photo-table">
						<a href="#!">
							<img class="media-object img-radius img-radius m-t-5" src="{{URL::asset('files/assets/images/avatar-4.jpg')}}" alt="Generic placeholder image">
						</a>
					</div>
				</div>
				<div class="chat-reply-box p-b-20">
					<div class="right-icon-control">
						<input type="text" class="form-control search-text" placeholder="Share Your Thoughts">
						<div class="form-icon">
							<i class="fa fa-paper-plane"></i>
						</div>
					</div>
				</div>
			</div>
		-->
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<nav class="pcoded-navbar" >
						<div class="sidebar_toggle">
							<a href="#">
								<i class="icon-close icons"></i>
							</a>
						</div>
						<div class="pcoded-inner-navbar main-menu">
							<div class="pcoded-navigation-label"></div>
							<ul class="pcoded-item pcoded-left-item">
                            <!--
                                <li class="user-profile header-notification">
								    <a href="/profile">
									    <img src="{{URL::asset('files/assets/images/avatar-4.jpg')}}" class="img-radius" alt="User-Profile-Image">
									    <span>John Doe</span>
									</a>
								</li> -->
								
								<li class="{{ Request::is('trainer/create-course') ? 'active' : '' }}">
									<a href="/trainer/create-course">
										<span class="pcoded-micon">
											<i class="ti-ruler-pencil"></i>
											<b>C</b>
										</span>
										<span class="pcoded-mtext">Create Course</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								
								<li class="{{ Request::is('trainer/manage-course') || Request::is('trainer/topic*') ||  Request::is('trainer/tweets-list*') ||  Request::is('trainer/tweet*')
								  ? 'active' : '' }}">
									<a href="/trainer/manage-course">
										<span class="pcoded-micon">
											<i class="ti-pencil-alt"></i>
											<b>C</b>
										</span>
										<span class="pcoded-mtext">Manage Courses</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								
								<li class="{{ Request::is('trainer/profile') || Request::is('trainer/change-password') ? 'active' : '' }}">
									<a href="/trainer/profile">
										<span class="pcoded-micon">
											<i class="ti-user"></i>
											<b>P</b>
										</span>
										<span class="pcoded-mtext">Profile</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>

								<li class="{{ Request::is('trainer/enrollment') ? 'active' : '' }}">
									<a href="/trainer/enrollment">
										<span class="pcoded-micon">
											<i class="ti-bell"></i>
											
											<b>N</b>
										</span>
										<span class="pcoded-mtext">Enrollments</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('trainer/notifications') ? 'active' : '' }}">
									<a href="/trainer/notifications">
										<span class="pcoded-micon">
											<i class="ti-bell"></i>
											
											<b>N</b>
										</span>
										<span class="pcoded-mtext">Notifications</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('trainer/chat') ? 'active' : '' }}">
									<a href="/trainer/chat">
										<span class="pcoded-micon">
											<i class="ti-comments"></i>
											<b>N</b>
										</span>
										<span class="pcoded-mtext">Messages</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('trainer/favorite-list') ? 'active' : '' }}">
									<a href="/trainer/favorite-list">
										<span class="pcoded-micon">
											<i class="ti-home"></i>
											<b>C</b>
										</span>
										<span class="pcoded-mtext">Favorite list</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('trainer/reports') || Request::is('trainer/course-reports') || Request::is('trainer/user-reports') ? 'active' : '' }}">
									<a href="/trainer/reports">
										<span class="pcoded-micon">
											<i class="ti-bar-chart"></i>
											<b>R</b>
										</span>
										<span class="pcoded-mtext">Reports</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('trainer/ticket*') ? 'active' : '' }}">
									<a href="/trainer/tickets">
										<span class="pcoded-micon">
											<i class="ti-support"></i>
											<b>N</b>
										</span>
										<span class="pcoded-mtext">Contact Support</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="">
									<a href="{{ route('logout') }}"
									onclick="event.preventDefault();
											 document.getElementById('logout-form').submit();">
										<span class="pcoded-micon">
											<i class="ti-layout-sidebar-left"></i>
											<b>L</b>
										</span>
										<span class="pcoded-mtext">Log Out</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
							</ul>
						</div>
					</nav>

					@yield('body_content')
				</div>
			</div>
		</div>
	</div>
	<!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="{{URL::asset('files/assets/images/browser/chrome.png')}}" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="{{URL::asset('files/assets/images/browser/firefox.png')}}" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="{{URL::asset('files/assets/images/browser/opera.png')}}" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="{{URL::asset('files/assets/images/browser/safari.png')}}" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="{{URL::asset('files/assets/images/browser/ie.png')}}" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->


	<script src="{{URL::asset('files/bower_components/jquery/js/jquery.min.js')}}"></script>
	<script src="{{URL::asset('files/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
	<script src="{{URL::asset('files/bower_components/popper.js/js/popper.min.js')}}"></script>
	<script src="{{URL::asset('files/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{URL::asset('files/assets/pages/widget/excanvas.js')}}"></script>

	<script src="{{URL::asset('files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>

	<script src="{{URL::asset('files/bower_components/modernizr/js/modernizr.js')}}"></script>

	<script src="{{URL::asset('files/assets/js/SmoothScroll.js')}}"></script>
	<script src="{{URL::asset('files/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>

	
	<script src="{{URL::asset('files/assets/js/pcoded.min.js')}}"></script>
    <script src="{{URL::asset('files/assets/js/dark-light/vertical-layout.min.js')}}"></script>
	<!-- 
		for RTL include this
		<script src="{{URL::asset('files/assets/js/dark-light/menu/menu-rtl.js')}}"></script>
		
	-->
	
	<script src="{{URL::asset('files/assets/js/script.js')}}"></script>
	<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.desktop.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.buttons.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.confirm.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.callbacks.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.animate.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.history.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.mobile.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.nonblock.js')}}"></script>
    <script src="//js.pusher.com/3.1/pusher.min.js"></script>
    

	
	<script type="text/javascript">
		
		var notificationsWrapper   = $('.header-notifications');
		var notificationsToggle    = notificationsWrapper.find('a[data-toggle]');
		var notificationsCountElem = notificationsToggle.find('span[data-count]');
		var notificationsCount     = parseInt(notificationsCountElem.data('count'));
		var notifications          = notificationsWrapper.find('ul.notification-dropdown');
  
		if (notificationsCount <= 0) {
		  notificationsWrapper.hide();
		}
  
		// Enable pusher logging - don't include this in production
		// Pusher.logToConsole = true;
  
		var pusher = new Pusher('8c4d1e032c2c25bf925f', {
		  encrypted: true
		});
		Pusher.logToConsole = true;
		// Subscribe to the channel we specified in our Laravel Event
		var channel = pusher.subscribe('{{\Auth::user()->user_id}}');
  console.log(pusher);
		// Bind a function to a Event (the full Laravel class)
		channel.bind('App\\Events\\NotificationPosted', function(data) {
			console.log(data);
			var user_image = `/`+data.sender.user_type+`/profile_image/`+data.sender.profile_image;
			var url = data.url+"?read="+data.notification_id;
			addNotificationToHTML(user_image, url, data.sender.name, data.message, data.created_at);
			var path = window.location.pathname;
			var pathArray = path.split( '/' );
			var loc = pathArray[1]+"/"+pathArray[2];
			if(data.notification_type=="Enrollment")
			{
				//console.log("Enrollment Happened");
				EnrollmentOccuredNotification(data.sender.name, data.message,data)
			}
			else{
				if(data.notification_type=="Comment" && loc=="trainer/tweet")
				{ 
					addLiveComments(data);
				}

				notifyMe(user_image, url, data.sender.name, data.message, data.created_at);
			}
		  
			
		});
		console.log(window.location.pathname);
		//////
		function addNotificationToHTML(image, url, name, message, created_at)
		{
			var existingNotifications = notifications.html();
			var newNotificationHtml = `
			<li>
				
				  <div class="media">
					  <img class="d-flex align-self-center img-radius" src="`+image+`" alt="Generic placeholder image">
					  <div class="media-body">
						  <a href="`+url+`" style="padding:0px;">
							  <h5 class="notification-user">`+name+`</h5>
							  <p class="notification-msg">`+message+`</p>
						  </a>
						  <span class="notification-time">`+created_at+`</span>
					  </div>
				  </div>
			  </li>
			  
			`;
			notifications.append(newNotificationHtml);
			//notificationsCount += 1;
			//notificationsCountElem.attr('data-count', notificationsCount);
			$('.notification-badge').addClass('badge bg-c-pink');
			//notificationsWrapper.find('.notif-count').text(notificationsCount);
			//notificationsWrapper.show();
		}

		$.get('/trainer/get-notifications', function (data) {
			//addNotifications(data, "#notifications");
			onLoadNotification(data);
			console.log(data);
		});
		
		function onLoadNotification(data)
		{
			data.forEach(function(entry) {
				
				var internalData = JSON.parse(entry.data);
				//internalData = internalData.data;
				var url = internalData.data.url+"?read="+entry.id;
				var user_image = `/`+internalData.data.user.user_type+`/profile_image/`+internalData.data.user.profile_image;
				console.log(internalData.data);
				var message = internalData.data.message;
				addNotificationToHTML(user_image, url, internalData.data.user.name, message, entry.created_at)
				//
			});
			
			//var
		}


		function EnrollmentOccuredNotification(name, message, data){
		
		swal({
            title: "Enrollment Request",
            text: name+" "+message,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
			cancelButtonText:"Cancel",
            confirmButtonText: "Approve",
            closeOnConfirm: false
        }, function() {
			var enrollment_id= data.data.enrollment_id;
            $.ajax({
				  
				  type:'POST',
				  url:'/trainer/updateEnrollment',
				  headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
				  data: { "enrollment_id" : enrollment_id },
				  success: function(data){
					if(data.data.success){
						swal("Success!", "Enrollment status Updated Successfully", "success");
					}else{
						swal("Error!", "Error in Updating Enrollment status", "error");
					}
				  }
			  });
			  swal.close();
        });
	}
		
	  </script>

	  <script>
		// request permission on page load
document.addEventListener('DOMContentLoaded', function () {
	if (!Notification) {
	  alert('Desktop notifications not available in your browser. Try Chromium.'); 
	  return;
	}
  
	if (Notification.permission !== "granted")
	 Notification.requestPermission();
	
  });
  
  function notifyMe(image, url, name, message, created_at) {
	if (Notification.permission !== "granted")
	{
		Notification.requestPermission();
		PNotify.desktop.permission();
		(new PNotify({
			title: 'Smart Tweet Notification',
			type: 'success',
			text: message,
			desktop: {
				desktop: true,
				icon: image
			}
		})).get().click(function(e) {
			if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
			window.location = url;
		});
	}
	else {
	  var notification = new Notification('Smart Tweet Notification', {
		icon: image,
		body: name+" "+message,
	  });
  
	  notification.onclick = function () {
		window.open(url);      
	  };
	  (new PNotify({
		title: 'Smart Tweet Notification',
		type: 'success',
		text: name+" "+message,
		icon: image,
		type: 'success'
	})).get().click(function(e) {
		if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
		window.location = url;
	});
	}
  
  }
	  </script>
	@yield('javascript-files')
</body>

</html>