@extends('layouts.trainer_layout') @section('title', 'Tweet') @section('css-files')

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/simple-line-icons/css/simple-line-icons.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/icofont/css/icofont.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/themify-icons/themify-icons.css')}}"> 
<style>
		.social-msg a.active i{
			color:#4099ff!important;
		}
		.social-msg a.active span{
			color:#4099ff!important;
		}
		.mce-top-part {
			position: absolute;
			bottom: 0px;
		}
		.image-upload{
			position: relative;
			left: 81px;
			bottom: 32px;
		}
		.max-width-400{
			max-width:400px;
		}
	</style>
@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-header card">
					<div class="card-block">
						<h5 class="m-b-10">Favorite Comments</h5>
					</div>
				</div>
				<div class="page-body">
					<div class="modal fade modal-flex" id="Modal-tab" tabindex="-1" role="dialog">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-body">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<ul class="nav nav-tabs" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">
												<i class="fa fa-heart"></i>
											</a>
										</li>

									</ul>
									<div class="tab-content modal-body">
										<div class="tab-pane active" id="tab-home" role="tabpanel">
											<div class="card-block user-box" id="favorite-modal-data">
												<div class="fa fa-spinner fa-spin fa-lg"></div>
												
											</div>

										</div>
										
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal fade modal-flex" id="Modal-tab-like" tabindex="-1" role="dialog">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-body">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<ul class="nav nav-tabs" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">
												<i class="fa fa-thumbs-up"></i>
											</a>
										</li>

									</ul>
									<div class="tab-content modal-body">
										<div class="tab-pane active" id="tab-home" role="tabpanel">
											<div class="card-block user-box" id="like-modal-data">
											
											</div>

										</div>
										
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal fade modal-flex" id="Modal-tab-dislike" tabindex="-1" role="dialog">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-body">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<ul class="nav nav-tabs" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">
												<i class="fa fa-thumbs-down"></i>
											</a>
										</li>

									</ul>
									<div class="tab-content modal-body">
										<div class="tab-pane active" id="tab-home" role="tabpanel">
											<div class="card-block user-box" id="dislike-modal-data">
												
											</div>

										</div>
										
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="md-overlay"></div>
					<div class="row">
						<div class="col-lg-12">
							<div class="row">

								<div class="col-md-12">

									<div>
										<div class="card bg-white p-relative">

											<div class="card-block user-box">
												@foreach($comments as $comment)
												<div class="media">
														<a class="media-left" href="/public-profile/{{$comment->user_id}}">
															<img class="media-object img-radius m-r-20" src="{{URL::asset($comment->user_type.'/profile_image/'.$comment->profile_image)}}" alt="">
														</a>
														<div class="media-body b-b-theme social-client-description">
															<div class="chat-header">{{$comment->name}}
																<span class="text-muted">{{$comment->created_at}}</span>
																<div class="f-right">
																	<a href="/trainer/tweet/{{$comment->topic_id}}/{{$comment->tweet_id}}">View Tweet</a>
																</div>
															</div>
															<p class="text-muted">{!! base64_decode($comment->comment_description) !!}</p>
															@if($comment->comment_image)
																<img class="max-width-400 col-sm-12" src="{{URL::asset('comment_content/'.$comment->comment_image)}}" alt="">
															@endif
															<div class=" social-msg m-2 ">
																	<a class="waves-effect {{$comment->comment_reaction_content && $comment->comment_reaction_content->comment_reaction_content=='favorite' ? 'active' :''}}" href="#!" id="favorite-comment-color-{{$comment->comment_id}}" >
																			<i class="icofont icofont-heart-alt text-muted">
																			</i>
																			<span class="p-l-0 m-r-0">Favorite </span>
																		</a>
																		
																		<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab" onclick='getCommentReactionUsers("{{$comment->comment_id}}","favorite")'>
																			<span class="b-r-theme" id="total_comment_favorites_{{$comment->comment_id}}">( {{$comment->total_favorites}} )</span>
																		</a>
						
																		<a class="waves-effect {{$comment->comment_reaction_content && $comment->comment_reaction_content->comment_reaction_content=='like' ? 'active' :''}}" href="#!" id="like-comment-color-{{$comment->comment_id}}" >
																			<i class="icofont icofont-thumbs-up text-muted">
																			</i>
																			<span class="p-l-0 m-r-0">Likes</span>
																		</a>
																		<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab-like" onclick='getCommentReactionUsers("{{$comment->comment_id}}","like")'>
																			<span class="b-r-theme" id="total_comment_likes_{{$comment->comment_id}}">( {{$comment->total_likes}} )</span>
																		</a>
						
																		<a class="waves-effect {{$comment->comment_reaction_content && $comment->comment_reaction_content->comment_reaction_content=='dislike' ? 'active' :''}}" id="dislike-comment-color-{{$comment->comment_id}}" >
																			<i class="icofont icofont-thumbs-down text-muted">
																			</i>
																			<span class="p-l-0 m-r-0" >Dislike</span>
																		</a>
																		<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab-dislike" onclick='getCommentReactionUsers("{{$comment->comment_id}}","dislike")'>
																			<span class="b-r-theme" id="total_comment_dislikes_{{$comment->comment_id}}">( {{$comment->total_dislikes}} )</span>
																		</a>
																</div>
														</div>
														
													</div>
												@endforeach
											</div>

										</div>
									</div>


								</div>

							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="card">
										<div class="card-block">
											<nav class="f-right">
												{{ $comments->appends(request()->query())->links("pagination::bootstrap-4") }}
											</nav>
										</div>
									</div>
								</div>
							</div>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function getCommentReactionUsers(id, reaction){
		$('#'+reaction+'-modal-data').html('<div class="fa fa-spinner fa-spin fa-lg"></div>');
		$.ajax({
				  
			type:'GET',
			url:'/trainer/comment-reaction-users/'+id+'/'+reaction,
			headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
			
			success: function(data){
			  if(data.success){
				  var appendid = $('#like-modal-data');
				if(reaction == 'favorite'){
					appendid = $('#favorite-modal-data');

				}else if(reaction == 'dislike'){
					appendid = $('#dislike-modal-data');

				}
				var reactionUsers = data.data;
				var userhtml = '';
				if(reactionUsers.length > 0){
					 reactionUsers.forEach(function(user){
						userhtml+= '<div class="media m-b-10">'+
								'<a class="media-left" href="#!">';
						
						userhtml+=	'<img class="media-object img-radius" src="/'+user.user_type+'/profile_image/'+user.profile_image+'" alt="Generic placeholder image" data-toggle="tooltip" data-placement="top" title="user image">'+
									'</a>'+
									'<div class="media-body">';
						userhtml+='<div class="chat-header">'+user.name+'</div>'+
									'<div class="text-muted social-designation">'+user.user_type+'</div>'+
									'</div></div>';
					  //alert(user.name);
				});
				appendid.html(userhtml);
				
			}else{
				appendid.html("no data available");
			}
			$('#total_comment_likes'+id).text("( "+data.total_likes+" )");
			$('#total_comment_favorites'+id).text("( "+data.total_favorites+" )");
			$('#total_comment_dislikes'+id).text("( "+data.total_dislikes+" )");
			
				
		}else{
			swal("Error!", "Unable to get reaction. Try Again!", "error");
		}
	}
	});

}
</script>
@stop