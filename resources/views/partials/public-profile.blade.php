@extends('layouts.'.\Auth::user()->user_type.'_layout') 
@section('title', $user->name?$user->name:'Profile') @section('css-files')


<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/ekko-lightbox/css/ekko-lightbox.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/lightbox2/css/lightbox.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css')}}"
/>

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datedropper/css/datedropper.min.css')}}"
/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('/files/bower_components/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}"
/>

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/social-timeline/timeline.css')}}">
<style>
	.bootstrap-tagsinput {
		width: 100%;
	}
</style>
@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">

		<div class="main-body">
			<div class="page-wrapper">

				<div class="page-body">

					<div class="row">
						<div class="col-sm-12">
							<div>
								<div class="content social-timeline">
									<div class="">

										<div class="row">
											<div class="col-md-12">

												<div class="social-wallpaper">
													<img src="{{$profile && $profile->cover_image ? URL::asset($user->user_type.'/cover_image/'.$profile->cover_image): URL::asset('files/assets/images/social/img1.jpg')}}"
													 class="img-fluid width-100 height-320" alt="" />
												</div>

											</div>
										</div>


										<div class="row">
											<div class="col-xl-3 col-lg-4 col-md-4 col-xs-12">

												<div class="social-timeline-left">

													<div class="card">
														<div class="social-profile">
															<img class="img-fluid width-100 height-width-200" src="{{ $user->profile_image ? URL::asset($user->user_type.'/profile_image/'.$user->profile_image): URL::asset('files/assets/images/social/profile.jpg')}}"
															 alt="">
														</div>
														<div class="card-block social-follower">
															<h4>{{$user->name}}</h4>
															<h5>{{ucfirst($user->user_type)}}</h5>

														</div>
													</div>
												</div>

											</div>
											<div class="col-xl-9 col-lg-8 col-md-8 col-xs-12 ">

												<div class="card social-tabs">
													<ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">

														<li class="nav-item">
															<a class="nav-link active" data-toggle="tab" href="#about" role="tab">About</a>
															<div class="slide"></div>
														</li>

													</ul>
												</div>

												<div class="tab-content">

													<div class="tab-pane active" id="about">
														<div class="row">
															<div class="col-sm-12">
																<div class="card">
																	<div class="card-header">
																		<h5 class="card-header-text">Basic Information</h5>
																	</div>
																	<div class="card-block">
																		<div id="view-info" class="row">
																			<div class="col-lg-8 col-md-12">
																				<form>
																					<table class="table table-responsive m-b-0">
																						<tr>
																							<th class="social-label b-none p-t-0">Full Name
																							</th>
																							<td class="social-user-name b-none p-t-0 text-muted">{{$user->name}}</td>
																						</tr>
																						<tr>
																							<th class="social-label b-none">Tagline</th>
																							<td class="social-user-name b-none text-muted">{{$profile &&$profile->tag_line ? $profile->tag_line : ''}}</td>
																						</tr>
																						<tr>
																							<th class="social-label b-none">Gender</th>
																							<td class="social-user-name b-none text-muted">{{$profile &&$profile->gender ? $profile->gender: ''}}</td>
																						</tr>
																						<tr>
																							<th class="social-label b-none">Birth Date</th>
																							<td class="social-user-name b-none text-muted">{{$profile &&$profile->dob ? $profile->dob: ''}}</td>
																						</tr>
																						<tr>
																							<th class="social-label b-none">Martail Status</th>
																							<td class="social-user-name b-none text-muted">{{$profile &&$profile->martial_status ? $profile->martial_status: ''}}</td>
																						</tr>

																					</table>
																				</form>
																			</div>
																		</div>
																		
																	</div>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="card">
																	<div class="card-header">
																		<h5 class="card-header-text">Contact Information</h5>
																		
																	</div>
																	<div class="card-block">
																		<div id="contact-info" class="row">
																			<div class="col-lg-12 col-md-12">
																				<table class="table table-responsive m-b-0">
																					<tr>
																						<th class="social-label b-none p-t-0">Mobile Number</th>
																						<td class="social-user-name b-none p-t-0 text-muted">{{$profile &&$profile->mobile_no ? $profile->mobile_no: ''}}</td>
																					</tr>
																					<tr>
																						<th class="social-label b-none">Email Address</th>
																						<td class="social-user-name b-none text-muted">
																							{{$user->email}}
																						</td>
																					</tr>
																					<tr>
																						<th class="social-label b-none">Address</th>
																						<td class="social-user-name b-none text-muted">{{$profile &&$profile->address ? $profile->address: ''}}</td>
																					</tr>

																				</table>
																			</div>
																		</div>
																		
																	</div>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="card">
																	<div class="card-header">
																		<h5 class="card-header-text">Work</h5>
																		
																	</div>
																	<div class="card-block">
																		<div id="work-info" class="row">
																			<div class="col-lg-6 col-md-12">
																				<table class="table table-responsive m-b-0">
																					<tr>
																						<th class="social-label b-none p-t-0">Education
																						</th>
																						<td class="social-user-name b-none p-t-0 text-muted">{{$profile && $profile->last_completed_degree ? $profile->last_completed_degree : ''}}</td>
																					</tr>
																					<tr>
																						<th class="social-label b-none">Skills</th>
																						<td class="social-user-name b-none text-muted">{{$profile && $profile->skills ? $profile->skills : ''}}</td>
																					</tr>
																					<tr>
																						<th class="social-label b-none">Occupation</th>
																						<td class="social-user-name b-none p-b-0 text-muted">{{$profile && $profile->current_occupation ? $profile->current_occupation : ''}}</td>
																					</tr>

																				</table>
																			</div>
																		</div>
																		
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')
<script src="{{URL::asset('files/assets/pages/advance-elements/moment-with-locales.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js')}}"></script>

<script src="{{URL::asset('files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js')}}"></script>

<script src="{{URL::asset('files/bower_components/datedropper/js/datedropper.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/bootstrap-tagsinput/js/bootstrap-tagsinput.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}">
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>

<script src="{{URL::asset('files/assets/pages/social-timeline/social.js')}}"></script>
<script>
	function showCover(input) {
			var file, img;
			if (input.files && input.files[0]) {
				
			  var reader = new FileReader();
		  
			  reader.onload = function(e) {
				img = new Image();
				img.src=e.target.result;;
				img.onload = function () {
					//alert(this.width + " " + this.height);
					if(this.width < 560)
					{
						alert('image width must be greater than 400');
						$('#cover_form').trigger("reset");
						$('#cover_image_modal').attr('src', '');
						return;
					}
					else if(this.height < 320)
					{
						alert('image height must be greater than 320');
						$('#cover_form').trigger("reset");
						$('#cover_image_modal').attr('src', '');
						return;
					}
					$('#cover_image_modal').attr('src', e.target.result);
				};
				
			  }
		  
			  reader.readAsDataURL(input.files[0]);
			}
		  }
		  
		  $("#cover_image").change(function() {
			showCover(this);
		  });

</script>

@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif


<script>
	function showprofile(input) {
		var file, img;
		if (input.files && input.files[0]) {
			
		  var reader = new FileReader();
	  
		  reader.onload = function(e) {
			img = new Image();
			img.src=e.target.result;;
			img.onload = function () {
				//alert(this.width + " " + this.height);
				if(this.width < 200)
					{
						alert('image width must be greater than 200');
						$('#profile_image_form').trigger("reset");
						$('#profile_image_modal').attr('src', '');
						return;
					}
					else if(this.height < 200)
					{
						alert('image height must be greater than 200');
						$('#profile_image_form').trigger("reset");
						$('#profile_image_modal').attr('src', '');
						return;
					}
					$('#profile_image_modal').attr('src', e.target.result);
			};
			
		  }
	  
		  reader.readAsDataURL(input.files[0]);
		}
	  }
	  
	  $("#profile_image").change(function() {
		showprofile(this);
	  });

</script>


@stop