@extends('layouts.trainer_layout') 
@section('title', 'Create Course')
@section('css-files')
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/jquery.steps/css/jquery.steps.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}">


@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-header card">
					<div class="card-block">
						<h5 class="m-b-10">Create Course</h5>
					</div>
				</div>
				<div class="page-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="card"> 
								<div class="card-header">
									
								</div>
								<div class="card-block">
									<div class="row">
										<div class="col-md-12">
											<div id="wizard">
												<section>
													<form class="wizard-form" id="form-create-course" action="/trainer/create-course" method="post">
                                                        {{ csrf_field() }}
														<h3> General information </h3>
														<fieldset>
															<div class="form-group row">
																<div class="col-md-12 col-lg-12">
																	<label for="coursetitle" class="block">Course Title *</label>
																</div>
																<div class="col-md-12 col-lg-12">
																	<input id="course_title" name="course_title" type="text" class="required form-control">
																</div>
															</div>
															<div class="form-group row">
																<div class="col-md-12 col-lg-12">
																	<label for="category" class="block">Course Category *</label>
																</div>
																<div class="col-md-12 col-lg-12">
																	<select id="course_category" name="course_category" type="text" class="required form-control">
																		<option value="">
																			Select Category
																			</option>
																		@foreach($categories as $category)
																		<option value="{{$category->category_title}}">
																			{{$category->category_title}}
																		</option>
																		@endforeach
                                                                    </select>
																</div>
															</div>
															<div class="form-group row">
																<div class="col-md-12 col-lg-12">
																	<label for="name-2" class="block">Description *</label>
																</div>
																<div class="col-md-12 col-lg-12">
																	<textarea id="course_description" name="course_description"  class="form-control required" rows=10></textarea>
																</div>
															</div>
															
														
														</fieldset>
														
														
														<h3> Review </h3>
														<fieldset>
															<div class="form-group row">
																<div class="col-md-4 col-lg-2">
																	<label  class="block">Course Title:</label>
																</div>
																<div class="col-md-8 col-lg-10">
																	<p id="c-title"></p>
																</div>
															</div>
															<div class="form-group row">
																<div class="col-md-4 col-lg-2">
																	<label  class="block">Course Category:</label>
																</div>
																<div class="col-md-8 col-lg-10">
																	<p id="c-category"></p>
																</div>
															</div>
															<div class="form-group row">
																<div class="col-md-4 col-lg-2">
																	<label  class="block">Course Description:</label>
																</div>
																<div class="col-md-8 col-lg-10">
																	<p id="c-desc"></p>
																</div>
															</div>
															
														</fieldset>
													</form>
												</section>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')
<script src="{{URL::asset('files/bower_components/jquery.cookie/js/jquery.cookie.js')}}"></script>
<script src="{{URL::asset('files/bower_components/jquery.steps/js/jquery.steps.js')}}"></script>
<script src="{{URL::asset('files/bower_components/jquery-validation/js/jquery.validate.js')}}"></script>
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/form-validation/validate.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/forms-wizard-validation/createcourse-wizard.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/forms-wizard-validation/form-wizard.js')}}"></script>
@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");
</script>

@endif
@if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");
</script>

@endif
@stop