@extends('layouts.admin_layout') 
@section('title', 'Dashboard') 
@section('css-files')
<link rel="stylesheet" href="{{URL::asset('files/assets/pages/chart/radial/css/radial.css')}}" type="text/css" media="all">
<style>
.table-image{
	max-height: 40px;
}
</style>
@stop

@section('body_content')

<div class="pcoded-content">
	<div class="pcoded-inner-content">

		<div class="main-body">
			<div class="page-wrapper">

				<div class="page-body">
					<div class="row">

						<div class="col-md-6 col-xl-3">
							<div class="card bg-c-blue order-card">
								<div class="card-block">
									<h6 class="m-b-20">{{trans('dashboard.TCourses') }}</h6>
									<h2 class="text-right">
										<i class="ti-medall f-left"></i>
										<span>{{ $course['total_course'] }}</span>
									</h2>
									<p class="m-b-0">{{trans('dashboard.month') }}
										<span class="f-right">{{ $course['this_month_course'] }}</span>
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-xl-3">
							<div class="card bg-c-green order-card">
								<div class="card-block">
									<h6 class="m-b-20">{{trans('dashboard.TTrainers') }}</h6>
									<h2 class="text-right">
										<i class="ti-user f-left"></i>
										<span>{{ $trainer['total_trainer'] }}</span>
									</h2>
									<p class="m-b-0">{{trans('dashboard.month') }}
										<span class="f-right">{{ $trainer['this_month_trainer'] }}</span>
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-xl-3">
							<div class="card bg-c-yellow order-card">
								<div class="card-block">
									<h6 class="m-b-20">{{trans('dashboard.TTrainees') }}</h6>
									<h2 class="text-right">
										<i class="ti-user f-left"></i>
										<span>{{ $trainee['total_trainee'] }}</span>
									</h2>
									<p class="m-b-0">{{trans('dashboard.month') }}
										<span class="f-right">{{ $trainee['this_month_trainee'] }}</span>
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-xl-3">
							<div class="card bg-c-pink order-card">
								<div class="card-block">
									<h6 class="m-b-20">{{trans('dashboard.T|T') }}</h6>
									<h2 class="text-right">
										<i class="ti-wallet f-left"></i>
										<span>{{ $tweet['total_tweet'] }}</span>
									</h2>
									<p class="m-b-0">{{trans('dashboard.month') }}
										<span class="f-right">{{ $tweet['this_month_tweet'] }}</span>
									</p>
								</div>
							</div>
						</div>


						<div class="col-lg-8 col-md-12">
							<div class="card">
								<div class="card-header">
									<h5>{{trans('dashboard.st') }}</h5>
									<span class="text-muted">{{trans('dashboard.NT&T') }}</span>
									<div class="card-header-right">
										<ul class="list-unstyled card-option">
											<li>
												<i class="fa fa-chevron-left"></i>
											</li>
											<li>
												<i class="fa fa-window-maximize full-card"></i>
											</li>
											<li>
												<i class="fa fa-minus minimize-card"></i>
											</li>
											<li>
												<i class="fa fa-refresh reload-card"></i>
											</li>
											<li>
												<i class="fa fa-times close-card"></i>
											</li>
										</ul>
									</div>
								</div>
								<div class="card-block">
									<div id="Statistics-chart" style="height:200px"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-12">
							<div class="card seo-card">
								<div class="card-block seo-statustic">
									
									<h5>{{trans('dashboard.NCourses') }}</h5>
									
								</div>
								<div class="seo-chart">
									<canvas id="seo-card1"></canvas>
								</div>
							</div>
						</div>
						


						<div class="col-sm-12">
							<div class="card tabs-card">
								<div class="card-block p-0">

									<ul class="nav nav-tabs md-tabs" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#home3" role="tab">
												<i class="fa fa-users"></i>{{trans('dashboard.LTrainers') }}</a>
											<div class="slide"></div>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#profile3" role="tab">
													<i class="fa fa-users"></i>{{trans('dashboard.LTrainees') }}</a>
											<div class="slide"></div>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#top-course" role="tab">
												<i class="ti-medall"></i>{{trans('dashboard.TCourses') }}</a>
											<div class="slide"></div>
										</li>
										
									</ul>

									<div class="tab-content card-block">
										<div class="tab-pane active" id="home3" role="tabpanel">
											<div class="table-responsive">
												<table class="table">
													<tr>
													<th>{{trans('dashboard.Image') }}</th>
																<th>{{trans('dashboard.Name') }}</th>
																<th>{{trans('dashboard.Email') }}</th>
																<th>{{trans('dashboard.Sector') }}</th>
																<th>{{trans('dashboard.Join') }}</th>
																<th>{{trans('dashboard.Sector') }}</th>
													</tr>
													@foreach($latest_five_trainers as $trainer)
													<tr>

														<td>
															<img src="{{URL::asset($trainer->user_type.'/profile_image/'.$trainer->profile_image)}}" alt="prod img" class="img-fluid table-image">
														</td>
														<td>{{$trainer->name}}</td>
														<td>{{$trainer->email}}</td>
														<td>{{$trainer->sector}}</td>
														<td>{{$trainer->created_at}}</td>
														<td>
															@if($trainer->is_active)
															<span class="label label-success">Active</span>
															@else
															<span class="label label-danger">Not Active</span>
															@endif
														</td>
														
													</tr>
													@endforeach
													
												</table>
											</div>
											
										</div>
										<div class="tab-pane" id="profile3" role="tabpanel">
											<div class="table-responsive">
												<table class="table">
														<tr>
																<th>{{trans('dashboard.Image') }}</th>
																<th>{{trans('dashboard.Name') }}</th>
																<th>{{trans('dashboard.Email') }}</th>
																<th>{{trans('dashboard.Sector') }}</th>
																<th>{{trans('dashboard.Join') }}</th>
																<th>{{trans('dashboard.Sector') }}</th>
															</tr>
														@foreach($latest_five_trainees as $trainee)
														<tr>
	
															<td>
																<img src="{{URL::asset($trainee->user_type.'/profile_image/'.$trainee->profile_image)}}" alt="prod img" class="img-fluid table-image">
															</td>
															<td>{{$trainee->name}}</td>
															<td>{{$trainee->email}}</td>
															<td>{{$trainee->sector}}</td>
															<td>{{$trainee->created_at}}</td>
															<td>
																@if($trainee->is_active)
																<span class="label label-success">Active</span>
																@else
																<span class="label label-danger">Not Active</span>
																@endif
															</td>
															
														</tr>
														@endforeach
												</table>
											</div>
											
										</div>
										<div class="tab-pane" id="top-course" role="tabpanel">
											<div class="table-responsive">
												<table class="table">
														<tr>
																<th>{{trans('dashboard.UInfo') }}</th>
																<th></th>
																<th>{{trans('dashboard.Ccate') }}</th>
																<th>{{trans('dashboard.CTitle') }}</th>
																<th>{{trans('dashboard.created') }}</th>
															</tr>
														@foreach($latest_five_courses as $course)
														<tr>
	
															<td>
																<img src="{{URL::asset($course->user_type.'/profile_image/'.$course->profile_image)}}" alt="prod img" class="img-fluid table-image">
															</td>
															<td>{{$course->name}}</td>
															<td>{{$course->course_title}}</td>
															<td>{{$course->course_category}}</td>
															<td>{{$course->created_at}}</td>
															
															
														</tr>
														@endforeach
												</table>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>

				</div>
			</div>

		</div>
	</div>
</div>
</div>


@stop

@section('javascript-files')

<script src="{{URL::asset('files/bower_components/chart.js/js/Chart.js')}}"></script>
	<script src="{{URL::asset('files/assets/pages/widget/amchart/amcharts.js')}}"></script>
	<script src="{{URL::asset('files/assets/pages/widget/amchart/serial.js')}}"></script>
	<script src="{{URL::asset('files/assets/pages/widget/amchart/light.js')}}"></script>

<script>
		'use strict';
		$(document).ready(function() {
			function buildchartoption() {
				return {
					title: {
						display: !1
					},
					tooltips: {
						enabled: true,
						intersect: !1,
						mode: "nearest",
						xPadding: 10,
						yPadding: 10,
						caretPadding: 10
					},
					legend: {
						display: !1,
						labels: {
							usePointStyle: !1
						}
					},
					responsive: !0,
					maintainAspectRatio: !0,
					hover: {
						mode: "index"
					},
					scales: {
						xAxes: [{
							display: !1,
							gridLines: !1,
							scaleLabel: {
								display: !0,
								labelString: "Month"
							}
						}],
						yAxes: [{
							display: !1,
							gridLines: !1,
							scaleLabel: {
								display: !0,
								labelString: "Value"
							},
							ticks: {
								beginAtZero: !0
							}
						}]
					},
					elements: {
						point: {
							radius: 4,
							borderWidth: 12
						}
					},
					layout: {
						padding: {
							left: 0,
							right: 0,
							top: 0,
							bottom: 0
						}
					}
				};
			}
		
		
		
			function seojs(a, b, c, f) {
				if (f == null) {
					f = "rgba(0,0,0,0)";
				}
				return {
					labels: c,
					datasets: [{
						label: "",
						borderColor: a,
						borderWidth: 2,
						hitRadius: 0,
						pointHoverRadius: 0,
						pointRadius: 3,
						pointBorderWidth: 2,
						pointHoverBorderWidth: 12,
						pointBackgroundColor: Chart.helpers.color("#fff").alpha(1).rgbString(),
						pointBorderColor: Chart.helpers.color(a).alpha(1).rgbString(),
						pointHoverBackgroundColor: a,
						pointHoverBorderColor: Chart.helpers.color("#000000").alpha(0).rgbString(),
						fill: true,
						backgroundColor: f,
						data: b,
					}]
				};
			}
			var data_array = '{!! json_encode($users_this_week) !!}';
			var data = JSON.parse(data_array);
			console.log(data);
			var chart = AmCharts.makeChart("Statistics-chart", {
				"type": "serial",
				"theme": "light",
				"dataDateFormat": "YYYY-MM-DD",
				"precision": 2,
				"valueAxes": [{
					"id": "v1",
					"title": "Users",
					"position": "left",
					"autoGridCount": false,
					"labelFunction": function(value) {
						return "$" + Math.round(value) + "M";
					}
				}, {
					"id": "v2",
					"gridAlpha": 0.1,
					"autoGridCount": false
				}],
				"graphs": [{
					"id": "g1",
					"valueAxis": "v2",
					"lineThickness": 0,
					"fillAlphas": 0.2,
					"lineColor": "#4099ff",
					"type": "line",
					"title": "Trainees",
					"useLineColorForBulletBorder": true,
					"valueField": "trainee_count",
					"balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
				}, {
					"id": "g2",
					"valueAxis": "v2",
					"fillAlphas": 0.6,
					"lineThickness": 0,
					"lineColor": "#4099ff",
					"type": "line",
					"title": "Trainer",
					"useLineColorForBulletBorder": true,
					"valueField": "trainer_count",
					"balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
				}],
				"chartCursor": {
					"pan": true,
					"valueLineEnabled": true,
					"valueLineBalloonEnabled": true,
					"cursorAlpha": 0,
					"valueLineAlpha": 0.2
				},
				"categoryField": "date",
				"categoryAxis": {
					"parseDates": true,
					"gridAlpha": 0,
					"minorGridEnabled": true
				},
				"legend": {
					"position": "top",
				},
				"balloon": {
					"borderThickness": 1,
					"shadowAlpha": 0
				},
				"export": {
					"enabled": true
				},
				"dataProvider":data
			});

			var course_array = '{!! json_encode($courses_this_week) !!}';
			var course_data = JSON.parse(course_array);
			var courses_count =[];
			var dates = [];
			for(var i=0; i<course_data.length; i++)
			{
				courses_count[i] = course_data[i].total_course;
				dates[i] = course_data[i].date;
			}
			var ctx = document.getElementById('seo-card1').getContext("2d");
			var gradientFill = ctx.createLinearGradient(300, 0, 0, 300);
			gradientFill.addColorStop(0, "#b9fdef");
			gradientFill.addColorStop(1, "#2ed8b6");
			var myChart = new Chart(ctx, {
				type: 'line',
				data: seojs('#2ed8b6', courses_count , dates, gradientFill),
				options: buildchartoption(),
			});
			
		});
		
</script>
@stop