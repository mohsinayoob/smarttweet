<?php

namespace App\Http\Controllers\trainee;

use App\Http\Controllers\Controller;
use App\Models\Chat\ChatDialog;
use App\Models\User;
use App\Models\Course\CourseEnrollment;

class TraineeChatController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainee');

    }
    public function chatBetweenTrainees($user_id, $course_id)
    {
        $course_id = base64_decode($course_id);
        $user_id = base64_decode($user_id);
        $chat_dialog = ChatDialog::where('trainee_id', \Auth::user()->user_id)
            ->where('second_trainee_id', $user_id)->first();
        if (!empty($chat_dialog)) {
            return view('partials.chat', compact('chat_dialog'));
        }
        $first_user_enrollment = CourseEnrollment::where('trainee_id', $user_id)->where('course_id', $course_id)->first();
        $second_user_enrollment = CourseEnrollment::where('trainee_id', \Auth::user()->user_id)->where('course_id', $course_id)->first();
        if (!empty($first_user_enrollment) && !empty($second_user_enrollment)) {
            $user = User::where('user_id', $user_id)->first();
            $response = ChatDialog::createChatDialog($user, NULL);
            $responseJSON = json_decode($response);
            if ($responseJSON) {
                $dialog_id = $responseJSON->_id;
                $data = array(
                    "second_trainee_id" => $user->user_id,
                    "trainee_id" => \Auth::user()->user_id,
                    "dialog_id" => $dialog_id,
                );
                $chat_dialog = ChatDialog::create($data);
                if(!empty($chat_dialog))
                {
                    return view('partials.chat', compact('chat_dialog'));
                   // return redirect()->to('/trainee/chat/'.base64_encode($user_id));
                }
            }
        }
        return redirect()->to('/trainee/chat');

    }
    public function chatByUserId($user_id)
    {
        $chat_dialog = ChatDialog::where('trainee_id', \Auth::user()->user_id)
            ->where('trainer_id', base64_decode($user_id))->first();
        return view('partials.chat', compact('chat_dialog'));

    }
    public function index()
    {
        $chat_dialog = false;
        return view('partials.chat', compact('chat_dialog'));
    }
}
