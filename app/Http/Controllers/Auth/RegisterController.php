<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\EmailVerification;
use App\Models\Course\Category;
use App\Models\User;
use DB;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mail;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $categories = Category::all();
        return view('auth.register', compact('categories'));
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'user_type' => 'required',

        ]);
    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'user_type' => $data['user_type'],
            'sector' => $data['sector'],
            'otp' => str_random(15),
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        //event(new Registered($user = $this->create($request->all())));

        DB::beginTransaction();
        try
        {
            $user = $this->create($request->all());
            if ($user->user_type != "admin") {
                $qb_id = $this->signupQB($user);
                if (!empty($qb_id)) {
                    $user->qb_id = $qb_id;
                    $user->save();
                }
            }

            DB::commit();
            // After creating the user send an email with the random token generated in the create method above
            $email = new EmailVerification(new User(['otp' => $user->otp, 'name' => $user->name]));

            Mail::to($user->email)->send($email);

            Session::flash('message', 'We have sent you a verification email!');
            return back();
        } catch (Exception $e) {
            DB::rollback();
            return back();
        }

        //$this->guard()->login($user);

        return $this->registered($request, $user)
        ?: redirect($this->redirectPath());
    }

    /*public function redirectPath()
    {
    if (\Auth::user()->user_type =='trainee') {
    return "/trainee/manage-course";
    // or return route('routename');
    }
    else if(\Auth::user()->user_type=='trainer')
    {
    $redirectTo = '/trainer/manage-course';
    }
    else if(\Auth::user()->user_type=='admin')
    {
    $redirectTo = '/admin/dashboard';
    }
    else
    {
    return response('Please try later.', 401);
    }
    }
     */
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    /////////////////////////////////////////////////

    public function verify($token)
    {
        // The verified method has been added to the user model and chained here
        try {
            $verify_user = User::where('otp', $token)->first();
            if (!empty($verify_user)) {
                $verify_user->verified();
                return redirect('login')->with('message', 'Email Successfully Verified!');
            }

            return redirect('login')->with('message', 'Verification Link Expired');
        } catch (Exception $e) {
            return redirect('login')->with('message', 'Unable to Verify email Kindly check Email again!');
        }
        // for better readability

    }

    protected function signupQB($user)
    {
        $session = $this->createSession(66308, 'S77-sr2xUBBQ2zd', 'KqD7Y9sZxpvQnHK', 'hV_LMoU4uRmkX3dJR8uU', 'mohsin2303');
        $token = $session->token;
        $post_body = http_build_query(array(
            'user[login]' => $user->email,
            'user[password]' => $user->email,
            'user[email]' => $user->email,
            'user[external_user_id]' => $user->user_id,
            'user[facebook_id]' => '',
            'user[twitter_id]' => '',
            'user[full_name]' => $user->name,
            'user[phone]' => '',
        ));

        $url = "https://api.quickblox.com/users.json";
        $curl = curl_init("https://api.quickblox.com/users.json");
        // curl_setopt($curl, $url); // Full path is - https://api.quickblox.com/session.json
        curl_setopt($curl, CURLOPT_POST, true); // Use POST
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('QB-Token: ' . $token));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        // Execute request and read response
        $response = curl_exec($curl);
        if (!empty($response)) {
            curl_close($curl);
            $responseJson = json_decode($response, true);
            $quickblock_id = $responseJson['user']['id'];
            return $quickblock_id;

        }

    }

    public function createSession($appId, $authKey, $authSecret, $login, $password)
    {

        if (!$appId || !$authKey || !$authSecret || !$login || !$password) {
            return false;
        }

        // Generate signature
        $nonce = rand();
        $timestamp = time(); // time() method must return current timestamp in UTC but seems like hi is return timestamp in current time zone
        $signature_string = "application_id=" . $appId . "&auth_key=" . $authKey . "&nonce=" . $nonce . "&timestamp=" . $timestamp;

        $signature = hash_hmac('sha1', $signature_string, $authSecret);

        // Build post body
        $post_body = urldecode(http_build_query(array(
            'application_id' => $appId,
            'auth_key' => "$authKey",
            'timestamp' => $timestamp,
            'nonce' => $nonce,
            'signature' => $signature,
        )));
        //echo $post_body;
        // Configure cURL
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.quickblox.com/session.json'); // Full path is - https://api.quickblox.com/session.json
        curl_setopt($curl, CURLOPT_POST, true); // Use POST
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        // Execute request and read response
        $response = curl_exec($curl);
        echo $response;
        //exit;
        $responseJSON = json_decode($response)->session;

        // Check errors
        if ($responseJSON) {
            return $responseJSON;
        } else {
            return "error_occured";
        }

    }
}
