<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        //App::setLocale(Session::has('locale') ? Session::get('locale') : config::get('app.locale'));
 

        if(! Session::has('locale')){

            Session::put('locale',Config::get('app.locale'));
        }

        App::setLocale(session('locale'));
        return $next($request);
     //   return $next($request);
    }
}
