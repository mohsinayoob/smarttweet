@extends('layouts.trainee_layout') @section('title', 'Tweets') @section('css-files')
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/lightgallery/css/lightgallery.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/simple-line-icons/css/simple-line-icons.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/icofont/css/icofont.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/themify-icons/themify-icons.css')}}">
<link rel="stylesheet" type="text/css" href="../files/bower_components/jquery-bar-rating/css/css-stars.css">
<link rel="stylesheet" type="text/css" href="../files/bower_components/jquery-bar-rating/css/fontawesome-stars-o.css">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/css/component.css')}}"> 
<style>
.stars-example-css{
	margin-left:33%;
}
</style>

@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-header card">
					<div class="card-block">
						<div class="card-header-right f-right">
							<button class="btn btn-warning btn-round md-trigger" data-modal="modal-rating">Rate Course</button>
						</div>
						<h5 class="m-b-10 m-2">All about Laravel</h5>

					</div>

				</div>
				<div class="page-body">
					<!--  Modal for rating -->
					<div class="md-modal md-effect-13 addcontact" id="modal-rating">
						<div class="md-content">
							<h3 class="f-26">Rate Course</h3>
							<div class="text-center">
								<form>
									<div class="text-center">
									<div class="stars stars-example-css ">
										<select id="example-css" class="rating-star" name="rating" autocomplete="off" >
											<option value="1" >1</option>
											<option value="2" >2</option>
											<option value="3" >3</option>
											<option value="4" >4</option>
											<option value="5" >5</option>
										</select>

									</div>
								</div>
									<div class="input-group">
											<textarea id="post-message" class="form-control " rows="3" cols="10" required="" placeholder="Your Comments"></textarea>
									</div>
									<div class="text-center">
										<button type="submit" class="btn btn-primary waves-effect m-r-20 f-w-600 d-inline-block save_btn">Save</button>
										<button type="button" class="btn btn-primary waves-effect m-r-20 f-w-600 md-close d-inline-block close_btn">Close</button>
									</div>
								</form>
							</div>
						</div>
					</div>

					<!-- Modal for list who liked -->
					<div class="modal fade modal-flex" id="Modal-tab" tabindex="-1" role="dialog">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-body">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<ul class="nav nav-tabs" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">
												<i class="fa fa-heart"></i>
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#tab-profile" role="tab">
												<i class="fa fa-thumbs-up"></i>
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#tab-messages" role="tab">
												<i class="fa fa-thumbs-down"></i>
											</a>
										</li>

									</ul>
									<div class="tab-content modal-body">
										<div class="tab-pane active" id="tab-home" role="tabpanel">
											<div class="card-block user-box">
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-1.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-3.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
											</div>

										</div>
										<div class="tab-pane" id="tab-profile" role="tabpanel">
											<div class="card-block user-box">
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-1.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-3.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane" id="tab-messages" role="tabpanel">
											<div class="card-block user-box">
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-1.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-3.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="md-overlay"></div>
					<div class="row">
						<div class="col-lg-9">
							<div class="row">
								<div class="col-md-12">
									<div>
										<div class="card bg-white p-relative">

											<div class="card-block">
												<div class="card-header-right">
													<a href="/trainee/tweet" class="btn btn-info btn-md b-none f-right" title="watch tweet">
														<i class="icofont icofont-eye-alt icofont-lg m-1"></i>
													</a>
												</div>
												<div class="media">
													<div class="media-left media-middle friend-box">
														<a href="#">
															<img class="media-object img-radius m-r-20" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="">
														</a>
													</div>

													<div class="media-body">
														<div class="chat-header">Josephin Doe </div>

														<div class="f-13 text-muted">50 minutes ago</div>
													</div>
												</div>

											</div>
											</a>
											<div id="lightgallery" class="lightgallery-popup">
												<div class="" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg 480, img/1.jpg
												 800 " data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} "
												 data-sub-html="<h4>Josephin Doe posted</h4>
													<p>This banking course explains in detail what fractional reserve banking is and how this banking system works. The course reviews the strengths and weaknesses of the fractional reserve banking system</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
											</div>
											<div class="card-block">
												<div class="timeline-details">
													<div class="chat-header">Josephin Doe posted </div>
													<p class="text-muted">This banking course explains in detail what fractional reserve banking is and how this banking system works. The course reviews the strengths and weaknesses of the fractional reserve banking system</p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-msg">
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab">
													<i class="icofont icofont-heart-alt text-muted">
													</i>
													<span class="b-r-theme">Like (20)</span>
												</a>
												<a href="/trainee/tweet">
													<i class="icofont icofont-comment text-muted">
													</i>
													<span class="b-r-theme">Comments (25)</span>
												</a>
												



											</div>
										</div>


									</div>
									<div>
										<div class="card bg-white p-relative">
											<div class="card-block">
												<div class="card-header-right">
													<a href="/trainee/tweet" class="btn btn-info btn-md b-none f-right" title="watch tweet">
														<i class="icofont icofont-eye-alt icofont-lg m-1"></i>
													</a>
												</div>
												<div class="media">
													<div class="media-left media-middle friend-box">
														<a href="#">
															<img class="media-object img-radius m-r-20" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="">
														</a>
													</div>
													<div class="media-body">
														<div class="chat-header">Josephin Doe </div>
														<div class="f-13 text-muted">50 minutes ago</div>
													</div>
												</div>
											</div>
											<div id="lightgallery1" class="wall-img-preview lightgallery-popup">
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
											</div>
											<div class="card-block">
												<div class="timeline-details">
													<div class="chat-header">Josephin Doe </div>
													<p class="text-muted">This banking course explains in detail what fractional reserve banking is and how this banking system works. The course reviews the strengths and weaknesses of the fractional reserve banking system</p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-msg
                                            ">
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab">
													<i class="icofont icofont-heart-alt text-muted">
													</i>
													<span class="b-r-theme">Like (20)</span>
												</a>
												<a href="/trainee/tweet">
													<i class="icofont icofont-comment text-muted">
													</i>
													<span class="b-r-theme">Comments (25)</span>
												</a>
												
											</div>
										</div>


									</div>
									<div>
										<div class="card bg-white p-relative">
											<div class="card-block">
												<div class="card-header-right">
													<a href="/trainee/tweet" class="btn btn-info btn-md b-none f-right" title="watch tweet">
														<i class="icofont icofont-eye-alt icofont-lg m-1"></i>
													</a>
												</div>
												<div class="media">
													<div class="media-left media-middle friend-box">
														<a href="#">
															<img class="media-object img-radius m-r-20" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="">
														</a>
													</div>
													<div class="media-body">
														<div class="chat-header">Josephin Doe </div>
														<div class="f-13 text-muted">50 minutes ago</div>
													</div>
												</div>
											</div>
											<div id="lightgallery2" class="wall-img-preview lightgallery-popup">
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
											</div>
											<div class="card-block">
												<div class="timeline-details">
													<div class="chat-header">Josephin Doe </div>
													<p class="text-muted">This banking course explains in detail what fractional reserve banking is and how this banking system works. The course reviews the strengths and weaknesses of the fractional reserve banking system</p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-msg
                                            ">
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab">
													<i class="icofont icofont-heart-alt text-muted">
													</i>
													<span class="b-r-theme">Like (20)</span>
												</a>
												<a href="/trainee/tweet">
													<i class="icofont icofont-comment text-muted">
													</i>
													<span class="b-r-theme">Comments (25)</span>
												</a>
												



											</div>

										</div>


									</div>
									<div>
										<div class="card bg-white p-relative">
											<div class="card-block">
												<div class="card-header-right">
													<a href="/trainee/tweet" class="btn btn-info btn-md b-none f-right" title="watch tweet">
														<i class="icofont icofont-eye-alt icofont-lg m-1"></i>
													</a>
												</div>
												<div class="media">
													<div class="media-left media-middle friend-box">
														<a href="#">
															<img class="media-object img-radius m-r-20" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="">
														</a>
													</div>
													<div class="media-body">
														<div class="chat-header">Josephin Doe </div>
														<div class="f-13 text-muted">50 minutes ago</div>
													</div>
												</div>
											</div>
											<div id="lightgallery3" class="wall-img-preview lightgallery-popup">
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-12 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
											</div>
											<div class="card-block">
												<div class="timeline-details">
													<div class="chat-header">Josephin Doe </div>
													<p class="text-muted">This banking course explains in detail what fractional reserve banking is and how this banking system works. The course reviews the strengths and weaknesses of the fractional reserve banking system</p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-msg
                                        ">
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab">
													<i class="icofont icofont-heart-alt text-muted">
													</i>
													<span class="b-r-theme">Like (20)</span>
												</a>
												<a href="/trainee/tweet">
													<i class="icofont icofont-comment text-muted">
													</i>
													<span class="b-r-theme">Comments (25)</span>
												</a>
												
											</div>

										</div>


									</div>
									<div>
										<div class="card bg-white p-relative">
											<div class="card-block">
												<div class="card-header-right">
													<a href="/trainee/tweet" class="btn btn-info btn-md b-none f-right" title="watch tweet">
														<i class="icofont icofont-eye-alt icofont-lg m-1"></i>
													</a>
												</div>
												<div class="media">
													<div class="media-left media-middle friend-box">
														<a href="#">
															<img class="media-object img-radius m-r-20" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="">
														</a>
													</div>
													<div class="media-body">
														<div class="chat-header">Josephin Doe </div>
														<div class="f-13 text-muted">50 minutes ago</div>
													</div>
												</div>
											</div>
											<div id="lightgallery4" class="wall-img-preview lightgallery-popup">
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
											</div>
											<div class="card-block c-both">
												<div class="timeline-details">
													<div class="chat-header">Josephin Doe </div>
													<p class="text-muted">This banking course explains in detail what fractional reserve banking is and how this banking system works. The course reviews the strengths and weaknesses of the fractional reserve banking system</p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-msg
                                    ">
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab">
													<i class="icofont icofont-heart-alt text-muted"></i>
													<span class="b-r-theme
                                    ">Like (20)</span>
												</a>
												<a href="/trainee/tweet">
													<i class="icofont icofont-comment text-muted"></i>
													<span class="b-r-theme
                                    ">Comments (25)</span>
												</a>
												
											</div>

										</div>


									</div>
									<div>
										<div class="card bg-white p-relative">
											<div class="card-block">
												<div class="card-header-right">
													<a href="/trainee/tweet" class="btn btn-info btn-md b-none f-right" title="watch tweet">
														<i class="icofont icofont-eye-alt icofont-lg m-1"></i>
													</a>
												</div>
												<div class="media">
													<div class="media-left media-middle friend-box">
														<a href="#">
															<img class="media-object img-radius m-r-20" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="">
														</a>
													</div>
													<div class="media-body">
														<div class="chat-header">Josephin Doe </div>
														<div class="f-13 text-muted">50 minutes ago</div>
													</div>
												</div>
											</div>
											<div id="lightgallery5" class="wall-img-preview lightgallery-popup">
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
											</div>
											<div class="card-block c-both">
												<div class="timeline-details">
													<div class="chat-header">Josephin Doe </div>
													<p class="text-muted">This banking course explains in detail what fractional reserve banking is and how this banking system works. The course reviews the strengths and weaknesses of the fractional reserve banking system </p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-msg
                                    ">
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab">
													<i class="icofont icofont-heart-alt text-muted"></i>
													<span class="b-r-theme
                                    ">Like (20)</span>
												</a>
												<a href="/trainee/tweet">
													<i class="icofont icofont-comment text-muted"></i>
													<span class="b-r-theme
                                    ">Comments (25)</span>
												</a>
												

											</div>

										</div>


									</div>
									<div>
										<div class="card bg-white p-relative">
											<div class="card-block">
												<div class="card-header-right">
													<a href="/trainee/tweet" class="btn btn-info btn-md b-none f-right" title="watch tweet">
														<i class="icofont icofont-eye-alt icofont-lg m-1"></i>
													</a>
												</div>
												<div class="media">
													<div class="media-left media-middle friend-box">
														<a href="#">
															<img class="media-object img-radius m-r-20" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="">
														</a>
													</div>
													<div class="media-body">
														<div class="chat-header">Josephin Doe </div>
														<div class="f-13 text-muted">50 minutes ago</div>
													</div>
												</div>
											</div>
											<div id="lightgallery6" class="wall-img-preview lightgallery-popup">
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
											</div>
											<div class="card-block c-both">
												<div class="timeline-details">
													<div class="chat-header">Josephin Doe </div>
													<p class="text-muted">This banking course explains in detail what fractional reserve banking is and how this banking system works. The course reviews the strengths and weaknesses of the fractional reserve banking system</p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-msg
                                        ">
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab">
													<i class="icofont icofont-heart-alt text-muted"></i>
													<span class="b-r-theme
                                        ">Like (20)</span>
												</a>
												<a href="/trainee/tweet">
													<i class="icofont icofont-comment text-muted"></i>
													<span class="b-r-theme
                                        ">Comments (25)</span>
												</a>
												

											</div>

										</div>


									</div>
									<div>
										<div class="card bg-white p-relative">
											<div class="card-block">
												<div class="card-header-right">
													<a href="/trainee/tweet" class="btn btn-info btn-md b-none f-right" title="watch tweet">
														<i class="icofont icofont-eye-alt icofont-lg m-1"></i>
													</a>
												</div>
												<div class="media">
													<div class="media-left media-middle friend-box">
														<a href="#">
															<img class="media-object img-radius m-r-20" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="">
														</a>
													</div>
													<div class="media-body">
														<div class="chat-header">Josephin Doe </div>
														<div class="f-13 text-muted">50 minutes ago</div>
													</div>
												</div>
											</div>
											<div id="lightgallery7" class="wall-img-preview lightgallery-popup">
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
											</div>
											<div class="card-block c-both">
												<div class="timeline-details">
													<div class="chat-header">Josephin Doe </div>
													<p class="text-muted">This banking course explains in detail what fractional reserve banking is and how this banking system works. The course reviews the strengths and weaknesses of the fractional reserve banking system</p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-msg
                                    ">
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab">
													<i class="icofont icofont-heart-alt text-muted"></i>
													<span class="b-r-theme
                                    ">Like (20)</span>
												</a>
												<a href="/trainee/tweet">
													<i class="icofont icofont-comment text-muted"></i>
													<span class="b-r-theme
                                    ">Comments (25)</span>
												</a>
												



											</div>

										</div>


									</div>
									<div>
										<div class="card bg-white p-relative">
											<div class="card-block">
												<div class="card-header-right">
													<a href="/trainee/tweet" class="btn btn-info btn-md b-none f-right" title="watch tweet">
														<i class="icofont icofont-eye-alt icofont-lg m-1"></i>
													</a>
												</div>
												<div class="media">
													<div class="media-left media-middle friend-box">
														<a href="#">
															<img class="media-object img-radius m-r-20" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="">
														</a>
													</div>
													<div class="media-body">
														<div class="chat-header">Josephin Doe </div>
														<div class="f-13 text-muted">50 minutes ago</div>
													</div>
												</div>
											</div>
											<div id="lightgallery8" class="wall-img-preview lightgallery-popup">
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
											</div>
											<div class="card-block c-both">
												<div class="timeline-details">
													<div class="chat-header">Josephin Doe </div>
													<p class="text-muted">This banking course explains in detail what fractional reserve banking is and how this banking system works. The course reviews the strengths and weaknesses of the fractional reserve banking system</p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-msg
                                        ">
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab">
													<i class="icofont icofont-heart-alt text-muted"></i>
													<span class="b-r-theme
                                        ">Like (20)</span>
												</a>
												<a href="/trainee/tweet">
													<i class="icofont icofont-comment text-muted"></i>
													<span class="b-r-theme
                                        ">Comments (25)</span>
												</a>
												



											</div>
										</div>


									</div>
									<div>
										<div class="card bg-white p-relative">
											<div class="card-block">
												<div class="card-header-right">
													<a href="/trainee/tweet" class="btn btn-info btn-md b-none f-right" title="watch tweet">
														<i class="icofont icofont-eye-alt icofont-lg m-1"></i>
													</a>
												</div>
												<div class="media">
													<div class="media-left media-middle friend-box">
														<a href="#">
															<img class="media-object img-radius m-r-20" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="">
														</a>
													</div>
													<div class="media-body">
														<div class="chat-header">Josephin Doe </div>
														<div class="f-13 text-muted">50 minutes ago</div>
													</div>
												</div>
											</div>
											<div id="lightgallery9" class="wall-img-preview lightgallery-popup">
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-12 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
											</div>
											<div class="card-block c-both">
												<div class="timeline-details">
													<div class="chat-header">Josephin Doe </div>
													<p class="text-muted">This banking course explains in detail what fractional reserve banking is and how this banking system works. The course reviews the strengths and weaknesses of the fractional reserve banking system</p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-msg
                                    ">
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab">
													<i class="icofont icofont-heart-alt text-muted"></i>
													<span class="b-r-theme
                                    ">Like (20)</span>
												</a>
												<a href="/trainee/tweet">
													<i class="icofont icofont-comment text-muted"></i>
													<span class="b-r-theme
                                    ">Comments (25)</span>
												</a>



											</div>
										</div>


									</div>
									<div>
										<div class="card bg-white p-relative">
											<div class="card-block">
												<div class="card-header-right">
													<a href="/trainee/tweet" class="btn btn-info btn-md b-none f-right" title="watch tweet">
														<i class="icofont icofont-eye-alt icofont-lg m-1"></i>
													</a>
												</div>
												<div class="media">
													<div class="media-left media-middle friend-box">
														<a href="#">
															<img class="media-object img-radius m-r-20" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="">
														</a>
													</div>
													<div class="media-body">
														<div class="chat-header">Josephin Doe </div>
														<div class="f-13 text-muted">50 minutes ago</div>
													</div>
												</div>
											</div>
											<div id="lightgallery10" class="wall-img-preview lightgallery-popup">
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
											</div>
											<div class="card-block c-both">
												<div class="timeline-details">
													<div class="chat-header">Josephin Doe </div>
													<p class="text-muted">This banking course explains in detail what fractional reserve banking is and how this banking system works. The course reviews the strengths and weaknesses of the fractional reserve banking system</p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-msg
                                        ">
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab">
													<i class="icofont icofont-heart-alt text-muted"></i>
													<span class="b-r-theme
                                        ">Like (20)</span>
												</a>
												<a href="/trainee/tweet">
													<i class="icofont icofont-comment text-muted"></i>
													<span class="b-r-theme
                                        ">Comments (25)</span>
												</a>
												



											</div>
										</div>


									</div>
									<div>
										<div class="card bg-white p-relative">
											<div class="card-block">
												<div class="card-header-right">
													<a href="/trainee/tweet" class="btn btn-info btn-md b-none f-right">
														<i class="icofont icofont-eye-alt icofont-lg m-1"></i>
													</a>
												</div>
												<div class="media">
													<div class="media-left media-middle friend-box">
														<a href="#">
															<img class="media-object img-radius m-r-20" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="">
														</a>
													</div>
													<div class="media-body">
														<div class="chat-header">Josephin Doe </div>
														<div class="f-13 text-muted">50 minutes ago</div>
													</div>
												</div>
											</div>
											<div id="lightgallery11" class="wall-img-preview lightgallery-popup">
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
												<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" 375, img/1-480.jpg
												 480, img/1.jpg 800 "
												 data-src="{{URL::asset( 'files/assets/images/timeline/img1.jpg')}} " data-sub-html="<h4>Fading Light</h4>
													<p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later
														on.
													</p>">
													<a href="#">
														<img src="{{URL::asset('files/assets/images/timeline/img1.jpg')}}" class="img-fluid " alt="">
													</a>
												</div>
											</div>
											<div class="card-block c-both">
												<div class="timeline-details">
													<div class="chat-header">Josephin Doe </div>
													<p class="text-muted">This banking course explains in detail what fractional reserve banking is and how this banking system works. The course reviews the strengths and weaknesses of the fractional reserve banking system.</p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-msg
                                        ">
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab">
													<i class="icofont icofont-heart-alt text-muted"></i>
													<span class="b-r-theme
                                        ">Like (20)</span>
												</a>
												<a href="/trainee/tweet">
													<i class="icofont icofont-comment text-muted"></i>
													<span class="b-r-theme
                                        ">Comments (25)</span>
												</a>
												



											</div>

										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="col-md-3">
							<div class="fb-timeliner">
								<h2 class="recent-highlight bg-danger">Recent</h2>
								<ul>
									<li class="active">
										<a href="#">December</a>
									</li>
									<li>
										<a href="#">November</a>
									</li>
									<li>
										<a href="#">October</a>
									</li>
									<li>
										<a href="#">September</a>
									</li>
									<li>
										<a href="#">August</a>
									</li>
									<li>
										<a href="#">July</a>
									</li>
									<li>
										<a href="#">June</a>
									</li>
									<li>
										<a href="#">May</a>
									</li>
									<li>
										<a href="#">April</a>
									</li>
									<li>
										<a href="#">March</a>
									</li>
									<li>
										<a href="#">February</a>
									</li>
									<li>
										<a href="#">January</a>
									</li>
								</ul>
							</div>
							<div class="fb-timeliner">
								<h2>2012</h2>
								<ul>
									<li>
										<a href="#">August</a>
									</li>
									<li>
										<a href="#">July</a>
									</li>
									<li>
										<a href="#">June</a>
									</li>
									<li>
										<a href="#">May</a>
									</li>
									<li>
										<a href="#">April</a>
									</li>
									<li>
										<a href="#">March</a>
									</li>
									<li>
										<a href="#">February</a>
									</li>
									<li>
										<a href="#">January</a>
									</li>
								</ul>
							</div>
							<div class="fb-timeliner">
								<h2>2011</h2>
								<ul>
									<li>
										<a href="#">May</a>
									</li>
									<li>
										<a href="#">April</a>
									</li>
									<li>
										<a href="#">March</a>
									</li>
									<li>
										<a href="#">February</a>
									</li>
									<li>
										<a href="#">January</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')
<script src="{{URL::asset('files/bower_components/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-fullscreen.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-video.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-autoplay.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-zoom.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-hash.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-pager.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/wall/wall.js')}}"></script>
<script src="{{URL::asset('files/bower_components/jquery-bar-rating/js/jquery.barrating.js')}}"></script>
<script src="{{URL::asset('files/assets/js/rating.js')}}"></script>
<script src="{{URL::asset('files/assets/js/modalEffects.js')}}"></script>
<script src="{{URL::asset('files/assets/js/classie.js')}}"></script>

<script>
	$(document).ready(function() {
            $('.lightgallery-popup').lightGallery();
        });

</script>
@stop