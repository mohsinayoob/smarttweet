<?php

namespace App\Http\Controllers\trainee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\TopicNotificationJob;
use App\Models\Course\Topic;
class TraineeNotificataionTestController extends Controller
{
    //
    public function index()
    {
        $topic = Topic::where('course_id',1)->first();
       // \Auth::user()->notify(new \App\Notifications\StatusLiked($topic));
       $notification = (new TopicNotificationJob($topic))->delay(13);
        $this->dispatch($notification);
        return "Event has been sent!";
    }
}
