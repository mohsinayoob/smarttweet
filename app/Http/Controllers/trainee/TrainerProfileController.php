<?php

namespace App\Http\Controllers\trainee;

use App\Http\Controllers\Controller;
use App\Models\Trainee\TraineeProfile;
use App\Models\Trainer\TrainerProfile;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;

class TrainerProfileController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    public function index($id)
    {
        if($id == \Auth::user()->user_id)
        {
            return redirect('/'.\Auth::user()->user_type.'/profile');
        }
        $user = User::where('user_id',$id)->first();
        if(!empty($user))
        {
            if($user->user_type=="trainer")
            {
                $profile = TrainerProfile::where('user_id', $user->user_id)->first();
                //return view('trainee.profile', $profile);
                return view('partials.public-profile', compact('profile', 'user'));
            }
            else if($user->user_type=="trainee")
            {
                $profile = TraineeProfile::where('user_id', $user->user_id)->first();
                //return view('trainee.profile', $profile);
                return view('partials.public-profile', compact('profile', 'user'));
            }
            else{
                $error_heaing = "Oops! 404 Not found ";
                $error_description = "This User does not exit or You are authorized to view this page.";
                return view('partials.error-page',compact('error_heaing','error_description'));
            }
            
        }
        else {
            $error_heaing = "Oops! 404 Not found ";
            $error_description = "This User does not exit or You are authorized to view this page.";
            return view('partials.error-page',compact('error_heaing','error_description'));
        }
        
    }
}
