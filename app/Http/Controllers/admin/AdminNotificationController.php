<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use DB;
Use Session;
class AdminNotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        
    }

    //
    public function getNotification()
    {
        $notifications = DB::table('notifications')
        ->where('notifiable_id', \Auth::user()->user_id)
        ->where('read_at', NULL)->take(10)->get();
       
        return $notifications;

    }
    public function getAllNotification()
    {
        $notifications = DB::table('notifications')
        ->where('notifiable_id', \Auth::user()->user_id)
        ->where('read_at', NULL)->paginate(10);
       
        return view('admin.notifications', compact('notifications'));

    }
    public function getAjaxAllNotification()
    {
        $notifications = DB::table('notifications')
        ->where('notifiable_id', \Auth::user()->user_id)
        ->where('read_at', NULL)->paginate(10);
       
        return $notifications;

    }
    
}
