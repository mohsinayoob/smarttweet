<?php

namespace App\Http\Controllers\Trainer;

use App\Http\Controllers\Controller;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketCategory;
use App\Models\Ticket\TicketComment;
use App\Models\User;
use App\Mail\SupportTicketInfo;
use Mail;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TrainerTicketController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainer');

    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $ticket_validator = Validator::make($request->all(), [
                'title' => 'required',
                'category' => 'required',
                'priority' => 'required',
                'message' => 'required',
            ]);

            if ($ticket_validator->fails()) {
                $errors = $ticket_validator->messages();

                //return $errors;
                return back()->withErrors($ticket_validator)->withInput()->with('error_messages', "Error in Creating Ticket");
            }
           
            $ticket = new Ticket([
                'title' => $request->input('title'),
                'user_id' => \Auth::user()->user_id,
                'ticket_id' => strtoupper(str_random(10)),
                'category_id' => $request->input('category'),
                'priority' => $request->input('priority'),
                'message' => base64_encode($request->input('message')),
                'status' => "Open",
            ]);

            $ticket->save();
            DB::commit();

            $email = new SupportTicketInfo(\Auth::user(), $ticket);
            Mail::to(\Auth::user()->email)->send($email);
//            

            return redirect()->to("/trainer/tickets")->with("update", "A ticket with ID: #$ticket->ticket_id has been opened.");
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Saving data');
        }
    }

    public function postComment(Request $request)
    {

        DB::beginTransaction();
        try {

            $comment_validator = Validator::make($request->all(), [
                'comment' => 'required',
            ]);

            if ($comment_validator->fails()) {
                $messages = $comment_validator->messages();

                return back()->withError($messages)->with('error_messages', "Comment Field is Required");
            }
            $comment = TicketComment::create([
                'ticket_id' => $request->input('ticket_id'),
                'user_id' => \Auth::user()->user_id,
                'comment' => base64_encode($request->input('comment')),
            ]);
            DB::commit();
            return back()->with("update", "Your comment has be submitted.");
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Saving data');
        }
    }

    public function create()
    {
        $categories = TicketCategory::all();

        return view('tickets.create', compact('categories'));
    }

    public function show($ticket_id)
    {
        $ticket = Ticket::where('ticket_id', $ticket_id)->first();
        // return $ticket;
        if (!empty($ticket)) {
            $comments = $ticket->comments;
            // return $comments;
            $category = $ticket->category;

            return view('tickets.show', compact('ticket', 'category', 'comments'));
        }
        return redirect()->to('/trainer/tickets')->with('error_messages', 'This Ticket does not exist');
    }

    public function userTickets()
    {
        $tickets = Ticket::where('user_id', \Auth::user()->user_id)->paginate(10);
        $categories = TicketCategory::all();

        return view('tickets.user_tickets', compact('tickets', 'categories'));
    }

}
