<?php

return [

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'name' => 'Name',
    'Email' => 'Email',
    'Choose Sector'=>'Choose Sector',
    'reg'=>'Registration',
    
    

    
    'Register' => 'Register',
    'Sign In' => 'Sign In',
    'Remember me' => 'Remember me',
    'FPassword' => 'Forgot Password',
    'Password' => 'Password',
    

    'Search Courses'=>'Search Courses',
    'My Courses'=>'My Courses',

    
    'CHAT'=>'CHAT',
    'Send'=>'Send',
    
    'My Tickets'=>'My Tickets',
    'Post Ticket'=>'Post Ticket',
    


   
    'Filter' => 'Filter:',
    'By Date' => 'By Date',
    'Show all' => 'Show all',
    'today' => 'Today',
    'This month'=>'This month',
    'This year' => 'This year',
    'By Status' => 'By Status',
    'Active' => 'Active',
    'De-active' => 'De-active',
    'Edit Course' => 'Edit Course',
    'Delete Course' => 'Delete Course',
    'Status' => 'Status',
    'empty' => 'No Course to show',
    'Active' => 'Active',
    
    'T|T' => 'Total Tweets',
    'NT&T'=> 'New Trainer and Trainee in 7 Days',
    'NCourses'=> 'New Courses in 7 Days',
    'LTrainers' => 'Latest Trainers',
    'LTrainees'=> 'Latest Trainees',
    'Top Courses'=> 'Top Courses',
    'Image'=> 'Image',
    
   
    
   
  
    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",

    'about' => 'About',
    'CPass' => 'Change Password',
    'Email Address' => 'Email Address',
    'mobile' => 'Mobile Number',
    'fname' => 'Full Name',
    'gender'=> 'Gender',
    'BDate'=> 'Birth Date',
    'MStatus' => 'Marital Status',
    'basicInfo'=> 'Basic Information',
    'tag'=> 'Tagline',
    'contactInfo'=> 'Contact Information',
    'address'=> 'Address',
    'work'=> 'Work',
    'education'=> 'Education',
    'skills'=>'Skills',
    'occupation'=>'Occupation',
    'OPass'=>'Old Password',
    'NPass'=>'New Password',
    'RPass'=>'Repeat New Password',
    'male'=>'Male',
    'finish'=>'Finish',
    'female'=>'Female',
    'married'=>'Married',
    'unmarried'=>'Unmarried',

    'Sign up' => 'Sign up. It is fast and easy.',
    'yourname' => 'Your name',
    'Your Email Address' => 'Your Email Address',
    'CPassword' => 'Choose Password',
    'ConPassword' => 'Confirm Password',
    'Who'=> 'Who you are',
    'SelectS|C'=> 'Select Sextor or Category',
    'read' => 'I read and Accept',
    'T&C'=> 'Terms & Conditions.',
    
    
    
    'Trainee'=> 'Trainee',
    'Profile' => 'Profile',
    'Noti'=> 'Notifications',
    'Msg'=> 'Messages',
    
    
    'ContactSupport'=> 'Contact Support',
    'Logout'=> 'Log Out',
    'Profile'=> 'Profile',
    

    
    'tickets' => 'Tickets',
    'empty' => 'There are currently no tickets.',
    


    
    'info'=>'Show _START_ of _END_ entries',
    'cancel'=>'Cancel',
    'lengthMenu'=>'Display _MENU_ records',
    'next'=>'Next',
    'previous'=>'Previous',
    

    
];