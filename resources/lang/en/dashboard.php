<?php

return [

   

    'TCourses' => 'Total Courses',
    'TTrainers' => 'Total Trainers',
    'TTrainees' => 'Total Trainees',
    'month' => 'This Month',
    'T|T' => 'Total Tweets',
    'NT&T'=> 'New Trainer and Trainee in 7 Days',
    'NCourses'=> 'New Courses in 7 Days',
    'LTrainers' => 'Latest Trainers',
    'LTrainees'=> 'Latest Trainees',
    'TCourses'=> 'Top Courses',
    'Image'=> 'Image',
    'Name'=> 'Name',
    'Email'=> 'Email',
    'Sector'=> 'Sector',
    'Join'=>'Joined',
    'Sta'=>'Status',
    'UInfo'=>'User Info',
    'Ccate'=>'Course Category',
    'CTitle'=>'Course Title',
    'created'=>'Created',
    'st'=>'Statistics',
    'TraineeMngt'=>'Trainee Management',
    'TrainerMngt'=>'Trainer Management',
    

];
