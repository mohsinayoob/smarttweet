@extends('layouts.trainer_layout') @section('title', 'User Reports') @section('css-files')
<link rel="stylesheet" href="{{URL::asset('files/assets/pages/chart/radial/css/radial.css')}}" type="text/css" media="all">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
<style>
	
    .user-image-in-card{
        vertical-align: top;
    }
</style>
@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-header card">
					<div class="card-block">
						<h5 class="m-b-10">User Reports</h5>
					</div>
				</div>
				<div class="page-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="card list-view-media">
								<div class="card-header">
                                    
									<div class=" f-left">

										<a href="#!">
											<img class="img-radius img-40 user-image-in-card" src="{{$reports[0]->profile_image && $reports[0]->profile_image ? URL::asset('/trainee/profile_image/'.$reports[0]->profile_image): URL::asset('files/assets/images/social/img1.jpg')}}" alt="chat-user">
										</a>
										<div class="project-contain d-inline-block">
											<h6>{{sizeof($reports)>0 && $reports[0]->name? $reports[0]->name : ''}}</h6>
											<p class="text-muted">
												<i class="fa fa-clock-o f-12 m-r-10"></i>Joined {{sizeof($reports)>0 && $reports[0]->created_at? $reports[0]->created_at : ''}}</p>
										</div>
									</div>
									
								</div>

							</div>
						</div>
						<div class="col-md-12 col-xl-12">
							<div class="card review-project">
								<div class="card-header">
									<div class="card-header-left">

									</div>
									<div class="card-header-right">

									</div>
								</div>
								<div class="card-block p-t-0 p-b-0 w-100">
									<div class="table-responsive">
										<table class="table table-hover" id="report-table">
											<thead>
												<tr>
													<th>Course</th>
													<th>Participation</th>
													<th>Favorite Comments</th>
													<th>Liked Comments</th>
													<th>Disliked Comments</th>
												</tr>
											</thead>
											<tbody>
												@foreach($reports as $report)
												<tr>
													<td>

														<div class="project-contain">
															<h6>{{$report->course_title}}</h6>
															<p class="text-muted">
																<i class="fa fa-clock-o f-12 m-r-10"></i>Joined {{$report->created_at}}</p>
														</div>
													</td>
													<td>
														<span>{{$report->trainees_comments>0 ? ($report->trainees_comments/$report->total_comments *100)."%" :'0%' }}
														</span><span class="pie_2" style="display: none;">{{$report->trainees_comments}}/{{$report->total_comments}}</span>
													</td>
													<td>
														{{$report->favorite_count}}
													</td>
													<td>
														{{$report->like_count}}
													</td>
													<td>
														{{$report->dislike_count}}
													</td>
												</tr>
												@endforeach
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')
<script src="../files/bower_components/peity/js/jquery.peity.js"></script>
<script src="{{URL::asset('files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>

<script >
		$("span.pie_2").peity("pie", {
            fill: ["#FFB64D", "#4099ff"]
		});
		var table=$('#report-table').DataTable();
</script>
@stop