<?php

namespace App\Models\Tweet;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Models\Tweet\TweetContent;
class Tweets extends Model
{
    //
    
    use Notifiable;
    protected $table = 'tweets';
    protected $primaryKey = 'tweet_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['course_id', 'topic_id','user_id', 'tweet_description', 'content_type', 'status', 'total_views', 'total_likes', 'total_favorite','created_at','updated_at', 'active_time'];
    public function tweet_content()
    {
        return $this->hasMany('App\Models\Tweet\TweetContent','tweet_id');
    }
}
