<?php

namespace App\Models\Tweet;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CommentReaction extends Model
{
    use Notifiable;
    protected $table = 'comments_reaction';
    protected $primaryKey = 'comment_reaction_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['tweet_id','comment_id',  'user_id', 'comment_reaction_content', 'created_at', 'updated_at', 'status'];
}
