<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\AdminProfile;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;

class AdminProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        
    }

    protected function create(array $data)
    {
        $existProfile = AdminProfile::where('user_id', \Auth::user()->user_id)->first();
        if (!empty($existProfile)) {
            $data && array_key_exists('gender', $data) ? $existProfile->gender = $data['gender'] : $existProfile->gender;
            $data && array_key_exists('dob', $data) ? $existProfile->dob = $data['dob'] : $existProfile->dob;
            $data && array_key_exists('martial_status', $data) ? $existProfile->martial_status = $data['martial_status'] : $existProfile->martial_status;
            $data && array_key_exists('tag_line', $data) ? $existProfile->tag_line = $data['tag_line'] : $existProfile->tag_line;

            /**************************** Second Form *******************************/
            $data && array_key_exists('mobile_no', $data) ? $existProfile->mobile_no = $data['mobile_no'] : $existProfile->mobile_no;
            $data && array_key_exists('address', $data) ? $existProfile->address = $data['address'] : $existProfile->address;

            /**************************** Third Form *******************************/
            $data && array_key_exists('skills', $data) ? $existProfile->skills = $data['skills'] : $existProfile->skills;
            $data && array_key_exists('last_completed_degree', $data) ? $existProfile->last_completed_degree = $data['last_completed_degree'] : $existProfile->last_completed_degree;
            $data && array_key_exists('current_occupation', $data) ? $existProfile->current_occupation = $data['current_occupation'] : $existProfile->current_occupation;

            $existProfile->save();
            return $existProfile;
        }
        $newProfile = array();
        $data['user_id'] = \Auth::user()->user_id;
        // $data && array_key_exists( 'gender' , $data ) ? newProfile = $data['gender'] : $existProfile->gender ;
        return AdminProfile::create($data);
    }

    public function updateProfile(Request $request)
    {
        // $this->validator($request->all())->validate();

        //event(new Registered($user = $this->create($request->all())));

        DB::beginTransaction();
        try
        {

            $user = $this->create($request->all());
            // After creating the user send an email with the random token generated in the create method above

            DB::commit();
            Session::flash('update', 'Data Saved Successfully');
            return redirect('admin/profile')->with('update', 'Data Successfully Saved');
        } catch (Exception $e) {
            DB::rollback();
            return redirect('admin/profile')->with('message', 'Error in Saving data');
        }

    }

    //********************************Update Password **************** */
    public function updatePassword(Request $request)
    {
        // $this->validator($request->all())->validate();
        $data = $request->all();
        DB::beginTransaction();
        try
        {
            $validator = $this->passwordValidator($data);;
            if ($validator->fails()) {
                $messages = $validator->messages();
                 return redirect('admin/change-password')->withErrors($messages);
            }
            //$user = $this->changePassword($request->all());
            // After creating the user send an email with the random token generated in the create method above
            //$user = User::where('otp',\Auth::user()->email)->first();
            \Auth::user()->password=bcrypt($data['password']);
            \Auth::user()->save();
            DB::commit();
            Session::flash('update', 'Passoword Changed Successfully');
            return redirect('admin/change-password')->with('update', 'Passoword Changed Successfully');
        } catch (Exception $e) {
            DB::rollback();
            return redirect('admin/change-password')->with('update_error', 'Passoword not changed Successfully');
        }

    }

    protected function passwordValidator(array $data)
    {
        Validator::extend('pwdvalidation', function ($attribute, $value, $parameters, $validator) {

            return Hash::check($value, \Auth::user()->password);

        });
        $messages = array('pwdvalidation' => 'The Old Password is Incorrect');
        return Validator::make($data, [
            'old_password' => 'required|pwdvalidation',
            'password' => 'required|string|min:6|confirmed|different:old_password',
        ], $messages);
    }

    public function updateCoverImage(Request $request)
    {
       
        $image_validator =  Validator::make($request->all(), [
            'cover_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        if($image_validator->fails())
        {
            $messages = $image_validator->messages();
            
            return back()->withError($messages)->with('error_messages',$messages->first('cover_image'));
        }

        $imageName = time().'.'.$request->cover_image->getClientOriginalExtension();
        $image_uploaded = $request->cover_image->move(public_path('admin/cover_image'), $imageName);
        if($image_uploaded)
        {
            DB::beginTransaction();
            $existProfile = AdminProfile::where('user_id', \Auth::user()->user_id)->first();
            if (!empty($existProfile)) {
                $existProfile->cover_image = $imageName;
                $existProfile->save();
                DB::commit();
            }
            else{
                $new_trainer = array('cover_image'=>$imageName, 'user_id'=> \Auth::user()->user_id);
                AdminProfile::create($new_trainer);
                DB::commit();
            }
            return back()
    		->with('update','Image Uploaded successfully.')
    		->with('path',$imageName);
        }
        return back()
    		->withError('error_messages','Unable to Upload image.');
    	
    }
    public function updateProfileImage(Request $request)
    {

        $image_validator = Validator::make($request->all(), [
            'profile_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        if ($image_validator->fails()) {
            $messages = $image_validator->messages();

            return back()->withError($messages)->with('error_messages', $messages->first('profile_image'));
        }

        $imageName = time() . '.' . $request->profile_image->getClientOriginalExtension();
        $image_uploaded = $request->profile_image->move(public_path('admin/profile_image'), $imageName);
        if ($image_uploaded) {
            DB::beginTransaction();

            \Auth::user()->profile_image = $imageName;
            \Auth::user()->save();
            DB::commit();
            return back()
                ->with('update', 'Image Uploaded successfully.')
                ->with('path', $imageName);
        }
        return back()
            ->withError('error_messages', 'Unable to Upload image.');

    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = AdminProfile::where('user_id', \Auth::user()->user_id)->first();
        //return view('admin.profile', $profile);
        return view('admin.profile', compact('profile'));
    }
}
