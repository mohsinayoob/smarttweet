<?php

return [

   

    'edit' => 'Edit Trainee',
    'editT' => 'Edit Trainer',
    'editInfo' => 'Edit Innformation',
    'certificate' => 'Certificate',
    'id' => 'User-Id',
    'sector' => 'Please Select Sector',
    'TName'=> 'Trainee Name',
    'TrainerName'=> 'Trainer Name',
    'info'=>'Show _START_ of _END_ entries',
    'save'=>'Save',
    'cancel'=>'Cancel',
    'search'=>'Search',
    'lengthMenu'=>'Display _MENU_ records',
    'next'=>'Next',
    'previous'=>'Previous',
    'status'=>'Trainee status Updated Successfully',
    'statustrainer'=>'Trainer status Updated Successfully',

  

];
