<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Course\Category;
use DB;
use Illuminate\Support\Facades\Validator;
use Session;

class AdminCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    public function AddCategory(Request $request)
    {
        

        DB::beginTransaction();
        try
        {
            $category_validator = Validator::make($request->all(), [
                'category_title' => 'required',
            ]);
    
            if ($category_validator->fails()) {
                $messages = $category_validator->messages();
    
                return back()->withError($messages)->with('error_messages', $messages->first('category_title'));
            }
            Category::create($request->all());
            // After creating the user send an email with the random token generated in the create method above

            DB::commit();
            Session::flash('update', 'Data Saved Successfully');
            return back()->with('update', 'Data Successfully Saved');
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Saving data');
        }

    }

    public function UpdateCategory(Request $request)
    {
        
        DB::beginTransaction();
        try
        {
            $category_validator = Validator::make($request->all(), [
                'category_title' => 'required',
                'category_id' => 'required',
            ]);
    
            if ($category_validator->fails()) {
                $messages = $category_validator->messages();
    
                return back()->withError($messages)->with('error_messages', 'An Error in Updating Category');
            }
           // Category::create();
            $data = $request->all();
            $existCategory = Category::where('category_id', $data['category_id'])->first();
            $existCategory->category_title = $data['category_title'];
            $existCategory->save();
            // After creating the user send an email with the random token generated in the create method above

            DB::commit();
            Session::flash('update', 'Category Saved Successfully');
            return back()->with('update', 'Category Successfully Saved');
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Saving Category');
        }

    }

    public function DeleteCategory(Request $request)
    {
        
        DB::beginTransaction();
        try
        {
            $category_validator = Validator::make($request->all(), [
                'category_id' => 'required',
            ]);
    
            if ($category_validator->fails()) {
                $messages = $category_validator->messages();
    
                return back()->withError($messages)->with('error_messages', 'An Error in Deleting Category');
            }
           // Category::create();
            $data = $request->all();
            $existCategory = Category::where('category_id', $data['category_id'])->first();
            $existCategory->delete();
            // After creating the user send an email with the random token generated in the create method above

            DB::commit();
            Session::flash('update', 'Category Deleted Successfully');
            return back()->with('update', 'Category Successfully Deleted');
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Deleted Category');
        }

    }

    public function index()
    {
        $categories = Category::all();
        //return $categories;
        //return view('admin.profile', $profile);
        return view('admin.manage-categories', compact('categories'));
    }
}
