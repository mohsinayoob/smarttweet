<!DOCTYPE html>
<html lang="en-us" class="no-js">
<head>
<meta charset="utf-8">
<title>Smart Tweet</title>
<meta name="description" content="Smart Tweet" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Smart Tweet">

<link rel="shortcut icon" href="{{URL::asset('files/assets/images/favicon.ico')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/error-page/css/style.css')}}" />
</head>
<body>
<div class="image"></div>

<a href="/" class="logo-link" title="back home">
<img src="{{URL::asset('files/assets/images/logo.png')}}" class="logo" alt="Company's logo" />
</a>
<div class="content">
<div class="content-box">
<div class="big-content">

<div class="list-square">
<span class="square"></span>
<span class="square"></span>
<span class="square"></span>
</div>

<div class="list-line">
<span class="line"></span>
<span class="line"></span>
<span class="line"></span>
<span class="line"></span>
<span class="line"></span>
<span class="line"></span>
</div>

<i class="fa fa-search" aria-hidden="true"></i>

<div class="clear"></div>
</div>

<h1>{{isset($error_heaing)? $error_heaing : "Oops! Error 404 not found."}}</h1>
<p>{{isset($error_description) ? $error_description : "The page you were looking for doesn't exist.
    We think the page may have moved."}}</p>
</div>
</div>
<footer class="light">
<ul class="text-center">
<li><a href="/"><h3>Go Back to Home</h3></a></li>

</ul>
</footer>
<script src="{{URL::asset('files/bower_components/jquery/js/jquery.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/popper.js/js/popper.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
</body>
</html>