<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Tweet\Tweets;
use App\Models\Tweet\Comments;
use App\Jobs\CommentNotificationJob;
use DB;
use Session;

class AdminCommentController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        
    }


    public function pictureUpload(Request $request)
    {
        $imageName = md5(time()).'.'.$request->file->getClientOriginalExtension();
        $image_uploaded = $request->file->move(public_path('comment_content'), $imageName);
        if($image_uploaded)
        { 
            return $imageName;
        }
        return 'erorr';
    }

    public function saveComment($topic_id,$tweet_id, Request $request)
    {
        //return $request;
        $data = $request->all();
        DB::beginTransaction();
        try
        {    
            // return $data['tweet_description'];
            $tweet = Tweets::where('tweet_id',$tweet_id)->first();
            if(empty($tweet))
            {
                return "Tweet Does not exist";
            }
            $comment = $data;
            $comment['tweet_id'] = $tweet_id;
            $comment['status'] = 'Active';
            $comment['comment_description'] = base64_encode($data['comment_description']);
            $comment['user_id'] = \Auth::user()->user_id;
            //return $comment;
            $newComment = Comments::create($comment);
            $tweet->total_comments +=1;
            $tweet->save();
            DB::commit();
            $notification_job = (new CommentNotificationJob($newComment))->delay(13);
            $this->dispatch($notification_job);
            return response()->json([
                'data' => [
                  'success' => 'true',
                  'comment'=>$newComment,
                  'total_likes'=> $tweet['total_likes'],
                'total_favorites'=> $tweet['total_favorites'],
                'total_dislikes'=> $tweet['total_dislikes'],
                'total_comments'=> $tweet['total_comments'],
                  
                ]
              ]);
            
        }
        catch(Exception $e) 
        {
            DB::rollback();
            return response()->json([
                'data' => [
                  'success' => 'false',

                ]
              ]);
        }

    }



    ///////

    public function getMoreComments($topic_id, $tweet_id)
    {
        
        $comments = Comments::join('users','users.user_id','=','comments.user_id')
        ->where('comments.tweet_id',$tweet_id)
        ->select('comments.*','users.name','users.profile_image', 'users.user_type')
        ->orderBy('comments.comment_id','ASC')->paginate(10);

        if($comments)
        {
            return response()->json([
                  'success' => 'true',
                  'comment'=>$comments,
              ]);
        }
        return response()->json([
            'data' => [
              'success' => 'false',
            ]
          ]);
        
    }
}
