@extends('layouts.'.\Auth::user()->user_type.'_layout')
@section('title', 'My Tickets')
 @section('css-files')

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}">

 @stop 
 @section('body_content')
 <div class="pcoded-content">
        <div class="pcoded-inner-content">
    
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header card">
                        <div class="card-block">
                            <div class="card-header-right f-right">
                                <a href="/{{\Auth::user()->user_type}}/ticket/create-ticket"class="btn btn-warning btn-round" >Post Ticket</a>
                            </div>
                            <h5 class="m-b-10">Tickets</h5>
                           
                        </div>
                        
                    </div>
                    <div class="page-body">
                        <div class="row">
                                <div class="col-md-12 col-md-offset-1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <i class="fa fa-ticket"> My Tickets</i>
                                        </div>
                        
                                        <div class="panel-body">
                                            @if ($tickets->isEmpty())
                                                <p>You have not created any tickets.</p>
                                            @else
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Category</th>
                                                            <th>Title</th>
                                                            <th>Status</th>
                                                            <th>Last Updated</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($tickets as $ticket)
                                                        <tr>
                                                            <td>
                                                            @foreach ($categories as $category)
                                                                @if ($category->id === $ticket->category_id)
                                                                    {{ $category->name }}
                                                                @endif
                                                            @endforeach
                                                            </td>
                                                            <td>
                                                                <a href="{{ url(\Auth::user()->user_type.'/tickets/'. $ticket->ticket_id) }}">
                                                                    #{{ $ticket->ticket_id }} - {{ $ticket->title }}
                                                                </a>
                                                            </td>
                                                            <td>
                                                            @if ($ticket->status === 'Open')
                                                                <span class="label label-success">{{ $ticket->status }}</span>
                                                            @else
                                                                <span class="label label-danger">{{ $ticket->status }}</span>
                                                            @endif
                                                            </td>
                                                            <td>{{ $ticket->updated_at }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                <div class="col-sm-12">
                                                        <div class="card">
                                                            <div class="card-block">
                                                                <div class="f-right">
                                                                    {{ $tickets->appends(request()->query())->links("pagination::bootstrap-4") }}
                                                                            
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
			</div>
		</div>
	</div>
</div>

@stop 
@section('javascript-files')

<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>

@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif 

@stop