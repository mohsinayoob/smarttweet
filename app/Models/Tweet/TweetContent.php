<?php

namespace App\Models\Tweet;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TweetContent extends Model
{
    //
    use Notifiable;
    protected $table = 'tweet_content';
    protected $primaryKey = 'content_id';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['tweet_id', 'file', 'content_type', 'status'];
}
