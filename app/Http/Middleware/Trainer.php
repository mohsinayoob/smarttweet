<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Trainer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if(Auth::guard($guard)->user()->user_type=='trainer')
            {
                if (!Auth::guard($guard)->user()->is_active)
                {
                    Auth::logout();
                    return redirect()->to('/login')->with('error_messages', 'Your Account has been found Deactive');
                }
                if (!Auth::guard($guard)->user()->is_verified)
                {
                    Auth::logout();
                    return redirect()->to('/login')->with('error_messages', 'Your Account has been found Un-Verified');
                }
                if($request->has('read')) {
                    $notification = $request->user()->notifications()->where('id', $request->read)->first();
                    if($notification) {
                        $notification->markAsRead();
                    }
                }
                return $next($request);
            }
            else
            {
                $error_heaing = "Oops !! Unauthorized ";
                $error_description = "This page does not exit or You are not authorized to view this page.";
                return response(view('partials.error-page',compact('error_heaing','error_description')));
            }
           
        }
        

        return $next($request);
    }
}
