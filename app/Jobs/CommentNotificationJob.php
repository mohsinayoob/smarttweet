<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Course\Topic;
use App\Models\Course\Course;
use App\Models\Tweet\Tweets;
use App\Models\User;
use App\Models\Course\CourseEnrollment;
class CommentNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $comment;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($comment)
    {
        //
        $this->comment = $comment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $tweet = Tweets::where('tweet_id',$this->comment->tweet_id)->first();
        $course = Course::where('course_id', $tweet->course_id)->first();
        if($tweet)
        {
            $enrollments = CourseEnrollment::join('course','course.course_id','=','course_enrollment.course_id')
            ->where('course_enrollment.course_id',$tweet->course_id)
            ->where('course_enrollment.trainee_id','!=', \Auth::user()->user_id)
            ->where('course_enrollment.status','Active')
            ->select()->get();
        }
        
        
        foreach($enrollments as $enrollment)
        {
           // echo $enrollment;
            $user = User::where('user_id',$enrollment->trainee_id)->first();
            $this->comment->user = \Auth::user();
            $this->comment->url = "/".$user->user_type."/topic/".$enrollment->course_id;
            $this->comment->message = " has posted a Comment on Tweet in ". $enrollment->course_title;
            $this->comment->notification_type = "Comment";
            $user->notify(new \App\Notifications\SaveNotification($this->comment, \Auth::user(), $this->comment->message, $this->comment->url));
        }
        if(\Auth::user()->user_type != 'trainer')
        {
            $user = User::where('user_id', $course->trainer_id)->first();
            $this->comment->user = \Auth::user();
            $this->comment->message = " has posted a Comment on Tweet  in ". $course->course_title;
            $this->comment->notification_type = "Comment";
            $this->comment->url = "/".$user->user_type."/tweet/".$tweet->topic_id."/".$tweet->tweet_id;          
            $user->notify(new \App\Notifications\SaveNotification($this->comment, \Auth::user(), $this->comment->message, $this->comment->url));
        }
        
    }
}
