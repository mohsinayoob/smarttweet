<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Course\Course;
use App\Models\User;
use App\Models\Course\Category;
use App\Models\Course\Topic;
use App\Models\Tweet\Tweets;
use App\Models\Tweet\Comments;
use App\Models\Course\CourseEnrollment;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
//use Illuminate\Support\Facades\Request;
use Session;

class AdminCourseController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        
    }
    public function createCourse (Request $request)
    {
        DB::beginTransaction();
        try{
        $category_validator = Validator::make($request->all(), [
            'course_title' => 'required',
            'course_category' => 'required',
            'course_description' => 'required',
        ]);

        if ($category_validator->fails()) {
            $messages = $category_validator->messages();

            return back()->withError($messages)->with('error_messages', "Error in Validations");
        }
        $data = $request->all();
        
        //$data["trainer_id"] = \Auth::user()->user_id;
        $data["created_by"] = "Admin";
        $data["status"] = "Active";
        //return $data;
        $course = Course::create($data);
        // After creating the user send an email with the random token generated in the create method above

        DB::commit();
        Session::flash('update', 'Course created. Now you can Add Topic here!!');
        return redirect()->to('/admin/topic/'.$course->course_id)->with('update', 'Course created. Now you can Add Topic here!!');
    } catch (Exception $e) {
        DB::rollback();
        return back()->with('error_messages', 'Error in Saving data');
    }

    }

    public function UpdateCourseStatus($course_id, $status)
    {
        
        DB::beginTransaction();
        try
        {
            
           // Category::create();
           
            $existCourse = Course::where('course_id', $course_id)->first();
            if(!empty($existCourse))
            {
                $existCourse->status = $status;
                $existCourse->save();
                DB::commit();
                Session::flash('update', 'Course Status Updated Successfully');
                return back()->with('update', 'Course Status Successfully Updated');
            }
            else
            {
                return back()->with('error_messages', 'Could not find the course');
            }
            
            
            
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Updating Course Status');
        }

    }

    public function index()
    {
        $categories = Category::all();
        $trainers = User::all()->where('user_type', "trainer");
        //return $trainers;
        //return $categories;
        //return view('trainer.profile', $profile);
        return view('admin.create-course', compact('categories','trainers'));
    }


    protected function searchCourses($q,$status,$date)
    {
        if($date == "Today")
        {
            if(!empty($status))
            {
                $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
                ->whereDay('created_at',date('d'))->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
                ->where('course.status', $status)
                ->paginate(10);
                //return $courses;
                return view('admin.manage-course', compact('courses'));
            }
            $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
            ->whereDay('created_at',date('d'))->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
            ->paginate(10);
           // return $courses;
            return view('admin.manage-course', compact('courses'));

        }
        else if($date == "Month")
        {
            if(!empty($status))
            {
                $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
                ->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
                ->where('course.status', $status)
                ->paginate(10);
               // return $courses;
                return view('admin.manage-course', compact('courses'));
            }
            $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
            ->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
            ->paginate(10);
            //return $courses;
            return view('admin.manage-course', compact('courses'));

        }
        else if($date == "Year")
        {
            if(!empty($status))
            {
                $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
                ->whereYear('created_at',date('Y'))
                ->where('course.status', $status)
                ->paginate(10);
                //return $courses;
                return view('admin.manage-course', compact('courses'));
            }
            $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
            ->whereYear('created_at',date('Y'))
            ->paginate(10);
            //return $courses;
            return view('admin.manage-course', compact('courses'));

        }
        if(!empty($status))
        {
            $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
            ->where('course.status', $status)
            ->paginate(10);
            // return $courses;
            return view('admin.manage-course', compact('courses'));
        }
        $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
        ->paginate(10);
       // return $courses;
        return view('admin.manage-course', compact('courses'));
    }

    public function ViewCourses()
    {
        $q = Input::get('q'); 
        $status = Input::get('status'); 
        $date = Input::get('date'); 
        if (!empty($q) || !empty($status) || !empty($date)) {
           return $this->searchCourses($q,$status,$date);
        }
        $courses = Course::paginate(10);
        //return $courses;
        //return $courses->perPage();
        //return view('trainer.profile', $profile);
        return view('admin.manage-course', compact('courses'));
    } 

    public function ViewEditCourses($course_id)
    {
        
        
        $editcourses = Course::where('course_id', $course_id)->first();
        if(!empty($editcourses)){
        $categories = Category::all();
        //return $courses;
        //return view('trainer.profile', $profile);
        return view('admin.edit-course', compact('editcourses','categories'));
    }
    return redirect()->to('/admin/manage-course')->with('error_messages','Could not find the Course');
    } 

    public function UpdateCourse($course_id, Request $request)
    {
        $data = $request->all();
        //return $data;
        $course_validator = Validator::make($data , [
            'course_title' => 'required',
            'course_description' => 'required',
            'course_category' => 'required',
        ]);

        if ($course_validator->fails()) {
            $messages = $course_validator->messages();
//return $messages;
            return back()->withError($messages)->with('error_messages', "Topic and title are required.");
        }
        DB::beginTransaction();
        try
        {
            
           // Category::create();
           
            $existCourse = Course::where('course_id', $course_id)->first();
            if(!empty($existCourse))
            {
                $existCourse->course_title=$data['course_title'];
                $existCourse->course_description=$data['course_description'];
                $existCourse->course_category=$data['course_category'];
                //$existCourse->status = $status;
                $existCourse->save($data);
                DB::commit();
                Session::flash('update', 'Course Updated Successfully');
                return redirect()->to('/admin/manage-course')->with('update', 'Course Successfully Updated');
            }
            else
            {
                return back()->with('error_messages', 'Error in Updating Course');
            }
            
            
            
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Updating Course Status');
        }

    }
    public function DeleteCourse($course_id)
    {
        //return $topic_id;
        DB::beginTransaction();
        try
        {
           // $existCourse = Course::where('course_id', $course_id)->first();
            $existTopic = Topic::where('course_id', $course_id)->select('topic_id')->get();
           //return $existTopic;
            $existTweets= Tweets::whereIn('topic_id', $existTopic)->select('tweet_id')->get();
            //return $existTweets;
         
            $existComments= DB::table('comments')->whereIn('tweet_id', $existTweets)->delete();
            //return $existComments; 
             $existTweetsdel= Tweets::whereIn('topic_id', $existTopic)->delete();
             $existTopicsdel= Topic::where('course_id', $course_id)->delete();
             $existEnrollmentsdel= CourseEnrollment::where('course_id', $course_id)->delete();
             $existCoursedel = Course::where('course_id', $course_id)->delete();
             //return $existCoursedel; 
             DB::commit();
            
            // After creating the user send an email with the random token generated in the create method above

           
            Session::flash('update', 'Course Deleted Successfully');
            return back()->with('update', 'Course Successfully Deleted');
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Deleted Course');
        }

    }
}
