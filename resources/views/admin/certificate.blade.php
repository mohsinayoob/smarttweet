@extends('layouts.admin_layout') @section('title', 'Certificate') @section('css-files')
<link rel="stylesheet" href="{{URL::asset('files/assets/css/certificate.css')}}" type="text/css" media="all">
<style>
	
</style>
@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-body" id="page-body">
					<div class="container">

						<div>

							<div class="card" id="certificate-border">
								<div class="row invoice-contact">
									<div class="col-md-12">
										<div class="invoice-box row">
											<div class="col-sm-12 text-center">
												<img src="{{URL::asset('files/assets/images/logo-certificate.png')}}" class="m-b-10" alt="">
											</div>
										</div>
									</div>
								</div>
								<div class="row invoice-contact">
									<div class="col-md-12">
										<div class="invoice-box row">
											<div class="col-sm-12 text-center">
												<h3>Certificate of Achivement</h3>
											</div>
										</div>
									</div>
								</div>
								<div class="row invoice-contact">
									<div class="col-md-12">
										<div class="invoice-box row">
											<div class="col-sm-12 text-center">
												<h4>This is to certify that</h4>
											</div>
											<div class="col-sm-12 text-center">
												<h1>{{$trainees->name}}</h1>
											</div>
										</div>
									</div>
								</div>
								<div class="row invoice-contact">
									<div class="col-md-12">
										<div class="invoice-box row">
											<div class="col-sm-12 text-center">
												<h6 class="lineheight" style="display:inline">has completed the course of <h5 style="display:inline">{{$courses->course_title}}</h5>, in the sector of <h5 style="display:inline">{{$courses->course_category}}</h5>.</br>This course was offered by SmartTweet.</h6>
											</div>
										</div>
									</div>
								</div>
								<div class="row invoice-contact">
									<div class="col-md-12">
										<div class="invoice-box row">
											<div class="col-sm-4 text-center">
												<h6>
													<hr class='hr-margin'>Signature:</h6>
											</div>
											<div class="col-sm-4 text-center">
												<img src="{{URL::asset('files/assets/images/stamp-certified.png')}}">
											</div>
											<div class="col-sm-4 text-center">
												<h6>
													<hr class='hr-margin'>Date:<span style="position: absolute;bottom: 50px;left: 126px;">{{date('d-m-Y')}}</span></h6>
											</div>
										</div>
									</div>
								</div>

							</div>

							<div class="row text-center">
								<div class="col-sm-12 invoice-btn-group text-center">
									<button type="button" class="btn btn-primary btn-print-invoice m-b-10 btn-sm waves-effect waves-light m-r-20" id="printing">Print</button>
									<button type="button" class="btn btn-danger waves-effect m-b-10 btn-sm waves-light"><a href='/admin/trainee' >Cancel</a></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop 
@section('extra-outer-content')

<div class="card certificate-printable" >
	<div class="row invoice-contact">
		<div class="col-md-12">
			<div class="invoice-box row">
				<div class="col-sm-12 text-center">
					<img src="{{URL::asset('files/assets/images/logo-certificate.png')}}" class="m-b-10" alt="">
				</div>
			</div>
		</div>
	</div>
	<div class="row invoice-contact">
		<div class="col-md-12">
			<div class="invoice-box row">
				<div class="col-sm-12 text-center">
					<h3>Certificate of Achivement</h3>
				</div>
			</div>
		</div>
	</div>
	<div class="row invoice-contact">
		<div class="col-md-12">
			<div class="invoice-box row">
				<div class="col-sm-12 text-center">
					<h4>This is to certify that</h4>
				</div>
				<div class="col-sm-12 text-center">
					<h1>{{$trainees->name}}</h1>
				</div>
			</div>
		</div>
	</div>
	<div class="row invoice-contact">
		<div class="col-md-12">
			<div class="invoice-box row">
				<div class="col-sm-12 text-center">
					<h6 class="lineheight" style="display:inline">has completed the course of <h5 style="display:inline">{{$courses->course_title}}</h5>, in the sector of <h5 style="display:inline">{{$courses->course_category}}</h5>.</br>This course was offered by SmartTweet.</h6>
				</div>
			</div>
		</div>
	</div>
	<div class="row invoice-contact">
		<div class="col-md-12">
			<div class="invoice-box row">
				<div class="col-sm-4 text-center">
					<h6>
						<hr class='hr-margin'>Signature:</h6>
				</div>
				<div class="col-sm-4 text-center">
					<img src="{{URL::asset('files/assets/images/stamp-certified.png')}}">
				</div>
				<div class="col-sm-4 text-center">
					<h6>
						<hr class='hr-margin'>Date:<span style="position: absolute;bottom: 50px;left: 126px;">{{date('d-m-Y')}}</span></h6>
				</div>
			</div>
		</div>
	</div>

</div>
@stop @section('javascript-files')

<script>
	$(document).ready(function() {
		$('#printing').click(function () {
			window.print();	
		});
	});

</script>
@stop