<?php

return [

   
    'MCourses' => 'Manage Courses',
    'Filter' => 'Filter:',
    'By Date' => 'By Date',
    'Show all' => 'Show all',
    'today' => 'Today',
    'This month'=>'This month',
    'This year' => 'This year',
    'By Status' => 'By Status',
    'Active' => 'Active',
    'De-active' => 'De-active',
    'Edit Course' => 'Edit Course',
    'Delete Course' => 'Delete Course',
    'Status' => 'Status',
    'empty' => 'No Course to show',
    'Active' => 'Active',
    'title'=> 'Are you sure?',
    'text'=> 'This Course will be deleted',
    'delete'=>'Yes,delete it!',
    'cancel'=>'Cancel',
  
];
