<?php

namespace App\Http\Controllers\Trainer;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Tweet\Tweets;
use App\Models\Tweet\Comments;
use DB;
use Session;

class TrainerDashboardController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainer');
        
    }

    public function index()
    {
        $comments =  Comments::with(['comment_reaction_content' => function($query)
        {
            $query->where('user_id', \Auth::user()->user_id);
        
        }])
        ->join('comments_reaction','comments_reaction.comment_id','=','comments.comment_id')
        ->join('users','users.user_id','=','comments.user_id')
        ->join('tweets','tweets.tweet_id','=','comments.tweet_id')
        ->where('comments_reaction.user_id', \Auth::user()->user_id)

        ->where('comments_reaction.comment_reaction_content','favorite')
        ->select('comments.*','users.name','users.profile_image', 'users.user_type','users.user_id','tweets.topic_id')
        ->paginate(10);
        //return $comments;
        return view('trainer.favorite-list',compact('comments'));
    }
}
