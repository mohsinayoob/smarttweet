<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Course\Topic;
use App\Models\Course\Course;
use App\Models\Tweet\Tweets;
use App\Models\User;
use App\Models\Course\CourseEnrollment;
class TweetReactionNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tweet_reaction;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tweet_reaction)
    {
        //
        $this->tweet_reaction = $tweet_reaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $tweet = Tweets::where('tweet_id',$this->tweet_reaction->tweet_id)->first();
        $course = Course::where('course_id', $tweet->course_id)->first();
        if($tweet && \Auth::user()->user_type!='trainer')
        {
            $user = User::where('user_id', $course->trainer_id)->first();
            $this->tweet_reaction->user = \Auth::user();
            $this->tweet_reaction->message = " reacted on Tweet  in ". $course->course_title;
            $this->tweet_reaction->url = "/".$user->user_type."/tweet/".$tweet->topic_id."/".$tweet->tweet_id;          
            $user->notify(new \App\Notifications\SaveNotification($this->tweet_reaction, \Auth::user(), $this->tweet_reaction->message, $this->tweet_reaction->url));
        }
        
    }
}
