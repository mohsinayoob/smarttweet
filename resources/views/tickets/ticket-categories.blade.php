@extends('layouts.admin_layout') @section('title', 'Categories') @section('css-files')
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/css/component.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/notification/notification.css')}}"> @stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-header card">
					<div class="card-block">
						<h5 class="m-b-10">Manage Support Ticket Catagories</h5>
					</div>
				</div>
				<div class="page-body">
					<div class="md-modal md-effect-13 addcontact" id="modal-add-category">
						<div class="md-content">
							<h3 class="f-26">Add Category</h3>
							<div>
								<form method="POST" action='/admin/ticket/add-category'>
									{{ csrf_field() }}
									<div class="input-group">
										<input type="text" class="form-control pname" placeholder="Category Name" name="name" id="add-category">
									</div>
									<div class="text-center">
										<button type="submit" class="btn btn-primary waves-effect m-r-20 f-w-600 d-inline-block save_btn waves-effect">Save</button>
										<button type="button" class="btn btn-primary waves-effect m-r-20 f-w-600 md-close d-inline-block close_btn">Close</button>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="md-modal md-effect-13 addcontact" id="modal-13">
						<div class="md-content">
							<h3 class="f-26">Edit Category</h3>
							<div>
								<form method="POST" action='/admin/ticket/edit-categories'>
									{{ csrf_field() }}
									<div class="input-group">
										<input type="hidden" name="id" id="edit_id" />
										<input type="text" class="form-control pname" placeholder="Category Name" name="name" id="edit-category">
									</div>
									<div class="text-center">
										<button type="submit" class="btn btn-primary waves-effect m-r-20 f-w-600 d-inline-block save_btn">Save</button>
										<button type="button" class="btn btn-primary waves-effect m-r-20 f-w-600 md-close d-inline-block close_btn">Close</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="md-overlay"></div>

					<div class="card">
						<div class="card-header">
							<button class="btn btn-success btn-round md-trigger" data-modal="modal-add-category">Add Category</button>
						</div>
						<div class="card-block">
							<div class="table-responsive dt-responsive">
								<table id="dom-jqry" class="table table-striped table-bordered nowrap">
									<thead>
										<tr>
											<th>Serial</th>
											<th>Detail</th>
											<th>Edit</th>
											<th>Delete</th>
										</tr>
									</thead>
									<tbody>
										@foreach($categories as $category)
										<tr>
											<td>{{$category->id}}</td>
											<td>
												<a href="#">{{$category->name}}</a>
											</td>
											<td>
												<a href="#" class="m-r-15 text-muted md-trigger" data-modal="modal-13">
													<i class="icofont icofont-ui-edit"></i>
												</a>

											</td>
											<td>
												<a class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="deleteCategory(this.id)"
												id="{{$category->id}}">
													<i class="icofont icofont-delete-alt"></i>
												</a>
											</td>
										</tr>
										@endforeach

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form method="post" action="/admin/ticket/delete-category" id="delete_category_form">
	{{ csrf_field() }}
<input type="hiddden" name="id" id="delete_id" />
</form>
@stop @section('javascript-files')
<script src="{{URL::asset('files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('files/assets/js/classie.js')}}"></script>
<script src="{{URL::asset('files/assets/js/modalEffects.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}">
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/animate.css/css/animate.css')}}"> @if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");
</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");
</script>

@endif
<script>
	var table=$('#dom-jqry').DataTable();

        $('#dom-jqry tbody').on( 'click', 'a', function () {
			var data = table.row( $(this).parents('tr') ).data();
			var name = $(data[ 1 ]).text();
			var id = data[ 0 ];			
			$('#edit-category').val(name);
            $('#edit_id').val(id);			
		});
	
	function deleteCategory(id){
		//alert(id);
		$("#delete_id").val(id);
		swal({
            title: "Are you sure?",
            text: "This category will be deleted",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function() {
            $('#delete_category_form').submit();
        });
	}
</script>
@stop