<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Course\Topic;
use App\Models\Course\Course;
use App\Models\Tweet\Tweets;
use App\Models\Tweet\Comments;
use App\Models\User;
use App\Models\Course\CourseEnrollment;
class CommentReactionNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $comment_reaction;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($comment_reaction)
    {
        //
        $this->comment_reaction = $comment_reaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $tweet = Tweets::where('tweet_id',$this->comment_reaction->tweet_id)->first();
        $course = Course::where('course_id', $tweet->course_id)->first();
        $comment = Comments::where('comment_id', $this->comment_reaction->comment_id)->first();
        if($comment && $tweet)
        {
            if($comment->user_id != $this->comment_reaction->user_id)
            {
                $user = User::where('user_id', $comment->user_id)->first();
                $this->comment_reaction->user = \Auth::user();
                $this->comment_reaction->message = " reacted on your comment in ".$course->course_title;
                $this->comment_reaction->url = "/".$user->user_type."/topic/".$course->course_id; 
                if($user->user_type != 'trainee')
                {
                    $this->comment_reaction->url = "/".$user->user_type."/tweet/".$tweet->topic_id."/".$tweet->tweet_id;
                }
                         
                $user->notify(new \App\Notifications\SaveNotification($this->comment_reaction, \Auth::user(), $this->comment_reaction->message, $this->comment_reaction->url));
                
                if(\Auth::user()->user_type != 'trainer' &&$user->user_type != 'trainer')
                {
                    $user = User::where('user_id', $course->trainer_id)->first();
                    $this->comment_reaction->user = \Auth::user();
                    $this->comment_reaction->message = " reacted on a comment in ". $course->course_title;
                    $this->comment_reaction->url = "/".$user->user_type."/tweet/".$tweet->topic_id."/".$tweet->tweet_id;          
                    $user->notify(new \App\Notifications\SaveNotification($this->comment_reaction, \Auth::user(), $this->comment_reaction->message, $this->comment_reaction->url));
                }
            }
            
        }
        
    }
}
