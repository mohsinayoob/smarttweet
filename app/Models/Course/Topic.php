<?php

namespace App\Models\Course;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Topic extends Model
{
    use Notifiable;
    protected $table = 'topic';
    protected $primaryKey = 'topic_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['course_id', 'topic_name', 'topic_description', 'status', 'topic_image'];
}
