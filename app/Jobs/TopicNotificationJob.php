<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Course\Topic;
use App\Models\User;
use App\Models\Course\CourseEnrollment;
class TopicNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $topic;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($topic)
    {
        //
        $this->topic = $topic;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $enrollments = CourseEnrollment::join('course','course.course_id','=','course_enrollment.course_id')
        ->where('course_enrollment.course_id',$this->topic->course_id)
        ->where('course_enrollment.status','Active')
        ->select()->get();
        
        foreach($enrollments as $enrollment)
        {
           // echo $enrollment;
            $user = User::where('user_id',$enrollment->trainee_id)->first();
            $enrollment->user = \Auth::user();
            $enrollment->url = "/".$user->user_type."/topic/".$enrollment->course_id;
            $enrollment->message = " has posted a topic in ". $enrollment->course_title;
            $user->notify(new \App\Notifications\SaveNotification($enrollment, \Auth::user(), $enrollment->message, $enrollment->url));
        }
        
    }
}
