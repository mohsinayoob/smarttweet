<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Course\Course;
use App\Models\Tweet\Tweets;
use DB;
use Mail;
use Session;

class AdminDashboardController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        
    }

    public function index()
    {
        $currentMonth = date('m');
       $course['total_course'] = Course::count();
       $course['this_month_course'] = Course::whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();

       /// Trainer
       $trainer['total_trainer'] = User::where('user_type','trainer')->count();
       $trainer['this_month_trainer'] = User::where('user_type','trainer')->whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();
       
        /// Trainee
        $trainee['total_trainee'] = User::where('user_type','trainee')->count();
        $trainee['this_month_trainee'] = User::where('user_type','trainee')->whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();
        
       /// Tweets
       $tweet['total_tweet'] = Tweets::count();
       $tweet['this_month_tweet'] = Tweets::whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();
       
       $fromDate = Carbon::now()->subDay(6)->toDateString(); // or ->format(..)
       $tillDate = Carbon::now()->toDateString();
       $trainers_this_week =  User::selectRaw('date(created_at) as date, COUNT(*) as count, user_type')
        ->whereBetween( DB::raw('date(created_at)'), [$fromDate, $tillDate] )
        ->where('user_type', 'trainer')
        ->groupBy('user_type','date')
        ->orderBy('date', 'DESC')
        ->get();
        $users_this_week = User::select(array(
            DB::raw('DATE(`created_at`) as `date`'),
            DB::raw("SUM(CASE
            WHEN user_type = 'trainer'
            THEN 1 ELSE 0 END) AS 'trainer_count'"),
            DB::raw(
                "SUM(CASE
                WHEN user_type = 'trainee'
                THEN 1 ELSE 0 END) AS 'trainee_count'"
         )
         ))
         ->whereBetween( DB::raw('date(created_at)'), [$fromDate, $tillDate] )
         ->groupBy('date')
        ->orderBy('date', 'DESC')->get();
        //return $users_this_week;
        $courses_this_week = Course::select(array(
            DB::raw('DATE(`created_at`) as `date`'),
            DB::raw("COUNT(*) as total_course")
         ))
         ->whereBetween( DB::raw('date(created_at)'), [$fromDate, $tillDate] )
         ->groupBy('date')
        ->orderBy('date', 'DESC')->get();
        //return $courses_this_week;

        $latest_five_trainers = User::where('user_type','trainer')->orderBy('user_id', 'desc')->take(5)->select('user_id','name', 'email','user_type', 'sector','profile_image','is_active', 'created_at')->get();
        $latest_five_trainees = User::where('user_type','trainee')->orderBy('user_id', 'desc')->take(5)->select('user_id','name', 'email','user_type', 'sector','profile_image','is_active', 'created_at')->get();
        $latest_five_courses = Course::join('users','user_id','=','trainer_id')->orderBy('course_id', 'desc')->take(5)->select('course.course_id','course.course_title','course.course_category', 'users.name','users.profile_image','users.user_type', 'course.created_at')->get();
       // return $latest_five_courses;
        return view('admin.dashboard', compact('course', 'trainer','trainee','tweet', 'users_this_week', 'courses_this_week','latest_five_trainers','latest_five_trainees','latest_five_courses'));
    }
}
