<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');


Route::get('/', function(){
    return redirect('/login');
});


Route::get('register/verify/{token}','Auth\RegisterController@verify');

Route::get('forget-password', function () {
    return view('shared.forget-password');
});

//////////////////////////////////////////////////////////////////////
/////                       Trainee          ////////////////////////
/////////////////////////////////////////////////////////////////////

Route::get('trainee/profile', 'trainee\TraineeProfileController@index');
Route::post('trainee/profile', 'trainee\TraineeProfileController@updateProfile');
Route::get('trainee/change-password', 'trainee\TraineeProfileController@index');
Route::post('trainee/change-password', 'trainee\TraineeProfileController@updatePassword');

Route::post('trainee/profile/change_cover','trainee\TraineeProfileController@updateCoverImage');
Route::post('trainee/profile/change_image','trainee\TraineeProfileController@updateProfileImage');
/********************* Search course ***************************************/
Route::post('trainee/search-course','trainee\TraineeCourseController@index');
Route::get('trainee/search-course','trainee\TraineeCourseController@index');
Route::get('trainee/course-details/{course_id}','trainee\TraineeCourseController@courseDetails');
Route::get('trainee/add-enrollment/{course_id}','trainee\TraineeCourseController@addEnrollment');
/********************* Manage course ***************************************/

Route::get('/trainee/manage-course', 'trainee\TraineeCourseController@ManageCourse');
Route::get('trainee/topic/{course_id}', 'trainee\TraineeTopicController@index');
/************************Trainee Course Rating *******************************/

Route::post('/trainee/post-course-rating/{enrollment_id}', 'trainee\TraineeTopicController@rateCourse');
/********************* Trainee Tweet ***************************************/
Route::get('trainee/tweet/{course_id}/{topic_id}','trainee\TraineeTweetController@viewSingleTweet');
Route::post('trainee/tweet-post-reaction', 'trainee\TraineeTweetReactionController@createTweetReaction');
Route::get('trainee/tweet-reaction-users/{tweet_id}/{reaction}', 'trainee\TraineeTweetReactionController@getReactionUsers');
/********************* Trainee Comment ***************************************/

Route::post('trainee/upload-comment-data', 'trainee\TraineeCommentController@pictureUpload');
    Route::post('trainee/postComment/{topic_id}/{tweet_id}', 'trainee\TraineeCommentController@saveComment');
    Route::get('trainee/getmorecomments/{topic_id}/{tweet_id}', 'trainee\TraineeCommentController@getMoreComments');
    Route::post('trainee/comment-post-reaction', 'trainee\TraineeCommentReactionController@createCommentReaction');
    Route::get('trainee/comment-reaction-users/{comment_id}/{reaction}', 'trainee\TraineeCommentReactionController@getCommentReactionUsers');


/*************************** Notification get ********** */
Route::get('/trainee/get-notifications', 'trainee\TraineeNotificationController@getNotification');
Route::get('trainee/notifications','trainee\TraineeNotificationController@getAllNotification');
Route::get('trainee/get-all-notifications','trainee\TraineeNotificationController@getAjaxAllNotification');

/*********************** Trainee Chat ***************************/
Route::get('trainee/chat','trainee\TraineeChatController@index');
Route::get('trainee/chat/{user_id}','trainee\TraineeChatController@chatByUserId');
Route::get('trainee/chat/{user_id}/{course_id}','trainee\TraineeChatController@chatBetweenTrainees');

/******************************* Trainer Support/Ticket   ********/
Route::get('trainee/tickets', 'trainee\TraineeTicketController@userTickets');
Route::get('trainee/tickets/{ticket_id}', 'trainee\TraineeTicketController@show');
Route::get('trainee/ticket/create-ticket', 'trainee\TraineeTicketController@create');
Route::post('trainee/ticket/new_ticket', 'trainee\TraineeTicketController@store');
//Route::post('trainer/close_ticket/{ticket_id}', 'admin\AdminTicketController@close');
Route::post('trainee/comment', 'trainee\TraineeTicketController@postComment');

//////////////////////////////////////////////////////////////////////
/////                       Trainer          ////////////////////////
/////////////////////////////////////////////////////////////////////

Route::get('trainer/profile', 'Trainer\TrainerProfileController@index');
Route::post('trainer/profile', 'Trainer\TrainerProfileController@updateProfile');
Route::get('trainer/change-password', 'Trainer\TrainerProfileController@index');
Route::post('trainer/change-password', 'Trainer\TrainerProfileController@updatePassword');

// Upload cover image ////
Route::post('trainer/profile/change_cover','Trainer\TrainerProfileController@updateCoverImage');
Route::post('trainer/profile/change_image','Trainer\TrainerProfileController@updateProfileImage');

// course category ////////////////////

Route::get('trainer/create-course', 'Trainer\TrainerCourseController@index');
Route::post('trainer/create-course', 'Trainer\TrainerCourseController@createCourse');
Route::get('trainer/manage-course', 'Trainer\TrainerCourseController@ViewCourses');
Route::get('trainer/activate-course/{course_id}/{status}', 'Trainer\TrainerCourseController@UpdateCourseStatus');

Route::post('trainer/manage-course','Trainer\TrainerCourseController@ViewCourses');
Route::get('trainer/edit-course/{course_id}', 'Trainer\TrainerCourseController@ViewEditCourses');
Route::post('trainer/edit-course/{course_id}', 'Trainer\TrainerCourseController@UpdateCourse');
Route::get('trainer/delete-course/{course_id}', 'Trainer\TrainerCourseController@DeleteCourse');
// course Topic ////////////////////
//Route::get('trainer/topic', function () {
    //return view('trainer.topic');
//});
Route::get('trainer/topic/{course_id}', 'Trainer\TrainerTopicController@index');
Route::post('trainer/topic/{course_id}', 'Trainer\TrainerTopicController@createTopic');
Route::post('trainer/update-topic', 'Trainer\TrainerTopicController@UpdateTopic');
Route::get('trainer/delete-topic/{topic_id}', 'Trainer\TrainerTopicController@DeleteTopic');
Route::get('trainer/topic-status/{topic_id}/{status}', 'Trainer\TrainerTopicController@UpdatetopicStatus');

/////////////// Trainer Enrollment ////////////////
Route::get('trainer/enrollment', 'Trainer\TrainerEnrollmentController@index');
Route::post('/trainer/updateEnrollment', 'Trainer\TrainerEnrollmentController@UpdateEnrollmentStatus' );
/////////////// Trainer Tweets ////////////////
Route::get('trainer/tweet/{topic_id}/{tweet_id}','Trainer\TrainerTweetController@viewSingleTweet');

Route::get('trainer/nexttweet/{topic_id}/{tweet_id}','Trainer\TrainerTweetController@viewNextTweet');

Route::get('trainer/tweets-list/{course_id}/{topic_id}','Trainer\TrainerTweetController@viewTweets');
Route::post('/trainer/postTweet/{course_id}/{topic_id}', 'Trainer\TrainerTweetController@saveTweet');
Route::get('/trainer/edit-tweet/{topic_id}/{tweet_id}', 'Trainer\TrainerTweetController@getEditTweet');
Route::post('/trainer/edit-tweet/{topic_id}/{tweet_id}', 'Trainer\TrainerTweetController@editTweet');
Route::post('trainer/upload-tweet-image', 'Trainer\TrainerTweetController@pictureUpload');
Route::post('trainer/upload-tweet-video', 'Trainer\TrainerTweetController@videoUpload');
Route::get('trainer/upload-tweet-data', 'Trainer\TrainerTweetController@pictureUpload');
Route::get('trainer/tweet-status/{topic_id}/{tweet_id}/{status}', 'Trainer\TrainerTweetController@UpdateTweetStatus');
Route::post('trainer/tweet-status/{topic_id}/{tweet_id}/{status}', 'Trainer\TrainerTweetController@UpdateTweetStatus');
Route::post('trainer/tweet-post-reaction', 'Trainer\TrainerTweetReactionController@createTweetReaction');
Route::get('trainer/tweet-reaction-users/{tweet_id}/{reaction}', 'Trainer\TrainerTweetReactionController@getReactionUsers');
Route::get('trainer/delete-tweet/{tweet_id}','Trainer\TrainerTweetController@DeleteTweet');

    ///////////////// Comment ///////////////////////////////////

    Route::post('trainer/upload-comment-data', 'Trainer\TrainerCommentController@pictureUpload');
    Route::post('trainer/postComment/{topic_id}/{tweet_id}', 'Trainer\TrainerCommentController@saveComment');
    Route::get('trainer/getmorecomments/{topic_id}/{tweet_id}', 'Trainer\TrainerCommentController@getMoreComments');
    Route::post('trainer/comment-post-reaction', 'Trainer\TrainerCommentReactionController@createCommentReaction');
    Route::get('trainer/comment-reaction-users/{comment_id}/{reaction}', 'Trainer\TrainerCommentReactionController@getCommentReactionUsers');

////////////////////////////// Reports  Trainer //////////////////////////////

Route::get('trainer/reports', 'Trainer\TrainerUserReportsController@index');
Route::get('trainer/user-reports','Trainer\TrainerUserReportsController@index');
Route::get('trainer/course-reports', 'Trainer\TrainerUserReportsController@index');

Route::post('trainer/user-reports','Trainer\TrainerUserReportsController@getUserReports');
Route::post('trainer/course-reports', 'Trainer\TrainerUserReportsController@getCourseReports');
/****************************Trainer Dashboard **************************/
Route::get('trainer/favorite-list', 'Trainer\TrainerDashboardController@index');

/***************************** Trainer Notification *******************/
Route::get('/trainer/get-notifications', 'Trainer\TrainerNotificationController@getNotification');
Route::get('trainer/notifications','Trainer\TrainerNotificationController@getAllNotification');
Route::get('trainer/get-all-notifications','Trainer\TrainerNotificationController@getAjaxAllNotification');
/*********************** Trainer Chat ***************************/
Route::get('trainer/chat','Trainer\TrainerChatController@index');
Route::get('trainer/chat/{user_id}','Trainer\TrainerChatController@chatByUserId');


/******************************* Trainer Support/Ticket   ********/
Route::get('trainer/tickets', 'Trainer\TrainerTicketController@userTickets');
Route::get('trainer/tickets/{ticket_id}', 'Trainer\TrainerTicketController@show');
Route::get('trainer/ticket/create-ticket', 'Trainer\TrainerTicketController@create');
Route::post('trainer/ticket/new_ticket', 'Trainer\TrainerTicketController@store');
//Route::post('trainer/close_ticket/{ticket_id}', 'admin\AdminTicketController@close');
Route::post('trainer/comment', 'Trainer\TrainerTicketController@postComment');

//////////////////////////////////////////////////////////////////////
/////                       Admin             ///////////////////////
/////////////////////////////////////////////////////////////////////

Route::get('admin/profile', 'admin\AdminProfileController@index');
Route::post('admin/profile', 'admin\AdminProfileController@updateProfile');
Route::get('admin/change-password', 'admin\AdminProfileController@index');
Route::post('admin/change-password', 'admin\AdminProfileController@updatePassword');
Route::post('admin/profile/change_cover','admin\AdminProfileController@updateCoverImage');
Route::post('admin/profile/change_image','admin\AdminProfileController@updateProfileImage');
//// Category admin///////////////////
Route::get('admin/manage-categories', 'admin\AdminCategoryController@index');
Route::post('admin/manage-categories', 'admin\AdminCategoryController@AddCategory');
Route::post('admin/edit-categories', 'admin\AdminCategoryController@UpdateCategory');
Route::post('admin/delete-category', 'admin\AdminCategoryController@DeleteCategory');

////// course admin//////////////////////////
Route::get('admin/create-course', 'admin\AdminCourseController@index');
Route::post('admin/create-course', 'admin\AdminCourseController@createCourse');
Route::get('admin/manage-course', 'admin\AdminCourseController@ViewCourses');
Route::get('admin/activate-course/{course_id}/{status}', 'admin\AdminCourseController@UpdateCourseStatus');

Route::get('admin/edit-course/{course_id}', 'admin\AdminCourseController@ViewEditCourses');
Route::post('admin/edit-course/{course_id}', 'admin\AdminCourseController@UpdateCourse');
Route::get('admin/delete-course/{course_id}', 'admin\AdminCourseController@DeleteCourse');

// course Topic admin////////////////////

Route::get('admin/topic/{course_id}', 'admin\AdminTopicController@index');
Route::post('admin/topic/{course_id}', 'admin\AdminTopicController@createTopic');
Route::post('admin/update-topic', 'admin\AdminTopicController@UpdateTopic');
Route::get('admin/topic-status/{topic_id}/{status}', 'admin\AdminTopicController@UpdatetopicStatus');
Route::get('admin/delete-topic/{topic_id}', 'admin\AdminTopicController@DeleteTopic');
////////// Trainee Management admin////////////
Route::get('admin/trainee', 'admin\AdminTraineeController@index' );
Route::post('admin/trainee', 'admin\AdminTraineeController@UpdateTrainee' );
Route::post('admin/updatetrainee', 'admin\AdminTraineeController@UpdateTraineeStatus' );

////////// Trainee Management admin////////////
Route::get('admin/trainer', 'admin\AdminTrainerController@index' );
Route::post('admin/trainer', 'admin\AdminTrainerController@UpdateTrainer' );
Route::post('admin/updatetrainer', 'admin\AdminTrainerController@UpdateTrainerStatus' );
// tweeets admin /////////////////
Route::get('admin/tweet/{topic_id}/{tweet_id}','admin\AdminTweetController@viewSingleTweet');
Route::get('admin/tweets-list/{course_id}/{topic_id}','admin\AdminTweetController@viewTweets');
Route::post('/admin/postTweet/{course_id}/{topic_id}', 'admin\AdminTweetController@saveTweet');
Route::get('/admin/edit-tweet/{topic_id}/{tweet_id}', 'admin\AdminTweetController@getEditTweet');
Route::post('/admin/edit-tweet/{topic_id}/{tweet_id}', 'admin\AdminTweetController@editTweet');
Route::post('admin/upload-tweet-image', 'admin\AdminTweetController@pictureUpload');
Route::post('admin/upload-tweet-video', 'admin\AdminTweetController@videoUpload');
Route::get('admin/upload-tweet-data', 'admin\AdminTweetController@pictureUpload');
Route::get('admin/delete-tweet/{tweet_id}','admin\AdminTweetController@DeleteTweet');

Route::post('admin/tweet-post-reaction', 'admin\AdminTweetReactionController@createTweetReaction');
Route::get('admin/tweet-reaction-users/{tweet_id}/{reaction}', 'admin\AdminTweetReactionController@getReactionUsers');

    ///////////////// Comment ///////////////////////////////////

Route::post('admin/upload-comment-data', 'admin\AdminCommentController@pictureUpload');
Route::post('/admin/postComment/{topic_id}/{tweet_id}', 'admin\AdminCommentController@saveComment');
Route::get('/admin/getmorecomments/{topic_id}/{tweet_id}', 'admin\AdminCommentController@getMoreComments');

////////////////////////////// Reports  Admin //////////////////////////////

Route::get('admin/reports', 'admin\AdminUserReportsController@index');
Route::get('admin/user-reports','admin\AdminUserReportsController@index');
Route::get('admin/course-reports', 'admin\AdminUserReportsController@index');

Route::post('admin/user-reports','admin\AdminUserReportsController@getUserReports');
Route::post('admin/course-reports', 'admin\AdminUserReportsController@getCourseReports');

//******************************  Admin Add Trainer ******************************/
Route::get('admin/add-trainer', 'admin\AdminAddTrainerController@index');
Route::post('admin/add-trainer', 'admin\AdminAddTrainerController@addTrainer');

/****************************Admin Dashboard ***************************************/
Route::get('admin/dashboard','admin\AdminDashboardController@index') ;
/*********************************Admin Notification ***************************/
Route::get('/admin/get-notifications', 'admin\AdminNotificationController@getNotification');

Route::get('admin/notifications','admin\AdminNotificationController@getAllNotification');
Route::get('admin/get-all-notifications','admin\AdminNotificationController@getAjaxAllNotification');
/****************************Admin Issue Certificate ***************************************/

//////////////////////////////////////////////////////////////
Route::get('admin/certificate-builder/{user_id}','admin\AdminCertificateController@index') ;
Route::post('admin/certificate/{user_id}', 'admin\AdminCertificateController@getCertificate');
Route::get('admin/certificate/{user_id}', function () {
    return redirect()->to('/admin/trainee')->with('error_messages', 'Kindly select Trainee first');
});
Route::get('admin/certificate', function () {
    return redirect()->to('/admin/trainee')->with('error_messages', 'Kindly select Trainee first');
});


/**********************************admin support tickets */
Route::get('admin/tickets', 'admin\AdminTicketController@index');
Route::get('admin/tickets/{ticket_id}', 'admin\AdminTicketController@show');
Route::post('admin/close_ticket/{ticket_id}', 'admin\AdminTicketController@close');
Route::post('admin/comment', 'admin\AdminTicketController@postComment');

Route::get('admin/ticket/categories', 'admin\AdminTicketController@ViewCategories');
Route::post('admin/ticket/add-category', 'admin\AdminTicketController@AddCategory');
Route::post('admin/ticket/edit-categories', 'admin\AdminTicketController@UpdateCategory');
Route::post('admin/ticket/delete-category', 'admin\AdminTicketController@DeleteCategory');


/****************************** Public Profile *********************************/
Route::get('public-profile/{id}', 'trainee\TrainerProfileController@index');

Route::get('test', 'trainee\TraineeNotificataionTestController@index');
Route::get('chat', function () {
    return view('partials.chat');
});

Route::get('language/{lang}',function($lang){

    Session::put('locale',$lang);
    return redirect()->back();
    
    })->middleware('locale');