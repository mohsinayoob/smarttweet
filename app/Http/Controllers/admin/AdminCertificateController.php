<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Course\Course;
use App\Models\Course\CourseEnrollment;
use App\Models\User;

class AdminCertificateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        
    }
    public function getCertificate ($user_id, Request $request)
    {
        $data = $request->all();
        if(!isset($data['course_id']))
        {
            return back()->with('error_messages','Could not genearate Certificate. try Again!!');
        }
        $courses = Course::where('course_id',$data['course_id'])
        ->where('status', 'Active')->select('course_title','course_category')->first();
        $trainees = User::where('user_id', $user_id)
        ->where('user_type', 'trainee')
        ->where('is_active', 1)
        ->where('is_verified', 1)
        ->select('name')->first();
        //return $trainees;
        
        //return $reports;
        return view('admin.certificate', compact('courses','trainees'));
    }
    public function index($user_id)
    {
        $courses = Course::join('course_enrollment','course_enrollment.course_id','=','course.course_id')
        ->where('course_enrollment.trainee_id', $user_id)
        ->where('course_enrollment.status', 'Active')
        ->select('course.*')->get();
        
        //return $courses;
        return view('admin.certificate-builder', compact('courses','user_id'));
    }
}
