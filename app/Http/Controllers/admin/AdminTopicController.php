<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Course\Course;
use App\Models\Course\Topic;
use App\Models\Tweet\Tweets;
use App\Models\Tweet\Comments;
use App\Models\Course\CourseEnrollment;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
//use Illuminate\Support\Facades\Request;
use Session;
use App\Jobs\TopicNotificationJob;

class AdminTopicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        
    }
    public function createTopic ($course_id, Request $request)
    {
        //return $course_id;
        DB::beginTransaction();
        try{
        $category_validator = Validator::make($request->all(), [
            'topic_name' => 'required',
            'topic_image' => ' mimes:jpeg,jpg,png,JPG, JPEG, PNG | max:3048',
            'topic_description' => 'required',
        ]);

        if ($category_validator->fails()) {
            $messages = $category_validator->messages();
//return $messages;
            return back()->withError($messages)->with('error_messages', "Topic and title are required.");
        }
        $imageName = null;

        if($request->topic_image){
            $imageName = time() . '.' . $request->topic_image->getClientOriginalExtension();
            $image_uploaded = $request->topic_image->move(public_path('topic_images'), $imageName);
            //$existCourse->topic_image= $imageName;

        }
        //$imageName = time() . '.' . $request->topic_image->getClientOriginalExtension();
        //$image_uploaded = $request->topic_image->move(public_path('topic_images'), $imageName);
        $data = $request->all();
        
        $data["course_id"] = $course_id;
        $data["status"] = 'Active';
        $data["topic_image"] = $imageName;
        $data["topic_description"] = base64_encode($data["topic_description"]);
        //return $data;
        
        $new_topic = Topic::create($data);
        // After creating the user send an email with the random token generated in the create method above

        DB::commit();
        $notification_job = (new TopicNotificationJob($new_topic))->delay(13);
        $this->dispatch($notification_job);
        Session::flash('update', 'Data Saved Successfully');
        return back()->with('update', 'Data Successfully Saved');
    } catch (Exception $e) {
        DB::rollback();
        return back()->with('error_messages', 'Error in Saving data');
    }

    }
    /////////////////////////
    public function UpdateTopic (Request $request)
    {
        //return $request;
        $data = $request->all();
        //return $data;
        $category_validator = Validator::make($data , [
            'topic_name' => 'required',
            'topic_description' => 'required',
        ]);

        if ($category_validator->fails()) {
            $messages = $category_validator->messages();
//return $messages;
            return back()->withError($messages)->with('error_messages', "Topic and title are required.");
        }
        //return $course_id;
        DB::beginTransaction();
        try{
        $existCourse = Topic::where('topic_id', $data["topic_id"])->first();
        if($existCourse){
            if($request->topic_image){
                $imageName = time() . '.' . $request->topic_image->getClientOriginalExtension();
                $image_uploaded = $request->topic_image->move(public_path('topic_images'), $imageName);
                $existCourse->topic_image= $imageName;

            }
        
        $existCourse->topic_description = base64_encode($data["topic_description"]);
        $existCourse->topic_name = $data["topic_name"];
        
        $existCourse->save($data);
        // After creating the user send an email with the random token generated in the create method above

        DB::commit();
        Session::flash('update', 'Data Updated Successfully');
        return back()->with('update', 'Data Successfully Updated');
    } else{
        return back()
        ->withError('error_messages', 'Unable to Update data.');
    }

}catch (Exception $e) {
        DB::rollback();
        return back()->with('error_messages', 'Error in Updating data');
    }

    }
    public function DeleteTopic($topic_id)
    {
        //return $topic_id;
        DB::beginTransaction();
        try
        {
            $existTweets= Tweets::where('topic_id', $topic_id)->select('tweet_id')->get();
            //return $existTweets;
            foreach($existTweets as $existTweet){
                $existComments= DB::table('comments')->where('tweet_id', $existTweet->tweet_id)->delete();
                //$existComments->;
                
            }
                foreach($existTweets as $existTweet){
                $existTweetsdel= Tweets::where('tweet_id', $existTweet->tweet_id)->delete();
                //$existTweetsdel;
                
                }
               
                    $existTopic = Topic::where('topic_id', $topic_id)->first();
                    $existTopic->delete();
              
                    DB::commit();
            Session::flash('update', 'Topic Deleted Successfully');
            return back()->with('update', 'Topic Successfully Deleted');
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Deleted Topic');
        }

    }
    public function UpdateTopicStatus($topic_id, $status)
    {
        
        DB::beginTransaction();
        try
        {
            
           // Category::create();
           
            $existCourse = Topic::where('topic_id', $topic_id)->first();
            if(!empty($existCourse))
            {
                $existCourse->status = $status;
                $existCourse->save();
                DB::commit();
                Session::flash('update', 'Topic Status Updated Successfully');
                return back()->with('update', 'Topic Status Successfully Updated');
            }
            else
            {
                return back()->with('error_messages', 'Error in Updating Topic Status');
            }
            
            
            
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Updating Topic Status');
        }

    }
    protected function searchTopics($q,$status,$date,$course_id, $courses, $enrollusers)
    {
        if($date == "Today")
        {
            if(!empty($status))
            {
                $topics = Topic::where('topic_name', 'like', '%' . $q . '%')
                ->whereDay('created_at',date('d'))->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
                ->where('topic.status', $status)
                ->where('course_id', $course_id)
                ->paginate(10);
                //return $courses;
                return view('admin.topic', compact('courses','topics','enrollusers'));
            }
            $topics  = Topic::where('topic_name', 'like', '%' . $q . '%')
            ->whereDay('created_at',date('d'))->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
            ->where('course_id', $course_id)
            ->paginate(10);
           // return $courses;
            return view('admin.topic', compact('courses','topics','enrollusers'));

        }
        else if($date == "Month")
        {
            if(!empty($status))
            {
                $topics = Topic::where('topic_name', 'like', '%' . $q . '%')
                ->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
                ->where('topic.status', $status)
                ->where('course_id', $course_id)
                ->paginate(10);
                //return $courses;
                return view('admin.topic', compact('courses','topics','enrollusers'));
            }
            $topics  = Topic::where('topic_name', 'like', '%' . $q . '%')
            ->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
            ->where('course_id', $course_id)
            ->paginate(10);
           // return $courses;
            return view('admin.topic', compact('courses','topics','enrollusers'));

        }
        else if($date == "Year")
        {
            if(!empty($status))
            {
                $topics = Topic::where('topic_name', 'like', '%' . $q . '%')
                ->whereYear('created_at',date('Y'))
                ->where('topic.status', $status)
                ->where('course_id', $course_id)
                ->paginate(10);
                //return $courses;
                return view('admin.topic', compact('courses','topics','enrollusers'));
            }
            $topics  = Topic::where('topic_name', 'like', '%' . $q . '%')
            ->whereYear('created_at',date('Y'))
            ->where('course_id', $course_id)
            ->paginate(10);
           // return $courses;
            return view('admin.topic', compact('courses','topics','enrollusers'));

        }
        if(!empty($status))
        {
            $topics  = Topic::where('topic_name', 'like', '%' . $q . '%')
            ->where('topic.status', $status)
            ->where('course_id', $course_id)
            ->paginate(10);
            // return $courses;
            return view('admin.topic', compact('courses','topics','enrollusers'));
        }
        $topics = Topic::where('topic_name', 'like', '%' . $q . '%')
        ->where('course_id', $course_id)
        ->paginate(10);
       // return $courses;
       return view('admin.topic', compact('courses','topics','enrollusers'));
    }
    public function index($course_id)
    {
        $courses = Course::where('course_id', $course_id)->first();
        $enrollusers = CourseEnrollment::join('users', 'users.user_id', '=', 'course_enrollment.trainee_id')
        ->where ('course_enrollment.course_id',$course_id)
        ->select('course_enrollment.*', 'users.name', 'users.sector','users.profile_image')->take(10)->get();
        $q = Input::get('q'); 
        $status = Input::get('status'); 
        $date = Input::get('date'); 
        if (!empty($q) || !empty($status) || !empty($date)) {
           return $this->searchTopics($q,$status,$date,$course_id, $courses, $enrollusers);
        }
        $topics = Topic::where('course_id', $course_id)->paginate(10);
        //return $topics;
        //return view('admin.profile', $profile);
        //return view('admin.topic', compact('topics'));
        //return $courses;
        //return view('admin.profile', $profile);
        return view('admin.topic', compact('courses','topics','enrollusers'));
    } 
}
