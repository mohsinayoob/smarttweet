<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SaveNotification extends Notification
{
    use Queueable;
    public $data, $sender, $message, $url, $notification_type;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data, $sender, $message,$url)
    {
        //
        $this->data = $data;
        $this->notification_type = NULL;
        $this->data->id=$this->id;
        $this->sender = $sender;
        $this->message = $message;
        $this->url = $url;
        if($data->notification_type)
        {
            $this->notification_type = $data->notification_type;
        }
       //echo $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //print_r($notifiable);
        
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    /*public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        event(new \App\Events\NotificationPosted($this->data, $this->sender, $this->message, $this->url, $notifiable->user_id, $this->id, $this->notification_type));
        
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => $this->data,
            'notification_type'=>$this->notification_type
        ];
    }
    public function toDatabase($notifiable)
    {
        
        
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => $this->data,
            'notification_type'=>$this->notification_type
        ];
    }
}
