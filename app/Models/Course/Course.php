<?php

namespace App\Models\Course;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Course extends Model
{
    use Notifiable;
    protected $table = 'course';
    protected $primaryKey = 'course_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['trainer_id', 'course_title', 'course_description', 'course_category', 'status', 'avg_rating', 'tags', 'created_by'];
}
