<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Redirect;
use App\Http\Requests;

class LanguageController extends Controller
{
    public function Index(){

       

        Session::set('locale',Input::get('locale'));
        //session(['u2f.registerData' => $req]);
      
        return Redirect::back();
    }

}
