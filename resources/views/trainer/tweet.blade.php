@extends('layouts.trainer_layout') @section('title', 'Tweet') @section('css-files')
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/lightgallery/css/lightgallery.min.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/simple-line-icons/css/simple-line-icons.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/icofont/css/icofont.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/themify-icons/themify-icons.css')}}"> 
<link rel="stylesheet" href="{{URL::asset('files/assets/css/flipclock.css')}}" />
<link rel="stylesheet" href="{{URL::asset('files/assets/css/timer.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/dropzone/dropzone.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}">
<style>
		.dropzone{
			margin-top: 10px;
			border: dashed #35AFAD;
			padding: 30px;
		}
	</style>
@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">
					
				<div class="page-body">
					<div class="modal fade modal-flex" id="Modal-Timer" tabindex="-1" role="dialog">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-body">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<ul class="nav nav-tabs" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">
												<i class="fa fa-clock-o"></i> Pick Time for Tweet
											</a>
										</li>

									</ul>
									<form method="post">
									<div class="tab-content modal-body">
										
											{{csrf_field()}}
										<div class="tab-pane active" id="tab-home" role="tabpanel">
											<input type="hidden" id="hidden-status-url" />
												<span class="label label-info" style="font-size:101%;margin-bottom:20px;">
														You can skip this step and Default Time will be 05:00.
													</span>
											<div class="card-block user-box" >
												<label for="minute">Minutes </label>
												<div class="input-group">
													<select name="minutes" class="form-control" id="tweet-minutes">
														@for($i=0; $i<60; $i++)
															<option value="{{$i}}">{{$i}}</option>
														@endfor
													</select>
												</div>
												<label for="seconds">Seconds </label>
												<div class="input-group">
													<select name="seconds" class="form-control" id="tweet-seconds">
														@for($i=0; $i<60; $i++)
															<option value="{{$i}}">{{$i}}</option>
														@endfor
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Skip</button>
										<button class="btn btn-primary">Save</button>
									</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="md-overlay"></div>
					<div class="row">
						<div class="col-lg-9">
							<div class="row">

								<div class="col-md-12">

									<div>
										@if(!empty($tweet))
										<div class="modal fade modal-flex" id="Modal-tab" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-body">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
														<ul class="nav nav-tabs" role="tablist">
															<li class="nav-item">
																<a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">
																	<i class="fa fa-heart"></i>
																</a>
															</li>
					
														</ul>
														<div class="tab-content modal-body">
															<div class="tab-pane active" id="tab-home" role="tabpanel">
																<div class="card-block user-box" id="favorite-modal-data">
																	<div class="fa fa-spinner fa-spin fa-lg"></div>
																	
																</div>
					
															</div>
															
															
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="modal fade modal-flex" id="Modal-tab-like" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-body">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
														<ul class="nav nav-tabs" role="tablist">
															<li class="nav-item">
																<a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">
																	<i class="fa fa-thumbs-up"></i>
																</a>
															</li>
					
														</ul>
														<div class="tab-content modal-body">
															<div class="tab-pane active" id="tab-home" role="tabpanel">
																<div class="card-block user-box" id="like-modal-data">
																
																</div>
					
															</div>
															
															
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="modal fade modal-flex" id="Modal-tab-dislike" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-body">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
														<ul class="nav nav-tabs" role="tablist">
															<li class="nav-item">
																<a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">
																	<i class="fa fa-thumbs-down"></i>
																</a>
															</li>
					
														</ul>
														<div class="tab-content modal-body">
															<div class="tab-pane active" id="tab-home" role="tabpanel">
																<div class="card-block user-box" id="dislike-modal-data">
																	
																</div>
					
															</div>
															
															
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="modal fade" id="Stats-Modal" tabindex="-1" role="dialog">
												<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title">Stats</h4>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														
														<div class="modal-body">
															<div class="col-md-12">
																<div class="row">
																	<div class="col-lg-6 col-md-6">
																		<div class="card">
																			<div class="card-header">
																				<h5>Tweet Feedback</h5>
																			</div>
																			<div class="card-block">
																				<span class="d-block text-c-blue f-24 f-w-600 text-center">{{$tweet->total_favorites + $tweet->total_likes + $tweet->total_dislikes}}</span>
																				<canvas id="feedback-charts" height="100"></canvas>
																				<div class="row justify-content-center m-t-15">
																					<div class="col-auto b-r-default m-t-5 m-b-5">
																						<h4 class="text-center">{{$tweet->total_favorites}}</h4>
																						<p class="text-primary m-b-0">
																							<i class="ti-heart m-r-5"></i>Favorites</p>
																					</div>
																					<div class="col-auto b-r-default m-t-5 m-b-5">
																						<h4 class="text-center">{{$tweet->total_likes}}</h4>
																						<p class="text-success m-b-0">
																							<i class="ti-thumb-up m-r-5"></i>Like</p>
																					</div>
																					<div class="col-auto m-t-5 m-b-5">
																						<h4 class="text-center">{{$tweet->total_dislikes}}</h4>
																						<p class="text-danger m-b-0">
																							<i class="ti-thumb-down m-r-5"></i>Dislike</p>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col-lg-6 col-md-6">
																		<div class="card bg-c-pink order-card">
																			<div class="card-block">
																				<h6 class="m-b-20">Total Comments</h6>
																				<h2 class="text-right">
																					<i class="ti-comment-alt f-left"></i>
																					<span>{{$tweet->total_comments}}</span>
																				</h2>
																				<p class="m-b-0">
																				</p>
																			</div>
																		</div>
									
									
									
																	</div>
									
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
															
														</div>
													
													</div>
												</div>
											</div>
										<div class="md-overlay"></div>
										<div class="card bg-white p-relative">
											<div class="card-block">
													<div class="timer-block">
															<div class="card-block timer-block-child">
																<div class="count-down-timer f-right" id="defaultCountdown"></div>
															</div>
														</div>
												<div class="media">
													<div class="media-left media-middle friend-box">
														<a href="/public-profile/{{$tweet->user_id}}">
															@if($tweet->user_type=="trainer")
																<img class="media-object img-radius m-r-20" src="{{URL::asset('trainer/profile_image/'.$tweet->profile_image)}}" alt="">
															@elseif($tweet->user_type=="admin")
																<img class="media-object img-radius m-r-20" src="{{URL::asset('admin/profile_image/'.$tweet->profile_image)}}" alt="">
															@endif
														</a>
													</div>
													<div class="media-body">
														<div class="chat-header">{{$tweet->name}} </div>
														<div class="f-13 text-muted">{{$tweet->created_at}}</div>
													</div>
												</div>
											</div>
											@if($tweet->content_type=="images")
											<div id="lightgallery1" class="wall-img-preview lightgallery-popup">
												@if($tweet->tweet_content)
													<?php $count=0;?>
													@foreach($tweet->tweet_content as $tweet_content)
													<?php $count ++;?>
													
													@if($count==4 || sizeof($tweet->tweet_content) < 2)
													<div class="col-md-12 p-0 wall-item" data-responsive="{{URL::asset('tweet_content/'.$tweet_content->file)}} 375, img/1-480.jpg 480, img/1.jpg 800"
													data-src="{{URL::asset( 'tweet_content/'.$tweet_content->file)}}" data-sub-html="{{ base64_decode($tweet->tweet_description) }}" >
													   <a href="#">
														   <img src="{{URL::asset('tweet_content/'.$tweet_content->file)}}" class="img-fluid " alt="" />
													   </a>
												   </div>
													
													@else
													<div class="col-md-6 p-0 wall-item" data-responsive="{{URL::asset('tweet_content/'.$tweet_content->file)}} 375, img/1-480.jpg
													480, img/1.jpg 800 "
													data-src="{{URL::asset( 'tweet_content/'.$tweet_content->file)}} " data-sub-html="{{base64_decode($tweet->tweet_description) }}">
													   <a href="#">
														   <img src="{{URL::asset('tweet_content/'.$tweet_content->file)}}" class="img-fluid " alt="">
													   </a>
												   </div>
													@endif
													
													@endforeach
												@endif
											</div>
											@elseif($tweet->content_type=="video")
											<!-- data-src should not be provided when you use html5 videos -->
												<div class="wall-img-preview">
													<div class="col-md-12 p-0 wall-item">
												
												@foreach($tweet->tweet_content as $tweet_content)
												<div style="display:none;" id="video{{$tweet_content->content_id}}">
														<video class="lg-video-object lg-html5 video-js vjs-default-skin" controls preload="none">
														<source src="{{URL::asset('tweet_content/'.$tweet_content->file)}}" type="video/mp4">
														 Your browser does not support HTML5 video.
													</video>
												</div>
												<ul id="html5-video" class="html5-videos">
												  <li  data-sub-html="{{base64_decode($tweet->tweet_description) }}" data-html="#video{{$tweet_content->content_id}}" >
													<video class="lg-video-object lg-html5 video-js vjs-default-skin" controls seekable="trur" canplaythrough="true" preload="none" style="width: 100%;" id="myvideo">
														<source src="{{URL::asset('tweet_content/'.$tweet_content->file)}}" type="video/mp4">
														Your browser does not support HTML5 video.
													</video>
													
												  </li>
												</ul>
												  @endforeach
												
											</div>
										</div>

											@endif
											<div class="card-block">
												<div class="timeline-details">
													<p class="text-muted">
														{!! base64_decode($tweet->tweet_description) !!}	
													</p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-msg">
												
												<a class="waves-effect {{$reactions && $reactions->reaction_content_favorite=='favorite' ? 'active' :''}}" href="#!" id="favorite-color" onclick='postreaction("{{$tweet->tweet_id}}","favorite")'>
													<i class="icofont icofont-heart-alt text-muted">
													</i>
													<span class="p-l-0 m-r-0">Favorite </span>
												</a>
												
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab" onclick='getReactionUsers("{{$tweet->tweet_id}}","favorite")'>
													<span class="b-r-theme" id="total_favorites">( {{$tweet->total_favorites}} )</span>
												</a>

												<a class="waves-effect {{$reactions && $reactions->reaction_content=='like' ? 'active' :''}}" href="#!" id="like-color" onclick='postreaction("{{$tweet->tweet_id}}","like")'>
													<i class="icofont icofont-thumbs-up text-muted">
													</i>
													<span class="p-l-0 m-r-0">Likes</span>
												</a>
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab-like" onclick='getReactionUsers("{{$tweet->tweet_id}}","like")'>

													<span class="b-r-theme" id="total_likes">( {{$tweet->total_likes}} )</span>
												</a>

												<a class="waves-effect {{$reactions && $reactions->reaction_content=='dislike' ? 'active' :''}}" href="#!" id="dislike-color" onclick='postreaction("{{$tweet->tweet_id}}","dislike")'>
													<i class="icofont icofont-thumbs-down text-muted">
													</i>
													<span class="p-l-0 m-r-0" >Dislike</span>
												</a>
												<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab-dislike" onclick='getReactionUsers("{{$tweet->tweet_id}}","dislike")'>
													<span class="b-r-theme" id="total_dislikes">( {{$tweet->total_dislikes}} )</span>
												</a>
												

												<a href="#!" data-toggle="modal" data-target="#Stats-Modal">
													<i class="icofont icofont-chart-histogram text-muted"></i>
												<span class="b-r-theme">Stats</span>
												</a>
												<a href="/trainer/nexttweet/{{$tweet->topic_id}}/{{$tweet->tweet_id}}">
													<i class="icofont icofont-block-right text-muted"></i>
												<span class="b-r-theme">Next</span>
											</a>
												<div class="dropdown-secondary dropdown f-right" style="display:inline;">
													<button class="btn btn-info btn-mini dropdown-toggle waves-light b-none txt-muted" type="button" id="dropdown3" data-toggle="dropdown"
													 aria-haspopup="true" aria-expanded="false">
														<i class="icofont icofont-navigation-menu"></i>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdown3" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">

														<a class="dropdown-item waves-light waves-effect md-trigger" href="/trainer/edit-tweet/{{$tweet->topic_id}}/{{$tweet->tweet_id}}"  >
															<i class="icofont icofont-spinner-alt-5"></i> Edit Tweet</a>
														<a class="dropdown-item waves-light waves-effect md-trigger"  onclick='deletetweet({{$tweet->tweet_id}})'>
																<i class="icofont icofont-delete-alt"></i> Delete Tweet</a>
															@if($tweet->status == 'Active')
														<a class="dropdown-item waves-light waves-effect"  onclick="updateTweetStatus('{{$tweet->topic_id}}', '{{$tweet->tweet_id}}', 'De-active');">
															<i class="icofont icofont-ui-edit"></i> De-activate</a>
														@else
															<a class="dropdown-item waves-light waves-effect" data-toggle="modal" data-target="#Modal-Timer" onclick="updateTweetStatusActive('{{$tweet->topic_id}}', '{{$tweet->tweet_id}}', 'Active');">
															<i class="icofont icofont-ui-edit"></i> Activate</a>
														@endif
													</div>

												</div>
											</div>
											<div class="card-block user-box">
												<div class="p-b-20">
													<span class="f-14">
														<a id="">Comments <span class id="total_comments"> ( {{$tweet->total_comments}} )</span>
															
														</a>
													</span>
												</div>
												<div id="comment-here">
														@foreach($comments as $comment)
														<div class="media">
															<a class="media-left" href="/public-profile/{{$comment->user_id}}">
																@if($comment->user_type=="trainer")
																	<img class="media-object img-radius m-r-20" src="{{URL::asset('trainer/profile_image/'.$comment->profile_image)}}" alt="">
																@elseif($comment->user_type=="admin")
																	<img class="media-object img-radius m-r-20" src="{{URL::asset('admin/profile_image/'.$comment->profile_image)}}" alt="">
																@elseif($comment->user_type=="trainee")
																	<img class="media-object img-radius m-r-20" src="{{URL::asset('trainee/profile_image/'.$comment->profile_image)}}" alt="">
																@endif
																
															</a>
															<div class="media-body b-b-theme social-client-description">
																<div class="chat-header">{{$comment->name}}
																	<span class="text-muted">{{$comment->created_at}}</span>
																</div>
																<p class="text-muted">{!! base64_decode($comment->comment_description) !!}</p>
																@if($comment->comment_image)
																	<img class="max-width-400 col-md-12 col-xs-12" src="{{URL::asset('comment_content/'.$comment->comment_image)}}" alt="">
																@endif
																<div class=" social-msg m-2 ">
																		<a class="waves-effect {{$comment->comment_reaction_content && $comment->comment_reaction_content->comment_reaction_content=='favorite' ? 'active' :''}}" href="#!" id="favorite-comment-color-{{$comment->comment_id}}" onclick='postcommentreaction("{{$comment->comment_id}}","{{$comment->tweet_id}}","favorite")'>
																				<i class="icofont icofont-heart-alt text-muted">
																				</i>
																				<span class="p-l-0 m-r-0">Favorite </span>
																			</a>
																			
																			<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab" onclick='getCommentReactionUsers("{{$comment->comment_id}}","favorite")'>
																				<span class="b-r-theme" id="total_comment_favorites_{{$comment->comment_id}}">( {{$comment->total_favorites}} )</span>
																			</a>
							
																			<a class="waves-effect {{$comment->comment_reaction_content && $comment->comment_reaction_content->comment_reaction_content=='like' ? 'active' :''}}" href="#!" id="like-comment-color-{{$comment->comment_id}}" onclick='postcommentreaction("{{$comment->comment_id}}","{{$comment->tweet_id}}","like")'>
																				<i class="icofont icofont-thumbs-up text-muted">
																				</i>
																				<span class="p-l-0 m-r-0">Likes</span>
																			</a>
																			<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab-like" onclick='getCommentReactionUsers("{{$comment->comment_id}}","like")'>
																				<span class="b-r-theme" id="total_comment_likes_{{$comment->comment_id}}">( {{$comment->total_likes}} )</span>
																			</a>
							
																			<a class="waves-effect {{$comment->comment_reaction_content && $comment->comment_reaction_content->comment_reaction_content=='dislike' ? 'active' :''}}" id="dislike-comment-color-{{$comment->comment_id}}" onclick='postcommentreaction("{{$comment->comment_id}}","{{$comment->tweet_id}}","dislike")'>
																				<i class="icofont icofont-thumbs-down text-muted">
																				</i>
																				<span class="p-l-0 m-r-0" >Dislike</span>
																			</a>
																			<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab-dislike" onclick='getCommentReactionUsers("{{$comment->comment_id}}","dislike")'>
																				<span class="b-r-theme" id="total_comment_dislikes_{{$comment->comment_id}}">( {{$comment->total_dislikes}} )</span>
																			</a>
																	</div>
															</div>
															
														</div>
														@endforeach
													</div>
												@if(sizeof($comments)>0)
												<div class="media">
													<div class="media-body b-b-theme social-client-description">
														<div class="chat-header" id="view-more-comments-div">
															<a onclick="getMoreComments()" >View more comments</a>			
														</div>
													</div>
												</div>
												@else
												<div class="media">
													<div class="media-body b-b-theme social-client-description">
														<div class="chat-header" id="view-more-comments-div">
															<a  >No more comments</a>			
														</div>
													</div>
												</div>
												@endif
												<div class="media">
														<a class="media-left" href="#">
																<img class="media-object img-radius m-r-20" src="{{URL::asset('trainer/profile_image/'.\Auth::user()->profile_image)}}" alt="">
														</a>
														<div class="media-body">
															
																<div class="">
																	<form class="" id="post-comment-form" method="post">
																		{{csrf_field()}}
																		<div id="comment_content_section">
	
																		</div>
																		<input type="hidden" name="comment_type" id="comment_content_type" value="text" />
																		<textarea id="post-message" class="f-13 form-control msg-send" rows="3" cols="10" placeholder="Write something....." name="comment_description"></textarea>
																		<span class="image-upload m-r-15" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add Photos">
																				<label id="imageselect" class="file-upload-lbl">
																					<i class="icofont icofont-image icofont-lg text-muted"></i>
																				</label>
																			</span>
																	</form>
																	<div class="" id="progress_view">
															
																	</div>
																	<div class="post-new-footer b-t-muted m-t-10 p-t-10">
																			<span>
																				<button id="post-new" class="btn btn-primary waves-effect waves-light f-right" onclick="commentSubmission()">
																					Post
																				</button>
																			</span>
								
																		</div>
																</div>
															
														</div>
														
													</div>
											</div>
										</div>
										@else
										<div class="card">
											<div class="card-header"></div>
											<div class="card-body">
												<h5 style="color:red">Tweet Does not exist</h3>
											</div>
										</div>
										@endif
									</div>

								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')
<script src="{{URL::asset('files/bower_components/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-fullscreen.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-video.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-autoplay.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-zoom.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-hash.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-pager.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/wall/wall.js')}}"></script>
<script src="{{URL::asset('files/bower_components/chart.js/js/Chart.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/widget/amchart/amcharts.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/widget/amchart/serial.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/widget/amchart/light.js')}}"></script>
<link href="{{URL::asset('files/assets/pages/videojs/video-js.css')}}" rel="stylesheet">
<script src="{{URL::asset('files/assets/pages/videojs/video.js')}}"></script>
<script src="{{URL::asset('files/assets/js/flipclock.js')}}"></script>
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/dropzone/dropzone.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/tinymce/tinymce.min.js?apiKey=jdw0qpvvruo7046n3j4g8o122piap638faeuii9s0rlk2fy3')}}"></script>
<style>
	.social-msg a.active i{
		color:#4099ff!important;
	}
	.social-msg a.active span{
		color:#4099ff!important;
	}
	.mce-top-part {
		position: absolute;
		bottom: 0px;
	}
	.image-upload{
		position: relative;
		left: 81px;
		bottom: 32px;
	}
	.max-width-400{
		max-width:400px;
	}
</style>
<script>
		var pageNumber = 2;
		var myDropzone;
	$(document).ready(function() {
		$('.lightgallery-popup').lightGallery();
		$('.html5-videos').lightGallery({
			thumbnail: false,
			videojs:true
		}); 
		clock = $('#defaultCountdown').FlipClock({
			clockFace: 'MinuteCounter',
			autoStart: false,
			@if($difference>0)
		 	callbacks: {
				stop: function() {
					window.location.reload();
		        	}
		        }
			@endif
		});
		clock.setCountdown(true);	    
		clock.setTime({{$difference}});
		clock.start();
		//////////////////////////////
		var editor =tinymce.init({ 
			selector:'textarea',
			mode : "textareas",
			menubar:false,
			statusbar: false,
			plugins:'smileys, emoticons',
			toolbar1: 'smileys | emoticons',
			theme_advanced_toolbar_location : "bottom",
			auto_convert_smileys: true,
			setup: function (editor) {
				editor.on('change', function () {
					tinymce.triggerSave();
				});
			}
			
		});

		$("#imageselect").click(function() {
			if(myDropzone)
			{
				//alert("already intitialized");
				return true;
			}
			$('#comment_content_type').val('images');
			$('#comment_content_section').html('');
			$('#progress_view').html('<form action="{{ url("/trainer/upload-comment-data")}}" enc-type="multipart/form-data"  class="dropzone" id="image-dropzone">'+'{{csrf_field()}}'+'<div class="dz-default dz-message"><span>Drop images here to upload</span></div></form>');
			myDropzone = new Dropzone("#image-dropzone",{
				
				headers: {
					'X-CSRF-TOKEN': '{{csrf_field()}}',
				},
				paramName: "file", // The name that will be used to transfer the file
				maxFilesize: 2, // MB
				addRemoveLinks: true,
				maxFiles: 1,
				acceptedFiles: ".jpeg,.jpg,.png,.gif",
				init: function() {
					  this.on("maxfilesexceeded", function() {
						if (this.files[1]!=null){
							  this.removeFile(this.files[1]);
						}
						swal("Error!", "You can Upload one image", "error");
					});
					this.on("error", function(file) {
						this.removeFile(file);
						swal("Error!", "Error Occured in Uploading", "error");
					});
					
					this.on("complete", function (data) {
						//var res = eval('(' + data.xhr.responseText + ')');
						
						if(data.xhr && data.xhr.response !== "error")
						{
							var imgName = data.xhr.response;
							var imgId = data.xhr.response.split('.')[0];
							$('#comment_content_section').append('<input type="hidden" name="comment_image" value="'+data.xhr.response+'" id="'+imgId+'">');
						
						}
						else
						{
							swal("Error!", "Error Occured in Uploading", "error");
							this.removeFile(data);
						}
						
						if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
							//alert("all uploaded");
							$('#post-new').show()
						  }
					});
					this.on('removedfile', function (file) {
						if(file.xhr)
						{
							var inputId = file.xhr.response;
							var imgId = file.xhr.response.split('.')[0];
							$( '#'+imgId ).remove();
						}
						
						//alert("removed.. " + inputId);
					});
					  this.on("addedfile", function(file) {
						$('#post-new').hide()
						  //alert("file added");
						/* Maybe display some more file information on your page */
					  });
				},
			});
		});
	});

	$('#post-comment-form').on('submit', function(e) {
		e.preventDefault(); 
		$.ajax({
			type: "POST",
			headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
			url: '/trainer/postComment/{{$topic_id}}/{{$tweet_id}}',
			data: $(this).serialize(),
			success: function(msg) {
			$("#form").trigger('reset');
			tinymce.get('post-message').setContent('');
			$('#comment_content_type').val('text');
			$('#comment_content_section').html('');
			$('#progress_view').html('');
			myDropzone=null;
			//alert(msg.data.comment);
			var new_comment = '<div class="media">'+
					'<a class="media-left" href="#">'+
						'<img class="media-object img-radius m-r-20" src="{{URL::asset("trainer/profile_image/".\Auth::user()->profile_image)}}" alt="Generic placeholder image">'+
						
					'</a>'+
					'<div class="media-body b-b-theme social-client-description">'+
						'<div class="chat-header">{{\Auth::user()->name}}'+
							' <span class="text-muted"> '+msg.data.comment.created_at+'</span>'+
						'</div>'+
						'<p class="text-muted">'+atob(msg.data.comment.comment_description)+'</p>';
			if(msg.data.comment.comment_image)
			{
				new_comment+= '<img class="max-width-400" src="/comment_content/'+msg.data.comment.comment_image+'" alt="">';
			}
			var new_comment_id=msg.data.comment.comment_id; 
			var new_tweet_id=msg.data.comment.tweet_id; 			
			new_comment+=
			'<div class="social-msg m-2 "><a class="waves-effect href="#!" id="favorite-comment-color-'+new_comment_id+'" onclick="postcommentreaction('+new_comment_id+','+new_tweet_id+','+"'favorite'"+')">'+
						'<i class="icofont icofont-heart-alt text-muted">'+
						'</i>'+
						'<span class="p-l-0 m-r-0">Favorite </span>'+
					'</a>'+
					
					'<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab" onclick="getCommentReactionUsers('+new_comment_id+','+"'favorite'"+')">'+
						'<span class="b-r-theme" id="total_comment_favorites_'+new_comment_id+'">( 0 )</span>'+
					'</a>'+

					'<a class="waves-effect " href="#!" id="like-comment-color-'+new_comment_id+'" onclick="postcommentreaction('+new_comment_id+','+new_tweet_id+','+"'like'"+')">'+
						'<i class="icofont icofont-thumbs-up text-muted">'+
						'</i>'+
						'<span class="p-l-0 m-r-0">Likes</span>'+
					'</a>'+
					'<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab-like" onclick="getCommentReactionUsers('+new_comment_id+','+"'like'"+')">'+
						'<span class="b-r-theme" id="total_comment_likes_'+new_comment_id+'">( 0 )</span>'+
					'</a>'+

					'<a class="waves-effect " id="dislike-comment-color-'+new_comment_id+'" onclick="postcommentreaction('+new_comment_id+','+new_tweet_id+','+"'dislike'"+')">'+
						'<i class="icofont icofont-thumbs-down text-muted">'+
						'</i>'+
						'<span class="p-l-0 m-r-0" >Dislike</span>'+
					'</a>'+
					'<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab-dislike" onclick="getCommentReactionUsers('+new_comment_id+','+"'dislike'"+')">'+
						'<span class="b-r-theme" id="total_comment_dislikes_'+new_comment_id+'">( 0 )</span>'+
					'</a>'+
			'</div>';
			new_comment	+='</div>'+
				'</div>';
				$('#comment-here').append(new_comment);
				
				$('#total_likes').text("( "+msg.data.total_likes+" )");
				$('#total_favorites').text("( "+msg.data.total_favorites+" )");
				$('#total_dislikes').text("( "+msg.data.total_dislikes+" )");
				$('#total_comments').text("( "+msg.data.total_comments+" )");
				
			}
		});
	});
	function commentSubmission()
	{
		var editorContent = tinyMCE.get('post-message').getContent();
		var imageVal =  $("#comment_content_section").children("input").val();
		//alert(imageVal);
		if (editorContent=="" && (imageVal=='' || imageVal==undefined))
		{
			swal("Error!", "Please write few words or you can select an image", "error");
			return;
		}
		

		$('#post-comment-form').submit();
		//alert('button clicked');
		
	}


	function getMoreComments()
	{
		$.ajax({
			type : 'GET',
			url: "/trainer/getmorecomments/{{$topic_id}}/{{$tweet_id}}?page=" + pageNumber,
			success : function(data){
				pageNumber +=1;
					   if(data.comment.data.length>0){
						   var comments = data.comment.data;
						   var new_comments_html="";
						   comments.forEach(function(comment) {
								console.log(comment);
								new_comments_html+='<div class="media">'+
									'<a class="media-left" href="#">'+
										'<img class="media-object img-radius m-r-20" src="/'+comment.user_type+'/profile_image/'+comment.profile_image+'" alt="Generic placeholder image">'+
										
									'</a>'+
									'<div class="media-body b-b-theme social-client-description">'+
										'<div class="chat-header">{{\Auth::user()->name}}'+
											' <span class="text-muted"> '+comment.created_at+'</span>'+
										'</div>'+
										'<p class="text-muted">'+atob(comment.comment_description)+'</p>';
										if(comment.comment_image)
										{
											new_comments_html+= '<img class="max-width-400" src="/comment_content/'+comment.comment_image+'" alt="">';
										}
										new_comments_html	+='</div>'+
										'</div>';
									});
							   $('#comment-here').append(new_comments_html);
					   }else{
						   // Great we have more articles
						   $('#view-more-comments-div').html('<a> No more Comments </a>');
							
					   }
					   
			},error: function(data){
														  
			},
		})   
	}

	

</script>

<script>
	function postreaction(id, reaction){
		//alert(reaction);
		var tweet_content = reaction;
		$.ajax({
				  
			type:'POST',
			url:'/trainer/tweet-post-reaction',
			headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
			data: { "reaction_content" : tweet_content, "tweet_id" : id },
			success: function(data){
			  if(data.data.success){
				if(data.data.status == 'updated'){
				$('#'+data.data.param+'-color').addClass('active');
				$('#'+data.data.previous_param+'-color').removeClass('active');
				}else if(data.data.status == 'deleted'){
				$('#'+data.data.param+'-color').removeClass('active');
				}else{
				$('#'+data.data.param+'-color').addClass('active');
				}
				$('#total_likes').text("( "+data.data.total_likes+" )");
				$('#total_favorites').text("( "+data.data.total_favorites+" )");
				$('#total_dislikes').text("( "+data.data.total_dislikes+" )");
				$('#total_comments').text("( "+data.data.total_comments+" )");
				
				  swal("Success!", "Reaction Updated Successfully", "success");
			  }else{
				  swal("Error!", "Error in Updating Reaction", "error");
			  }
			}
		});

	}
</script>
<script>
	function getReactionUsers(id, reaction){
		$('#'+reaction+'-modal-data').html('<div class="fa fa-spinner fa-spin fa-lg"></div>');
		$.ajax({
				  
			type:'GET',
			url:'/trainer/tweet-reaction-users/'+id+'/'+reaction,
			headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
			
			success: function(data){
			  if(data.success){
				  var appendid = $('#like-modal-data');
				if(reaction == 'favorite'){
					appendid = $('#favorite-modal-data');

				}else if(reaction == 'dislike'){
					appendid = $('#dislike-modal-data');

				}
				var reactionUsers = data.data;
				var userhtml = '';
				if(reactionUsers.length > 0){
 					reactionUsers.forEach(function(user){
						userhtml+= '<div class="media m-b-10">'+
								'<a class="media-left" href="#!">';
						
						userhtml+=	'<img class="media-object img-radius" src="/'+user.user_type+'/profile_image/'+user.profile_image+'" alt="Generic placeholder image" data-toggle="tooltip" data-placement="top" title="user image">'+
									'</a>'+
									'<div class="media-body">';
						userhtml+='<div class="chat-header">'+user.name+'</div>'+
									'<div class="text-muted social-designation">'+user.user_type+'</div>'+
									'</div></div>';
                      //alert(user.name);
				});
				appendid.html(userhtml);
				
			}else{
				appendid.html("no data available");
			}
			$('#total_likes').text("( "+data.total_likes+" )");
			$('#total_favorites').text("( "+data.total_favorites+" )");
			$('#total_dislikes').text("( "+data.total_dislikes+" )");
			$('#total_comments').text("( "+data.total_comments+" )");
				
		}else{
			swal("Error!", "Unable to get reaction. Try Again!", "error");
		}
	}
	});

}
</script>
<script>
		function postcommentreaction(comment_id, tweet_id, reaction){
			//alert(reaction);
			var tweet_content = reaction;
			$.ajax({
					  
				type:'POST',
				url:'/trainer/comment-post-reaction',
				headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
				data: { "comment_reaction_content" : tweet_content, "tweet_id" : tweet_id,"comment_id" : comment_id},
				success: function(data){
				  if(data.data.success){

					if(data.data.status == 'updated'){
					$('#'+data.data.param+'-comment-color-'+comment_id).addClass('active');
					$('#'+data.data.previous_param+'-comment-color-'+comment_id).removeClass('active');
					}else if(data.data.status == 'deleted'){
					$('#'+data.data.param+'-comment-color-'+comment_id).removeClass('active');
					}else{
					$('#'+data.data.param+'-comment-color-'+comment_id).addClass('active');
					}
					$('#total_comment_likes_'+comment_id).text("("+data.data.total_likes+")");
					$('#total_comment_favorites_'+comment_id).text("("+data.data.total_favorites+")");
					$('#total_comment_dislikes_'+comment_id).text("("+data.data.total_dislikes+")");
					
					  swal("Success!", "Reaction Updated Successfully", "success");
				  }else{
					  swal("Error!", "Error in Updating Reaction", "error");
				  }
				}
			});
	
		}
	</script>

	<script>
		function getCommentReactionUsers(id, reaction){
			$('#'+reaction+'-modal-data').html('<div class="fa fa-spinner fa-spin fa-lg"></div>');
			$.ajax({
					  
				type:'GET',
				url:'/trainer/comment-reaction-users/'+id+'/'+reaction,
				headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
				
				success: function(data){
				  if(data.success){
					  var appendid = $('#like-modal-data');
					if(reaction == 'favorite'){
						appendid = $('#favorite-modal-data');
	
					}else if(reaction == 'dislike'){
						appendid = $('#dislike-modal-data');
	
					}
					var reactionUsers = data.data;
					var userhtml = '';
					if(reactionUsers.length > 0){
						 reactionUsers.forEach(function(user){
							userhtml+= '<div class="media m-b-10">'+
									'<a class="media-left" href="#!">';
							
							userhtml+=	'<img class="media-object img-radius" src="/'+user.user_type+'/profile_image/'+user.profile_image+'" alt="Generic placeholder image" data-toggle="tooltip" data-placement="top" title="user image">'+
										'</a>'+
										'<div class="media-body">';
							userhtml+='<div class="chat-header">'+user.name+'</div>'+
										'<div class="text-muted social-designation">'+user.user_type+'</div>'+
										'</div></div>';
						  //alert(user.name);
					});
					appendid.html(userhtml);
					
				}else{
					appendid.html("no data available");
				}
				$('#total_comment_likes'+id).text("( "+data.total_likes+" )");
				$('#total_comment_favorites'+id).text("( "+data.total_favorites+" )");
				$('#total_comment_dislikes'+id).text("( "+data.total_dislikes+" )");
				
					
			}else{
				swal("Error!", "Unable to get reaction. Try Again!", "error");
			}
		}
		});
	
	}

	function addLiveComments(data)
	{
		if(data.data.tweet_id == "{{$tweet_id}}")
		{
			var profile_image = "/"+data.sender.user_type +"/profile_image/"+data.sender.profile_image;
		var new_comment = '<div class="media">'+
					'<a class="media-left" href="#">'+
						'<img class="media-object img-radius m-r-20" src="'+profile_image+'" alt="Generic placeholder image">'+
						
					'</a>'+
					'<div class="media-body b-b-theme social-client-description">'+
						'<div class="chat-header">{{\Auth::user()->name}}'+
							' <span class="text-muted"> '+data.data.created_at+'</span>'+
						'</div>'+
						'<p class="text-muted">'+atob(data.data.comment_description)+'</p>';
			if(data.data.comment_image)
			{
				new_comment+= '<img class="max-width-400" src="/comment_content/'+data.data.comment_image+'" alt="">';
			}
			var new_comment_id=data.data.comment_id; 
			var new_tweet_id=data.data.tweet_id; 			
			new_comment+=
			'<div class="social-msg m-2 "><a class="waves-effect href="#!" id="favorite-comment-color-'+new_comment_id+'" onclick="postcommentreaction('+new_comment_id+','+new_tweet_id+','+"'favorite'"+')">'+
						'<i class="icofont icofont-heart-alt text-muted">'+
						'</i>'+
						'<span class="p-l-0 m-r-0">Favorite </span>'+
					'</a>'+
					
					'<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab" onclick="getCommentReactionUsers('+new_comment_id+','+"'favorite'"+')">'+
						'<span class="b-r-theme" id="total_comment_favorites_'+new_comment_id+'">( 0 )</span>'+
					'</a>'+

					'<a class="waves-effect " href="#!" id="like-comment-color-'+new_comment_id+'" onclick="postcommentreaction('+new_comment_id+','+new_tweet_id+','+"'like'"+')">'+
						'<i class="icofont icofont-thumbs-up text-muted">'+
						'</i>'+
						'<span class="p-l-0 m-r-0">Likes</span>'+
					'</a>'+
					'<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab-like" onclick="getCommentReactionUsers('+new_comment_id+','+"'like'"+')">'+
						'<span class="b-r-theme" id="total_comment_likes_'+new_comment_id+'">( 0 )</span>'+
					'</a>'+

					'<a class="waves-effect " id="dislike-comment-color-'+new_comment_id+'" onclick="postcommentreaction('+new_comment_id+','+new_tweet_id+','+"'dislike'"+')">'+
						'<i class="icofont icofont-thumbs-down text-muted">'+
						'</i>'+
						'<span class="p-l-0 m-r-0" >Dislike</span>'+
					'</a>'+
					'<a class="waves-effect" href="#!" data-toggle="modal" data-target="#Modal-tab-dislike" onclick="getCommentReactionUsers('+new_comment_id+','+"'dislike'"+')">'+
						'<span class="b-r-theme" id="total_comment_dislikes_'+new_comment_id+'">( 0 )</span>'+
					'</a>'+
			'</div>';
			new_comment	+='</div>'+
				'</div>';
				$('#comment-here').append(new_comment);
		}
		
	}
	function deletetweet(id){
		//alert(id);
		var href="/trainer/delete-tweet/"+ id;
		//$("#deletetopic").attr('href', href);
		swal({
			title: "Are you sure?",
			text: "This Tweet will be deleted",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}, function() {
			//$('#deletetopic').trigger("click");
			window.location = href;
		});
	}

	function updateTweetStatus(topic_id, tweet_id, status){
		//alert(id);
		var href="/trainer/tweet-status/"+topic_id+"/"+tweet_id +"/"+status;
		//$("#deletetopic").attr('href', href);
		swal({
			title: "Are you sure?",
			text: "You want to change status of Tweet.",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, Proceed!",
			closeOnConfirm: false
		}, function() {
			//$('#deletetopic').trigger("click");
			window.location = href;
		});
	}

	function updateTweetStatusActive(topic_id, tweet_id, status){
		var href="/trainer/tweet-status/"+topic_id+"/"+tweet_id +"/"+status;
		$('form').attr('action',href)
		$('#hidden-status-url').val(href);
	}
	</script>
	@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif 
	@include('partials.tweet-draw-stats',['tweet'=>$tweet])
@stop
