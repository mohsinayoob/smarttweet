<?php

return [

   
    'manage_categories' => 'إدارة الفئات',
    'tickets' => 'تذاكر',
    'empty' => 'لا يوجد حاليا أي تذاكر.',
    'category' => 'الفئة',
    'title' => 'عنوان',
    'Status' => 'الحالة',
    'last_updated' => 'آخر تحديث',
    'actions' => 'أفعال',
    'comment' => 'تعليق',
    'close' => 'قريب',
  
];
