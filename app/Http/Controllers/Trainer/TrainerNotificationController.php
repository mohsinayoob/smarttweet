<?php

namespace App\Http\Controllers\Trainer;

use App\Http\Controllers\Controller;
use App\Models\User;
use DB;

class TrainerNotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainer');

    }

    //
    public function getNotification()
    {
        $notifications = DB::table('notifications')
            ->where('notifiable_id', \Auth::user()->user_id)
            ->where('read_at', null)->where('viewed', 0)->get();

        return $notifications;

    }

    public function getAllNotification()
    {
        $notifications = DB::table('notifications')
            ->where('notifiable_id', \Auth::user()->user_id)
            ->where('read_at', null)->paginate(10);

        return view('trainer.notifications', compact('notifications'));

    }
    public function getAjaxAllNotification()
    {
        $notifications = DB::table('notifications')
            ->where('notifiable_id', \Auth::user()->user_id)
            ->where('read_at', null)->paginate(10);

            DB::table('notifications')->where('notifiable_id', \Auth::user()->user_id)
            ->update(['viewed' => 1]);

        return $notifications;

    }
}
