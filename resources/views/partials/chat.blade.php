@extends('layouts.'.\Auth::user()->user_type.'_layout') @section('title', 'Messages') @section('css-files')

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}">

<link rel="stylesheet" href="{{URL::asset('files/assets/quickblox/libs/stickerpipe/css/stickerpipe.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('files/assets/quickblox/css/style.css')}}">
 @stop 
 @section('body_content')
<div class="pcoded-content">
	<div class="">
		<div class="main-body">
			<div class="">
				<div class="page-body">
                  <!-- Main block -->
                  <div class="container">
                    <div id="main_block">
                
                        <div class="panel panel-primary">
                          <div class="panel-body">
                            <div class="row">
                
                              <div class="col-md-4">
                                <div class="list-header">
                                  <h4 class="list-header-title">Chat</h4>
                                </div>
                                <div class="list-group pre-scrollable nice-scroll" id="dialogs-list">
                
                                  <!-- list of chat dialogs will be here -->
                                  <span class="loading-dialogs-spinner"><i class="fa fa-spinner fa-spin" ></i></span>
                
                                </div>
                              </div>
                
                              <div id="mcs_container" class="col-md-8">
                                <div class="container del-style">
                                  <div class="content list-group pre-scrollable nice-scroll" id="messages-list">
                                       
                                    <span class="loading-chats-spinner"><i class="fa fa-spinner fa-spin" ></i></span>
                                    <!-- list of chat messages will be here -->
                
                                  </div>
                                </div>
                
                                <div><img src="{{URL::asset('files/assets/quickblox/images/ajax-loader.gif')}}" class="load-msg"></div>
                                <form class="form-inline" role="form" method="POST" action="" onsubmit="return submit_handler(this)">
                                  <div class="input-group">
                                    <span class="input-group-btn input-group-btn_change_load">
                                        <input id="load-img" type="file">
                                        <button type="button" id="attach_btn" class="btn btn-default" onclick="$('#load-img').click();">
                                          <i class="icon-photo"></i>
                                        </button>
                                    </span>
                                    <span class="input-group-btn input-group-btn_change_load">
                                        <button type="button" id="stickers_btn" class="btn btn-default" onclick="">
                                          <i class="icon-sticker"></i>
                                        </button>
                                    </span>
                                    <span class="input-group-btn" style="width: 100%;">
                                        <input type="text" class="form-control" id="message_text" placeholder="Enter message">
                                    </span>
                                    <span class="input-group-btn">
                                        <button  type="submit" id="send_btn" class="btn btn-default" onclick="clickSendMessage()">Send</button>
                                    </span>
                                  </div>
                                  <img src="{{URL::asset('files/assets/quickblox/images/ajax-loader.gif')}}" id="progress">
                                </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                
                    </div>
                    
                    <!-- Modal (login to chat)-->
                
                
                    <!-- Modal (new dialog)-->
                    
                
                    
				</div>
			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')
<script>
    var QBUser = {
        id: "{{\Auth::user()->qb_id}}",
        name:'{{\Auth::user()->name}}',
        login:'{{\Auth::user()->email}}',
        pass: '{{\Auth::user()->email}}'
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.0/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.4.1/jquery.timeago.min.js"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/quickblox/2.4.0/quickblox.min.js"></script>
<script src="{{URL::asset('files/assets/quickblox/libs/stickerpipe/js/stickerpipe.js')}}"></script>
<script src="{{URL::asset('files/assets/quickblox/js/config.js')}}"></script>
<script src="{{URL::asset('files/assets/quickblox/js/connection.js')}}"></script>
<script src="{{URL::asset('files/assets/quickblox/js/messages.js')}}"></script>
<script src="{{URL::asset('files/assets/quickblox/js/stickerpipe.js')}}"></script>
<script src="{{URL::asset('files/assets/quickblox/js/ui_helpers.js')}}"></script>
@if($chat_dialog)
@include('partials.chat-dialogs', ['chat_dialog' => $chat_dialog]);

@else
@include('partials.chat-dialogs')

@endif

<script src="{{URL::asset('files/assets/quickblox/js/users.js')}}"></script>
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-fullscreen.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-video.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-autoplay.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-zoom.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-hash.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-pager.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/wall/wall.js')}}"></script>
<script>
$(document).ready(function(){
  $('.lightgallery-message').click(function(e){
      //$(this).fancybox();
    e.preventDefault();
      $(this).lightGallery();
  });
});
</script>
@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif 

@stop