<?php

namespace App\Http\Controllers\trainee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use DB;
Use Session;
class TraineeNotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainee');
        
    }

    //
    public function getNotification()
    {
        $notifications = DB::table('notifications')
        ->where('notifiable_id', \Auth::user()->user_id)
        ->where('read_at', NULL)->where('viewed', 0)->get();
    
        return $notifications;

    }

    public function getAllNotification()
    {
        $notifications = DB::table('notifications')
        ->where('notifiable_id', \Auth::user()->user_id)
        ->where('read_at', NULL)->paginate(10);
       
        return view('trainee.notifications', compact('notifications'));

    }
    public function getAjaxAllNotification()
    {
        $notifications = DB::table('notifications')
        ->where('notifiable_id', \Auth::user()->user_id)
        ->where('read_at', NULL)->paginate(10);
        DB::table('notifications')->where('notifiable_id', \Auth::user()->user_id)
            ->update(['viewed' => 1]);
        return $notifications;

    }
}
