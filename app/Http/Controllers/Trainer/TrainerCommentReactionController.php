<?php

namespace App\Http\Controllers\Trainer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Tweet\CommentReaction;
use App\Models\Tweet\Comments;
use App\Models\User;
use App\Jobs\UpdateTweetReactionJob;
use App\Models\Tweet\Tweets;
use App\Jobs\CommentReactionNotificationJob;

class TrainerCommentReactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainer');
        
    }
    public function createCommentReaction (Request $request)
    {
        //return $course_id;
        $data = $request->all();
        DB::beginTransaction();
        try{
            $existreaction = CommentReaction::where('user_id', \Auth::user()->user_id )
            ->where('tweet_id', $data["tweet_id"] )
            ->where('comment_id', $data["comment_id"] )
            ->first();
            $comment_to_update = Comments::where('comment_id', $data["comment_id"])
            ->first();
           $comment_reaction_content = $data["comment_reaction_content"];
           $total = "total_$comment_reaction_content"."s";
            if(!empty($existreaction))
            {
                if($existreaction->comment_reaction_content ==  $data["comment_reaction_content"]){
                    CommentReaction::where('comment_reaction_id', $existreaction->comment_reaction_id )->delete();
                    $comment_to_update[$total] -=1;
                    $comment_to_update->save();
                    DB::commit();
                    return response()->json([
                        'data' => [
                          'success' => 'true',
                          'status' => 'deleted',
                          'param' =>  $data["comment_reaction_content"],
                          'total_likes'=> $comment_to_update['total_likes'],
                        'total_favorites'=> $comment_to_update['total_favorites'],
                        'total_dislikes'=> $comment_to_update['total_dislikes'],
                        ]
                      ]);
                }
                $comment_to_update[$total] +=1;
                $previous = $existreaction->comment_reaction_content;
                $total = "total_$previous"."s";
                $comment_to_update[$total] -=1;
                $existreaction->comment_reaction_content = $data["comment_reaction_content"];
                $existreaction->save($data);
                $comment_to_update->save();
                DB::commit();
                return response()->json([
                    'data' => [

                        'success' => 'true',
                        'status' => 'updated',
                        'param' =>  $data["comment_reaction_content"],
                        'previous_param' =>  $previous,
                        'total_likes'=> $comment_to_update['total_likes'],
                        'total_favorites'=> $comment_to_update['total_favorites'],
                        'total_dislikes'=> $comment_to_update['total_dislikes'],
                    ]
                  ]);
            }
        else{

        $data["status"] = 'Active';
        $data["user_id"] = \Auth::user()->user_id;
        //return $data;
        
        $comment_reaction = CommentReaction::create($data);
        $comment_to_update[$total] +=1;
        $comment_to_update->save();
        DB::commit();
        $notification_job = (new CommentReactionNotificationJob($comment_reaction))->delay(13);
        $this->dispatch($notification_job);
        return response()->json([
            'data' => [
                'success' => 'true',
                'status' => 'inserted',
                'param' =>  $data["comment_reaction_content"],
                'total_likes'=> $comment_to_update['total_likes'],
                'total_favorites'=> $comment_to_update['total_favorites'],
                'total_dislikes'=> $comment_to_update['total_dislikes'],
            ]
          ]);
            }
    } catch (Exception $e) {
        return response()->json([
            'data' => [
              'success' => 'false',
            ]
          ]);
    }

    }
    public function getCommentReactionUsers ($comment_id, $reaction_content)
    {
        $commentReactionUsers = CommentReaction::join('users', 'users.user_id', '=', 'comments_reaction.user_id')
        ->where ('comments_reaction.comment_id', $comment_id)
        ->where ('comments_reaction.comment_reaction_content', $reaction_content)
        ->select( 'users.name', 'users.profile_image','users.user_type')->get();
        $comment = Comments::where('comment_id',$comment_id)
        ->select('total_likes','total_favorites','total_dislikes')->first();

        //return $reactionUsers;
        return response()->json([
                'success' => 'true',
                'data' => $commentReactionUsers,
                'total_likes'=> $comment['total_likes'],
                'total_favorites'=> $comment['total_favorites'],
                'total_dislikes'=> $comment['total_dislikes'],
                
          ]);
    } 
    
}
