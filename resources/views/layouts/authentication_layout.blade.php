<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">


<head>
	<title>@yield('title') </title>


	<!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')}}"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js')}}/1.4.2/respond.min.js')}}"></script>
      <![endif]-->

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="Smart Tweet"/>
	<meta name="keywords" content="Smart Tweet"/>
	<meta name="author" content="Smart Tweet" />

	<link rel="icon" href="{{URL::asset('files/assets/images/favicon.ico')}}" type="image/x-icon">

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/icofont/css/icofont.css')}}">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/themify-icons/themify-icons.css')}}">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/font-awesome/css/font-awesome.min.css')}}">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/css/style.css')}}"> 
	@yield('css-files')
</head>

<body>

	<div class="theme-loader">
		<div class="loader-track">
			<div class="loader-bar"></div>
		</div>
	</div>

	<div id="pcoded" class="pcoded load-height">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<nav class="navbar header-navbar pcoded-header auth-header" header-theme="theme5">
				<div class="navbar-wrapper">
					<div class="navbar-logo">

						<a href="/">
							<img class="img-fluid" src="{{URL::asset('files/assets/images/logo.png')}}" alt="Samrt Tweet" />
						</a>
						<a class="mobile-options">
							<i class="ti-more"></i>
						</a>
					</div>
					<div class="navbar-container container-fluid">
						<ul class="nav-left">

							<li>
								<a href="#!" onclick="javascript:toggleFullScreen()">
									<i class="ti-fullscreen"></i>
								</a>
							</li>
						</ul>
						<ul class="nav-right">
						
							<li class="active">
								
    										
											@if(Lang::locale()==='ar')
											<a href="{{url('language/en')}}"> 
												{{"الإنجليزية"}}
											</a>
											@else
												<a href="{{url('language/ar')}}"> 
												{{"Arabic"}}
											</a>
											
											@endif
											

							</li>

							<li class="active">
								<a href="/register">

									<i class="ti-unlock"></i>
									{{trans ('label.Register') }}
								</a>
							</li>
							<ul class="nav-right">
								
								<li >
									<a href="/login">
										<span class="pcoded-micon">
											<i class="ti-lock"></i>
										</span>
										<span class="pcoded-mtext">{{trans('label.Sign In') }}</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>

							</ul>
					</div>
				</div>
			</nav>
		</div>
		@yield('body_content')

	</div>
	<script src="{{URL::asset('files/bower_components/jquery/js/jquery.min.js')}}"></script>
	<script src="{{URL::asset('files/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
	<script src="{{URL::asset('files/bower_components/popper.js/js/popper.min.js')}}"></script>
	<script src="{{URL::asset('files/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>


	<script src="{{URL::asset('files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>

	<script src="{{URL::asset('files/bower_components/modernizr/js/modernizr.js')}}"></script>

	<script src="{{URL::asset('/files/bower_components/modernizr/js/css-scrollbars.js')}}"></script>

	<script src="{{URL::asset('files/assets/js/SmoothScroll.js')}}"></script>
	<script src="{{URL::asset('files/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>


	<script src="{{URL::asset('files/assets/js/pcoded.min.js')}}"></script>
	@if(Lang::locale()==='en')
	<script src="{{URL::asset('files/assets/js/dark-light/vertical-layout.min.js')}}"></script>
	@else
		
	<script src="{{URL::asset('files/assets/js/dark-light/menu/menu-rtl.js')}}"></script>
	@endif


	<script src="{{URL::asset('files/assets/js/script.js')}}"></script>
	@yield('javascript-files')
</body>

</html>