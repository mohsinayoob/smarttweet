<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Course\Topic;
use App\Models\User;
use App\Models\Course\CourseEnrollment;
class TweetNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tweet;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tweet)
    {
        //
        $this->tweet = $tweet;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $enrollments = CourseEnrollment::join('course','course.course_id','=','course_enrollment.course_id')
        ->where('course_enrollment.course_id',$this->tweet->course_id)
        ->where('course_enrollment.status','Active')
        ->select()->get();
        
        foreach($enrollments as $enrollment)
        {
           // echo $enrollment;
            $user = User::where('user_id',$enrollment->trainee_id)->first();
            $this->tweet->user = \Auth::user();
            $this->tweet->url = "/".$user->user_type."/topic/".$enrollment->course_id;
            $this->tweet->notification_type = "TweetActivated";
            $this->tweet->message = " activated new Tweet in ". $enrollment->course_title;
            $user->notify(new \App\Notifications\SaveNotification($this->tweet, \Auth::user(), $this->tweet->message, $this->tweet->url));
        }
        
    }
}
