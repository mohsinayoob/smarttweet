<?php

namespace App\Http\Controllers\Trainer;

use App\Http\Controllers\Controller;
use App\Jobs\EnrollmentApprovedNotificationJob;
use App\Models\Chat\ChatDialog;
use App\Models\Course\CourseEnrollment;
use App\Models\Course\Course;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Session;

class TrainerEnrollmentController extends Controller
{
    protected $qb_token;
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainer');

    }

    protected function createChatDialog($user)
    {
        $session =ChatDialog::createSession(env('QB_APP_ID'), env('QB_API_KEY'), env('QB_API_SECRET'), env('QB_ACCOUNT_REFERENCE'), env('QB_PASSWORD'), \Auth::user());
        $this->qb_token = $session->token;
        return ChatDialog::createChatDialog($user, $this->qb_token);

    }

    
    public function UpdateEnrollmentStatus(Request $request)
    {
        $data = $request->all();
        //return $data;
        DB::beginTransaction();
        try
        {

            // Category::create();

            $existCourse = CourseEnrollment::where('enrollment_id', $data['enrollment_id'])->first();
            if (!empty($existCourse)) {
                $user = User::where('user_id', $existCourse->trainee_id)->first();
                
                if ($existCourse->status == 'Pending') {
                    
                    $existCourse->status = 'Active';
                    $course = Course::where('course_id',$existCourse->course_id)->first();
                    $chat_dialog = ChatDialog::where('trainee_id', $user->user_id)
                        ->where('trainer_id', \Auth::user()->user_id)->first();
                    $group_chat_dialog = ChatDialog::where('course_id', $existCourse->course_id)->first();
                    if (empty($chat_dialog)) {
                        $response = $this->createChatDialog($user);
                        $responseJSON = json_decode($response);
                        if ($responseJSON) {
                            $dialog_id = $responseJSON->_id;
                            $data = array(
                                "trainee_id" => $user->user_id,
                                "trainer_id" => \Auth::user()->user_id,
                                "dialog_id" => $dialog_id,
                            );
                            $new_dialog = ChatDialog::create($data);
                        }
                    }
                    if (empty($group_chat_dialog)) {
                        
                        $response = ChatDialog::createGroupChatDialog($user,$course, $group_chat_dialog, $this->qb_token);
                        $responseJSON = json_decode($response);
                        if ($responseJSON) {
                            $dialog_id = $responseJSON->_id;
                            $data = array(
                                "course_id" => $course->course_id,
                                "trainer_id" => \Auth::user()->user_id,
                                "dialog_id" => $dialog_id,
                            );
                            $new_dialog = ChatDialog::create($data);
                        }
                    } else {
                        $new_dialog = ChatDialog::createGroupChatDialog($user,$course, $group_chat_dialog, $this->qb_token);
                    }

                    $notification_job = (new EnrollmentApprovedNotificationJob($existCourse))->delay(13);
                    $this->dispatch($notification_job);
                } else {
                    $existCourse->status = 'Pending';
                }

                $existCourse->save();
                DB::commit();

                return response()->json([
                    'data' => [
                        'success' => 'true',
                    ],
                ]);

            } else {
                return response()->json([
                    'data' => [
                        'success' => 'false',
                    ],
                ]);

            }

        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'data' => [
                    'success' => 'false',
                ],
            ]);
        }

    }

    public function index()
    {
        $enrollments = CourseEnrollment::join('users', 'users.user_id', '=', 'course_enrollment.trainee_id')
            ->join('course', 'course.course_id', '=', 'course_enrollment.course_id')
            ->where('course_enrollment.trainer_id', \Auth::user()->user_id)
            ->select('course_enrollment.*', 'users.name', 'users.profile_image', 'course.course_title')->get();

        //return $enrollments;
        return view('trainer.enrollment', compact('enrollments'));
    }

}
