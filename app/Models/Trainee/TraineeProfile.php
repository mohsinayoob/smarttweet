<?php

namespace App\Models\trainee;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class TraineeProfile extends Model
{
    use Notifiable;
    protected $table = 'trainee_profile';
    protected $primaryKey = 'trainee_profile_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','gender','dob','martial_status','address', 'mobile_no','last_completed_degree',"skills",'current_occupation','is_complete','tag_line','created_at','updated_at','cover_image'];

   
}
