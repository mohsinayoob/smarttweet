@extends('layouts.authentication_layout') @section('title', 'Register') 
@section('css-files')
<style>
.login{
	position:relative!important;
}
</style>
@stop


@section('body_content')
<section class="login header d-flex text-center">

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">

				<div class="signup-card card-block auth-body mr-auto ml-auto">
				@if (session('message'))
				<div class="alert alert-success">
					{{ session('message') }}
				</div>
				@endif
					<form class="md-float-material" method="POST" action="{{ route('register') }}">
						{{ csrf_field() }}
						<div class="auth-box">
							<div class="row m-b-20">
								<div class="col-md-12">
									<h3 class="text-center txt-primary">{{trans ('register.Sign up') }}</h3>
								</div>
							</div>
							<hr /> @if ($errors->has('name'))
							<span class="help-block" style="color:red;">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
							<div class="input-group">
								<input type="text" class="form-control" placeholder="{{trans ('register.name') }}" name="name" value="{{ old('name') }}" required autofocus>
								<span class="md-line"></span>
							</div>
							@if ($errors->has('email'))
							<span class="help-block" style="color:red;">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
							<div class="input-group">
								<input type="text" class="form-control" placeholder="{{trans ('register.Email') }}" name="email" value="{{ old('email') }}" required>
								<span class="md-line"></span>
							</div>
							@if ($errors->has('password'))
							<span class="help-block" style="color:red;">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
							<div class="input-group">
								<input type="password" class="form-control" placeholder="{{trans ('register.CPassword') }}" name="password" required>
								<span class="md-line"></span>
							</div>
							<div class="input-group">
								<input type="password" class="form-control" placeholder="{{trans ('register.ConPassword') }}" name="password_confirmation" required>
								<span class="md-line"></span>
							</div>
							@if ($errors->has('user_type'))
							<span class="help-block" style="color:red;">
								<strong>{{ $errors->first('user_type') }}</strong>
							</span>
							@endif
							<div class="input-group">
								<select class="form-control form-control-primary" name="user_type" required>
									<option value="">{{trans ('register.Who') }}</option>
									<option value="trainer" {{ old('user_type') =="trainer" ? "selected" : "" }}>Trainer</option>
									<option value="trainee" {{ old('user_type') =="trainee" ? "selected" : "" }}>Trainee</option>

								</select>
							</div>

							@if ($errors->has('category'))
							<span class="help-block" style="color:red;">
								<strong>{{ $errors->first('category') }}</strong>
							</span>
							@endif
							<div class="input-group">
								<select class="form-control form-control-primary" name="sector" required>
									<option value="">{{trans ('register.SelectS|C') }}</option>
									@foreach($categories as $category)
									<option value="{{$category->category_title}}" {{ old('sector')==$category->category_title ? "selected":"" }}>{{$category->category_title}}</option>
									@endforeach

								</select>
							</div>

							<div class="row m-t-25 text-left">
								<div class="col-md-12">
									<div class="checkbox-fade fade-in-primary">
										<label>
											<input type="checkbox" value="" name="accept_term" required>
											<span class="cr">
												<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
											</span>
											<span class="text-inverse">{{trans ('register.read') }}
												<a href="#">{{trans ('register.T&C') }}</a>
											</span>
										</label>
									</div>
								</div>

							</div>
							<div class="row m-t-30">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">{{trans ('register.Register') }}</button>
								</div>
							</div>
							<hr />

						</div>
					</form>

				</div>

			</div>

		</div>

	</div>

</section>


@stop