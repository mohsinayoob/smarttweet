@if(!empty($tweet))
<script>
        $(document).ready(function() {
        var feedback_charts=document.getElementById("feedback-charts").getContext("2d");
        var config= {
            type:'doughnut', data: {
                datasets:[ {
                    data: [ {{$tweet->total_favorites}},{{$tweet->total_likes}}, {{$tweet->total_dislikes}} ], backgroundColor: ["#4099ff", "#2ed8b6","#FF5370"], label: 'Dataset 1', borderWidth: 0
                }
                ], labels:["Favorites","Likes","Dislikes"]
            }
            , options: {
                responsive:true, legend: {
                    display: false,
                }
                , title: {
                    display: false, text: 'Chart.js Doughnut Chart'
                }
                , animation: {
                    animateScale: true, animateRotate: true
                }
            }
        };
        window.myDoughnut=new Chart(feedback_charts, config);
    });
    </script>
    @endif