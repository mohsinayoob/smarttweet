<?php

namespace App\Models\Tweet;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Comments extends Model
{
    //
    use Notifiable;
    protected $table = 'comments';
    protected $primaryKey = 'comment_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['tweet_id', 'user_id', 'comment_type','comment_description','comment_image', 'total_favorite','total_like', 'total_dislikes','created_at','updated_at' , 'status'];
    public function comment_reaction_content()
    {
        return $this->hasOne('App\Models\Tweet\CommentReaction','comment_id');
    }
}
