<?php

namespace App\Http\Controllers\Trainer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Course\Course;
use App\Models\Course\Category;
use App\Models\Course\Topic;
use App\Models\Tweet\Tweets;
use App\Models\Tweet\Comments;
use App\Models\Course\CourseEnrollment;
use App\Models\Chat\ChatDialog;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
//use Illuminate\Support\Facades\Request;
use Session;

class TrainerCourseController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainer');
        
    }

    protected function createGroupDialog($course)
    {
        $group_chat_dialog = ChatDialog::where('course_id', $course->course_id)->first();
        $qb_token = NULL;
        if (empty($group_chat_dialog)) {
            $response = ChatDialog::createGroupChatDialog(\Auth::user(),$course, $group_chat_dialog, $qb_token);
            $responseJSON = json_decode($response);
            if ($responseJSON) {
                $dialog_id = $responseJSON->_id;
                $data = array(
                    "course_id" => $course->course_id,
                    "trainer_id" => \Auth::user()->user_id,
                    "dialog_id" => $dialog_id,
                );
                $new_dialog = ChatDialog::create($data);
            }
        } else {
            $new_dialog = ChatDialog::createGroupChatDialog(NULL,$course, $group_chat_dialog, $qb_token);
        }
    }
    public function createCourse (Request $request)
    {
        DB::beginTransaction();
        try{
        $category_validator = Validator::make($request->all(), [
            'course_title' => 'required',
            'course_category' => 'required',
            'course_description' => 'required',
        ]);

        if ($category_validator->fails()) {
            $messages = $category_validator->messages();

            return back()->withError($messages)->with('error_messages', "Error in Validations");
        }
        $data = $request->all();
        
        $data["trainer_id"] = \Auth::user()->user_id;
        $data["status"] = 'Active';
        $data["created_by"] = 'Trainer';
        //return $data;
        
        $course = Course::create($data);
        $this->createGroupDialog($course);
        // After creating the user send an email with the random token generated in the create method above

        DB::commit();
        Session::flash('update', 'Course created. Now you can Add Topic here!!');
        return redirect()->to('/trainer/topic/'.$course->course_id)->with('update', 'Course created. Now you can Add Topic here!!');
    } catch (Exception $e) {
        DB::rollback();
        return back()->with('error_messages', 'Error in Saving data');
    }

    }

    public function index()
    {
        $categories = Category::all();
        //return $categories;
        //return view('trainer.profile', $profile);
        return view('trainer.create-course', compact('categories'));
    } 
    protected function searchCourses($q,$status,$date)
    {
        if($date == "Today")
        {
            if(!empty($status))
            {
                $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
                ->whereDay('created_at',date('d'))->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
                ->where('course.status', $status)
                ->where('trainer_id', \Auth::user()->user_id)
                ->paginate(10);
                //return $courses;
                return view('trainer.manage-course', compact('courses'));
            }
            $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
            ->whereDay('created_at',date('d'))->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
            ->where('trainer_id', \Auth::user()->user_id)
            ->paginate(10);
           // return $courses;
            return view('trainer.manage-course', compact('courses'));

        }
        else if($date == "Month")
        {
            if(!empty($status))
            {
                $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
                ->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
                ->where('trainer_id', \Auth::user()->user_id)
                ->where('course.status', $status)
                ->paginate(10);
               // return $courses;
                return view('trainer.manage-course', compact('courses'));
            }
            $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
            ->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
            ->where('trainer_id', \Auth::user()->user_id)
            ->paginate(10);
            //return $courses;
            return view('trainer.manage-course', compact('courses'));

        }
        else if($date == "Year")
        {
            if(!empty($status))
            {
                $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
                ->whereYear('created_at',date('Y'))
                ->where('trainer_id', \Auth::user()->user_id)
                ->where('course.status', $status)
                ->paginate(10);
                //return $courses;
                return view('trainer.manage-course', compact('courses'));
            }
            $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
            ->whereYear('created_at',date('Y'))
            ->where('trainer_id', \Auth::user()->user_id)
            ->paginate(10);
            //return $courses;
            return view('trainer.manage-course', compact('courses'));

        }
        if(!empty($status))
        {
            $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
            ->where('course.status', $status)
            ->where('trainer_id', \Auth::user()->user_id)
            ->paginate(10);
            // return $courses;
            return view('trainer.manage-course', compact('courses'));
        }
        $courses = Course::where('course.course_title', 'like', '%' . $q . '%')
        ->where('trainer_id', \Auth::user()->user_id)
        ->paginate(10);
       // return $courses;
        return view('trainer.manage-course', compact('courses'));
    }

    public function ViewCourses()
    {
        $q = Input::get('q'); 
        $status = Input::get('status'); 
        $date = Input::get('date'); 
        if (!empty($q) || !empty($status) || !empty($date)) {
            return $this->searchCourses($q,$status,$date);
        }
        
        $courses = Course::where('trainer_id', \Auth::user()->user_id)->paginate(10);
        //return $courses;
        //return view('trainer.profile', $profile);
        return view('trainer.manage-course', compact('courses'));
    } 

    public function UpdateCourseStatus($course_id, $status)
    {
        
        DB::beginTransaction();
        try
        {
            
           // Category::create();
           
            $existCourse = Course::where('course_id', $course_id)->first();
            if(!empty($existCourse))
            {
                $existCourse->status = $status;
                $existCourse->save();
                DB::commit();
                Session::flash('update', 'Course Status Updated Successfully');
                return back()->with('update', 'Course Status Successfully Updated');
            }
            else
            {
                return back()->with('error_messages', 'Error in Updating Course Status');
            }
            
            
            
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Updating Course Status');
        }

    }
    public function ViewEditCourses($course_id)
    {
        
        
        $editcourses = Course::where('trainer_id', \Auth::user()->user_id)
        ->where('course_id', $course_id)->first();
        if(!empty($editcourses)){
        $categories = Category::all();
        //return $courses;
        //return view('trainer.profile', $profile);
        return view('trainer.edit-course', compact('editcourses','categories'));
    }
    return redirect()->to('/trainer/manage-course')->with('error_messages','Could not find the Course');
    } 

    public function UpdateCourse($course_id, Request $request)
    {
        $data = $request->all();
        //return $data;
        $course_validator = Validator::make($data , [
            'course_title' => 'required',
            'course_description' => 'required',
            'course_category' => 'required',
        ]);

        if ($course_validator->fails()) {
            $messages = $course_validator->messages();
//return $messages;
            return back()->withError($messages)->with('error_messages', "Topic and title are required.");
        }
        DB::beginTransaction();
        try
        {
            
           // Category::create();
           
            $existCourse = Course::where('course_id', $course_id)->first();
            if(!empty($existCourse))
            {
                $existCourse->course_title=$data['course_title'];
                $existCourse->course_description=$data['course_description'];
                $existCourse->course_category=$data['course_category'];
                //$existCourse->status = $status;
                $existCourse->save($data);
                $this->createGroupDialog($existCourse);
                DB::commit();
                Session::flash('update', 'Course Updated Successfully');
                return redirect()->to('/trainer/manage-course')->with('update', 'Course Successfully Updated');
            }
            else
            {
                return back()->with('error_messages', 'Error in Updating Course');
            }
            
            
            
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Updating Course Status');
        }

    }
    public function DeleteCourse($course_id)
    {
        //return $topic_id;
        DB::beginTransaction();
        try
        {
           // $existCourse = Course::where('course_id', $course_id)->first();
            $existTopic = Topic::where('course_id', $course_id)->select('topic_id')->get();
           //return $existTopic;
            $existTweets= Tweets::whereIn('topic_id', $existTopic)->select('tweet_id')->get();
            //return $existTweets;
         
            $existComments= DB::table('comments')->whereIn('tweet_id', $existTweets)->delete();
            //return $existComments; 
             $existTweetsdel= Tweets::whereIn('topic_id', $existTopic)->delete();
             $existTopicsdel= Topic::where('course_id', $course_id)->delete();
             $existEnrollmentsdel= CourseEnrollment::where('course_id', $course_id)->delete();
             $existCoursedel = Course::where('course_id', $course_id)->delete();
             //return $existCoursedel; 
             DB::commit();
            
            // After creating the user send an email with the random token generated in the create method above

           
            Session::flash('update', 'Course Deleted Successfully');
            return back()->with('update', 'Course Successfully Deleted');
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Deleted Course');
        }

    }

}
