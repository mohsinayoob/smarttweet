@extends('layouts.trainee_layout') @section('title', 'Single Course') @section('css-files')
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/lightgallery/css/lightgallery.min.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/simple-line-icons/css/simple-line-icons.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/icofont/css/icofont.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/themify-icons/themify-icons.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/jquery-bar-rating/css/css-stars.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/jquery-bar-rating/css/fontawesome-stars-o.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}">
<style>
	.br-wrapper {
		margin: 0px!important;
	}
</style>
@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">

				<div class="page-body">
					<div class="modal fade modal-flex" id="Modal-tab" tabindex="-1" role="dialog">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-body">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<ul class="nav nav-tabs" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">
												<i class="fa fa-heart"></i>
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#tab-profile" role="tab">
												<i class="icofont icofont-thumbs-up"></i>
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#tab-messages" role="tab">
												<i class="icofont icofont-thumbs-down"></i>
											</a>
										</li>

									</ul>
									<div class="tab-content modal-body">
										<div class="tab-pane active" id="tab-home" role="tabpanel">
											<div class="card-block user-box">
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-1.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-3.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
											</div>

										</div>
										<div class="tab-pane" id="tab-profile" role="tabpanel">
											<div class="card-block user-box">
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-1.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-3.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane" id="tab-messages" role="tabpanel">
											<div class="card-block user-box">
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-1.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-3.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
												<div class="media m-b-10">
													<a class="media-left" href="#!">
														<img class="media-object img-radius" src="{{URL::asset('files/assets/images/avatar-2.jpg')}}" alt="Generic placeholder image"
														 data-toggle="tooltip" data-placement="top" title="user image">

													</a>
													<div class="media-body">
														<div class="chat-header">Josephin Doe</div>
														<div class="text-muted social-designation">Softwear Engineer</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="md-overlay"></div>
					<div class="row">
						<div class="col-lg-9">
							<div class="row">

								<div class="col-md-12">

									<div>
										<div class="card bg-white p-relative">

											<div class="card-block">
												<div class="timeline-details">
													<div class="card p-3">
														<div class="media">
															<div class="media-body">
																<div class="dropdown-secondary dropdown f-left">
																	<h4 class="d-inline-block m-2">
																		{{$course->course_title}}
																	</h4>
																</div>
																<div class="dropdown-secondary dropdown f-right">
																@if(!empty($enrollment) && $enrollment->status=="Active")
																<a class="btn btn-success" >Already Enrolled</a>
																@elseif(!empty($enrollment) && $enrollment->status=="Pending")
																<a class="btn btn-success" >Waiting Approval</a>
																@else
                                                                
																	<a class="btn btn-success" href="/trainee/add-enrollment/{{$course->course_id}}">Enroll Now</a>
																
																@endif
															</div>
															</div>
														</div>

													</div>
													<p class="text-muted">
														{{$course->course_description}}
													</p>
												</div>
											</div>
											<div class="card-block b-b-theme b-t-theme social-m">
												<div class="row col-md-12">

													<span class="m-2">Total Rating:</span>
													<select id="example-css" class="rating-star-readonly" name="rating" autocomplete="off" disabled>
														<option value="1"{{$course->avg_rating>0 &&$course->avg_rating<=1 ? 'selected="selected"':''}}>1</option>
														<option value="2" {{$course->avg_rating>1 &&$course->avg_rating<=2 ? 'selected="selected"':''}}>2</option>
														<option value="3" {{$course->avg_rating>2 &&$course->avg_rating<=3 ? 'selected="selected"':''}}>3</option>
														<option value="4" {{$course->avg_rating>3 &&$course->avg_rating<=4 ? 'selected="selected"':''}}>4</option>
														<option value="5"{{$course->avg_rating>4 ? 'selected="selected"':''}}>5</option>
													</select>


												</div>
											</div>

											<div class="card-block user-box">
												@if(!empty($comments))
												@foreach($comments as $comment)
												<div class="media">
													<a class="media-left" href="#">
														<img class="media-object img-radius m-r-20" src="{{ $comment->profile_image ? URL::asset('/trainee/profile_image/'.$comment->profile_image): URL::asset('files/assets/images/social/profile.jpg')}}" alt="Trainee">
													</a>
													<div class="media-body b-b-theme social-client-description
                                    ">
														<div class="chat-header">{{$comment->trainee_name}}
															<span class="text-muted">Jane 04, 2015</span>
														</div>
														<p class="text-muted">
																{{$comment->rating_comments}}
														</p>
														<div class=" social-msg m-2 ">
																<select id="example-css" class="rating-star-readonly" name="rating" autocomplete="off" disabled>
																		<option value="1"{{$comment->rating>0 &&$comment->rating<=1 ? 'selected="selected"':''}}>1</option>
																		<option value="2" {{$comment->rating>1 &&$comment->rating<=2 ? 'selected="selected"':''}}>2</option>
																		<option value="3" {{$comment->rating>2 &&$comment->rating<=3 ? 'selected="selected"':''}}>3</option>
																		<option value="4" {{$comment->rating>3 &&$comment->rating<=4 ? 'selected="selected"':''}}>4</option>
																		<option value="5"{{$comment->rating>4 ? 'selected="selected"':''}}>5</option>
																	</select>
														</div>
													</div>
												</div>
												@endforeach
												@else
												<h6>No one rated this course yet </h6>
												@endif

											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')
<script src="{{URL::asset('files/bower_components/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-fullscreen.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-video.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-autoplay.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-zoom.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-hash.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/lightgallery/js/lg-pager.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/wall/wall.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/dashboard/custom-dashboard.js')}}"></script>
<script src="{{URL::asset('files/bower_components/chart.js/js/Chart.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/widget/amchart/amcharts.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/widget/amchart/serial.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/widget/amchart/light.js')}}"></script>
<script src="{{URL::asset('files/bower_components/jquery-bar-rating/js/jquery.barrating.js')}}"></script>
<script src="{{URL::asset('files/assets/js/rating.js')}}"></script>
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>

@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif
@stop