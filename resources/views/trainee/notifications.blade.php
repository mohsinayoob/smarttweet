@extends('layouts.trainee_layout') 
@section('title', 'All Notifications') 
@section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-header card">
					<div class="card-block">
						<h5 class="m-b-10">Notifications</h5>
					</div>
				</div>
				<div class="page-body">
					<div class="row">
						<div class="col-md-12 col-xl-12">
							<div class="card review-project">
								<div class="card-header">

									
								</div>
								<div class="card-block p-t-0 p-b-0 w-100">
									<div class="table-responsive">
										<table class="table table-hover" id="notification-table">

											<tbody id="append-data">								
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="card">
								<div class="card-block">
									<nav class="f-right">
										{{ $notifications->appends(request()->query())->links("pagination::bootstrap-4") }}
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop 
@section('javascript-files')


<script>

	var page = "{{$notifications->currentPage()}}"
	$.get('/trainee/get-all-notifications?page='+page, function (data) {
		//addNotifications(data, "#notifications");
		
		//location ='/trainee/get-all-notifications?page='+page
		AllNotification(data);
		console.log(data);
	});


function AllNotification(data)
{
	data = data.data;
	$('#append-data').html('');
	data.forEach(function(entry) {
		
		var internalData = JSON.parse(entry.data);
		//internalData = internalData.data;
		var url = internalData.data.url+"?read="+entry.id;
		var user_image = `/`+internalData.data.user.user_type+`/profile_image/`+internalData.data.user.profile_image;
		console.log(internalData.data);
		var message = internalData.data.message;
		
		var row = `<tr>
			<td>
				<a href="`+url+`">
					<img class="img-radius img-40" src="`+user_image+`" alt="chat-user">
				
				<div class="project-contain">
					<h6>`+internalData.data.user.name+` <span> `+message+`</h6>
					<p class="text-muted">
						<i class="fa fa-clock-o f-12 m-r-10"></i>`+entry.created_at+`</p>
				</div>
			</td>
		</a>
		</tr>`;
		console.log(row);
		$('#append-data').append(row);
		//addNotificationToHTML(user_image, url, internalData.data.user.name, message, entry.created_at)
		//
	});
	
	//var
}

</script>
@stop