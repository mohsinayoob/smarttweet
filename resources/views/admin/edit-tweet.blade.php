@extends('layouts.admin_layout') @section('title', 'Tweets') @section('css-files')
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/dropzone/dropzone.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/themify-icons/themify-icons.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}"> 
<style>
	.dropzone{
		margin-top: 10px;
		border: dashed #35AFAD;
		padding: 30px;
	}
</style>

@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">
					<div class="page-header card">
							<div class="card-block">
								<h5 class="m-b-10">Edit Tweet</h5>
							</div>
		
						</div>
				<div class="page-body">
					<div class="row">
						<div class="col-lg-9">
								
							<div class="row">
								<div class="col-md-12">
									<div class="card bg-white">
										<div class="post-new-contain row card-block">
											<div class="col-md-1 col-xs-3 post-profile">
												<img class="img-fluid" src="{{URL::asset('admin/profile_image/'.\Auth::user()->profile_image)}}" alt="">
											</div>
											<div class="col-md-11 col-xs-9">
											<form  action="/admin/edit-tweet/{{$topic_id}}/{{$tweet_id}}" method="post" id="postTweet">
												{{ csrf_field() }}
												<div class="">
													<div id="tweet_content_image_section">

													</div>
													<div id="tweet_content_video_section">

														</div>
													<input type="hidden" name="content_type" id="tweet_content_type" value="text" />
													<textarea id="post-message" class="form-control post-input" rows="3" cols="10" placeholder="Write something....." name="tweet_description"></textarea>
													
												</div>
											</form>
												<div class="" id="progress_view">
													<div id="image-selection-area">
															<form action="{{ url("/admin/upload-tweet-image")}}" enc-type="multipart/form-data"   id="image-dropzone">
																{{csrf_field()}}
																<div class="dz-default dz-message">
																	<span>Drop images here to upload</span>
																</div>
															</form>
													</div>
													<div id="video-selection-area">
															<form action="{{ url("/admin/upload-tweet-image")}}" enc-type="multipart/form-data"  class="video-dropzone" >
																{{csrf_field()}}
																<div class="dz-default dz-message">
																	<span>Drop video here to upload<br>You can upload one Video
																	</span>
																</div>
															</form>
													</div>
												</div>
											</div>
											
											
										</div>
										
										<div class="post-new-footer b-t-muted p-15">
											<span class="image-upload m-r-15" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add Photos">
												<label id="imageselect" class="file-upload-lbl">
													<i class="icofont icofont-image text-muted"></i>
												</label>
											</span>
											<span class="image-upload m-r-15" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add Video">
													<label  id ="videoselect" class="file-upload-lbl">
														<i class="icofont icofont-video text-muted"></i>
													</label>
												</span>
											<span>
												<button id="post-new" class="btn btn-primary waves-effect waves-light f-right" onclick="tweetSubmition()">
													Post
												</button>
											</span>

										</div>
									</div>
									

								</div>
							</div>
						</div>


						<div class="col-md-3">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')

<script src="{{URL::asset('files/assets/pages/dropzone/dropzone.js')}}"></script>
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>
<link href="{{URL::asset('files/assets/pages/videojs/video-js.css')}}" rel="stylesheet">
<script src="{{URL::asset('files/assets/pages/videojs/video.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/topic/base64js.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/tinymce/tinymce.min.js?apiKey=jdw0qpvvruo7046n3j4g8o122piap638faeuii9s0rlk2fy3')}}"></script>
<style>
		.mce-top-part {
			position: absolute;
			bottom: 0px;
		}
</style>
<script>
		function Base64Decode(str, encoding = 'utf-8') {
			var bytes = base64js.toByteArray(str);
			return new (TextDecoder || TextDecoderLite)(encoding).decode(bytes);
		}
		$('#post-message').html(Base64Decode("{!!$tweet->tweet_description!!}"));
		$(document).ready(function() {
			
			var editor =tinymce.init({ 
				selector:'textarea',
				menubar:false,
				statusbar: false,
				plugins:'smileys, emoticons',
				toolbar1: 'smileys | emoticons',
				theme_advanced_toolbar_location : "bottom",
				auto_convert_smileys: true,
			});
			
			var is_video_selected =false;
			var images_count = 0;
			var myDropzone;
			var videoDropZone;
			$('#video-selection-area').hide();
				$('#image-selection-area').hide();
			@if(sizeof($tweet_content)>0)
				@if($tweet->content_type=="images")
					imageDropZone();
					@foreach($tweet_content as $content)
					var mockFile = { name: "{{$content->file}}", size: "{{File::size(public_path('tweet_content/'.$content->file))}}" ,xhr:{response:"{{$content->file}}"} };
					myDropzone.options.addedfile.call(myDropzone, mockFile);
					myDropzone.options.thumbnail.call(myDropzone, mockFile, "{{'/tweet_content/'.$content->file}}");
					myDropzone.emit("complete", mockFile);

					@endforeach
				@elseif($tweet->content_type=="video")
					initvideoDropZone();
					@foreach($tweet_content as $content)
						var mockFile = { name: "{{$content->file}}", size: "{{File::size(public_path('tweet_content/'.$content->file))}}",xhr:{response:"{{$content->file}}"} };
						videoDropZone.options.addedfile.call(videoDropZone, mockFile);
						//videoDropZone.options.thumbnail.call(videoDropZone, mockFile, "{{'/tweet_content/'.$content->file}}");
						videoDropZone.emit("complete", mockFile);

					@endforeach
			
				@endif
				
			@endif

			function imageDropZone()
			{
				
				$('#video-selection-area').hide();
				$('#image-selection-area').show();
				//$('#progress_view').html('');
				if(myDropzone!=undefined && myDropzone!='')
				{
					return;
				}
				$('#image-dropzone').addClass('dropzone');
				myDropzone = new Dropzone("#image-dropzone",{
					
					headers: {
						'X-CSRF-TOKEN': '{{csrf_field()}}',
					},
					paramName: "file", // The name that will be used to transfer the file
    				maxFilesize: 2, // MB
    				addRemoveLinks: true,
					maxFiles: 4,
					acceptedFiles: ".jpeg,.jpg,.png,.gif",
    				init: function() {
      					this.on("maxfilesexceeded", function() {
        					if (this.files[4]!=null){
          						this.removeFile(this.files[4]);
							}
						});
						this.on("error", function(file) {
							this.removeFile(file);
							swal("Error!", "Error Occured in Uploading", "error");
						});
						
						this.on("complete", function (data) {
							$('#tweet_content_type').val('images');
							//On completing video upload remove previous one.///
							$('#tweet_content_video_section').html('');
							if(videoDropZone!=undefined && videoDropZone!='')
							{
								videoDropZone.removeAllFiles(true );
								$('.video-dropzone')[0].dropzone.files.forEach(function(file) { 
									file.previewElement.remove(); 
							  	});
								$('.video-dropzone').removeClass('dz-started');
								$(".video-dropzone .dz-preview").remove();
								videoDropZone.destroy()
								videoDropZone='';
							}
							
							if(data.xhr && data.xhr.response !== "error")
							{
								var imgName = data.xhr.response;
								var imgId = data.xhr.response.split('.')[0];
								$('#tweet_content_image_section').append('<input type="hidden" name="tweet_contents[]" value="'+data.xhr.response+'" id="'+imgId+'">');
							
							}
							else
							{
								swal("Error!", "Error Occured in Uploading", "error");
								this.removeFile(data);
							}
							
							if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
								//alert("all uploaded");
								$('#post-new').show()
							  }
						});
						this.on('removedfile', function (file) {
							if(file.xhr)
							{
								var inputId = file.xhr.response;
								var imgId = file.xhr.response.split('.')[0];
								$( '#'+imgId ).remove();
							}
							
							//alert("removed.. " + inputId);
						});
						  this.on("addedfile", function(file) {
							$('#post-new').hide()
							  //alert("file added");
							/* Maybe display some more file information on your page */
						  });
    				},
				});
			}
			function initvideoDropZone()
			{
				
				$('#image-selection-area').hide();
				$('#video-selection-area').show();
				
				if(videoDropZone!=undefined && videoDropZone!='')
				{
					return;
				}
				$('.video-dropzone').addClass('dropzone');
				//$('#tweet_content_section').html('');
				//$('#progress_view').html('');
				videoDropZone = new Dropzone(".video-dropzone",{
					
					headers: {
						'X-CSRF-TOKEN': '{{csrf_field()}}',
					},
					paramName: "file", // The name that will be used to transfer the file
    				maxFilesize: 100, // MB
    				addRemoveLinks: true,
					maxFiles: 1,
					acceptedFiles: ".mp4,.mkv,.flv,.avi,.mov",
    				init: function() {
      					this.on("maxfilesexceeded", function() {
        					if (this.files[1]!=null){
          						this.removeFile(this.files[1]);
							}
						});
						this.on("error", function(file) {
							this.removeFile(file);
							swal("Error!", "Error Occured in Uploading", "error");
						});
						
						this.on("complete", function (data) {
							$('#tweet_content_type').val('video');
							//On completing video upload remove previous one.///
							$('#tweet_content_image_section').html('');
							if(myDropzone!=undefined && myDropzone!='')
							{
								myDropzone.removeAllFiles(true );
							$('#image-dropzone')[0].dropzone.files.forEach(function(file) { 
								file.previewElement.remove(); 
							  });
							$('#image-dropzone').removeClass('dz-started');
							$("#image-dropzone .dz-preview").remove();
							myDropzone.destroy()
							myDropzone='';
							}
							
							if(data.xhr && data.xhr.response !== "error")
							{
								var imgName = data.xhr.response;
								var imgId = data.xhr.response.split('.')[0];
								$('#tweet_content_video_section').append('<input type="hidden" name="tweet_contents[]" value="'+data.xhr.response+'" id="'+imgId+'">');
							
							}
							else
							{
								swal("Error!", "Error Occured in Uploading", "error");
								this.removeFile(data);
							}
							
							if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
								//alert("all uploaded");
								$('#post-new').show()
							  }
						});
						this.on('removedfile', function (file) {
							if(file.xhr)
							{
								var inputId = file.xhr.response;
								var imgId = file.xhr.response.split('.')[0];
								$( '#'+imgId ).remove();
							}
							
							//alert("removed.. " + inputId);
						});
						  this.on("addedfile", function(file) {
							$('#post-new').hide()
							  //alert("file added");
							/* Maybe display some more file information on your page */
						  });
    				},
				});
			}


			$("#imageselect").click(function() {
				imageDropZone();
			});

			//////////////////////////Video selecting drop zone /////////////////////////
			$("#videoselect").click(function() {
				initvideoDropZone();
			});

	});
	
	function tweetSubmition()
	{
		var editorContent = tinyMCE.get('post-message').getContent();
		if (editorContent=="")
		{
			swal("Error!", "Description should not be empty", "error");
			return;
		}
		
		if($('#post-message').text()=="")
		{
			//alert('Please Write few words for tweet');
			//return;
		}

		$('#postTweet').submit();
		//alert('button clicked');
		
	}
	
</script>
@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif 

@stop