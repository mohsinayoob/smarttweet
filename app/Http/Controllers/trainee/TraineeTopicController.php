<?php

namespace App\Http\Controllers\trainee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Course\Course;
use App\Models\Course\Topic;
use App\Models\User;
use App\Models\Course\CourseEnrollment;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
//use Illuminate\Support\Facades\Request;
use Session;

class TraineeTopicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainee');
        
    }
    public function rateCourse($enrollment_id, Request $request)
    {
        $data = $request->all();
        DB::beginTransaction();
        try
        {
            $enrollment = CourseEnrollment::where('enrollment_id', $enrollment_id)->first(); 
            if(!empty($enrollment) && $enrollment->trainee_id == \Auth::user()->user_id)
            {
                $enrollment->rating = $data['rating'];
                $enrollment->is_rated = 1;
                $enrollment->rating_comments = $data['rating_comments'];
                $enrollment->save();
                DB::commit();
                DB::beginTransaction();
                $course = Course::where('course_id',$enrollment->course_id)->first();
                $average = CourseEnrollment::where('is_rated',1)->where('course_id', $enrollment->course_id)->avg('rating');
                $course->avg_rating = $average;
                $course->save();
                DB::commit();
                //return $average;
                
                Session::flash('update', 'Rating Save Successfully');
                return back()->with('update', 'Rating Successfully Saved');            
            }
        }catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Rating cannot be saved. Try Later !');
        }
    }
    protected function searchTopics($q,$status,$date,$course_id, $courses, $enrollusers)
    {
        if($date == "Today")
        {
            if(!empty($status))
            {
                $topics = Topic::where('topic_name', 'like', '%' . $q . '%')
                ->whereDay('created_at',date('d')) ->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
                ->where('topic.status', $status)
                ->where('course_id', $course_id)
                ->paginate(10);
                //return $courses;
                return view('trainee.topic', compact('courses','topics','enrollusers'));
            }
            $topics  = Topic::where('topic_name', 'like', '%' . $q . '%')
            ->whereDay('created_at',date('d')) ->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
            ->where('course_id', $course_id)
            ->paginate(10);
           // return $courses;
            return view('trainee.topic', compact('courses','topics','enrollusers'));

        }
        else if($date == "Month")
        {
            if(!empty($status))
            {
                $topics = Topic::where('topic_name', 'like', '%' . $q . '%')
                ->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))
                ->where('topic.status', $status)
                ->where('course_id', $course_id)
                ->paginate(10);
                //return $courses;
                return view('trainee.topic', compact('courses','topics','enrollusers'));
            }
            $topics  = Topic::where('topic_name', 'like', '%' . $q . '%')
            ->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->whereYear('created_at',date('Y'))
            ->where('course_id', $course_id)
            ->paginate(10);
           // return $courses;
            return view('trainee.topic', compact('courses','topics','enrollusers'));

        }
        else if($date == "Year")
        {
            if(!empty($status))
            {
                $topics = Topic::where('topic_name', 'like', '%' . $q . '%')
                ->whereYear('created_at',date('Y'))
                ->where('topic.status', $status)
                ->where('course_id', $course_id)
                ->paginate(10);
                //return $courses;
                return view('trainee.topic', compact('courses','topics','enrollusers'));
            }
            $topics  = Topic::where('topic_name', 'like', '%' . $q . '%')
            ->whereYear('created_at',date('Y'))
            ->where('course_id', $course_id)
            ->paginate(10);
           // return $courses;
            return view('trainee.topic', compact('courses','topics','enrollusers'));

        }
        if(!empty($status))
        {
            $topics  = Topic::where('topic_name', 'like', '%' . $q . '%')
            ->where('topic.status', $status)
            ->where('course_id', $course_id)
            ->paginate(10);
            // return $courses;
            return view('trainee.topic', compact('courses','topics','enrollusers'));
        }
        $topics = Topic::where('topic_name', 'like', '%' . $q . '%')
        ->where('course_id', $course_id)
        ->paginate(10);
       // return $courses;
       return view('trainee.topic', compact('courses','topics','enrollusers'));
    }
    public function index($course_id)
    {
        $courses = Course::join('course_enrollment','course_enrollment.course_id','=','course.course_id')
        ->where('course.course_id', $course_id)->first();
        //return $courses;
        if(!empty($courses))
        {
            $enrollusers = CourseEnrollment::join('users', 'users.user_id', '=', 'course_enrollment.trainee_id')
            ->where ('course_enrollment.course_id',$course_id)
            ->select('course_enrollment.*', 'users.name', 'users.sector','users.profile_image')->take(10)->get();
            $q = Input::get('q'); 
            $status = Input::get('status'); 
            $date = Input::get('date'); 
            if (!empty($q) || !empty($status) || !empty($date)) {
           return $this->searchTopics($q,$status,$date,$course_id, $courses, $enrollusers);
           }
            $topics = Topic::where('course_id', $course_id)->paginate(10);
            $trainer = User::where('user_id', $courses->trainer_id)->first();
            //return $enrollusers;
            //return view('trainee.profile', $profile);
            //return view('trainee.topic', compact('topics'));
            //return $courses;
            //return view('trainee.profile', $profile);
            return view('trainee.topic', compact('courses','topics','enrollusers', 'trainer'));

        }
        return back()->with('error_messages','You are not enrolled in that course');
        
    } 
}
