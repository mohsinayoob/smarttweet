<?php
namespace App\Mail;
use App\Models\User;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketComment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
class SupportTicketComment extends Mailable
{
    use Queueable, SerializesModels;
    public $user, $ticket, $comment;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Ticket $ticket, TicketComment $comment)
    {
        $this->user = $user;
        $this->ticket = $ticket;
        $this->comment = $comment;        
                
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.ticket-comments');
    }
}