<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NotificationPosted implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $data, $sender, $message, $created_at, $url;
    public $notifiable_id, $notification_id, $notification_type;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data, $sender, $message, $url, $notifiable_id, $notification_id, $notification_type)
    {
        
        $this->data = $data;
        $this->sender = $sender;
        $this->message = $message;
        $this->url = $url;
        $this->created_at = date("Y-m-d h:i:s", time());
        $this->notifiable_id = $notifiable_id;
        $this->notification_id = $notification_id;
        $this->notification_type = $notification_type;
        
        //$this->message  = "{$username} liked your status";
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        //$user_id = \Auth::user()->user_id;
        return [$this->notifiable_id];
    }
}