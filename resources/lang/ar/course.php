<?php

return [

   
    'MCourses' => 'إدارة الدورات',
    'Filter' => 'منقي:',
    'By Date' => 'حسب التاريخ',
    'Show all' => 'عرض الكل',
    'today' => 'اليوم',
    'This month'=>'هذا الشهر',
    'This year' => 'هذا العام',
    'By Status' => 'حسب الحالة',
    'Active' => 'نشيط',
    'De-active' => 'دي نشطة',
    'Edit Course' => 'تحرير الدورة التدريبية',
    'Delete Course' => 'حذف الدورة التدريبية',
    'Status' => 'الحالة',
    'empty' => 'لا يوجد دورة لتظهر',
    'title'=> 'هل أنت واثق؟',
    'text'=> 'سيتم حذف هذه الدورة',
    'delete'=>'نعم ، احذفها!',
    'cancel'=>'إلغاء',
    
  
];
