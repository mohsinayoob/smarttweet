@extends('layouts.trainer_layout') @section('title', 'Trainee-Enrollment') @section('css-files')
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/css/component.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css')}}"
/>

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datedropper/css/datedropper.min.css')}}"
/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/spectrum/css/spectrum.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/switchery/css/switchery.min.css')}}"> @stop
 @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">

		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-header card">
					<div class="card-block">
						<h5 class="m-b-10">Trainee Enrollments</h5>
					</div>
				</div>
				<div class="page-body">
					<div class="row">
						<div class="col-md-12 col-xl-12">
							<div class="card past-payment-card">
								<div class="card-header">
									<div class="card-header-left ">
									</div>
								</div>
								<div class="card-block">
									<div class="table-responsive">
										<table class="table table-hover" id="trainee-table">
											<thead>
												<tr>
													<th>Enrollment Id</th>
													<th>Trainee Name</th>
													<th>Course</th>
													<th>Apply Date</th>
													<th>Status</th>
													
												</tr>
											</thead>
											<tbody>
												@foreach($enrollments as $enrollment)
												<tr>
														<td>
																{{$enrollment->enrollment_id}}
														</td>
													<td>
														<a href="/public-profile/{{$enrollment->trainee_id}}" target = '_blank'>
															<img class="img-radius img-40" src="{{ $enrollment->profile_image ? URL::asset('/trainee/profile_image/'.$enrollment->profile_image): URL::asset('files/assets/images/social/profile.jpg')}}" alt="chat-user">
														</a>
														<p class="d-inline-block m-l-10 f-w-600">{{$enrollment->name}}</p>
													</td>
													<td>
														<a href="/trainer/topic/{{$enrollment->course_id}}"<p>{{$enrollment->course_title}}</p></a>
													</td>
													<td>
														<p>{{$enrollment->created_at}}</p>
													</td>
													
													<td> 
														@if($enrollment->status == "Active")
														<input type="checkbox" class="js-switch" value='1' checked>
														@else
														<input type="checkbox" class="js-switch" value='1'>
														@endif
														
														
													</td>
													
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')
<script src="{{URL::asset('files/bower_components/switchery/js/switchery.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/advance-elements/swithces.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('files/assets/js/modalEffects.js')}}"></script>
<script src="{{URL::asset('files/assets/js/classie.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/advance-elements/moment-with-locales.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/spectrum/js/spectrum.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/advance-elements/custom-picker.js')}}"></script>
<script src="{{URL::asset('files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js')}}"></script>

<script src="{{URL::asset('files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js')}}"></script>

<script src="{{URL::asset('files/bower_components/datedropper/js/datedropper.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>

</script>
@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif 
<script>
	var table=$('#trainee-table').DataTable();
</script>

<script>
		$(document).ready(function(){
			$("input:checkbox").change(function() {
				
				var data = table.row( $(this).parents('tr') ).data();
			  var enrollment_id = data[ 0 ];
			  //alert(enrollment_id);
			  
			  $.ajax({
				  
					  type:'POST',
					  url:'/trainer/updateEnrollment',
					  headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
					  data: { "enrollment_id" : enrollment_id },
					  success: function(data){
						if(data.data.success){
							swal("Success!", "Enrollment status Updated Successfully", "success");
						}else{
							swal("Error!", "Error in Updating Enrollment status", "error");
						}
					  }
				  });
			  });
		  });

</script>

@stop