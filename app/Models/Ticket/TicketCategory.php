<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Models\User;
use App\Models\Ticket\Ticket;

class TicketCategory extends Model
{
    //
    use Notifiable;
    protected $table = 'ticket_categories';
    protected $primaryKey = 'id';
    protected $fillable = ['name'];


    /**
     * A category can have many tickets
     */
    public function tickets()
    {
    	return $this->hasMany(Ticket::class);
    }
}
