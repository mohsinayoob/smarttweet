<?php

namespace App\Models\Chat;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ChatDialog extends Model
{
    //
    use Notifiable;
    protected $table = 'chat_dialogs';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['dialog_id', 'trainee_id', 'trainer_id', 'course_id', 'created_at', 'updated_at', 'second_trainee_id'];

    public static function createGroupChatDialog($user, $course, $group_chat_dialog = null, $qb_token)
    {
        if (empty($qb_token)) {
            $session = ChatDialog::createSession(env('QB_APP_ID'), env('QB_API_KEY'), env('QB_API_SECRET'), env('QB_ACCOUNT_REFERENCE'), env('QB_PASSWORD'), \Auth::user());
            $qb_token = $session->token;
        }
        //intialize Request
        $post_body;
        $curl = curl_init();
        $url = "https://api.quickblox.com/chat/Dialog.json";
        if (!empty($user)) {
            $post_body = http_build_query(array(
                'type' => 2,
                'occupants_ids' => $user->qb_id,
                'name' => $course->course_title,
                'photo' => '',
            ));
        }

        if (!empty($group_chat_dialog)) {
            $url = "https://api.quickblox.com/chat/Dialog/" . $group_chat_dialog->dialog_id . ".json";
            $post_body = http_build_query(array(
                'type' => 2,
                //'push_all[occupants_ids][]' => $user->qb_id ,
                'name' => $course->course_title,
                'photo' => '',
            ));
            if (!empty($user)) {
                $post_body = http_build_query(array(
                    'type' => 2,
                    'push_all[occupants_ids][]' => $user->qb_id,
                    'name' => $course->course_title,
                    'photo' => '',
                ));
            }

            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT"); // Use POST

        } else {
            curl_setopt($curl, CURLOPT_POST, true);
        }
        //echo $post_body;
        curl_setopt($curl, CURLOPT_URL, $url);
        // curl_setopt($curl, $url); // Full path is - https://api.quickblox.com/session.json

        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('QB-Token: ' . $qb_token));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        // Execute request and read response
        $response = curl_exec($curl);
        //echo $response;
        if (!empty($response)) {

            return $response;

        } else {
            return "error_occured";
        }

    }

    public static function createChatDialog($user, $qb_token)
    {
        if (empty($qb_token)) {
            $session = ChatDialog::createSession(env('QB_APP_ID'), env('QB_API_KEY'), env('QB_API_SECRET'), env('QB_ACCOUNT_REFERENCE'), env('QB_PASSWORD'), \Auth::user());
            $qb_token = $session->token;
        }
        $post_body = http_build_query(array(
            'type' => 3,
            'occupants_ids' => $user->qb_id . "," . \Auth::user()->qb_id,
            'name' => '',
            'photo' => '',
        ));
        //echo $post_body;
        $curl = curl_init("https://api.quickblox.com/chat/Dialog.json");
        // curl_setopt($curl, $url); // Full path is - https://api.quickblox.com/session.json
        curl_setopt($curl, CURLOPT_POST, true); // Use POST
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('QB-Token: ' . $qb_token));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        // Execute request and read response
        $response = curl_exec($curl);
        // echo $response;
        if (!empty($response)) {

            return $response;

        } else {
            return "error_occured";
        }

    }

    public static function createSession($appId, $authKey, $authSecret, $login, $password, $user)
    {

        if (!$appId || !$authKey || !$authSecret || !$login || !$password) {
            return false;
        }

        // Generate signature
        $nonce = rand();
        $timestamp = time(); // time() method must return current timestamp in UTC but seems like hi is return timestamp in current time zone
        $signature_string = "application_id=" . $appId . "&auth_key=" . $authKey . "&nonce=" . $nonce . "&timestamp=" . $timestamp . "&user[login]=" . $user->email . "&user[password]=" . $user->email;

        $signature = hash_hmac('sha1', $signature_string, $authSecret);

        // Build post body
        $post_body = urldecode(http_build_query(array(
            'application_id' => $appId,
            'auth_key' => "$authKey",
            'timestamp' => $timestamp,
            'nonce' => $nonce,
            'signature' => $signature,
            'user[login]' => $user->email,
            'user[password]' => $user->email,
        )));
        //echo $post_body;
        // Configure cURL
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.quickblox.com/session.json'); // Full path is - https://api.quickblox.com/session.json
        curl_setopt($curl, CURLOPT_POST, true); // Use POST
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        // Execute request and read response
        $response = curl_exec($curl);
        //echo $response;
        //exit;
        $responseJSON = json_decode($response)->session;

        // Check errors
        if ($responseJSON) {
            return $responseJSON;
        } else {
            return "error_occured";
        }

    }

}
