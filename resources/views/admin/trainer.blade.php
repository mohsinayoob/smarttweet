@extends('layouts.admin_layout') @section('title', 'Trainer') @section('css-files')
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/css/component.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css')}}"
/>

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datedropper/css/datedropper.min.css')}}"
/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/spectrum/css/spectrum.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/switchery/css/switchery.min.css')}}"> @stop
 @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">

		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-header card">
					<div class="card-block">
						<h5 class="m-b-10">{{trans ('dashboard.TrainerMngt') }}</h5>
					</div>
				</div>
				<div class="page-body">

					

					<div class="md-modal md-effect-13 addcontact" id="modal-13">
						<div class="md-content">
							<h3 class="f-26">Edit Trainer</h3>
							<div>
								 <form method="post" action="/admin/trainer">
									{{ csrf_field() }}
								<div class="input-group">
									<span class="input-group-addon">
										<i class="icofont icofont-user"></i>
									</span>
									<input type="text" class="form-control pname" name="name" id="trainer_name" placeholder="Trainer Name">
								</div>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="icofont icofont-envelope"></i>
									</span>
									<input type="text" class="form-control pamount" placeholder="Email" name="email" id="email">
								</div>

								<div class="input-group">
									<span class="input-group-addon">
										<i class="icofont icofont-list"></i>
									</span>
									<select type="text" class="form-control pname" placeholder="Sector" name="sector" id="sector">
										<option value="">Please Select Sector</option>
										@foreach($categories as $category)
										<option value="{{$category->category_title}}">{{$category->category_title}}</option>
										@endforeach
									</select>
									<input type="hidden" class="form-control pname" name="user_id" id="user_id" placeholder="Trainer Name">
								</div>
								
								
								<div class="text-center">
									<button type="submit" class="btn btn-primary waves-effect m-r-20 f-w-600 d-inline-block save_btn">Save</button>
									<button type="button" class="btn btn-primary waves-effect m-r-20 f-w-600 md-close d-inline-block close_btn">Close</button>
								</div>
							</form>
							</div>
						</div>
					</div>
					<div class="md-overlay"></div>
					<div class="row">
						<div class="col-md-12 col-xl-12">
							<div class="card past-payment-card">
								<div class="card-header">
									<div class="card-header-left ">
									</div>
								</div>
								<div class="card-block">
									<div class="table-responsive">
										<table class="table table-hover" id="trainer-table">
											<thead>
												<tr>
													<th>User-Id</th>
													<th>{{trans ('dashboard.Name') }}</th>
													<th>{{trans ('dashboard.Email') }}</th>
													<th>{{trans ('dashboard.Sector') }}</th>
													<th>{{trans ('dashboard.Sta') }}</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($trainers as $trainer)
												<tr>
														<td>
																{{$trainer->user_id}}
														</td>
													<td>
														<a href="/public-profile/{{$trainer->user_id}}" target="_blank">
															<img class="img-radius img-40" src="{{ $trainer->profile_image ? URL::asset('/trainer/profile_image/'.$trainer->profile_image): URL::asset('files/assets/images/social/profile.jpg')}}" alt="chat-user">
														</a>
														<p class="d-inline-block m-l-10 f-w-600">{{$trainer->name}}</p>
													</td>
													<td>
														<p>{{$trainer->email}}</p>
													</td>
													<td>
														<p>{{$trainer->sector}}</p>
													</td>
													
													<td> 
														@if($trainer->is_active == 1)
														<input type="checkbox" class="js-switch" value='1' checked>
														@else
														<input type="checkbox" class="js-switch" value="0">
														@endif
													</td>
													<td>
														<div class="dropdown-secondary dropdown">
															<button class="btn btn-primary btn-mini dropdown-toggle waves-effect waves-light" type="button" id="dropdown10" data-toggle="dropdown"
															 aria-haspopup="true" aria-expanded="false">Action</button>
															<div class="dropdown-menu" aria-labelledby="dropdown10" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
																<a class="dropdown-item waves-light waves-effect md-trigger " href="#!" data-modal="modal-13" >
																	<span class="point-marker bg-warning"></span>Edit Information</a>
																

																
															</div>
														</div>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')
<script src="{{URL::asset('files/bower_components/switchery/js/switchery.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/advance-elements/swithces.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('files/assets/js/modalEffects.js')}}"></script>
<script src="{{URL::asset('files/assets/js/classie.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/advance-elements/moment-with-locales.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/spectrum/js/spectrum.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/advance-elements/custom-picker.js')}}"></script>
<script src="{{URL::asset('files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js')}}"></script>

<script src="{{URL::asset('files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js')}}"></script>

<script src="{{URL::asset('files/bower_components/datedropper/js/datedropper.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>

</script>
@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif 
<script>
	var table=$('#trainer-table').DataTable({
		"language": {
        "info": "{{trans ('Trainee&Trainer.info') }}",
		"lengthMenu": "{{trans ('Trainee&Trainer.lengthMenu') }}",
		"search": "{{trans ('Trainee&Trainer.search') }}",
		"paginate": {
      		"previous": "{{trans ('Trainee&Trainer.previous') }}",
			"next":"{{trans ('Trainee&Trainer.next') }}"
    		}
  		
    }
	
	
	});

	$('#trainer-table tbody').on( 'click', 'a', function () {
		var data = table.row( $(this).parents('tr') ).data();
		var trainer_name = $(data[1]+'td p').text().trim();
		var email = $(data[2]).text();
		var sector = $(data[3]).text();
		var user_id = data[ 0 ];	
		$('#user_id').val(user_id);		
		$('#trainer_name').val(trainer_name);
		$('#email').val(email);	
		$('#sector').val(sector);		
	});
</script>

<script>
		$(document).ready(function(){
			$("input:checkbox").change(function() {
				
				var data = table.row( $(this).parents('tr') ).data();
			  var user_id = data[ 0 ];
			  
			  $.ajax({
				  
					  type:'POST',
					  url:'/admin/updatetrainer',
					  headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
					  data: { "user_id" : user_id },
					  success: function(data){
						if(data.data.success){
							swal("{{trans ('Trainee&Trainer.success') }}", "{{trans ('Trainee&Trainer.statustrainer') }}", "success");
						}else{
							swal("Error!", "Error in Updating Trainer status", "error");
						}
					  }
				  });
			  });
		  });

</script>

@stop