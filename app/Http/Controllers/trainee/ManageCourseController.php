<?php

namespace App\Http\Controllers\trainee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageCourseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainee');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('trainee.manage-course');
    }
}

