@extends('layouts.authentication_layout') @section('title', 'Forget Password') @section('body_content')
<section class="login header p-fixed d-flex text-center">

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
				@endif
				<div class="login-card card-block auth-body mr-auto ml-auto">
					<form class="md-float-material" method="POST" action="{{ route('password.email') }}">
						{{ csrf_field() }}

						<div class="auth-box">
							<div class="row m-b-20">
								<div class="col-md-12">
									<h3 class="text-left txt-primary">Forget Password</h3>
								</div>
							</div>
							<hr /> @if ($errors->has('email'))
							<span class="help-block" style="color:red">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
							<div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">

								<input type="email" class="form-control" placeholder="Your Email Address" name="email" value="{{ old('email') }}" required>
								<span class="md-line"></span>
							</div>
							<div class="row m-t-25 text-left">
								<div class="col-12">

									<div class="forgot-phone text-right f-right">
										<a href="/" class="text-right f-w-600 text-inverse"> Sign In?</a>
									</div>
								</div>
							</div>

							<div class="row m-t-30">
								<div class="col-md-12">
									<input type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20" value="Submit">
								</div>
							</div>
							<hr />

						</div>
					</form>

				</div>

			</div>

		</div>

	</div>

</section>

@stop