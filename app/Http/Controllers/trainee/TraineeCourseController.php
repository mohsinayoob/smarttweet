<?php

namespace App\Http\Controllers\Trainee;

use App\Http\Controllers\Controller;
use App\Models\Course\Course;
use App\Models\Course\CourseEnrollment;
use App\Models\User;
use DB;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
//use Illuminate\Support\Facades\Request;
use Session;
use App\Jobs\UpdateTweetReactionJob;
use App\Jobs\EnrollmentNotificationJob;

class TraineeCourseController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainee');
        
    }

    ///// Add Enrollment of course ////////////////
    public function addEnrollment($course_id = 0)
    {

        DB::beginTransaction();
        try
        {

            // Category::create();

            $existCourse = Course::where('course_id', $course_id)->first();
            if (!empty($existCourse)) {
                $existEnrollment = CourseEnrollment::where('course_id', $course_id)
                    ->where('trainee_id', \Auth::user()->user_id)
                    ->first();
                if (!empty($existEnrollment)) {
                    return back()->with('error_messages', 'You are Already Enrolled in this course');
                }
                $data['trainer_id'] = $existCourse['trainer_id'];
                $data['trainee_id'] = \Auth::user()->user_id;
                $data['course_id'] = $course_id;
                $data['status'] = "Pending";
                $new_enrollment = CourseEnrollment::create($data);
                DB::commit();
                $notification_job = (new EnrollmentNotificationJob($new_enrollment))->delay(13);
                $this->dispatch($notification_job);
                Session::flash('update', 'Enrolled !!! Wait for Triner Approval');
                return back()->with('update', 'Enrolled !!! Wait for Triner Approval');
            } else {
                return back()->with('error_messages', 'Could not find the course');
            }

        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Updating Course Status');
        }

    }

    ///// Get Details of a course /////////////////

    public function courseDetails($course_id = 0)
    {
        // get course
        $course = Course::where('course.course_category', 'like', '%' . \Auth::user()->sector . '%')
            ->where('course.course_id', '=', $course_id)
            ->select('course.*')->first();
        //get if already enrolled
        $enrollment = CourseEnrollment::where('course_id', $course_id)
            ->where('trainee_id', '=', \Auth::user()->user_id)->first();
           // return $enrollment;
        // get others comments 
        $comments = CourseEnrollment::join('users', 'users.user_id', '=', 'course_enrollment.trainee_id')
        ->where('course_enrollment.course_id', $course_id)
        ->where('course_enrollment.is_rated', true)
        ->select('course_enrollment.*', 'users.profile_image', 'users.name as trainee_name')
        ->get();

        return view('trainee.course-details', compact('course', 'enrollment', 'comments'));

    }

    ///// Get and Search Courses /////////////////////////
    protected function searchCourses($q,$status,$date)
    {
        if($date == "Today")
        {
            if(!empty($status))
            {
                $courses = Course::join('users', 'users.user_id', '=', 'course.trainer_id')
                ->where('course.course_category', 'like', '%' . \Auth::user()->sector . '%')
                ->where('course.course_title', 'like', '%' . $q . '%')
                ->where('course.status', $status)
                ->whereDay('course.created_at',date('d')) ->whereMonth('course.created_at',date('m'))->whereYear('course.created_at',date('Y'))
                ->select('course.*', 'users.user_id', 'users.profile_image', 'users.name as trainer_name')
                ->paginate(10);
            //return $courses;
            return view('trainee.search-course', compact('courses'));
            }
            $courses = Course::join('users', 'users.user_id', '=', 'course.trainer_id')
                ->where('course.course_category', 'like', '%' . \Auth::user()->sector . '%')
                ->where('course.course_title', 'like', '%' . $q . '%')
                ->whereDay('course.created_at',date('d'))->whereMonth('course.created_at',date('m'))->whereYear('course.created_at',date('Y'))
                ->select('course.*', 'users.user_id', 'users.profile_image', 'users.name as trainer_name')
                ->paginate(10);
            //return $courses;
            return view('trainee.search-course', compact('courses'));

        }
        else if($date == "Month")
        {
            if(!empty($status))
            {
                $courses = Course::join('users', 'users.user_id', '=', 'course.trainer_id')
                ->where('course.course_category', 'like', '%' . \Auth::user()->sector . '%')
                ->where('course.course_title', 'like', '%' . $q . '%')
                ->where('course.status', $status)
                ->whereMonth('course.created_at',date('m'))->whereYear('course.created_at',date('Y'))
                ->select('course.*', 'users.user_id', 'users.profile_image', 'users.name as trainer_name')
                ->paginate(10);
            //return $courses;
            return view('trainee.search-course', compact('courses'));
            }
            $courses = Course::join('users', 'users.user_id', '=', 'course.trainer_id')
                ->where('course.course_category', 'like', '%' . \Auth::user()->sector . '%')
                ->where('course.course_title', 'like', '%' . $q . '%')
                ->whereMonth('course.created_at',date('m'))->whereYear('course.created_at',date('Y'))
                ->select('course.*', 'users.user_id', 'users.profile_image', 'users.name as trainer_name')
                ->paginate(10);
            //return $courses;
            return view('trainee.search-course', compact('courses'));

        }
        else if($date == "Year")
        {
            if(!empty($status))
            {
                
                $courses = Course::join('users', 'users.user_id', '=', 'course.trainer_id')
                ->where('course.course_category', 'like', '%' . \Auth::user()->sector . '%')
                ->where('course.course_title', 'like', '%' . $q . '%')
                ->where('course.status', $status)
                ->whereYear('course.created_at',date('Y'))
                ->select('course.*', 'users.user_id', 'users.profile_image', 'users.name as trainer_name')
                ->paginate(10);
            //return $courses;
            return view('trainee.search-course', compact('courses'));
            }
            $courses = Course::join('users', 'users.user_id', '=', 'course.trainer_id')
                ->where('course.course_category', 'like', '%' . \Auth::user()->sector . '%')
                ->where('course.course_title', 'like', '%' . $q . '%')
                ->whereYear('course.created_at',date('Y'))
                ->select('course.*', 'users.user_id', 'users.profile_image', 'users.name as trainer_name')
                ->paginate(10);
            //return $courses;
            return view('trainee.search-course', compact('courses'));

        }
        if(!empty($status))
        {
            $courses = Course::join('users', 'users.user_id', '=', 'course.trainer_id')
                ->where('course.course_category', 'like', '%' . \Auth::user()->sector . '%')
                ->where('course.course_title', 'like', '%' . $q . '%')
                ->where('course.status', $status)
                ->select('course.*', 'users.user_id', 'users.profile_image', 'users.name as trainer_name')
                ->paginate(10);
            //return $courses;
            return view('trainee.search-course', compact('courses'));
        }
        $courses = Course::join('users', 'users.user_id', '=', 'course.trainer_id')
                ->where('course.course_category', 'like', '%' . \Auth::user()->sector . '%')
                ->where('course.course_title', 'like', '%' . $q . '%')
                ->select('course.*', 'users.user_id', 'users.profile_image', 'users.name as trainer_name')
                ->paginate(10);
            //return $courses;
            return view('trainee.search-course', compact('courses'));
    }

    ////////////////////// Search manage courses /////////////////////////////
    protected function searchManageCourses($q,$status,$date)
    {
        if($date == "Today")
        {
            if(!empty($status))
            {
                $courses = Course::join('course_enrollment', 'course_enrollment.course_id', '=', 'course.course_id')
            ->where('course_enrollment.trainee_id', \Auth::user()->user_id)
            ->where('course.course_title', 'like', '%' . $q . '%')
            ->where('course.status', $status)
            ->whereDay('course.created_at',date('d'))
            ->select('course.*','course_enrollment.status' )
            ->paginate(10);
            //return $courses;
            return view('trainee.manage-course', compact('courses'));
            }
            $courses = Course::join('course_enrollment', 'course_enrollment.course_id', '=', 'course.course_id')
            ->where('course_enrollment.trainee_id', \Auth::user()->user_id)
            ->where('course.course_title', 'like', '%' . $q . '%')
            ->whereDay('course.created_at',date('d'))->whereMonth('course.created_at',date('m'))->whereYear('course.created_at',date('Y'))
            ->select('course.*','course_enrollment.status' )
            ->paginate(10);
            //return $courses;
            return view('trainee.manage-course', compact('courses'));

        }
        else if($date == "Month")
        {
            if(!empty($status))
            {
                $courses =  Course::join('course_enrollment', 'course_enrollment.course_id', '=', 'course.course_id')
                ->where('course_enrollment.trainee_id', \Auth::user()->user_id)
                ->where('course.course_title', 'like', '%' . $q . '%')
                ->where('course.status', $status)
                ->whereMonth('course.created_at',date('m'))->whereYear('course.created_at',date('Y'))
                ->select('course.*','course_enrollment.status' )
                ->paginate(10);
                //return $courses;
                return view('trainee.manage-course', compact('courses'));

            }
            $courses =  Course::join('course_enrollment', 'course_enrollment.course_id', '=', 'course.course_id')
                ->where('course_enrollment.trainee_id', \Auth::user()->user_id)
                ->where('course.course_title', 'like', '%' . $q . '%')
                ->whereMonth('course.created_at',date('m'))->whereYear('course.created_at',date('Y'))
                ->select('course.*','course_enrollment.status' )
                ->paginate(10);
                //return $courses;
                return view('trainee.manage-course', compact('courses'));

        }
        else if($date == "Year")
        {
            if(!empty($status))
            {

                $courses =   Course::join('course_enrollment', 'course_enrollment.course_id', '=', 'course.course_id')
                ->where('course_enrollment.trainee_id', \Auth::user()->user_id)
                ->where('course.course_title', 'like', '%' . $q . '%')
                ->where('course.status', $status)
                ->whereYear('course.created_at',date('Y'))
                ->select('course.*','course_enrollment.status' )
                ->paginate(10);
                //return $courses;
                return view('trainee.manage-course', compact('courses'));
            }
            $courses =  Course::join('course_enrollment', 'course_enrollment.course_id', '=', 'course.course_id')
                ->where('course_enrollment.trainee_id', \Auth::user()->user_id)
                ->where('course.course_title', 'like', '%' . $q . '%')
                ->whereYear('course.created_at',date('Y'))
                ->select('course.*','course_enrollment.status' )
                ->paginate(10);
                //return $courses;
                return view('trainee.manage-course', compact('courses'));

        }
        if(!empty($status))
        {
            $courses =  Course::join('course_enrollment', 'course_enrollment.course_id', '=', 'course.course_id')
                ->where('course_enrollment.trainee_id', \Auth::user()->user_id)
                ->where('course.course_title', 'like', '%' . $q . '%')
                ->where('course.status', $status)
                ->select('course.*','course_enrollment.status' )
                ->paginate(10);
                //return $courses;
                return view('trainee.manage-course', compact('courses'));
        }
        $courses =  Course::join('course_enrollment', 'course_enrollment.course_id', '=', 'course.course_id')
                ->where('course_enrollment.trainee_id', \Auth::user()->user_id)
                ->where('course.course_title', 'like', '%' . $q . '%')
                ->select('course.*','course_enrollment.status' )
                ->paginate(10);
                //return $courses;
                return view('trainee.manage-course', compact('courses'));
    }
/////////////////////////// Search Manage Courses End //////////////////////////////////////

    public function index()
    {
        $q = Input::get('q'); 
        $status = Input::get('status'); 
        $date = Input::get('date'); 
        if (!empty($q) || !empty($status) || !empty($date)) {
            return $this->searchCourses($q,$status,$date);  
        }

        $courses = Course::join('users', 'users.user_id', '=', 'course.trainer_id')
            ->where('course.course_category', 'like', '%' . \Auth::user()->sector . '%')
            ->select('course.*', 'users.user_id','users.profile_image', 'users.name as trainer_name')
            ->paginate(10);
        //return $courses;
        //return $courses->perPage();
        return view('trainee.search-course', compact('courses'));

    }
    public function ManageCourse()
    {
       
        $q = Input::get('q'); 
        $status = Input::get('status'); 
        $date = Input::get('date'); 
        if (!empty($q) || !empty($status) || !empty($date)) {
            return $this->searchManageCourses($q,$status,$date);  
        }
        $courses = Course::join('course_enrollment', 'course_enrollment.course_id', '=', 'course.course_id')
            ->where('course_enrollment.trainee_id', \Auth::user()->user_id)
            ->select('course.*','course_enrollment.status' )
            ->paginate(10);
        //return $courses;
        //return $courses->perPage();
        //$notification = (new UpdateTweetReactionJob())->delay(13);
        //$this->dispatch($notification);
        // \Auth::user()->notify(new \App\Notifications\TweetPosted($courses));
        return view('trainee.manage-course', compact('courses'));

    }
}
