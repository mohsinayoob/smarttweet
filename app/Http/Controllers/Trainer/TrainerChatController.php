<?php

namespace App\Http\Controllers\Trainer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Chat\ChatDialog;
use DB;
use Session;
class TrainerChatController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainer');
        
    }
    public function chatByUserId($user_id)
    {
        $chat_dialog = ChatDialog::where('trainer_id', \Auth::user()->user_id)
        ->where('trainee_id', base64_decode($user_id))->first();
        return view('partials.chat', compact('chat_dialog'));

    }
    public function index()
    {
        $chat_dialog = false;
        return view('partials.chat', compact('chat_dialog'));
    }
}
