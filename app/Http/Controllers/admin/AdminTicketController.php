<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Mail\SupportTicketComment;
use App\Mail\SupportTicketStatus;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketCategory;
use App\Models\Ticket\TicketComment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mail;
use Session;
Use DB;

class AdminTicketController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');

    }
    public function index()
    {
        $tickets = Ticket::paginate(10);
        $categories = TicketCategory::all();

        return view('tickets.index', compact('tickets', 'categories'));
    }

    public function viewCategories()
    {
       
        $categories = TicketCategory::all();

        return view('tickets.ticket-categories', compact('categories'));
    }
  
    public function AddCategory(Request $request)
    {
        

        DB::beginTransaction();
        try
        {
            $category_validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);
    
            if ($category_validator->fails()) {
                $messages = $category_validator->messages();
    
                return back()->withError($messages)->with('error_messages', $messages->first('name'));
            }
            TicketCategory::create($request->all());
            // After creating the user send an email with the random token generated in the create method above

            DB::commit();
            Session::flash('update', 'Data Saved Successfully');
            return back()->with('update', 'Data Successfully Saved');
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Saving data');
        }

    }

    public function UpdateCategory(Request $request)
    {
        
        DB::beginTransaction();
        try
        {
            $category_validator = Validator::make($request->all(), [
                'name' => 'required',
                'id' => 'required',
            ]);
    
            if ($category_validator->fails()) {
                $messages = $category_validator->messages();
    
                return back()->withError($messages)->with('error_messages', 'An Error in Updating Category');
            }
           // Category::create();
            $data = $request->all();
            $existCategory = TicketCategory::where('id', $data['id'])->first();
            $existCategory->name = $data['name'];
            $existCategory->save();
            // After creating the user send an email with the random token generated in the create method above

            DB::commit();
            Session::flash('update', 'Category Saved Successfully');
            return back()->with('update', 'Category Successfully Saved');
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Saving Category');
        }

    }

    public function DeleteCategory(Request $request)
    {
        
        DB::beginTransaction();
        try
        {
            $category_validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);
    
            if ($category_validator->fails()) {
                $messages = $category_validator->messages();
    
                return back()->withError($messages)->with('error_messages', 'An Error in Deleting Category');
            }
           // Category::create();
            $data = $request->all();
            $existCategory = TicketCategory::where('id', $data['id'])->first();
            $existCategory->delete();
            // After creating the user send an email with the random token generated in the create method above

            DB::commit();
            Session::flash('update', 'Category Deleted Successfully');
            return back()->with('update', 'Category Successfully Deleted');
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Deleted Category');
        }

    }

    public function show($ticket_id)
    {
        $ticket = Ticket::where('ticket_id', $ticket_id)->first();
        // return $ticket;
        if (!empty($ticket)) {
            $comments = $ticket->comments;
            // return $comments;
            $category = $ticket->category;

            return view('tickets.show', compact('ticket', 'category', 'comments'));
        }
        return redirect()->to('/admin/tickets')->with('error_messages', 'This Ticket does not exist');
    }

    /**
     * Close the specified ticket.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function close($ticket_id)
    {
        $ticket = Ticket::where('ticket_id', $ticket_id)->first();
        if (!empty($ticket)) {

            $ticket->status = 'Closed';
            $ticket->save();
            $ticketOwner = $ticket->user;

            $email = new SupportTicketStatus($ticketOwner, $ticket);
            Mail::to($ticketOwner->email)->send($email);

            return back()->with("update", "The ticket has been closed.");
        }
        return back()->with("error_messages", "The ticket cannot be closed.");

    }

    public function postComment(Request $request)
    {

        DB::beginTransaction();
        try {

            $comment_validator = Validator::make($request->all(), [
                'comment' => 'required',
            ]);

            if ($comment_validator->fails()) {
                $messages = $comment_validator->messages();

                return back()->withError($messages)->with('error_messages', "Comment Field is Required");
            }
            $comment = TicketComment::create([
                'ticket_id' => $request->input('ticket_id'),
                'user_id' => \Auth::user()->user_id,
                'comment' => base64_encode($request->input('comment')),
            ]);
            DB::commit();

            $user = User::where('user_id', $comment->ticket->user_id)->first();

            $email = new SupportTicketComment($user, $comment->ticket, $comment);
            Mail::to($user->email)->send($email);

            return back()->with("update", "Your comment has be submitted.");
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error_messages', 'Error in Saving data');
        }
    }
}
