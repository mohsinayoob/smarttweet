<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">


<head>
	<title>@yield('title') </title>


	<!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')}}"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js')}}/1.4.2/respond.min.js')}}"></script>
      <![endif]-->

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="Smart Tweet"/>
	<meta name="keywords" content="Smart Tweet"/>
	<meta name="author" content="Smart Tweet" />

	<link rel="icon" href="{{URL::asset('files/assets/images/favicon.ico')}}" type="image/x-icon">

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/icofont/css/icofont.css')}}">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/themify-icons/themify-icons.css')}}">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/icon/font-awesome/css/font-awesome.min.css')}}">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/css/jquery.mCustomScrollbar.css')}}">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/css/style.css')}}"> 
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/pnotify/css/pnotify.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/pnotify/css/pnotify.brighttheme.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/pnotify/css/pnotify.buttons.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/pnotify/css/pnotify.history.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/pnotify/css/pnotify.mobile.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/pnotify/notify.css')}}">
	
	@yield('css-files')
</head>

<body>

	<div class="theme-loader">
		<div class="loader-track">
			<div class="loader-bar"></div>
		</div>
	</div>

	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<nav class="navbar header-navbar pcoded-header">
				<div class="navbar-wrapper">
					<div class="navbar-logo">
						<a class="mobile-menu" id="mobile-collapse" href="#!">
							<i class="ti-menu"></i>
						</a>
						<div class="mobile-search">
							<div class="header-search">
								<div class="main-search morphsearch-search">
									<div class="input-group">
										<span class="input-group-addon search-close">
											<i class="ti-close"></i>
										</span>
										<input type="text" class="form-control" placeholder="Enter Keyword">
										<span class="input-group-addon search-btn">
											<i class="ti-search"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
						<a href="/">
							<img class="img-fluid" src="{{URL::asset('files/assets/images/logo.png')}}" alt="Theme-Logo" />
						</a>
						<a class="mobile-options">
							<i class="ti-more"></i>
						</a>
					</div>
					<div class="navbar-container container-fluid">
						<ul class="nav-left">
							<li>
								<div class="sidebar_toggle">
									<a href="javascript:void(0)">
										<i class="ti-menu"></i>
									</a>
								</div>
							</li>
							
							<li>
								<a href="#!" onclick="javascript:toggleFullScreen()">
									<i class="ti-fullscreen"></i>
								</a>
							</li>
						</ul>
						<ul class="nav-right">
							<li class=" header-notification header-notifications">
								<a href="#!" >
									<i class="ti-bell"></i>
									<span data-count="0" class="notification-badge"></span>
								</a>
								<ul class="show-notification notification-dropdown" style="max-height: 350px;overflow-y: scroll;">
										<li>
											<a href="/admin/notifications" style="padding:0px;">
											<label class="label label-danger f-right">View All</label>
											<h6>Notifications</h6>
											</a>
										</li>
									</ul>
				  
							  </li>
							<li class="">
								<a href="/admin/tickets" class="">
									<i class="ti-comments"></i>
									<span class="badge bg-c-green"></span>
								</a>
							</li>
							<li class="user-profile header-notification">
								<a href="#!">
									<img src="{{ \Auth::user() && \Auth::user()->profile_image ? URL::asset('admin/profile_image/'.\Auth::user()->profile_image): URL::asset('files/assets/images/social/profile.jpg')}}" class="img-radius" alt="User-Profile-Image">
									<span>{{\Auth::user() && \Auth::user()->name ? \Auth::user()->name :""}}</span>
									<i class="ti-angle-down"></i>
								</a>
								<ul class="show-notification profile-notification">
									
									<li>
										<a href="/admin/profile">
											<i class="ti-user"></i>{{trans ('admin.MProfile') }}
										</a>
									</li>
									<li>
										<a href="/admin/tickets">
											<i class="ti-email"></i> {{trans ('admin.ViewTick') }}
										</a>
									</li>

									<li><a href="{{url('language/en')}}">English</a></li><br>

									<li><a href="{{url('language/ar')}}">Arabic</a></li>
									
									<li>
										<a href="{{ route('logout') }}"
										onclick="event.preventDefault();
												 document.getElementById('logout-form').submit();">
											<i class="ti-layout-sidebar-left"></i> {{trans ('admin.Logout') }}
										</a>
										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
												{{ csrf_field() }}
										</form>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>

			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<nav class="pcoded-navbar">
						<div class="sidebar_toggle">
							<a href="#">
								<i class="icon-close icons"></i>
							</a>
						</div>
						<div class="pcoded-inner-navbar main-menu">
							<div class="pcoded-navigation-label"></div>
							<ul class="pcoded-item pcoded-left-item">
								<!--
                                <li class="user-profile header-notification">
								    <a href="/profile">
									    <img src="{{URL::asset('files/assets/images/avatar-4.jpg')}}" class="img-radius" alt="User-Profile-Image">
									    <span>John Doe</span>
									</a>
								</li> -->
								<li class="{{ Request::is('admin/dashboard') ? 'active' : '' }}">
									<a href="/admin/dashboard">
										<span class="pcoded-micon">
											<i class="ti-home"></i>
											<b>D</b>
										</span>
										<span class="pcoded-mtext">{{trans ('admin.Dashboard') }}</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('admin/manage-categories') ? 'active' : '' }}">
									<a href="/admin/manage-categories">
										<span class="pcoded-micon">
											<i class="ti-list"></i>
											<b>C</b>
										</span>
										<span class="pcoded-mtext">{{trans ('admin.Categories') }}</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('admin/create-course') ? 'active' : '' }}">
									<a href="/admin/create-course">
										<span class="pcoded-micon">
											<i class="ti-ruler-pencil"></i>
											<b>C</b>
										</span>
										<span class="pcoded-mtext">{{trans ('admin.CCourse') }}</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('admin/manage-course') ||  Request::is('admin/topic*') ||  Request::is('admin/tweets-list*') ||  Request::is('admin/tweet*')
								
								? 'active' : '' }}">
									<a href="/admin/manage-course">
										<span class="pcoded-micon">
											<i class="ti-pencil-alt"></i>
											<b>C</b>
										</span>
										<span class="pcoded-mtext">{{trans ('admin.MCourse') }}</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								
								<li class="{{ Request::is('admin/add-trainer') ? 'active' : '' }}">
									<a href="/admin/add-trainer">
										<span class="pcoded-micon">
											<i class="ti-medall"></i>
											<b>C</b>
										</span>
										<span class="pcoded-mtext">{{trans ('admin.AddT') }}</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('admin/trainer') ? 'active' : '' }}">
									<a href="/admin/trainer">
										<span class="pcoded-micon">
											<i class="icofont icofont-user-suited"></i>
											<b>C</b>
										</span>
										<span class="pcoded-mtext">{{trans ('admin.Trainer') }}</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('admin/trainee') ? 'active' : '' }}">
									<a href="/admin/trainee">
										<span class="pcoded-micon">
											<i class="icofont icofont-user-alt-7"></i>
											<b>C</b>
										</span>
										<span class="pcoded-mtext">{{trans ('admin.Trainee') }}</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('admin/profile') ||Request::is('admin/change-password') ? 'active' : '' }}">
									<a href="/admin/profile">
										<span class="pcoded-micon">
											<i class="ti-user"></i>
											<b>P</b>
										</span>
										<span class="pcoded-mtext">{{trans ('admin.Profile') }}</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('admin/notifications') ? 'active' : '' }}">
									<a href="/admin/notifications">
										<span class="pcoded-micon">
											<i class="ti-bell"></i>

											<b>N</b>
										</span>
										<span class="pcoded-mtext">{{trans ('admin.Noti') }}</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('admin/messages') ? 'active' : '' }}">
									<a href="/admin/messages">
										<span class="pcoded-micon">
											<i class="ti-comments"></i>
											<b>N</b>
										</span>
										<span class="pcoded-mtext">{{trans ('admin.Msg') }}</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('admin/reports') || Request::is('admin/user-reports') || Request::is('admin/course-reports') ? 'active' : '' }}">
									<a href="/admin/reports">
										<span class="pcoded-micon">
											<i class="ti-bar-chart"></i>
											<b>R</b>
										</span>
										<span class="pcoded-mtext">{{trans ('admin.Rep') }}</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="{{ Request::is('admin/ticket*')  ? 'active' : '' }}">
									<a href="/admin/tickets">
										<span class="pcoded-micon">
											<i class="ti-support"></i>
											<b>S</b>
										</span>
										<span class="pcoded-mtext">{{trans ('admin.STick') }}</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
								<li class="">
									<a href="{{ route('logout') }}"
									onclick="event.preventDefault();
											 document.getElementById('logout-form').submit();">
										<span class="pcoded-micon">
											<i class="ti-layout-sidebar-left"></i>
											<b>L</b>
										</span>
										<span class="pcoded-mtext">{{trans ('admin.Logout') }}</span>
										<span class="pcoded-mcaret"></span>
									</a>
								</li>
							</ul>
						</div>
					</nav>

					@yield('body_content')
				</div>
			</div>
		</div>
	</div>
	@yield('extra-outer-content')
	<!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="{{URL::asset('files/assets/images/browser/chrome.png')}}" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="{{URL::asset('files/assets/images/browser/firefox.png')}}" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="{{URL::asset('files/assets/images/browser/opera.png')}}" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="{{URL::asset('files/assets/images/browser/safari.png')}}" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="{{URL::asset('files/assets/images/browser/ie.png')}}" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->


	<script src="{{URL::asset('files/bower_components/jquery/js/jquery.min.js')}}"></script>
	<script src="{{URL::asset('files/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
	<script src="{{URL::asset('files/bower_components/popper.js/js/popper.min.js')}}"></script>
	<script src="{{URL::asset('files/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{URL::asset('files/assets/pages/widget/excanvas.js')}}"></script>

	<script src="{{URL::asset('files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>

	<script src="{{URL::asset('files/bower_components/modernizr/js/modernizr.js')}}"></script>

	<script src="{{URL::asset('files/assets/js/SmoothScroll.js')}}"></script>
	<script src="{{URL::asset('files/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>


	<script src="{{URL::asset('files/assets/js/pcoded.min.js')}}"></script>

	@if(Lang::locale()==='en')
	<script src="{{URL::asset('files/assets/js/dark-light/vertical-layout.min.js')}}"></script>
	@else
		
	<script src="{{URL::asset('files/assets/js/dark-light/menu/menu-rtl.js')}}"></script>
	@endif
	

	<script src="{{URL::asset('files/assets/js/script.js')}}"></script>
	<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.desktop.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.buttons.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.confirm.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.callbacks.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.animate.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.history.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.mobile.js')}}"></script>
<script src="{{URL::asset('files/bower_components/pnotify/js/pnotify.nonblock.js')}}"></script>
    <script src="//js.pusher.com/3.1/pusher.min.js"></script>
	<script type="text/javascript">
		
		var notificationsWrapper   = $('.header-notifications');
		var notificationsToggle    = notificationsWrapper.find('a[data-toggle]');
		var notificationsCountElem = notificationsToggle.find('span[data-count]');
		var notificationsCount     = parseInt(notificationsCountElem.data('count'));
		var notifications          = notificationsWrapper.find('ul.notification-dropdown');
  
		if (notificationsCount <= 0) {
		  notificationsWrapper.hide();
		}
  
		// Enable pusher logging - don't include this in production
		// Pusher.logToConsole = true;
  
		var pusher = new Pusher('8c4d1e032c2c25bf925f', {
		  encrypted: true
		});
		Pusher.logToConsole = true;
		// Subscribe to the channel we specified in our Laravel Event
		var channel = pusher.subscribe('{{\Auth::user()->user_id}}');
  console.log(pusher);
		// Bind a function to a Event (the full Laravel class)
		channel.bind('App\\Events\\NotificationPosted', function(data) {
			console.log(data);
			var user_image = `/`+data.sender.user_type+`/profile_image/`+data.sender.profile_image;
			var url = data.url+"?read="+data.notification_id;
			addNotificationToHTML(user_image, url, data.sender.name, data.message, data.created_at);
		  	notifyMe(user_image, url, data.sender.name, data.message, data.created_at);
		});

		//////
		function addNotificationToHTML(image, url, name, message, created_at)
		{
			var existingNotifications = notifications.html();
			var newNotificationHtml = `
			<li>
				
				  <div class="media">
					  <img class="d-flex align-self-center img-radius" src="`+image+`" alt="Generic placeholder image">
					  <div class="media-body">
						  <a href="`+url+`" style="padding:0px;">
							  <h5 class="notification-user">`+name+`</h5>
							  <p class="notification-msg">`+message+`</p>
						  </a>
						  <span class="notification-time">`+created_at+`</span>
					  </div>
				  </div>
			  </li>
			  
			`;
			notifications.append(newNotificationHtml);
			//notificationsCount += 1;
			//notificationsCountElem.attr('data-count', notificationsCount);
			$('.notification-badge').addClass('badge bg-c-pink');
			//notificationsWrapper.find('.notif-count').text(notificationsCount);
			//notificationsWrapper.show();
		}

		$.get('/admin/get-notifications', function (data) {
			//addNotifications(data, "#notifications");
			onLoadNotification(data);
			console.log(data);
		});
		
		function onLoadNotification(data)
		{
			data.forEach(function(entry) {
				
				var internalData = JSON.parse(entry.data);
				//internalData = internalData.data;
				var url = internalData.data.url+"?read="+entry.id;
				var user_image = `/`+internalData.data.user.user_type+`/profile_image/`+internalData.data.user.profile_image;
				console.log(internalData.data);
				var message = internalData.data.message;
				addNotificationToHTML(user_image, url, internalData.data.user.name, message, entry.created_at)
				//
			});
			
			//var
		}
		
	  </script>

	  <script>
		// request permission on page load
document.addEventListener('DOMContentLoaded', function () {
	if (!Notification) {
	  alert('Desktop notifications not available in your browser. Try Chromium.'); 
	  return;
	}
  
	if (Notification.permission !== "granted")
	 Notification.requestPermission();
	
  });
  
  function notifyMe(image, url, name, message, created_at) {
	if (Notification.permission !== "granted")
	{
		Notification.requestPermission();
		PNotify.desktop.permission();
		(new PNotify({
			title: 'Smart Tweet Notification',
			type: 'success',
			text: name+" "+message,
			desktop: {
				desktop: true,
				icon: image
			}
		})).get().click(function(e) {
			if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
			window.location = url;
		});
	}
	else {

	  var notification = new Notification('Smart Tweet Notification', {
		icon: image,
		body: name+" "+message,
	  });
	  
	  notification.onclick = function () {
		window.open(url);      
	  };
	  (new PNotify({
		title: 'Smart Tweet Notification',
		type: 'success',
		text: name+" "+message,
		icon: image,
		type: 'success'
	})).get().click(function(e) {
		if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
		window.location = url;
	});
  
	}
  
  }
	  </script>
	@yield('javascript-files')
</body>

</html>