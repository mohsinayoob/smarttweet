@extends('layouts.trainer_layout') @section('title', 'Trainer Profile') @section('css-files')


<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/ekko-lightbox/css/ekko-lightbox.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/lightbox2/css/lightbox.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css')}}"
/>

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datedropper/css/datedropper.min.css')}}"
/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('/files/bower_components/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}"
/>

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/social-timeline/timeline.css')}}">
<style>
	.bootstrap-tagsinput {
		width: 100%;
	}
</style>
@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">

		<div class="main-body">
			<div class="page-wrapper">

				<div class="page-body">

					<div class="modal fade" id="large-Modal" tabindex="-1" role="dialog">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Change Cover</h4>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<form method="post" action="{{url('trainer/profile/change_cover')}}" enctype="multipart/form-data" id="cover_form">
									{{ csrf_field() }}
									<div class="modal-body">
										<div class="col-md-12">

											<input type="file" name="cover_image" id="cover_image" />
											<div class="social-wallpaper">
												<img src="{{$profile && $profile->cover_image ? URL::asset('/trainer/cover_image/'.$profile->cover_image): URL::asset('files/assets/images/social/img1.jpg')}}"
												 class="img-fluid width-100" alt="" id="cover_image_modal" />

											</div>

										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-primary waves-effect waves-light ">Update</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="modal fade" id="dp-large-Modal" tabindex="-1" role="dialog">
						<div class="modal-dialog modal-md" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Profile Picture</h4>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<form method="post" action="{{url('trainer/profile/change_image')}}" enctype="multipart/form-data" id="profile_image_form">
									{{ csrf_field() }}
									<div class="modal-body">
										<div class="col-md-12">

											<input type="file" name="profile_image" id="profile_image" />
											<div class="social-wallpaper">
												<img src="{{ \Auth::user()->profile_image ? URL::asset('/trainer/profile_image/'.\Auth::user()->profile_image): URL::asset('files/assets/images/social/profile.jpg')}}"
												 class="img-fluid width-100" alt="" id="profile_image_modal" />

											</div>

										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-primary waves-effect waves-light ">Update</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div>
								<div class="content social-timeline">
									<div class="">

										<div class="row">
											<div class="col-md-12">

												<div class="social-wallpaper">
													<img src="{{$profile && $profile->cover_image ? URL::asset('/trainer/cover_image/'.$profile->cover_image): URL::asset('files/assets/images/social/img1.jpg')}}"
													 class="img-fluid width-100 height-320" alt="" />
													<div class="profile-hvr">
														<i class="icofont icofont-ui-edit p-r-10" data-toggle="modal" data-target="#large-Modal"></i>

													</div>
												</div>

											</div>
										</div>


										<div class="row">
											<div class="col-xl-3 col-lg-4 col-md-4 col-xs-12">

												<div class="social-timeline-left">

													<div class="card">
														<div class="social-profile">
															<img class="img-fluid width-100 height-width-200" src="{{ \Auth::user()->profile_image ? URL::asset('/trainer/profile_image/'.\Auth::user()->profile_image): URL::asset('files/assets/images/social/profile.jpg')}}"
															 alt="">
															<div class="profile-hvr m-t-15">
																<i class="icofont icofont-ui-edit p-r-10" data-toggle="modal" data-target="#dp-large-Modal"></i>
															</div>
														</div>
														<div class="card-block social-follower">
															<h4>{{\Auth::user()->name}}</h4>
															<h5>Trainer</h5>

														</div>
													</div>
												</div>

											</div>
											<div class="col-xl-9 col-lg-8 col-md-8 col-xs-12 ">

												<div class="card social-tabs">
													<ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">

														<li class="nav-item">
															<a class="nav-link {{ Request::is('trainer/profile') ? 'active' : '' }}" data-toggle="tab" href="#about" role="tab">About</a>
															<div class="slide"></div>
														</li>
														<li class="nav-item">
															<a class="nav-link {{ Request::is('trainer/change-password') ? 'active' : '' }}" data-toggle="tab" href="#change-password-tab"
															 role="tab">Change Password</a>
															<div class="slide"></div>
														</li>

													</ul>
												</div>

												<div class="tab-content">

													<div class="tab-pane {{ Request::is('trainer/profile') ? 'active' : '' }}" id="about">
														<div class="row">
															<div class="col-sm-12">
																<div class="card">
																	<div class="card-header">
																		<h5 class="card-header-text">Basic Information</h5>
																		<button id="edit-btn" type="button" class="btn btn-primary waves-effect waves-light f-right">
																			<i class="icofont icofont-edit"></i>
																		</button>
																	</div>
																	<div class="card-block">
																		<div id="view-info" class="row">
																			<div class="col-lg-8 col-md-12">
																				<form>
																					<table class="table table-responsive m-b-0">
																						<tr>
																							<th class="social-label b-none p-t-0">Full Name
																							</th>
																							<td class="social-user-name b-none p-t-0 text-muted">{{\Auth::user()->name}}</td>
																						</tr>
																						<tr>
																							<th class="social-label b-none">Tagline</th>
																							<td class="social-user-name b-none text-muted">{{$profile &&$profile->tag_line ? $profile->tag_line : ''}}</td>
																						</tr>
																						<tr>
																							<th class="social-label b-none">Gender</th>
																							<td class="social-user-name b-none text-muted">{{$profile &&$profile->gender ? $profile->gender: ''}}</td>
																						</tr>
																						<tr>
																							<th class="social-label b-none">Birth Date</th>
																							<td class="social-user-name b-none text-muted">{{$profile &&$profile->dob ? $profile->dob: ''}}</td>
																						</tr>
																						<tr>
																							<th class="social-label b-none">Martail Status</th>
																							<td class="social-user-name b-none text-muted">{{$profile &&$profile->martial_status ? $profile->martial_status: ''}}</td>
																						</tr>

																					</table>
																				</form>
																			</div>
																		</div>
																		<div id="edit-info" class="row">
																			<div class="col-lg-12 col-md-12">
																				<form method="POST" action='/trainer/profile' id="basic-Form">
																					{{ csrf_field() }}
																					<div class="input-group">
																						<input type="text" class="form-control" placeholder="Full Name" value="{{\Auth::user()->name}}" readonly>
																					</div>
																					<div class="input-group">
																						<input type="text" class="form-control" placeholder="Tagline e.g. Software Engineer" name="tag_line" value="{{$profile && $profile->tag_line ? $profile->tag_line : ''}}">
																					</div>
																					<div class="input-group">
																						<div class="form-radio">
																							<div class="form-radio">
																								<label class="md-check p-0">Gender</label>
																								<div class="radio radio-inline">
																									<label>

																										<input type="radio" name="gender" value="male" {{$profile &&($profile->gender=='male' ||$profile->gender=='' ) ? 'checked': ''}}>
																										<i class="helper"></i>Male
																									</label>
																								</div>
																								<div class="radio radio-inline">
																									<label>
																										<input type="radio" name="gender" value="female" {{$profile &&$profile->gender=='female' ? 'checked': ''}}>
																										<i class="helper"></i>Female
																									</label>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="input-group">
																						<input id="dropper-default" class="form-control" type="text" name="dob" placeholder="Birth Date" value="{{$profile && $profile->dob ? $profile->dob : ''}}"
																						/>
																					</div>
																					<div class="input-group">
																						<select id="hello-single" name="martial_status" class="form-control">
																							<option value="">---- Marital Status ----</option>
																							<option value="married" {{$profile && $profile->martial_status=='married' ? 'selected': ''}}>Married</option>
																							<option value="unmarried" {{$profile && $profile->martial_status=='unmarried' ? 'selected': ''}}>Unmarried</option>
																						</select>
																					</div>
																					<div class="text-center m-t-20">
																						<button type="submit" class="btn btn-primary waves-effect waves-light m-r-20">Save</button>
																						<a href="javascript:;" id="edit-cancel" class="btn btn-default waves-effect waves-light">Cancel</a>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="card">
																	<div class="card-header">
																		<h5 class="card-header-text">Contact Information</h5>
																		<button id="edit-Contact" type="button" class="btn btn-primary waves-effect waves-light f-right">
																			<i class="icofont icofont-edit"></i>
																		</button>
																	</div>
																	<div class="card-block">
																		<div id="contact-info" class="row">
																			<div class="col-lg-12 col-md-12">
																				<table class="table table-responsive m-b-0">
																					<tr>
																						<th class="social-label b-none p-t-0">Mobile Number</th>
																						<td class="social-user-name b-none p-t-0 text-muted">{{$profile &&$profile->mobile_no ? $profile->mobile_no: ''}}</td>
																					</tr>
																					<tr>
																						<th class="social-label b-none">Email Address</th>
																						<td class="social-user-name b-none text-muted">
																							{{\Auth::user()->email}}
																						</td>
																					</tr>
																					<tr>
																						<th class="social-label b-none">Address</th>
																						<td class="social-user-name b-none text-muted">{{$profile &&$profile->address ? $profile->address: ''}}</td>
																					</tr>

																				</table>
																			</div>
																		</div>
																		<div id="edit-contact-info" class="row">
																			<div class="col-lg-12 col-md-12">
																				<form method="POST" action='/trainer/profile' id="contact-Form">
																					{{ csrf_field() }}
																					<div class="input-group">
																						<input type="number" class="form-control" placeholder="Mobile number" name="mobile_no" value="{{$profile && $profile->mobile_no ? $profile->mobile_no : ''}}"  min=0 oninput="validity.valid||(value='');" pattern="\d*">
																					</div>

																					<div class="md-group-add-on">
																						<textarea rows="5" cols="5" class="form-control" name="address" placeholder="Address...">{{$profile && $profile->address ? $profile->address : ''}}</textarea>
																					</div>
																					<div class="text-center m-t-20">
																						<button type="submit" class="btn btn-primary waves-effect waves-light m-r-20">Save</button>
																						<a href="javascript:;" id="contact-cancel" class="btn btn-default waves-effect waves-light">Cancel</a>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="card">
																	<div class="card-header">
																		<h5 class="card-header-text">Work</h5>
																		<button id="edit-work" type="button" class="btn btn-primary waves-effect waves-light f-right">
																			<i class="icofont icofont-edit"></i>
																		</button>
																	</div>
																	<div class="card-block">
																		<div id="work-info" class="row">
																			<div class="col-lg-6 col-md-12">
																				<table class="table table-responsive m-b-0">
																					<tr>
																						<th class="social-label b-none p-t-0">Education
																						</th>
																						<td class="social-user-name b-none p-t-0 text-muted">{{$profile && $profile->last_completed_degree ? $profile->last_completed_degree : ''}}</td>
																					</tr>
																					<tr>
																						<th class="social-label b-none">Skills</th>
																						<td class="social-user-name b-none text-muted">{{$profile && $profile->skills ? $profile->skills : ''}}</td>
																					</tr>
																					<tr>
																						<th class="social-label b-none">Occupation</th>
																						<td class="social-user-name b-none p-b-0 text-muted">{{$profile && $profile->current_occupation ? $profile->current_occupation : ''}}</td>
																					</tr>

																				</table>
																			</div>
																		</div>
																		<div id="edit-contact-work" class="row">
																			<div class="col-lg-12 col-md-12">
																				<form method="POST" action='/trainer/profile' id="work-Form">
																					{{ csrf_field() }}
																					<div class="input-group">
																						<select id="occupation" class="form-control" name="last_completed_degree">
																							<option value=""> Select Education </option>
																							<option value="Masters" {{$profile && $profile->last_completed_degree=='Masters' ? 'selected': ''}}> Masters
																							</option>
																							<option value="Bachelors" {{$profile && $profile->last_completed_degree=='Bachelors' ? 'selected': ''}}> Bachelors
																							</option>
																							<option value="Intermediate" {{$profile && $profile->last_completed_degree=='Intermediate' ? 'selected': ''}}>Intermediate</option>
																						</select>
																					</div>
																					<div class="input-group">
																						<input class="form-control" type="text" name="skills" value="{{$profile && $profile->skills ? $profile->skills : ''}}" data-role="tagsinput"
																						 style="width:100%;" placeholder="skills e.g. PHP, JAVA">
																					</div>
																					<div class="input-group">
																						<input class="form-control" type="text" name="current_occupation" value="{{$profile && $profile->current_occupation ? $profile->current_occupation : ''}}"
																						 placeholder="Occupation">
																					</div>
																					<div class="text-center m-t-20">
																						<button type="submit" class="btn btn-primary waves-effect waves-light m-r-20">Save</button>
																						<a href="javascript:;" id="work-cancel" class="btn btn-default waves-effect waves-light">Cancel</a>
																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>


													<div class="tab-pane {{ Request::is('trainer/change-password') ? 'active' : '' }}" id="change-password-tab">
														<div class="row">
															<div class="col-sm-12">
																<div class="card">
																	<div class="card-header">
																		<h5 class="card-header-text">Change Password</h5>
																	</div>
																	<div class="card-block">

																		<div id="edit-info" class="row">
																			<div class="col-lg-12 col-md-12">
																				<form method="post" action="/trainer/change-password">
																					{{ csrf_field() }} @if ($errors->has('old_password'))
																					<span class="help-block" style="color:red">
																						<strong>{{ $errors->first('old_password') }}</strong>
																					</span>
																					@endif
																					<div class="input-group">
																						<input type="password" class="form-control" placeholder="Old Password" name="old_password">
																					</div>
																					@if ($errors->has('password'))
																					<span class="help-block" style="color:red">
																						<strong>{{ $errors->first('password') }}</strong>
																					</span>
																					@endif
																					<div class="input-group">
																						<input type="password" class="form-control" placeholder="New Password" name="password">
																					</div>
																					<div class="input-group">
																						<input type="password" class="form-control" placeholder="Repeat New Password" name="password_confirmation">
																					</div>
																					<div class="text-center m-t-20">
																						<button type="submit" class="btn btn-primary waves-effect waves-light m-r-20">Save</button>

																					</div>
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>

												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

@stop @section('javascript-files')
<script src="{{URL::asset('files/assets/pages/advance-elements/moment-with-locales.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js')}}"></script>

<script src="{{URL::asset('files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js')}}"></script>

<script src="{{URL::asset('files/bower_components/datedropper/js/datedropper.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/bootstrap-tagsinput/js/bootstrap-tagsinput.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}">
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>

<script src="{{URL::asset('files/assets/pages/social-timeline/social.js')}}"></script>
<script>
	function showCover(input) {
			var file, img;
			if (input.files && input.files[0]) {
				
			  var reader = new FileReader();
		  
			  reader.onload = function(e) {
				img = new Image();
				img.src=e.target.result;;
				img.onload = function () {
					//alert(this.width + " " + this.height);
					if(this.width < 560)
					{
						alert('image width must be greater than 400');
						$('#cover_form').trigger("reset");
						$('#cover_image_modal').attr('src', '');
						return;
					}
					else if(this.height < 320)
					{
						alert('image height must be greater than 320');
						$('#cover_form').trigger("reset");
						$('#cover_image_modal').attr('src', '');
						return;
					}
					$('#cover_image_modal').attr('src', e.target.result);
				};
				
			  }
		  
			  reader.readAsDataURL(input.files[0]);
			}
		  }
		  
		  $("#cover_image").change(function() {
			showCover(this);
		  });

</script>

@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif


<script>
	function showprofile(input) {
		var file, img;
		if (input.files && input.files[0]) {
			
		  var reader = new FileReader();
	  
		  reader.onload = function(e) {
			img = new Image();
			img.src=e.target.result;;
			img.onload = function () {
				//alert(this.width + " " + this.height);
				if(this.width < 200)
					{
						alert('image width must be greater than 200');
						$('#profile_image_form').trigger("reset");
						$('#profile_image_modal').attr('src', '');
						return;
					}
					else if(this.height < 200)
					{
						alert('image height must be greater than 200');
						$('#profile_image_form').trigger("reset");
						$('#profile_image_modal').attr('src', '');
						return;
					}
					$('#profile_image_modal').attr('src', e.target.result);
			};
			
		  }
	  
		  reader.readAsDataURL(input.files[0]);
		}
	  }
	  
	  $("#profile_image").change(function() {
		showprofile(this);
	  });

</script>


@stop