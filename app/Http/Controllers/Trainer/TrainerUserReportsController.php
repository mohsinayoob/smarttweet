<?php

namespace App\Http\Controllers\Trainer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Course\Course;
use App\Models\Course\CourseEnrollment;
use App\Models\Tweet\Comments;
use App\Models\Tweet\CommentReaction;
use App\Models\User;

class TrainerUserReportsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('trainer');
        
    }
    public function getUserReports (Request $request)
    {
        $data = $request->all();
        if(!isset($data['trainee_id']))
        {
            return back()->with('error_messages','Could not find the user. try Again!!');
        }
        $trainee_id = $data['trainee_id'];
        $courses = CourseEnrollment::join('course','course.course_id','=', 'course_enrollment.course_id' )
        ->join('users', 'user_id','=', 'course_enrollment.trainee_id')
        ->where('course_enrollment.trainer_id', \Auth::user()->user_id)
        ->where('course_enrollment.trainee_id', $trainee_id)->select('course.course_title','course.course_id', 'users.name', 'users.profile_image', 'course_enrollment.created_at')->get();
        $reports = array();
        foreach($courses as $course)
        {
            /// Total Comments ////
            $total_comments = Comments::join('tweets','tweets.tweet_id','=', 'comments.tweet_id' )
            ->where('tweets.course_id', $course->course_id)->selectRaw('COUNT(comments.comment_id) as total_comments')->first();
            $course->total_comments = $total_comments->total_comments;

            ///// Trainees comments ////
            $trainees_comments = Comments::join('tweets','tweets.tweet_id','=', 'comments.tweet_id' )
            ->where('tweets.course_id', $course->course_id)
            ->where('comments.user_id', $trainee_id)
            ->selectRaw('COUNT(comments.comment_id) as trainees_comments')->first();
            $course->trainees_comments = $trainees_comments->trainees_comments;

            /// Count of favorites ///
            $favorite_count = Comments::join('tweets','tweets.tweet_id','=', 'comments.tweet_id' )
            ->where('tweets.course_id', $course->course_id)
            ->where('comments.user_id', $trainee_id)
            ->join ('comments_reaction', 'comments_reaction.comment_id','=', 'comments.comment_id')
            ->where('comments_reaction.comment_reaction_content', 'favorite')
            ->selectRaw('COUNT(comments_reaction.comment_reaction_id) as favorite_count')->first();
            $course->favorite_count = $favorite_count->favorite_count;

            /// Count of Likes ///
            $like_count = Comments::join('tweets','tweets.tweet_id','=', 'comments.tweet_id' )
            ->where('tweets.course_id', $course->course_id)
            ->where('comments.user_id', $trainee_id)
            ->join ('comments_reaction', 'comments_reaction.comment_id','=', 'comments.comment_id')
            ->where('comments_reaction.comment_reaction_content', 'like')
            ->selectRaw('COUNT(comments_reaction.comment_reaction_id) as like_count')->first();
            $course->like_count = $like_count->like_count;

            /// Count of Dislikes ///
            $dislike_count = Comments::join('tweets','tweets.tweet_id','=', 'comments.tweet_id' )
            ->where('tweets.course_id', $course->course_id)
            ->where('comments.user_id', $trainee_id)
            ->join ('comments_reaction', 'comments_reaction.comment_id','=', 'comments.comment_id')
            ->where('comments_reaction.comment_reaction_content', 'dislike')
            ->selectRaw('COUNT(comments_reaction.comment_reaction_id) as dislike_count')->first();
            $course->dislike_count = $dislike_count->dislike_count;


            //return $favorite_count;
            array_push($reports, $course);
        }
        //return $reports;
        return view('trainer.user-reports', compact('reports'));
    }

    public function getCourseReports (Request $request)
    {
        $data = $request->all();
        if(!isset($data['course_id']))
        {
            return back()->with('error_messages','Could not find the user. try Again!!');
        }
        $course_id = $data['course_id'];
        $course = Course::where('course_id',$course_id)->first();
        $enrollments = Course::join('course_enrollment','course_enrollment.course_id','=', 'course.course_id' )
        ->join('users', 'user_id','=', 'trainee_id')
        ->where('course_enrollment.trainer_id', \Auth::user()->user_id)
        ->where('course_enrollment.course_id', $course_id)
        ->select('course.course_title','course.course_description','course.created_at AS course_creation','course.avg_rating','course.course_id', 'users.name', 'users.user_type', 'users.profile_image', 'course_enrollment.created_at', 'course_enrollment.trainee_id','course_enrollment.is_rated','course_enrollment.rating')->get();

        $reports = array();
        foreach($enrollments as $enrollment)
        {
            /// Total Comments ////
            $total_comments = Comments::join('tweets','tweets.tweet_id','=', 'comments.tweet_id' )
            ->where('tweets.course_id', $course_id)->selectRaw('COUNT(comments.comment_id) as total_comments')->first();
            $enrollment->total_comments = $total_comments->total_comments;

            ///// Trainees comments ////
            $trainees_comments = Comments::join('tweets','tweets.tweet_id','=', 'comments.tweet_id' )
            ->where('tweets.course_id', $course_id)
            ->where('comments.user_id', $enrollment->trainee_id)
            ->selectRaw('COUNT(comments.comment_id) as trainees_comments')->first();
            $enrollment->trainees_comments = $trainees_comments->trainees_comments;

            /// Count of favorites ///
            $favorite_count = Comments::join('tweets','tweets.tweet_id','=', 'comments.tweet_id' )
            ->where('tweets.course_id', $course_id)
            ->where('comments.user_id', $enrollment->trainee_id)
            ->join ('comments_reaction', 'comments_reaction.comment_id','=', 'comments.comment_id')
            ->where('comments_reaction.comment_reaction_content', 'favorite')
            ->selectRaw('COUNT(comments_reaction.comment_reaction_id) as favorite_count')->first();
            $enrollment->favorite_count = $favorite_count->favorite_count;

            /// Count of Likes ///
            $like_count = Comments::join('tweets','tweets.tweet_id','=', 'comments.tweet_id' )
            ->where('tweets.course_id', $course_id)
            ->where('comments.user_id', $enrollment->trainee_id)
            ->join ('comments_reaction', 'comments_reaction.comment_id','=', 'comments.comment_id')
            ->where('comments_reaction.comment_reaction_content', 'like')
            ->selectRaw('COUNT(comments_reaction.comment_reaction_id) as like_count')->first();
            $enrollment->like_count = $like_count->like_count;

            /// Count of Dislikes ///
            $dislike_count = Comments::join('tweets','tweets.tweet_id','=', 'comments.tweet_id' )
            ->where('tweets.course_id', $course_id)
            ->where('comments.user_id', $enrollment->trainee_id)
            ->join ('comments_reaction', 'comments_reaction.comment_id','=', 'comments.comment_id')
            ->where('comments_reaction.comment_reaction_content', 'dislike')
            ->selectRaw('COUNT(comments_reaction.comment_reaction_id) as dislike_count')->first();
            $enrollment->dislike_count = $dislike_count->dislike_count;
            //return $favorite_count;
            array_push($reports, $enrollment);
        }
        //return $reports;
        return view('trainer.course-reports', compact('reports','course'));
    }

    public function index()
    {
        $courses = Course::where('trainer_id', \Auth::user()->user_id)->get();
        $trainees = User::join('course_enrollment','trainee_id','=','user_id')
        ->where('course_enrollment.trainer_id', \Auth::user()->user_id)
        ->select('users.name','users.profile_image','users.user_id')->distinct()->get();
        //return $trainees;
        return view('trainer.reports', compact('courses','trainees'));
    }

}
