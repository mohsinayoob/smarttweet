@extends('layouts.trainee_layout') @section('title', 'Topics') @section('css-files')
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/css/component.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css')}}">

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css')}}"
/>

<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/datedropper/css/datedropper.min.css')}}"
/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/jquery-bar-rating/css/css-stars.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/jquery-bar-rating/css/fontawesome-stars-o.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/spectrum/css/spectrum.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/switchery/css/switchery.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('files/bower_components/sweetalert/css/sweetalert.css')}}"> 
<style>
	.card-comment .comment-desc {
		padding-left: 229px;
		vertical-align: top;
	}
	.card-comment img {
		float: left;
		width: auto;
        max-height: 150px;
        height: auto;
        max-width: 203px;
	}
	.comment-desc img{
		height:auto;
		width:auto;
		float:none;
	}

	@media only screen and (max-width: 720px) {
		.card-comment .comment-desc {
			padding-left: 0px!important;
			vertical-align: top;
		}
		
		
		.card-comment img {
			float: left;
			height: auto;
            width: 100%;
            max-width: 100%;
            max-height: 100%;
			padding-bottom: 10px;
		}
		.comment-desc img{
			height:auto;
			width:auto;
			float:none;
		}
	}
	</style>
@stop @section('body_content')
<div class="pcoded-content">
	<div class="pcoded-inner-content">

		<div class="main-body">
			<div class="page-wrapper">
				<div class="page-header card">
					<div class="card-block">
						<h5 class="m-b-10">Topics</h5>
					</div>

				</div>
				<div class="page-body">
					

					<div class="row">
						<div class="col-sm-12">
							<div class="card list-view-media">
								<div class="card-header">
									<div class="dropdown-secondary dropdown f-left">
										<h6 class="d-inline-block">
												{{$courses->course_title}}
										</h6>
										<div class="f-13 text-muted m-b-15">
												{{$courses->created_at}}
										</div>
									</div>
									<div class="dropdown-secondary dropdown f-right">
										@if($courses->is_rated)
										<select id="example-css" class="rating-star-readonly" name="rating" autocomplete="off" disabled>
											<option value="1"{{$courses->rating>0 &&$courses->rating<=1 ? 'selected="selected"':''}}>1</option>
											<option value="2" {{$courses->rating>1 &&$courses->rating<=2 ? 'selected="selected"':''}}>2</option>
											<option value="3" {{$courses->rating>2 &&$courses->rating<=3 ? 'selected="selected"':''}}>3</option>
											<option value="4" {{$courses->rating>3 &&$courses->rating<=4 ? 'selected="selected"':''}}>4</option>
											<option value="5"{{$courses->rating>4 ? 'selected="selected"':''}}>5</option>
										</select>
										@else
										<div class="card-header-right f-right">
											<button class="btn btn-warning btn-round md-trigger" data-modal="modal-rating">Rate Course</button>
										</div>
											<!--  Modal for rating -->
					<div class="md-modal md-effect-13 addcontact" id="modal-rating">
						<div class="md-content">
							<h3 class="f-26">Rate Course</h3>
							<div class="text-center">
								<form action="/trainee/post-course-rating/{{$courses->enrollment_id}}" method="post">
									{{csrf_field()}}
									<div class="text-center">
									<div class="stars stars-example-css ">
										<select id="example-css" class="rating-star" name="rating" autocomplete="off" >
											<option value="1" >1</option>
											<option value="2" >2</option>
											<option value="3" >3</option>
											<option value="4" >4</option>
											<option value="5" >5</option>
										</select>

									</div>
								</div>
									<div class="input-group">
											<textarea id="post-message" class="form-control " rows="3" cols="10"  placeholder="Your Comments" name="rating_comments"></textarea>
									</div>
									<div class="text-center">
										<button type="submit" class="btn btn-primary waves-effect m-r-20 f-w-600 d-inline-block save_btn">Save</button>
										<button type="button" class="btn btn-primary waves-effect m-r-20 f-w-600 md-close d-inline-block close_btn">Close</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="md-overlay"></div>
										@endif
									</div>
								</div>
								<div class="card-block">
									<div class="media">

										<div class="media-body">

											<p>{{$courses->course_description}}</p>

										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-12 push-xl-9">

							<div class="card">
								
								<div class="card-block p-t-10">
										<div class="task-right">
												<div class="task-right-header-users trainer">
													<span data-toggle="collapse">Trainer</span>
													<i class="icofont icofont-rounded-down f-right"></i>
												</div>
												<div class="user-box assign-trainer taskboard-right-users trainer">
													
													<div class="media">
														<div class="media-left media-middle photo-table">
															<a href="/trainee/chat/{{base64_encode($trainer->user_id)}}">
																<img class="media-object img-radius" src="{{ $trainer->profile_image ? URL::asset('/'.$trainer->user_type.'/profile_image/'.$trainer->profile_image): URL::asset('files/assets/images/social/profile.jpg')}}" alt="Generic placeholder image">
																
															</a>
														</div>
														<div class="media-body">
															<h6>{{$trainer->name}}</h6>
															<p>Trainer</p>
														</div>
													</div>
													
													
												</div>
		
		
		
											</div>
									<div class="task-right">
										<div class="task-right-header-users">
											<span data-toggle="collapse">Enrolled Users</span>
											<i class="icofont icofont-rounded-down f-right"></i>
										</div>
										<div class="user-box assign-user taskboard-right-users">
											@foreach($enrollusers as $enrolluser)
											@if($enrolluser->trainee_id != \Auth::user()->user_id)
											<div class="media">
												<div class="media-left media-middle photo-table">
												<a href="/trainee/chat/{{base64_encode($enrolluser->trainee_id)}}/{{base64_encode($courses->course_id)}}">
														<img class="media-object img-radius" src="{{ $enrolluser->profile_image ? URL::asset('/trainee/profile_image/'.$enrolluser->profile_image): URL::asset('files/assets/images/social/profile.jpg')}}" alt="Generic placeholder image">
														@if($enrolluser->status == 'Pending')
														<div class="live-status bg-danger"></div>
														@else
														<div class="live-status bg-success"></div>
														@endif
													</a>
												</div>
												<div class="media-body">
													<h6>{{$enrolluser->name}}</h6>
													<p>{{$enrolluser->sector}}</p>
												</div>
											</div>
											@endif
											@endforeach
											
											
										</div>



									</div>

								</div>

							</div>

						</div>
						<div class="col-xl-9 col-lg-12 pull-xl-3 filter-bar">

							<nav class="navbar navbar-light bg-faded m-b-30 p-10">
								<form method="get">
							<ul class="nav navbar-nav p-t-10">
								<li class="nav-item active">
									<a class="nav-link" href="#!">Filter:
										<span class="sr-only">(current)</span>
									</a>
								</li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#!" id="bydate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="icofont icofont-clock-time"></i> By Date</a>
									<div class="dropdown-menu" aria-labelledby="bydate">
										<a class="dropdown-item" href="#!">Show all</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" onclick="searchdate('Today')">Today</a>
										<a class="dropdown-item" onclick="searchdate('Month')">This month</a>
										<a class="dropdown-item" onclick="searchdate('Year')">This year</a>
									</div>
								</li>

								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#!" id="bystatus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="icofont icofont-chart-histogram-alt"></i> By Status</a>
									<div class="dropdown-menu" aria-labelledby="bystatus">
										<a class="dropdown-item" >Show all</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" onclick="searchstatus('Active')">Active</a>
										<a class="dropdown-item" onclick="searchstatus('De-active')">De-active</a>
										
									</div>
								</li>

								
							</ul>
							<div class="nav-item nav-grid p-t-10">
									
								<div class="input-group">
									<input type="text" class="form-control" name='q' placeholder="Search here...">
									<button class="btn btn-success f-right m-l-10">
										Filter
									</button>
								</div>
							
							</div>
							
							<input type='hidden' name='date' id='searchdate'>
							<input type='hidden' name='status' id='searchstatus'>
						</form>
						</nav>

							<div class="row">
									@if(sizeof($topics) > 0)
								@foreach($topics as $topic)
								<div class="col-sm-12">
									<div class="card card-comment card-border-primary">
										<div class="card-block-small">
											<img class="height-width" src="{{ $topic->topic_image ? URL::asset('/topic_images/'.$topic->topic_image): URL::asset('/topic_images/no_image.jpg')}}" alt="Topic Image">

											<div class="comment-desc">
												<h6>
													<a href="/trainee/tweet/{{$topic->course_id}}/{{$topic->topic_id}}"> {{$topic->topic_name}} </a>
												</h6>
												<p class="text-muted ">{!! base64_decode($topic->topic_description)!!} </p>
												<div class="comment-btn">
													@if($topic->status == 'Active')
													<button class="btn bg-c-green btn-round btn-comment ">{{$topic->status}}</button>
													@else
													<button class="btn btn-danger btn-round btn-comment ">{{$topic->status}}</button>
													@endif
												</div>

											</div>
										</div>
										<div class="card-footer">
											<div class="task-list-table">
												<p class="task-due">
													<strong> Date : </strong>
													<strong class="label label-success">{{$topic->created_at}}</strong>
												</p>
											</div>
											<div class="task-board m-0">
												<a href="/trainee/tweet/{{$topic->course_id}}/{{$topic->topic_id}}" class="btn btn-info btn-mini b-none">
													<i class="icofont icofont-eye-alt m-0"></i>
												</a>

												

											</div>

										</div>
									</div>
								</div>
								@endforeach
								@else
								<div class="col-sm-12">
										<div class="card">
										<div class="card-block">
									<h4>No Topic is available </h4>
										</div>
									</div>
								</div>
								@endif
								<div class="col-sm-12">
										<div class="card">
											<div class="card-block">
												<nav  class="f-right">
													{{ $topics->appends(request()->query())->links("pagination::bootstrap-4") }}
															
												</nav>
											</div>
										</div>
									</div>

							</div>





						</div>
						
					</div>

				</div>
			</div>
		</div>

	</div>
</div>
@stop @section('javascript-files')
<script src="{{URL::asset('files/assets/pages/task-board/task-board.js')}}"></script>
<script src="{{URL::asset('files/bower_components/jquery-bar-rating/js/jquery.barrating.js')}}"></script>
<script src="{{URL::asset('files/assets/js/rating.js')}}"></script>
<script src="{{URL::asset('files/assets/js/modalEffects.js')}}"></script>
<script src="{{URL::asset('files/assets/js/classie.js')}}"></script>

<script src="{{URL::asset('files/assets/pages/topic/base64js.min.js')}}"></script>
<script src="{{URL::asset('files/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>
<script src="{{URL::asset('files/assets/pages/tinymce/tinymce.min.js?apiKey=jdw0qpvvruo7046n3j4g8o122piap638faeuii9s0rlk2fy3')}}"></script>


@if (session('update'))
<script>
	swal("Success!", "{{ session('update') }}", "success");

</script>

@endif @if (session('error_messages'))
<script>
	swal("Error!", "{{ session('error_messages') }}", "error");

</script>

@endif 
<script>
	function searchdate(date)
	{
		document.getElementById("searchdate").setAttribute('value',date);
	}
	function searchstatus(status)
	{
		document.getElementById("searchstatus").setAttribute('value',status);
	}
</script>
@stop