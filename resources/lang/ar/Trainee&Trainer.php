<?php

return [

   

    'edit' => 'Edit Trainee',
    'editT' => 'Edit Trainer',
    'editInfo' => 'Edit Innformation',
    'certificate' => 'Certificate',
    'id' => 'User-Id',
    'sector' => 'Please Select Sector',
    'TName'=> 'Trainee Name',
    'TrainerName'=> 'Trainer Name',
    'info'=> "عرض _START_ من _END_ إدخالات",
    'save'=>'حفظ',
    'cancel'=>'إلغاء',
    'search'=>'بحث',
    'lengthMenu'=>'عرض _MENU_ تسجيل',
    'previous'=>'سابق',
    'next'=>'التالى',
    'success'=>'نجاح',
    'status'=>'تم تحديث حالة المتدرب بنجاح',
    'statustrainer'=>'تم تحديث حالة المدرب بنجاح',

  

];
